USE [PLSSKIC]
GO
/****** Object:  StoredProcedure [dbo].[GI_RecordID]    Script Date: 9/10/2018 10:43:00 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[GI_RecordID]
 @HH_ID VARCHAR(2) ,
 @User_ID VARCHAR(20) , 
 @Record_ID INT , 
 @Delivery_No VARCHAR(10) ,
 @WH_No INT
 AS
 DECLARE @getID INT
 DECLARE @recID INT
 DECLARE @Int_Error INT

 SET @recID = 0
 SET @Int_Error = 0

BEGIN TRAN
SELECT * FROM HH_GI_Item WHERE Delivery_No=@Delivery_No
IF @@ROWCOUNT > 0
	BEGIN
			INSERT INTO Control (Prev_Process,HH_ID,Process_ID,[User_Name],Create_Date_Time,Flag_Del,Call_Date_Time) VALUES (@Record_ID,@HH_ID,'Picking',@User_ID,getdate(),'N',getdate())			
			IF (@@ERROR <>0)  GOTO ERR_HANDLER
			
			--SET @recID = SCOPE_IDENTITY()

			SELECT @recID = MAX(Record_ID) FROM Control WHERE Process_ID='Picking' AND [User_Name]=@User_ID AND HH_ID=@HH_ID

			INSERT INTO Picking_Head (Record_ID,Delivery_No) VALUES (@recID,@Delivery_No)
			IF (@@ERROR <> 0) GOTO ERR_HANDLER
			
			DECLARE @Material_No VARCHAR(18)
			DECLARE @Batch_No VARCHAR(10)
			DECLARE @Plant VARCHAR(4)
			DECLARE @Item_No VARCHAR(10)
			DECLARE @Line_No VARCHAR(10)
			DECLARE @Qty DECIMAL(8,3)

			DECLARE Table_Cursor CURSOR
			FOR SELECT Material_No,Batch_No,Plant,Item_No,Line_No,Qty FROM HH_GI_Item WHERE Delivery_No=@Delivery_No
			OPEN Table_Cursor
			
			FETCH Table_Cursor INTO @Material_No,@Batch_No,@Plant,@Item_No,@Line_No,@Qty
			WHILE(@@FETCH_STATUS <> -1)
				BEGIN
					INSERT INTO Picking_item (Record_ID,Delivery_No,Material_No,Batch_No,Plant,Item_No,Line_No,Pick_qty)
					VALUES (@recID,@Delivery_No,@Material_No,@Batch_No,@Plant,@Item_No,@Line_No,@Qty)
					IF (@@ERROR <> 0)
					 BEGIN
						SET @Int_Error = @@ERROR
						GOTO ENDWHILELOOP
					END

					FETCH NEXT FROM Table_Cursor INTO @Material_No,@Batch_No,@Plant,@Item_No,@Line_No,@Qty
				END

			ENDWHILELOOP:
			CLOSE Table_Cursor
			DEALLOCATE Table_Cursor
			
			IF (@Int_Error <> 0) GOTO ERR_HANDLER

			UPDATE Control SET Return_Code='N' WHERE Record_ID=@recID
			IF (@@ERROR <> 0) GOTO ERR_HANDLER

		/*	UPDATE Delivery_Head SET Time_ScanComplete=getdate() , Time_Send=getdate() , Status='W' WHERE Delivery_No=@Delivery_No AND Record_ID=@Record_ID AND Status IS NOT NULL AND WH_No=@WH_No
		*/
		
		/*	UPDATE Delivery_Head SET Time_ScanComplete=getdate() , Time_Send=getdate() , Status='W' WHERE Delivery_No=@Delivery_No AND Record_ID=@Record_ID

		*/
			UPDATE Delivery_Head SET Status='W' WHERE Delivery_No=@Delivery_No AND Record_ID=@Record_ID
			IF (@@ERROR <> 0) GOTO ERR_HANDLER			

			COMMIT TRAN
			RETURN @recID
	END

ERR_HANDLER:
ROLLBACK TRAN
RETURN 0






