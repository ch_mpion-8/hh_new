﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="hh_siamkraft.index" %>
<%@ Register TagPrefix="uc1" TagName="header_control" Src="header_control.ascx"   %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>MAIN MENU</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C# .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"/>
		
		<LINK href="css/style.css" type="text/css" rel="stylesheet"/>
		<script src="javascript/check_func.js"></script>
	    <style type="text/css">
            .style1
            {
                height: 25px;
            }
        </style>
        
        <script language="javascript">
			function progr_onclose(){
				if(confirm("Do you want Exit?")) {
					window.opener = "";
					window.close();
				}else{
					return false;
				}
			}
			
			function Check_Item(){
				if(myForm.Tb_username.value == ""){
					alert("Please Scan 'Slip No'");
					myForm.Tb_username.focus();
					//myForm.step.value = 1;
					return;
				}

			//myForm.step.value = 2;
			//myForm.action = "index.php?tab=receive&action=receive_form";
			//myForm.submit();
		
			}
			
			function keyInteger()
			{
				alert(event.keyCode);
			}
		</script>
		
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
</head>

	<body leftMargin="1" topMargin="1" rightmargin="1" bottommargin="1" ms_positioning="GridLayout">
		<form id="myForm" method="post" runat="server">
			<table style="BORDER-BOTTOM: #4791c5 1px solid; BORDER-LEFT: #4791c5 1px solid; PADDING-BOTTOM: 3px; PADDING-LEFT: 3px; PADDING-RIGHT: 3px; BORDER-TOP: #4791c5 1px solid; BORDER-RIGHT: #4791c5 1px solid; PADDING-TOP: 3px"
				cellSpacing="0" cellPadding="0" border="0" width="225">
				<tr>
					<td vAlign="middle" align="center">
						<P><IMG src="Images/logo_scg.jpg" align="absMiddle" 
                                style="WIDTH: 141px; HEIGHT: 48px" width="141"
								height="48"></P>
					</td>
				</tr>
				<tr>
					<td class="smallerText" noWrap align="left"><IMG src="images/red_arrow.gif" align="absMiddle">&nbsp;<asp:label 
                            id="lblInfo" Runat="server">Please Insert 'Username' and 'Password'</asp:label>
					</td>
				</tr>
				<tr>
					<td>
						<table cellSpacing="0" cellPadding="0" width="100%">
							<tr bgColor="Lime" id="trAuth" runat="server">
								<td class="smallerText" align="left">Authorize</td>
								<td align="left"><IMG src="images/red_arrow.gif" align="absMiddle"></td>
                                <td>
                                    <asp:dropdownlist 
                                        id="ddlAuthorize" runat="server" CssClass="colorBox" Height="16px" 
                                        Width="81px" ondatabinding="ddlAuthorize_DataBinding">
                                    <asp:ListItem Value="0">Private</asp:ListItem>
                                    <asp:ListItem Value="1">Domain</asp:ListItem>
                                    </asp:dropdownlist>
                                </td>
							</tr>
							<tr bgColor="#99FF99" id="trUser" runat="server">
								<td class="smallerText" align="left">Username</td>
								<td align="left" class="style2"><IMG src="images/red_arrow.gif" align="absMiddle"></td>
                                <td><asp:textbox 
                                        id="txtUserName" runat="server" size="15" CssClass="inputTag" ></asp:textbox></td>
							</tr>
							<tr bgColor="Lime" id="trPass" runat="server">
								<td class="smallerText" align="left">Password</td>
								<td align="left"><IMG src="images/red_arrow.gif" align="absMiddle"></td>
                                <td><asp:textbox 
                                        id="txtPassword" runat="server" size="15" CssClass="inputTag" 
                                        TextMode="Password" ontextchanged="txtPassword_TextChanged" ></asp:textbox></td>
							</tr>
                            <tr id="trServer" bgColor="#99FF99" runat="server">
								<td class="smallerText" align="left">PM</td>
								<td align="left" bgcolor="#99FF99"><IMG align="absMiddle" src="images/red_arrow.gif"></td>
                                <td>
									<asp:dropdownlist id="ddlServer" runat="server" CssClass="colorBox" AutoPostBack="True" OnSelectedIndexChanged="ddlServer_SelectedIndexChanged"></asp:dropdownlist></td>
							</tr>
							<tr id="trPlant" bgColor="Lime" runat="server">
								<td class="smallerText" align="left">Plant</td>
								<td align="left" bgcolor="#99FF99"><IMG align="absMiddle" src="images/red_arrow.gif"></td>
                                <td>
									<asp:dropdownlist id="ddlWarehouse" runat="server" CssClass="colorBox"></asp:dropdownlist></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr class="tr" style="PADDING-BOTTOM: 2px; PADDING-LEFT: 2px; PADDING-RIGHT: 2px; PADDING-TOP: 2px">
					<td align="left" bgColor="#009933">
                        <asp:button id="btnLogin" runat="server" CssClass="buttonTag" Width="72px" Text="Log In" onclick="btnLogin_Click"></asp:button>&nbsp;
                        <asp:button id="btnReset" runat="server" CssClass="buttonTag" Width="64px" Text="Reset" onclick="btnReset_Click"></asp:button>&nbsp;
                        <INPUT class="buttonTag" style="WIDTH: 56px; HEIGHT: 20px" onclick="progr_onclose()" type="button" value="Exit" name="btnExit">&nbsp;
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>