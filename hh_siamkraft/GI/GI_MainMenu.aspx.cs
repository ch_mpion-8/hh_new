﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{
    public partial class GI_MainMenu : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
         
        }

        protected void Bt_Mainmenu_Click(object sender, EventArgs e)
        {
            Response.Redirect("../main_menu.aspx");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user_id"] == null)
            {
                Session.RemoveAll();
                Response.Redirect("../index.aspx", false);
                return;
            }
          
            if(!(Page.IsPostBack))
            {
                showMenu();
            }
        }

        private void showMenu()
        {
            var permission = Convert.ToString(Session["Permission"]);
            var list = permission.Split(',').ToList();
           

            if (list.Where(o => o == "601").Count() > 0)
            {
                tr_601.Visible = true;
            }

            if (list.Where(o => o == "VIEWSTATUS").Count() > 0)
            {
                tr_viewstatus.Visible = true;
            }

            if (list.Where(o => o == "SELECTDP").Count() > 0)
            {
                tr_selectdp.Visible = true;
            }
        }

        protected void Bt_check_dp_Click(object sender, EventArgs e)
        {
            Response.Redirect("GI_SelectDP.aspx", false);
        }

        protected void Bt_gi_Click(object sender, EventArgs e)
        {
            Response.Redirect("GI_ScanDP.aspx", false);

        }

        protected void Bt_GI_ViewStatus_Click(object sender, EventArgs e)
        {
            Response.Redirect("GI_ViewStatus.aspx", false);
        }

        protected void Bt_MainMenu_Click1(object sender, EventArgs e)
        {
            Response.Redirect("../main_menu.aspx", false);
        }
    }
}