﻿<%@ Register TagPrefix="uc1" TagName="header_control" Src="../header_control.ascx"   %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GI_SelectDP.aspx.cs" Inherits="hh_siamkraft.GI_SelectDP" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>GI_SelectDP</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C# .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"/>
		<LINK href="../css/style.css" type="text/css" rel="stylesheet"/>
	    <style type="text/css">
            .style1
            {
                height: 25px;
            }
        </style>
</head>
    <body leftMargin="1" topMargin="1" rightmargin="1" bottommargin="1" MS_POSITIONING="GridLayout">
		<form id="myForm" method="post" runat="server">
			<table class="table_main" cellSpacing="0" cellPadding="0" width="225" border="0">
				<tr>
					<td class="headtable"><uc1:header_control id="Header_control1" runat="server"></uc1:header_control></td>
				</tr>
				<tr>
					<td class="headtable" align="center">GI - Goods Issue</td>
				</tr>
				<tr height="3">
					<td class="smallerText">
						<asp:Label id="Lb_msg" runat="server"></asp:Label></td>
				</tr>
				<tr style="PADDING-RIGHT: 2px; PADDING-LEFT: 2px; PADDING-BOTTOM: 2px; PADDING-TOP: 2px">
					<td align="center">
                        <asp:button id="Tb_save" runat="server" Width="48px" CssClass="buttonTag" Text="Save" OnClick="Tb_save_Click"></asp:button>&nbsp;
						<asp:button id="Bt_mainMenu" runat="server" Width="63px" CssClass="buttonTag" Text="GI Menu" OnClick="Bt_mainMenu_Click1"></asp:button>&nbsp;
                        <asp:button id="Tb_goodIssue" runat="server" Text="Goods Issue" CssClass="buttonTag" Width="80px" OnClick="Tb_goodIssue_Click" Visible="false"></asp:button>
					</td>
				</tr>
				<tr>
					<td>
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td align="left" colSpan="2"><asp:datagrid id="Dg_select" runat="server" AutoGenerateColumns="False" CellSpacing="1" CellPadding="1"
										BorderWidth="1px" BorderStyle="None" BorderColor="#E7E7FF" AllowPaging="True" PageSize="15" OnPageIndexChanged="Dg_select_PageIndexChanged">
										<FooterStyle ForeColor="#4A3C8C" BackColor="#B5C7DE"></FooterStyle>
										<SelectedItemStyle Font-Bold="True" ForeColor="#F7F7F7" BackColor="#738A9C"></SelectedItemStyle>
										<AlternatingItemStyle BackColor="#F7F7F7"></AlternatingItemStyle>
										<ItemStyle ForeColor="#4A3C8C" BackColor="#E7E7FF"></ItemStyle>
										<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#4A3C8C"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn HeaderText="Select">
												<HeaderStyle HorizontalAlign="Center" ForeColor="White" Width="60px" VerticalAlign="Middle"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
												<ItemTemplate>
													<asp:CheckBox ID="chk_id" Runat="server"></asp:CheckBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn DataField="Delivery_No" HeaderText="DP No.">
												<HeaderStyle HorizontalAlign="Center" ForeColor="White" Width="250px" VerticalAlign="Middle"></HeaderStyle>
												<ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="Record_ID" HeaderText="Record_ID">
												<HeaderStyle HorizontalAlign="Center" ForeColor="White" VerticalAlign="Middle"></HeaderStyle>
											</asp:BoundColumn>
										</Columns>
										<PagerStyle HorizontalAlign="Right" ForeColor="#4A3C8C" BackColor="#E7E7FF" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr class="tr" style="PADDING-RIGHT: 2px; PADDING-LEFT: 2px; PADDING-BOTTOM: 2px; PADDING-TOP: 2px">
					<td align="left">&nbsp;</td>
				</tr>
			</table>
		</form>
	</body>
</html>
