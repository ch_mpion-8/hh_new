﻿using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using hh_siamkraft.Entities.GR;
using hh_siamkraft.GI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{
    public partial class GI_TimeOut : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();
        protected void Page_Init(object sender, EventArgs e)
        {
         
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user_id"] == null)
            {
                Session.RemoveAll();
                Response.Redirect("../index.aspx", false);
                return;
            }

            if (Session["obj_gi"] == null)
            {
                Response.Redirect("GI_ScanDP.aspx", false);
                return;
            }

       

            if (!Page.IsPostBack)
            {
                variable_gi obj_gi = new variable_gi();
                obj_gi = (variable_gi)Session["obj_gi"];

                this.Lb_dp_no.Text = obj_gi.str_dpNo;
                this.Lb_id.Text = obj_gi.str_recID;
                this.Lb_plant.Text = obj_gi.str_plant;
                obj_gi = null;
            }

        }

        protected void Bt_retry_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32( Session["view_weight"]) == 1)
            {
                Session.Remove("view_weight");
                Response.Redirect("GI_WaitWeight.aspx", false);
            }
        }

        protected void Bt_back_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(Session["view_weight"]) == 1)
            {
                Session.Remove("view_weight");
                Response.Redirect("GI_ViewStatus.aspx", false);
            }
        }
    }
}