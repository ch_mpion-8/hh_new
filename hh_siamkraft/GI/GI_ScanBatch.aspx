﻿<%@ Register TagPrefix="uc1" TagName="header_control" Src="../header_control.ascx"   %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GI_ScanBatch.aspx.cs" Inherits="hh_siamkraft.GI_ScanBatch" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>GI_ScanBatch</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C# .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"/>
		<LINK href="../css/style.css" type="text/css" rel="stylesheet"/>
	    <style type="text/css">
            .style1
            {
                height: 25px;
            }
        </style>
</head>
	<body style="MARGIN: 1px" MS_POSITIONING="GridLayout">
		<form id="myForm" name="myForm" method="post" runat="server">
			<table class="table_main" cellSpacing="0" cellPadding="0" width="225" border="0">
				<tr>
					<td class="headtable"><uc1:header_control id="Header_control1" runat="server"></uc1:header_control></td>
				</tr>
				<tr>
					<td class="headtable" align="center">GI - Goods Issue</td>
				</tr>
				<tr height="3">
					<td class="smallerText"><asp:label id="Lb_msg" runat="server"></asp:label></td>
				</tr>
				<tr>
					<td>
						<table cellSpacing="0" cellPadding="0" width="100%">
							<tr>
								<td class="normalText" align="left">DP&nbsp;No.</td>
								<td class="normalText" align="left"><IMG src="../images/red_arrow.gif" align="absMiddle">&nbsp;
									<asp:label id="Lb_DP_no" runat="server" CssClass="normalText"></asp:label></td>
							</tr>
							<tr>
								<td class="normalText" align="left">
									UOM
								</td>
								<td class="normalText" align="left">
									<IMG src="../images/red_arrow.gif" align="absMiddle">&nbsp;
									<asp:RadioButton style="Z-INDEX: 0" id="rdbRoll" runat="server" AutoPostBack="True" Text="Roll" GroupName="UOM"
										Checked="True" OnCheckedChanged="rdbRoll_CheckedChanged"></asp:RadioButton>&nbsp;
									<asp:RadioButton style="Z-INDEX: 0" id="rdbSheet" runat="server" AutoPostBack="True" Text="Sheet"
										GroupName="UOM" Visible="False" OnCheckedChanged="rdbSheet_CheckedChanged"></asp:RadioButton>
								</td>
							</tr>
							<tr>
								<td class="normalText" align="left">Batch&nbsp;No.</td>
								<td align="left"><IMG src="../images/red_arrow.gif" align="absMiddle">&nbsp;
									<asp:textbox id="Tb_batch" runat="server" CssClass="inputTag" Width="110px" MaxLength="10" AutoPostBack="True" OnTextChanged="Tb_batch_TextChanged"></asp:textbox>
									<asp:button id="Bt_save" CssClass="buttonTag" Width="0px" Text="Save Data" Runat="server" Visible="False"></asp:button>
								</td>
							</tr>
							<tr>
								<td class="normalText" align="left">
									<asp:label style="Z-INDEX: 0" id="Label5" runat="server" CssClass="normalText" ForeColor="Black">Scan Total</asp:label></td>
								<td class="normalText" align="left"><IMG hspace="1" src="../images/red_arrow.gif" align="absMiddle">&nbsp;
									<asp:label id="Label2" runat="server" CssClass="normalText" style="Z-INDEX: 0"></asp:label>
									<asp:label id="Lb_scantotal" runat="server" CssClass="normalText"></asp:label>&nbsp;
									<asp:Label style="Z-INDEX: 0" id="Label6" runat="server" CssClass="normalText" ForeColor="Black">/</asp:Label>&nbsp;
									<asp:Label id="Lb_total" runat="server" CssClass="normalText"></asp:Label>
									<asp:Label id="Label1" runat="server" CssClass="normalText" style="Z-INDEX: 0"></asp:Label></td>
							</tr>
							<tr id="tr_send" style="PADDING-BOTTOM: 2px; PADDING-LEFT: 2px; PADDING-RIGHT: 2px; PADDING-TOP: 2px"
								runat="server">
								<td class="normalText" colSpan="2" align="center">
									<asp:button id="Bt_sendtosap" runat="server" CssClass="buttonTag" Width="80px" Text="Send To SAP" OnClick="Bt_sendtosap_Click"></asp:button>
									<asp:button id="Bt_hold" runat="server" CssClass="buttonTag" Width="40px" Text="Hold" Visible="False" OnClick="Bt_hold_Click"></asp:button>
									<asp:button id="btnSplit" runat="server" CssClass="buttonTag" Width="75px" Text="Split Ream"
										Visible="False" OnClick="btnSplit_Click"></asp:button>&nbsp;
									<asp:button id="Bt_back" runat="server" CssClass="buttonTag" Width="56px" Text="Back" style="Z-INDEX: 0" OnClick="Bt_back_Click"></asp:button>
								</td>
							</tr>
							<tr id="tr_hold" runat="server">
								<td colSpan="2">
									<table id="tbl_hold" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
										<tr>
											<td class="normalText">&nbsp;<IMG src="../images/question.gif" align="absMiddle">&nbsp;<asp:label id="Lb_msghold" CssClass="normalText" Runat="server" ForeColor="Red">Status is 'Hold' , Do you want send to SAP?</asp:label></td>
										</tr>
										<tr>
											<td class="normalText" align="left"><asp:radiobutton id="Rb_yes" Text="Yes , send to SAP" Runat="server" GroupName="hold"></asp:radiobutton><br>
												<asp:radiobutton id="Rb_no" Text="No , send to SAP" Runat="server" GroupName="hold"></asp:radiobutton></td>
										</tr>
										<tr class="tr" style="PADDING-BOTTOM: 2px; PADDING-LEFT: 2px; PADDING-RIGHT: 2px; PADDING-TOP: 2px">
											<td align="left"><asp:button id="Bt_send" runat="server" CssClass="buttonTag" Width="50px" Text="OK" OnClick="Bt_send_Click"></asp:button>&nbsp;&nbsp;</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td align="left" colSpan="2"><asp:datagrid id="Dg_detail" runat="server" Width="100%" CellSpacing="1" AutoGenerateColumns="False"
										CellPadding="1" BorderWidth="1px" BorderStyle="Solid" BorderColor="#E7E7FF" AllowPaging="True" OnItemDataBound="Dg_detail_ItemDataBound" OnPageIndexChanged="Dg_detail_PageIndexChanged">
										<FooterStyle ForeColor="#4A3C8C" BackColor="#B5C7DE"></FooterStyle>
										<SelectedItemStyle Font-Bold="True" ForeColor="#F7F7F7" BackColor="#738A9C"></SelectedItemStyle>
										<AlternatingItemStyle BackColor="#F7F7F7"></AlternatingItemStyle>
										<ItemStyle HorizontalAlign="Center" ForeColor="#4A3C8C" VerticalAlign="Middle" BackColor="#E7E7FF"></ItemStyle>
										<HeaderStyle Font-Bold="True" ForeColor="#F7F7F7" BackColor="#4A3C8C"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn HeaderText="No.">
												<HeaderStyle HorizontalAlign="Center" ForeColor="White" VerticalAlign="Middle"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
												<ItemTemplate>
													<asp:Label ID="Lb_no" Runat="server"></asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn DataField="Batch_No" HeaderText="Batch No.">
												<HeaderStyle HorizontalAlign="Center" ForeColor="White" VerticalAlign="Middle"></HeaderStyle>
												<ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
											</asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Qty">
												<HeaderStyle HorizontalAlign="Center" ForeColor="White" VerticalAlign="Middle"></HeaderStyle>
												<ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
												<ItemTemplate>
													<asp:Label ID="Lb_qty" Runat="server"></asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn DataField="Material_No" HeaderText="Material">
												<HeaderStyle Wrap="False" HorizontalAlign="Center" ForeColor="White" VerticalAlign="Middle"></HeaderStyle>
												<ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
											</asp:BoundColumn>
										</Columns>
										<PagerStyle VerticalAlign="Middle" Font-Underline="True" Font-Bold="True" HorizontalAlign="Left"
											ForeColor="#4A3C8C" BackColor="#E7E7FF" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr class="tr" style="PADDING-BOTTOM: 2px; PADDING-LEFT: 2px; PADDING-RIGHT: 2px; PADDING-TOP: 2px">
					<td align="left"></td>
				</tr>
			</table>
			<table>
				<tr>
					<td><asp:label id="Lb_id" Runat="server" Visible="False"></asp:label><asp:label id="Lb_plant" Runat="server" Visible="False"></asp:label></td>
				</tr>
			</table>
		</form>
	</body>
</html>
