﻿using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using hh_siamkraft.Entities.GR;
using hh_siamkraft.GI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{
    public partial class GI_ResultError : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();
        protected void Page_Init(object sender, EventArgs e)
        {
         
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user_id"] == null)
            {
                Session.RemoveAll();
                Response.Redirect("../index.aspx", false);
                return;
            }

            if (!Page.IsPostBack)
            {
                this.Tb_msg.Text = Convert.ToString(Session["error"]);
            }
        }

        protected void Bt_back_Click(object sender, EventArgs e)
        {
            if ( Convert.ToString(Session["view_weight"]) == "1")
            {
                Session.Remove("view_weight");
                Session.Remove("error");
                Response.Redirect("GI_ViewStatus.aspx", false);
            }
            else
            {
                Session.Remove("error");
                Response.Redirect("GI_ScanDP.aspx", false);
            }

        }
    }
}