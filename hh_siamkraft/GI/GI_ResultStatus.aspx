﻿<%@ Register TagPrefix="uc1" TagName="header_control" Src="../header_control.ascx"   %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GI_ResultStatus.aspx.cs" Inherits="hh_siamkraft.GI_ResultStatus" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>GI_ResultStatus</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C# .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"/>
		<LINK href="../css/style.css" type="text/css" rel="stylesheet"/>
	    <style type="text/css">
            .style1
            {
                height: 25px;
            }
        </style>
</head>
<body bottomMargin="1" leftMargin="1" topMargin="1" rightMargin="1" MS_POSITIONING="GridLayout">
		<form name="myForm" method="post" encType="multipart/form-data" runat="server">
			<table class="table_main" cellSpacing="0" cellPadding="0" width="225" border="0">
				<tr>
					<td class="headtable"><uc1:header_control id="Header_control1" runat="server"></uc1:header_control></td>
				</tr>
				<tr>
					<td class="headtable" align="center">Result Status</td>
				</tr>
				<tr>
					<td>
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="normalText" align="left">DP&nbsp;No.</td>
								<td class="normalText" align="left"><IMG src="../images/red_arrow.gif" align="absMiddle">
									<asp:label id="Lb_dp_no" runat="server" CssClass="normalText"></asp:label></td>
							</tr>
							<tr>
								<td class="normalText" align="left">Message</td>
								<td class="normalText" align="left"><IMG src="../images/red_arrow.gif" align="absMiddle">&nbsp;<asp:textbox id="Tb_msg" Height="96px" ReadOnly="True" TextMode="MultiLine" Runat="server" CssClass="inputTag"></asp:textbox></td>
							</tr>
							<tr id="tr_view" runat="server">
								<td class="normalText" align="left" colSpan="2"><IMG src="../images/red_arrow.gif" align="absMiddle">&nbsp;<asp:button id="Bt_view" runat="server" CssClass="buttonTag" Text="View Weight" OnClick="Bt_view_Click"></asp:button></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr class="tr">
					<td style="PADDING-BOTTOM: 2px; PADDING-LEFT: 2px; PADDING-RIGHT: 2px; PADDING-TOP: 2px"><asp:button id="Bt_Back" runat="server" CssClass="buttonTag" Text="Back" OnClick="Bt_Back_Click"></asp:button></td>
				</tr>
			</table>
		</form>
	</body>
</html>
