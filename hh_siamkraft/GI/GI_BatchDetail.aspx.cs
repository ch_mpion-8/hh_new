﻿using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using hh_siamkraft.GI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{
    public partial class GI_BatchDetail : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();
        protected void Page_Init(object sender, EventArgs e)
        {
         
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user_id"] == null)
            {
                Session.RemoveAll();
                Response.Redirect("../index.aspx", false);
                return;
            }

            if (!(Page.IsPostBack))
            {

                variable_gi obj_gi = new variable_gi();
                obj_gi = (variable_gi)Session["obj_gi"];
                this.Lb_dp_no.Text = obj_gi.str_dpNo;
                this.Lb_id.Text = obj_gi.str_recID;
                obj_gi = null;
            
                this.Lb_material_no.Text = Convert.ToString(Session["material_no"]);
                bindData();
            }
        }

        private void bindData()
        {
            var connect = Convert.ToString(Session["Connection_String"]);
            using (var session = new SkRepositorySession(connect))
            {
                var repo = new GIRepository(session);
                var tbl = repo.GetVwGIBatch(this.Lb_dp_no.Text.Trim(), this.Lb_material_no.Text, Convert.ToInt32(this.Lb_id.Text));
                this.Dg_Detail.DataSource = tbl.Value;
                this.Dg_Detail.DataBind();
            }
        }

        protected void Bt_back_Click(object sender, EventArgs e)
        {
            variable_gi obj_gi = new variable_gi();
            obj_gi.str_dpNo = this.Lb_dp_no.Text;
            obj_gi.str_recID = this.Lb_id.Text;
            Session["obj_gi"] = obj_gi;
            obj_gi = null;
            Response.Redirect("GI_ResultWeight.aspx", false);
        }

        protected void Bt_mainmenu_Click(object sender, EventArgs e)
        {
            Session.Remove("obj_gi");
            Response.Redirect("GI_MainMenu.aspx", false);
        }

        protected void Dg_Detail_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            Label lb_no = (Label) e.Item.FindControl("Lb_no");
            Label lb_pick_hh = (Label)e.Item.FindControl("Lb_pick_hh");
            Label lb_pi = (Label)e.Item.FindControl("Lb_pi");
            if (!(lb_no == null))
            {
                lb_no.Text = ((Dg_Detail.CurrentPageIndex * 10)  + (this.Dg_Detail.Items.Count + 1)).ToString();
            }

            if (!(lb_pick_hh == null))
            {
                lb_pick_hh.Text = (Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "Qty"))).ToString("0.000");
            }

            if (!(lb_pi == null))
            {
                lb_pi.Text = (Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "Actual_Qty"))).ToString("0.000"); 
            }
        }

        public string total_pick_hh()
        {
            int i;
            Label lbTotal;
            decimal TotalPick = 0;
            for (i = 0; i <= (Dg_Detail.Items.Count - 1); i++)
            {
                lbTotal = (Label)Dg_Detail.Items[i].FindControl("Lb_pick_hh");
                TotalPick = (decimal.Parse(lbTotal.Text) + TotalPick);
            }

            return TotalPick.ToString("0.000");
        }

        public string total_pi()
        {
            int i;
            Label lbTotal;
            decimal TotalPI = 0;
            for (i = 0; i <= (Dg_Detail.Items.Count - 1); i++)
            {
                lbTotal = (Label)Dg_Detail.Items[i].FindControl("Lb_pi");
                TotalPI = (decimal.Parse(lbTotal.Text) + TotalPI);
            }

            return TotalPI.ToString("0.000");
        }

        protected void Dg_Detail_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            Dg_Detail.CurrentPageIndex = e.NewPageIndex;
            bindData();
        }
    }
}