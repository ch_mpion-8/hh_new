﻿<%@ Register TagPrefix="uc1" TagName="header_control" Src="../header_control.ascx"   %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GI_BatchDetail.aspx.cs" Inherits="hh_siamkraft.GI_BatchDetail" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>GI_BatchDetail</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C# .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"/>
		<LINK href="../css/style.css" type="text/css" rel="stylesheet"/>
	    <style type="text/css">
            .style1
            {
                height: 25px;
            }
        </style>
</head>
	<body MS_POSITIONING="GridLayout" topmargin="1" leftmargin="1" rightmargin="1" bottommargin="1">
		<form name="myForm" id="myForm" method="post" runat="server">
			<table class="table_main" width="225" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td class="headtable"><uc1:header_control id="Header_control1" runat="server"></uc1:header_control></td>
				</tr>
				<tr>
					<td align="center" class="headtable">View Weight</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr align="left">
								<td align="left" class="normalText">DP&nbsp;No.</td>
								<td align="left" class="normalText"><img src="../images/red_arrow.gif" align="absMiddle">&nbsp;
									<asp:Label id="Lb_dp_no" runat="server" CssClass="normalText"></asp:Label></td>
							</tr>
							<tr>
								<td align="left" class="normalText">Material</td>
								<td align="left" class="normalText"><img src="../images/red_arrow.gif" align="absMiddle">&nbsp;
									<asp:Label id="Lb_material_no" runat="server" CssClass="normalText"></asp:Label></td>
							</tr>
							<tr>
								<td align="left" colspan="2">
									<asp:DataGrid id="Dg_Detail" runat="server" Width="100%" BorderColor="#E7E7FF" BorderStyle="Solid"
										BorderWidth="1px" CellPadding="1" CellSpacing="1" AutoGenerateColumns="False" ShowFooter="True"
										ForeColor="White" AllowPaging="True" OnItemDataBound="Dg_Detail_ItemDataBound" OnPageIndexChanged="Dg_Detail_PageIndexChanged">
										<FooterStyle Font-Bold="True" HorizontalAlign="Right" ForeColor="#4A3C8C" VerticalAlign="Middle"
											BackColor="#B5C7DE"></FooterStyle>
										<SelectedItemStyle Font-Bold="True" ForeColor="#F7F7F7" BackColor="#738A9C"></SelectedItemStyle>
										<AlternatingItemStyle BackColor="#F7F7F7"></AlternatingItemStyle>
										<ItemStyle HorizontalAlign="Right" ForeColor="#4A3C8C" VerticalAlign="Middle" BackColor="#E7E7FF"></ItemStyle>
										<HeaderStyle Font-Bold="True" ForeColor="#F7F7F7" BackColor="#4A3C8C"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn HeaderText="No" FooterText="Total">
												<HeaderStyle HorizontalAlign="Center" ForeColor="White" VerticalAlign="Middle"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
												<ItemTemplate>
													<asp:Label ID="Lb_no" Runat="server"></asp:Label>
												</ItemTemplate>
												<FooterStyle Font-Bold="True"></FooterStyle>
											</asp:TemplateColumn>
											<asp:BoundColumn DataField="Batch_No" HeaderText="Batch">
												<HeaderStyle HorizontalAlign="Center" ForeColor="White" VerticalAlign="Middle"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
											</asp:BoundColumn>
											<asp:TemplateColumn HeaderText="H/H">
												<HeaderStyle HorizontalAlign="Center" ForeColor="White" VerticalAlign="Middle"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
												<ItemTemplate>
													<asp:Label ID="Lb_pick_hh" Runat="server"></asp:Label>
												</ItemTemplate>
												<FooterStyle HorizontalAlign="Center" VerticalAlign="Middle"></FooterStyle>
												<FooterTemplate>
													<%= total_pick_hh() %>
												</FooterTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="PI">
												<HeaderStyle HorizontalAlign="Center" ForeColor="White" VerticalAlign="Middle"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
												<ItemTemplate>
													<asp:Label ID="Lb_pi" Runat="server"></asp:Label>
												</ItemTemplate>
												<FooterStyle HorizontalAlign="Center" VerticalAlign="Middle"></FooterStyle>
												<FooterTemplate>
													<%= total_pi() %>
												</FooterTemplate>
											</asp:TemplateColumn>
										</Columns>
										<PagerStyle Font-Underline="True" Font-Bold="True" HorizontalAlign="Left" ForeColor="#4A3C8C"
											BackColor="#E7E7FF" Mode="NumericPages"></PagerStyle>
									</asp:DataGrid></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr class="tr">
					<td align="left">
						<asp:Button id="Bt_back" runat="server" CssClass="buttonTag" Width="48px" Text="Back" OnClick="Bt_back_Click"></asp:Button>
						<asp:Button id="Bt_mainmenu" runat="server" CssClass="buttonTag" Width="70px" Text="GI Menu" OnClick="Bt_mainmenu_Click"></asp:Button></td>
				</tr>
			</table>
			<table>
				<tr>
					<td><asp:Label ID="Lb_id" Runat="server" Visible="False"></asp:Label></td>
				</tr>
			</table>
		</form>
	</body>
</html>
