﻿<%@ Register TagPrefix="uc1" TagName="header_control" Src="../header_control.ascx"   %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GI_ResultError.aspx.cs" Inherits="hh_siamkraft.GI_ResultError" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>GI_ResultError</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C# .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"/>
		<LINK href="../css/style.css" type="text/css" rel="stylesheet"/>
	    <style type="text/css">
            .style1
            {
                height: 25px;
            }
        </style>
</head>
<body MS_POSITIONING="GridLayout" style="MARGIN:1px">
		<form id="myForm" method="post" runat="server">
			<table class="table_main" width="225" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td class="headtable"><uc1:header_control id="Header_control1" runat="server"></uc1:header_control></td>
				</tr>
				<tr>
					<td align="center" class="headtable">
						Error Result</td>
				</tr>
				<tr>
					<td>
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td align="left" class="normalText">Status</td>
								<td align="left" class="normalText"><img src="../images/red_arrow.gif" hspace="1" align="absMiddle">&nbsp;<font color="red">ERROR</font></td>
							</tr>
							<tr>
								<td align="left" class="normalText">Message</td>
								<td align="left" class="normalText"><img src="../images/red_arrow.gif" hspace="1" align="absMiddle">&nbsp;<asp:TextBox ID="Tb_msg" Runat="server" CssClass="inputTag" TextMode="MultiLine" ReadOnly="True"
										Height="96px"></asp:TextBox></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr class="tr" style="PADDING-RIGHT:2px;PADDING-LEFT:2px;PADDING-BOTTOM:2px;PADDING-TOP:2px">
					<td><asp:Button id="Bt_back" Runat="server" Text=" Back" CssClass="buttonTag" OnClick="Bt_back_Click"></asp:Button></td>
				</tr>
			</table>
		</form>
	</body>

</html>
