﻿<%@ Register TagPrefix="uc1" TagName="header_control" Src="../header_control.ascx"   %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GI_Detail.aspx.cs" Inherits="hh_siamkraft.GI_Detail" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>GI_Detail</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C# .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"/>
		<LINK href="../css/style.css" type="text/css" rel="stylesheet"/>
	    <style type="text/css">
            .style1
            {
                height: 25px;
            }
        </style>
</head>
	<body style="MARGIN: 1px" MS_POSITIONING="GridLayout">
		<form id="myForm" method="post" runat="server">
			<table class="table_main" cellSpacing="0" cellPadding="0" width="225" border="0">
				<tr>
					<td class="headtable"><uc1:header_control id="Header_control1" runat="server"></uc1:header_control></td>
				</tr>
				<tr>
					<td class="headtable" align="center">GI&nbsp;-&nbsp;GOODS ISSUE</td>
				</tr>
				<tr>
					<td>
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="normalText" align="left" style="WIDTH: 121px">DP&nbsp;No.</td>
								<td class="normalText" align="left"><IMG src="../images/red_arrow.gif" align="absMiddle">&nbsp;
									<asp:label id="Lb_dp_no" runat="server" CssClass="normalText"></asp:label></td>
							</tr>
							<tr>
								<td class="normalText" align="left" style="WIDTH: 121px">Total&nbsp;Material&nbsp;/Total&nbsp;Qty</td>
								<td class="normalText" align="left"><IMG src="../images/red_arrow.gif" align="absMiddle">&nbsp;
									<asp:label id="Lb_total" runat="server" CssClass="normalText"></asp:label>&nbsp;/&nbsp;<asp:Label ID="Lb_total_roll" Runat="server" CssClass="normalText"></asp:Label></td>
							</tr>
							<tr style="PADDING-BOTTOM: 2px; PADDING-LEFT: 2px; PADDING-RIGHT: 2px; PADDING-TOP: 2px">
								<td colspan="2"><img src="../images/red_arrow.gif" align="absMiddle" hspace="1">&nbsp;<asp:button id="Bt_next" runat="server" Width="72px" CssClass="buttonTag" Text="Scan Data" OnClick="Bt_next_Click"></asp:button>&nbsp;
									<asp:button id="Bt_back" runat="server" Width="50px" CssClass="buttonTag" Text="Back" OnClick="Bt_back_Click"></asp:button></td>
							</tr>
							<tr>
								<td noWrap align="left" colSpan="2">
                                    <asp:datagrid id="Dg_Detail" runat="server" Width="100%" ShowFooter="True" AutoGenerateColumns="False"
										CellSpacing="1" CellPadding="1" BackColor="White" BorderWidth="1px" BorderStyle="Solid" BorderColor="#E7E7FF" OnItemDataBound="Dg_Detail_ItemDataBound">
										<SelectedItemStyle Font-Bold="True" ForeColor="#F7F7F7" BackColor="#738A9C"></SelectedItemStyle>
										<AlternatingItemStyle BackColor="#F7F7F7"></AlternatingItemStyle>
										<ItemStyle HorizontalAlign="Right" ForeColor="#4A3C8C" VerticalAlign="Middle" BackColor="#E7E7FF"></ItemStyle>
										<HeaderStyle Font-Bold="True" ForeColor="#F7F7F7" BackColor="#4A3C8C"></HeaderStyle>
										<FooterStyle HorizontalAlign="Right" ForeColor="#4A3C8C" VerticalAlign="Middle" BackColor="#B5C7DE"></FooterStyle>
										<Columns>
											<asp:TemplateColumn HeaderText="No" FooterText="Total :">
												<HeaderStyle HorizontalAlign="Center" ForeColor="White" VerticalAlign="Middle"></HeaderStyle>
												<ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
												<ItemTemplate>
													<asp:Label ID="Lb_no" Runat="server"></asp:Label>
												</ItemTemplate>
												<FooterStyle Font-Bold="True" HorizontalAlign="Right" VerticalAlign="Middle"></FooterStyle>
											</asp:TemplateColumn>
											<asp:BoundColumn DataField="Material_No" HeaderText="Material">
												<HeaderStyle HorizontalAlign="Center" ForeColor="White" VerticalAlign="Middle"></HeaderStyle>
												<ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
											</asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Order">
												<HeaderStyle HorizontalAlign="Center" ForeColor="White" VerticalAlign="Middle"></HeaderStyle>
												<ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
												<ItemTemplate>
													<asp:Label id="Lb_order" Runat="server"></asp:Label>
												</ItemTemplate>
												<FooterStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle"></FooterStyle>
												<FooterTemplate>
													<%# total_order() %>
												</FooterTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Scan">
												<HeaderStyle HorizontalAlign="Center" ForeColor="White" VerticalAlign="Middle"></HeaderStyle>
												<ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
												<ItemTemplate>
													<asp:Label id="Lb_ao" Runat="server"></asp:Label>
												</ItemTemplate>
												<FooterStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle"></FooterStyle>
												<FooterTemplate>
													<%# total_ao() %>
												</FooterTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="HH">
												<HeaderStyle HorizontalAlign="Center" ForeColor="White" VerticalAlign="Middle"></HeaderStyle>
												<ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
												<ItemTemplate>
													<asp:Label ID="Lb_hh_qty" Runat="server"></asp:Label>
												</ItemTemplate>
												<FooterStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle"></FooterStyle>
												<FooterTemplate>
													<%# total_qty() %>
												</FooterTemplate>
											</asp:TemplateColumn>
										</Columns>
										<PagerStyle HorizontalAlign="Right" ForeColor="#4A3C8C" BackColor="#E7E7FF" Mode="NumericPages"></PagerStyle>
									</asp:datagrid>

								</td>
							</tr>
							<!--	<tr>
								<td class="normalText">Text</td>
								<td class="normalText"><IMG src="../images/red_arrow.gif" align="absMiddle">&nbsp;
									<asp:TextBox id="Tb_msg" TextMode="MultiLine" ReadOnly="True" Height="96px" Runat="server" CssClass="inputTag"></asp:TextBox></td>
							</tr>	-->
						</table>
					</td>
				</tr>
				<tr class="tr" style="PADDING-BOTTOM: 2px; PADDING-LEFT: 2px; PADDING-RIGHT: 2px; PADDING-TOP: 2px">
					<td align="left"></td>
				</tr>
			</table>
			<table>
				<tr>
					<td><asp:label id="Lb_id" Runat="server" Visible="False"></asp:label><asp:label id="Lb_plant" Runat="server" Visible="False"></asp:label></td>
				</tr>
			</table>
		</form>
	</body>
</html>
