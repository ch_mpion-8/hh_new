﻿using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using hh_siamkraft.GI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{
    public partial class GI_ScanBatch : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();
        protected void Page_Init(object sender, EventArgs e)
        {
         
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user_id"] == null)
            {
                Session.RemoveAll();
                Response.Redirect("../index.aspx", false);
                return;
            }

            if (Session["obj_gi"] == null)
            {
                Response.Redirect("GI_ScanDP.aspx", false);
                return;
            }


            if (!(Page.IsPostBack))
            {

                this.tr_hold.Visible = false;
                if (!(Session["lb_msg"] == null))
                {
                    this.Lb_msg.Text = Convert.ToString(Session["lb_msg"]);
                    Session.Remove("lb_msg");
                }

                variable_gi obj_gi = new variable_gi();
                obj_gi = (variable_gi)Session["obj_gi"];
                this.Lb_DP_no.Text = obj_gi.str_dpNo;
                this.Lb_id.Text = obj_gi.str_recID;
                this.Lb_plant.Text = obj_gi.str_plant;

                if ((obj_gi.str_ChkType != "RM"))
                {
                    this.Label2.Visible = false;
                    this.Label1.Visible = false;
                }
                else
                {
                    this.Lb_scantotal.Visible = false;
                    this.Lb_total.Visible = false;
                }

                this.Label1.Text = obj_gi.str_Total;
                obj_gi = null;
                script_func.script_focus(this.Tb_batch.ID, Page);
                bindData();
            }
            this.Lb_msg.Text = String.Empty;
        }

        private void initialForm()
        {
            this.Tb_batch.Text = "";
            script_func.script_focus(this.Tb_batch.ClientID, Page);
        }

        private void bindData()
        {
            decimal SumScan = 0;
            var connect = Convert.ToString(Session["Connection_String"]);
            using (var session = new SkRepositorySession(connect))
            {
                var repo = new GIRepository(session);
                var result = repo.GetDeliveryDetail(this.Lb_DP_no.Text, Convert.ToInt32( this.Lb_id.Text));
                this.Dg_detail.DataSource = result.Value.Item2;
                this.Dg_detail.DataBind();

                this.Lb_scantotal.Text = Convert.ToString(result.Value.Item2.Count());
                this.Lb_total.Text = Convert.ToString(result.Value.Item1);
                for (int i = 0; i <= result.Value.Item2.Count() - 1; i++)
                {
                    SumScan = (SumScan + result.Value.Item2[i].Qty);
                }

                this.Label2.Text = SumScan.ToString("0.0");

            }

        }

        protected void Tb_batch_TextChanged(object sender, EventArgs e)
        {
            SaveScanBarcode();
        }

        private void SaveScanBarcode()
        {
            try
            {
                if (this.Tb_batch.Text.Trim() == "")
                {
                    script_func.script_alert("Please scan 'Batch No'.", Page);
                    script_func.script_focus(this.Tb_batch.ClientID, Page);
                    return;
                }

                if ((this.Tb_batch.Text.Length != 10))
                {
                    script_func.script_alert("Scan 'Batch No' Incorrect", Page);
                    script_func.script_focus(this.Tb_batch.ClientID, Page);
                    script_func.script_select(this.Tb_batch.ClientID, Page);
                    return;
                }

                string uom = "";
                if (rdbRoll.Checked)
                {
                    uom = "ROL";
                }
                else if (rdbSheet.Checked)
                {
                    uom = "RM";
                }

                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession(connect))
                {
                    var repo = new GIRepository(session);
                    var result = repo.SaveGIScan(Lb_DP_no.Text, Convert.ToDecimal(Lb_id.Text), Tb_batch.Text.Trim().ToUpper(), Convert.ToString(Session["UserLogin_WHCode"]), Convert.ToString(Session["user_id"]), Convert.ToInt32(Convert.ToString(Session["UserLogin_WHID"])), uom);
                    int flag = result.Value.Item1;
                    if (flag != -1)
                    {
                        if ((flag == 1))
                        {
                            // batch_no not found
                            Tb_batch.Text = String.Empty;
                            script_func.script_alert(Convert.ToString(result.Error.Message), Page);
                            script_func.script_focus(this.Tb_batch.ClientID, Page);
                            script_func.script_select(this.Tb_batch.ClientID, Page);
                            return;
                        }
                        else if ((flag == 2))
                        {
                            // batch_no duplicate
                            Session.Remove("dt_dup");
                            Session["dt_dup"] = result.Value.Item2;
                            Response.Redirect("GI_Duplicate.aspx", false);
                        }
                        else
                        {
                            // save data complete
                            this.Lb_msg.Text = String.Empty;
                            bindData();
                            initialForm();
                        }

                    }
                    else
                    {
                        Tb_batch.Text = String.Empty;
                        script_func.script_alert("exec error!!", Page);
                        script_func.script_focus(this.Tb_batch.ClientID, Page);
                    }
                }
            }
            catch (Exception ex)
            {
                Tb_batch.Text = string.Empty;
                script_func.script_alert(("Save Data error: " + ex.Message), Page);
            }

        }


        private string retActual(decimal actual)
        {
            string[] dot;
            dot = actual.ToString().Split('.');
            if ((dot.Length == 2))
            {
                // have dot
                if ((decimal.Parse(dot[1].ToCharArray(0, 1).ToString()) >= 5))
                {
                    dot[0] = dot[1];
                }

            }

            return dot[0];
        }

        protected void Bt_back_Click(object sender, EventArgs e)
        {
            //variable_gi obj_gi = new variable_gi();
            //obj_gi = (variable_gi)Session["obj_gi"];
            //Session["obj_gi"] = obj_gi;
            //obj_gi = null;
            Response.Redirect("GI_Detail.aspx", false);
        }

        protected void Dg_detail_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            try
            {
                if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
                {
                    Label lb_no = (Label) e.Item.FindControl("Lb_no");
                    Label lb_qty = (Label) e.Item.FindControl("Lb_qty");
                    string sales_unit;
                    if (!(lb_no == null))
                    {
                        lb_no.Text = ((Dg_detail.CurrentPageIndex * Dg_detail.PageSize) + (Dg_detail.Items.Count + 1)).ToString();
                    }

                    sales_unit = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "UOM"));
                    if (!(lb_qty == null))
                    {
                        lb_qty.Text = DataBinder.Eval(e.Item.DataItem, "Qty").ToString();
                        if(Convert.ToString(Session["DB_Description"]).Contains("17"))
                        {
                            lb_qty.Text = (Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "Qty"))/1000).ToString("0.000");
                        }
                        //if (string.Equals("RM", sales_unit.ToUpper()))
                        //{
                        //    lb_qty.Text = DataBinder.Eval(e.Item.DataItem, "Qty").ToString();//((double)DataBinder.Eval(e.Item.DataItem, "Qty")).ToString("0.000");
                        //}
                        //else
                        //{
                        //    lb_qty.Text = DataBinder.Eval(e.Item.DataItem, "Qty").ToString();//((double)DataBinder.Eval(e.Item.DataItem, "Qty")).ToString("0.000");
                        //}
                    }

                }

            }
            catch (Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
            }

        }

        protected void Bt_sendtosap_Click(object sender, EventArgs e)
        {
            var connect = Convert.ToString(Session["Connection_String"]);
            using (var session = new SkRepositorySession(connect))
            {
                var repo = new GIRepository(session);
                var dt_dp = repo.GetHHGiItem(Lb_DP_no.Text, "%");
                if (dt_dp != null)
                {
                    if (dt_dp.Value.Count() == 0)
                    {
                        script_func.script_alert("No record send to SAP", Page);
                        bindData();
                        initialForm();
                        return;
                    }
                }

                var resultDL = repo.GetDLHead(Convert.ToInt32(this.Lb_id.Text), this.Lb_DP_no.Text);
                string dp_status = resultDL.Value[0].Status;


                if ((check_Scan() == true))
                {
                    if ((dp_status == "H"))
                    {
                        script_func.script_alert("Status is Hold", Page);
                        this.tr_hold.Visible = true;
                        this.tr_send.Visible = false;
                        script_func.script_select(this.Tb_batch.ClientID, Page);
                        return;
                    }

                    if ((get_Status(dp_status) == false))
                    {
                        variable_gi obj_gi = new variable_gi();
                        obj_gi.str_dpNo = this.Lb_DP_no.Text;
                        obj_gi.str_recID = this.Lb_id.Text;
                        Session["obj_gi"] = obj_gi;
                        obj_gi = null;
                        Response.Redirect("GI_ConfirmSend.aspx", false);
                    }

                }
                else
                {
                    if ((get_Status(dp_status) == false))
                    {
                    
                        var record_id = repo.SaveGIToSap(Convert.ToInt32(Lb_id.Text), Lb_DP_no.Text, Convert.ToString(Session["HH_ID"]), Convert.ToString(Session["user_id"]), Convert.ToInt32(Session["WH_No"]));

                        if ((record_id.Value != 0))
                        {
                            Session["msg"] = "<font color=lightgreen><b>SUCCESS !!</b></font> send to SAP on success.";
                            Session.Remove("obj_gi");
                            Response.Redirect("GI_ScanDP.aspx", false);
                        }
                        else
                        {
                            script_func.script_alert("Get Record ID on error", Page);
                        }

                    }

                }

            }

        }

        private bool get_Status(string str_status)
        {
            bool ret = false;
            switch (str_status)
            {
                case "W":
                    script_func.script_alert(("DP No -> " + (this.Lb_DP_no.Text + "\r\n waiting send to SAP")), Page);
                    initialForm();
                    ret = true;
                    break;
                case "C":
                    script_func.script_alert(("DP No -> " + (this.Lb_DP_no.Text + "\r\n send to SAP complete")), Page);
                    initialForm();
                    ret = true;
                    break;
            }
            return ret;
        }

        protected void Bt_hold_Click(object sender, EventArgs e)
        {
            try
            {
                var connect2 = Convert.ToString(Session["Connection_String"]);
                using (var session2 = new SkRepositorySession(connect2))
                {
                    var repo2 = new GIRepository(session2);
                    var result = repo2.UpdateStatusDLHead_NoTime("H", this.Lb_DP_no.Text.Trim(), Convert.ToInt32( this.Lb_id.Text.Trim()));
                    if (result.Value)
                    {
                        this.Lb_msg.Text = "<font color=lightgreen><b>SUCCESS !!</b></font> status is Hold on success.";
                        Session["hold"] = true;
                        script_func.script_select(this.Tb_batch.ClientID, Page);
                    }
                }
            }
            catch (Exception ex)
            {
                script_func.script_alert("Set status on error", Page);
            }

        }

        protected void Bt_send_Click(object sender, EventArgs e)
        {
            if ((this.Rb_yes.Checked == false) && (this.Rb_no.Checked == false))
            {
                script_func.script_alert("Please Select 'yes/no'", Page);
                return;
            }


            var connect2 = Convert.ToString(Session["Connection_String"]);
            using (var session2 = new SkRepositorySession(connect2))
            {
                var repo2 = new GIRepository(session2);
                if ((this.Rb_yes.Checked == true))
                {
                    // send to sap
                    var dt_dp = repo2.GetHHGiItem(this.Lb_DP_no.Text, "%");
                    if (!(dt_dp == null))
                    {
                        if ((dt_dp.Value.Count() == 0))
                        {
                            script_func.script_alert("No record send to SAP", Page);
                            bindData();
                            initialForm();
                            this.tr_hold.Visible = false;
                            this.tr_send.Visible = true;
                            return;
                        }

                    }

                    var resultDL = repo2.GetDLHead(Convert.ToInt32(this.Lb_DP_no.Text), this.Lb_id.Text);
                    string dp_status = resultDL.Value[0].Status;
                    if ((check_Scan() == true))
                    {
                        if ((get_Status(dp_status) == false))
                        {
                            variable_gi obj_gi = new variable_gi();
                            obj_gi.str_recID = this.Lb_id.Text;
                            obj_gi.str_dpNo = this.Lb_DP_no.Text;
                            obj_gi.str_plant = this.Lb_plant.Text;

                            Session["obj_gi"] = obj_gi;
                            obj_gi = null;
                            Response.Redirect("GI_ConfirmSend.aspx", false);
                            return;
                        }

                    }
                    else
                    {
                        if ((get_Status(dp_status) == false))
                        {
                            //var record_id = repo2.GIRecordID(Convert.ToString(Session["HH_ID"]), Convert.ToString(Session["user_id"]), this.Lb_id.Text.Trim(), this.Lb_DP_no.Text.Trim());
                            var record_id = repo2.SaveGIToSap(Convert.ToInt32(Lb_id.Text), Lb_DP_no.Text, Convert.ToString(Session["HH_ID"]), Convert.ToString(Session["user_id"]), Convert.ToInt32(Session["WH_No"]));

                            if ((record_id.Value != 0))
                            {
                                Session["msg"] = "<font color=lightgreen><b>SUCCESS !!</b></font> send to SAP on success.";
                                Session.Remove("obj_gi");
                                Response.Redirect("GI_ScanDP.aspx", false);
                            }
                            else
                            {
                                script_func.script_alert("Get Record ID on error", Page);
                            }

                        }

                    }

                }
                else
                {
                    // not send
                    this.tr_hold.Visible = false;
                    this.tr_send.Visible = true;
                    initialForm();
                }
            }
        }
        private bool check_Scan()
        {
            var connect2 = Convert.ToString(Session["Connection_String"]);
            using (var session2 = new SkRepositorySession(connect2))
            {
                var repo2 = new GIRepository(session2);
                var dt_dp = repo2.GICountBatch(this.Lb_DP_no.Text, Convert.ToInt32( this.Lb_id.Text));

                //if(dt_dp.Value.Item1 < dt_dp.Value.Item2)
                if (dt_dp.Value == "1")
                {
                    variable_gi obj_gi = new variable_gi();
                    obj_gi.str_recID = this.Lb_id.Text.Trim();
                    obj_gi.str_dpNo = this.Lb_DP_no.Text.Trim();
                    obj_gi.str_plant = this.Lb_plant.Text.Trim();

                    Session["obj_gi"] = obj_gi;
                    obj_gi = null;
                    return true;
                }
            }

            return false;
        }

        private int getID()
        {
            var connect2 = Convert.ToString(Session["Connection_String"]);
            using (var session2 = new SkRepositorySession(connect2))
            {
                var repo2 = new GIRepository(session2);
                var record_id = repo2.GIRecordID(Convert.ToString(Session["HH_ID"]) , Convert.ToString(Session["user_id"]) , this.Lb_id.Text.Trim(), this.Lb_DP_no.Text.Trim());
                if ((record_id.Value != 0))
                {
                    variable_gi obj_gi = new variable_gi();
                    obj_gi.str_recID = Convert.ToString(record_id.Value);
                    obj_gi.str_dpNo = this.Lb_DP_no.Text.Trim();
                    obj_gi.str_plant = this.Lb_plant.Text.Trim();

                    Session["obj_gi"] = obj_gi;
                    obj_gi = null;
                    return record_id.Value;
                }
                else
                {
                    return 0;
                }
            }
        }

        protected void Dg_detail_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            this.Dg_detail.CurrentPageIndex = e.NewPageIndex;
            bindData();
        }

        protected void btnSplit_Click(object sender, EventArgs e)
        {
            variable_gi obj_gi = new variable_gi();
            obj_gi.str_recID = this.Lb_id.Text.Trim();
            obj_gi.str_dpNo = this.Lb_DP_no.Text.Trim();
            obj_gi.str_plant = this.Lb_plant.Text.Trim();
            Session["obj_gi"] = obj_gi;
            obj_gi = null;
            Response.Redirect("GI_SplitReam.aspx", false);
        }

        protected void rdbRoll_CheckedChanged(object sender, EventArgs e)
        {
            if(rdbRoll.Checked)
            {
                script_func.script_focus(Tb_batch.ClientID, Page);
            }
        }

        protected void rdbSheet_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbSheet.Checked) {
                script_func.script_focus(Tb_batch.ClientID, Page);
            }
        }

       

      
    }
}