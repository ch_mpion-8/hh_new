﻿<%@ Register TagPrefix="uc1" TagName="header_control" Src="../header_control.ascx"   %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GI_MainMenu.aspx.cs" Inherits="hh_siamkraft.GI_MainMenu" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>GI_MainMenu</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C# .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"/>
		<LINK href="../css/style.css" type="text/css" rel="stylesheet"/>
	    <style type="text/css">
            .style1
            {
                height: 25px;
            }
        </style>
</head>
<body bottomMargin="1" leftMargin="1" rightMargin="1" topMargin="1" MS_POSITIONING="GridLayout">
		<form id="myForm" method="post" runat="server">
			<table class="table_main" border="0" cellSpacing="0" cellPadding="0" width="225">
				<tr>
					<td class="headtable"><uc1:header_control id="Header_control1" runat="server"></uc1:header_control></td>
				</tr>
				<tr>
					<td class="headtable" align="center"><font color="white">GI Main Menu</font></td>
				</tr>
				<tr height="3">
					<td></td>
				</tr>
				<tr>
					<td>
						<table border="0" cellSpacing="0" cellPadding="2" width="100%">
							<tr id="tr_selectdp" runat="server" Visible="false">
								<td align="center"><asp:button id="Bt_check_dp" runat="server" Text="SELECT DP ALREADY FOR SCAN" Width="210px"
										CssClass="buttonTag" Height="22px" OnClick="Bt_check_dp_Click"></asp:button></td>
							</tr>
							<tr id="tr_601" runat="server" Visible="false">
								<td align="center"><asp:button id="Bt_gi" runat="server" Text="GOODS ISSUE" Width="210px" CssClass="buttonTag"
										Height="22px" OnClick="Bt_gi_Click"></asp:button></td>
							</tr>
							<TR id="tr_viewstatus" runat="server" Visible="false">
								<TD align="center"><asp:button id="Bt_GI_ViewStatus" runat="server" Text="VIEW STATUS" Width="210px" CssClass="buttonTag"
										Height="22px" style="Z-INDEX: 0" OnClick="Bt_GI_ViewStatus_Click"></asp:button></TD>
							</TR>
							<tr>
								<td align="center"><FONT face="Tahoma"><asp:button id="Bt_MainMenu" runat="server" Height="22px" CssClass="buttonTag" Width="210px"
											Text="MAIN MENU" style="Z-INDEX: 0" OnClick="Bt_MainMenu_Click1"></asp:button></FONT></td>
							</tr>
							<tr>
								<td align="center"><asp:button style="Z-INDEX: 0" id="Button1" runat="server" Text="201" Width="34px" CssClass="buttonTag"
										Height="22px" Visible="False" ></asp:button></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
