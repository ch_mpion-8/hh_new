﻿<%@ Register TagPrefix="uc1" TagName="header_control" Src="../header_control.ascx"   %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GI_Duplicate.aspx.cs" Inherits="hh_siamkraft.GI_Duplicate" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Scan Duplicate</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C# .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"/>
		<LINK href="../css/style.css" type="text/css" rel="stylesheet"/>
	    <style type="text/css">
            .style1
            {
                height: 25px;
            }
        </style>
</head>
	<body MS_POSITIONING="GridLayout" topmargin="1" leftmargin="1" rightmargin="1" bottommargin="1">
		<form id="myForm" method="post" runat="server">
			<table class="table_main" cellSpacing="0" cellPadding="0" width="225" border="0">
				<tr>
					<td class="headtable"><uc1:header_control id="Header_control1" runat="server"></uc1:header_control></td>
				</tr>
				<tr>
					<td class="headtable" align="center">GI - Goods Issue</td>
				</tr>
				<tr>
					<td>
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="normalText" align="left">Batch</td>
								<td align="left" class="normalText"><img src="../images/red_arrow.gif" align="absMiddle">&nbsp;
									<asp:Label id="Lb_batch" runat="server" CssClass="normalText"></asp:Label></td>
							</tr>
							<tr>
								<td class="normalText" align="left">Qty</td>
								<td align="left" class="normalText"><img src="../images/red_arrow.gif" align="absMiddle">&nbsp;
									<asp:Label id="Lb_qty" runat="server" CssClass="normalText"></asp:Label></td>
							</tr>
							<tr>
								<td align="left" class="normalText">Materail</td>
								<td align="left" class="normalText"><img src="../images/red_arrow.gif" align="absMiddle">&nbsp;
									<asp:Label id="Lb_material" runat="server" CssClass="normalText"></asp:Label></td>
							</tr>
							<tr>
								<td align="left" class="normalText" colspan="2" nowrap>&nbsp;<img src="../images/question.gif" align="absMiddle">&nbsp;<asp:Label ID="Lb_info" Runat="server" ForeColor="red" CssClass="normalText">Batch duplicated , Do you want delete data?</asp:Label><font color="red"></font></td>
							</tr>
							<tr>
								<td align="left" colspan="2" class="normalText"><asp:radiobutton id="Rb_yes" runat="server" Text="Yes ,  delete data" GroupName="confirm"></asp:radiobutton></td>
							</tr>
							<tr>
								<td align="left" colspan="2" class="normalText"><asp:radiobutton id="Rb_no" runat="server" Text="No , delete data" GroupName="confirm" tabIndex="1"></asp:radiobutton></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr class="tr" style="PADDING-BOTTOM: 2px; PADDING-LEFT: 2px; PADDING-RIGHT: 2px; PADDING-TOP: 2px">
					<td align="left">
						<P>
							<asp:button id="BT_Save" runat="server" Width="50px" Text="OK" CssClass="buttonTag" tabIndex="2" OnClick="BT_Save_Click"></asp:button></P>
					</td>
				</tr>
			</table>
			</TABLE>
			<table>
				<tr>
					<td><asp:Label ID="Lb_id" Runat="server" Visible="False"></asp:Label><asp:Label ID="Lb_dp_no" Runat="server" Visible="False"></asp:Label><asp:Label ID="Lb_plant" Runat="server" Visible="False"></asp:Label></td>
				</tr>
			</table>
		</form>
	</body>
</html>
