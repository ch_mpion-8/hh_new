﻿using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using hh_siamkraft.Entities.GI;
using hh_siamkraft.GI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{
    public partial class GI_Duplicate : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();
        protected void Page_Init(object sender, EventArgs e)
        {
         
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user_id"] == null)
            {
                Session.RemoveAll();
                Response.Redirect("../index.aspx", false);
                return;
            }

            if (Session["obj_gi"] == null)
            {
                Response.Redirect("GI_ScanDP.aspx", false);
                return;
            }


            if (!(Page.IsPostBack))
            {

                variable_gi obj_gi = new variable_gi();
                obj_gi = (variable_gi)Session["obj_gi"];
                this.Lb_dp_no.Text = obj_gi.str_dpNo;
                this.Lb_id.Text = obj_gi.str_recID;
                this.Lb_plant.Text = obj_gi.str_plant;
                obj_gi = null;

                
                List<HHGiItem> giItem = (List<HHGiItem>)Session["dt_dup"];
                if (!(giItem == null))
                {
                    if ((giItem.Count() > 0))
                    {
                        var dt = giItem.FirstOrDefault();
                        this.Lb_batch.Text = dt.Batch_No;
                        this.Lb_material.Text = dt.Material_No;
                        if ((dt.UOM== "RM"))
                        {
                            this.Lb_qty.Text = dt.Qty.ToString("0.000");
                        }
                        else
                        {
                            this.Lb_qty.Text = (dt.Qty/1000).ToString("0.000");
                        }

                    }
                }
            }


        }

        protected void BT_Save_Click(object sender, EventArgs e)
        {
            if ((this.Rb_yes.Checked == false)  && (this.Rb_no.Checked == false))
            {
                script_func.script_alert("Please Select yes/no", Page);
                return;
            }

            if ((this.Rb_yes.Checked == true))
            {
                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession(connect))
                {
                    var repo = new GIRepository(session);
                    var result = repo.DeleteGiItem(this.Lb_dp_no.Text, this.Lb_batch.Text);


                    if (result.Value == true)
                    {
                        Session["lb_msg"] = "<font color=lightgreen><b>SUCCESS !!</b></font> delete data on success.";
                    }
                    else
                    {
                        Session["lb_msg"] = "<font color=red><b>ERROR !!</b></font> delete data on error.";
                    }
                }
            }

            variable_gi obj_gi = new variable_gi();
            obj_gi.str_recID = this.Lb_id.Text;
            obj_gi.str_dpNo = this.Lb_dp_no.Text;
            obj_gi.str_plant = this.Lb_plant.Text;
            Session["obj_gi"] = obj_gi;
            obj_gi = null;
            Response.Redirect("GI_ScanBatch.aspx", false);

        }
    }
}