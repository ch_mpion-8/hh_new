﻿using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using hh_siamkraft.GI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{
    public partial class GI_SplitReam : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();
        protected void Page_Init(object sender, EventArgs e)
        {
         
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user_id"] == null)
            {
                Session.RemoveAll();
                Response.Redirect("../index.aspx", false);
                return;
            }
            
            try
            {
                if (!Page.IsPostBack)
                {
                    variable_gi obj_gi = new variable_gi();
                    obj_gi = (variable_gi)Session["obj_gi"];
                    this.lblDPNo.Text = obj_gi.str_dpNo;
                    this.Lb_id.Text = obj_gi.str_recID;
                    this.Lb_plant.Text = obj_gi.str_plant;
                    obj_gi = null;
                    this.txtConfirmQty.Attributes.Add("onkeypress", "javascript:return keyCheck(event,this);");
                    script_func.script_focus(txtBatch.ClientID, Page);
                }

            }
            catch (Exception ex)
            {
                Lb_msg.Text = ex.Message;
            }
        }

        private void GetProductDetail()
        {
            try
            {
                if ((txtBatch.Text.Trim().Length == 0))
                {
                    script_func.script_focus(txtBatch.ClientID, Page);
                    return;
                }


                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession(connect))
                {
                    var repo = new GIRepository(session);
                    var dtResult = repo.GetBatchDetailRM(txtBatch.Text.Trim().ToUpper(), lblDPNo.Text, Lb_plant.Text, Convert.ToInt32(Lb_id.Text));
                    if ( dtResult.Value.Item1 != 0)
                    {
                        if ((dtResult.Value.Item2.Count > 0))
                        {
                            lblMaterial.Text = dtResult.Value.Item2[0].Material_No.ToString(); 
                            lblQty.Text = dtResult.Value.Item2[0].Qty.ToString();
                            script_func.script_focus(txtConfirmQty.ClientID, Page);
                        }

                    }
                    else
                    {
                        script_func.script_alert(dtResult.Error.ToString(), Page);
                        this.ClearScreen();
                        script_func.script_focus(txtBatch.ClientID, Page);
                    }
                }
            }
            catch (Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
            }

        }

        private void ClearScreen()
        {
            txtBatch.Text = String.Empty;
            lblMaterial.Text = String.Empty;
            lblQty.Text = String.Empty;
            txtConfirmQty.Text = String.Empty;
        }

        protected void txtBatch_TextChanged(object sender, EventArgs e)
        {
            GetProductDetail();
        }

        private void SaveData()
        {
            try
            {
                if (!this.ValidateSave())
                {
                    return;
                }

                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession(connect))
                {
                    var repo = new GIRepository(session);
                    var result = repo.SaveScanBarcodeRM(lblDPNo.Text, Convert.ToInt32(Lb_id.Text), lblMaterial.Text.Trim(), txtBatch.Text.Trim().ToUpper(), Convert.ToDecimal(txtConfirmQty.Text.Trim()), Lb_plant.Text, Convert.ToString(Session["UserLogin_UserName"]), Convert.ToInt32(Session["WH_No"]));

                if (result.Value)
                    {
                        // save data complete
                        variable_gi obj_gi = new variable_gi();
                        obj_gi.str_recID = this.Lb_id.Text.Trim();
                        obj_gi.str_dpNo = this.lblDPNo.Text.Trim();
                        obj_gi.str_plant = this.Lb_plant.Text.Trim();

                        Session["obj_gi"] = obj_gi;
                        obj_gi = null;
                        Response.Redirect("GI_ScanBatch.aspx", false);
                    }
                    else
                    {
                        // error
                        script_func.script_alert(("Save Data Error: " + result.Error.ToString()), Page);
                        this.ClearScreen();
                        script_func.script_focus(txtBatch.ClientID, Page);
                    }
                }

            }
            catch (Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
            }

        }



        private bool ValidateSave()
        {
            if ((txtBatch.Text.Trim().Length == 0))
            {
                script_func.script_alert("Please Scan Batch", Page);
                script_func.script_focus(txtBatch.ClientID, Page);
                return false;
            }

            if ((lblMaterial.Text.Trim().Length == 0))
            {
                script_func.script_alert("Please Scan Batch", Page);
                script_func.script_focus(txtBatch.ClientID, Page);
                return false;
            }

            if ((txtConfirmQty.Text.Trim().Length == 0))
            {
                script_func.script_alert("Please input Picking Qty", Page);
                script_func.script_focus(txtConfirmQty.ClientID, Page);
                return false;
            }
            else
            {
                if ((Convert.ToDecimal(txtConfirmQty.Text.Trim()) == 0))
                {
                    txtConfirmQty.Text = String.Empty;
                    script_func.script_alert("Can't input Qty is zero", Page);
                    script_func.script_focus(txtConfirmQty.ClientID, Page);
                    return false;
                }

                if ((Convert.ToDecimal(txtConfirmQty.Text.Trim()) > Convert.ToDecimal(lblQty.Text.Trim())))
                {
                    txtConfirmQty.Text = String.Empty;
                    script_func.script_alert("Can't input Picking Qty than Qty in PLS", Page);
                    script_func.script_focus(txtConfirmQty.ClientID, Page);
                    return false;
                }

            }

            return true;
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            variable_gi obj_gi = new variable_gi();
            obj_gi.str_recID = this.Lb_id.Text.Trim();
            obj_gi.str_dpNo = this.lblDPNo.Text.Trim();
            obj_gi.str_plant = this.Lb_plant.Text.Trim();
            Session["obj_gi"] = obj_gi;
            obj_gi = null;
            Response.Redirect("GI_ScanBatch.aspx", false);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            SaveData();
        }
    }
}