﻿<%@ Register TagPrefix="uc1" TagName="header_control" Src="../header_control.ascx"   %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GI_ResultWeight.aspx.cs" Inherits="hh_siamkraft.GI_ResultWeight" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>GI_ResultWeight</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C# .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"/>
		<LINK href="../css/style.css" type="text/css" rel="stylesheet"/>
	    <style type="text/css">
            .style1
            {
                height: 25px;
            }
        </style>
</head>
	<body leftMargin="3" topMargin="3" MS_POSITIONING="GridLayout">
		<form id="myForm" name="myForm" method="post" runat="server">
			<table class="table_main" cellSpacing="0" cellPadding="0" width="225" border="0">
				<tr>
					<td class="headtable"><uc1:header_control id="Header_control1" runat="server"></uc1:header_control></td>
				</tr>
				<tr>
					<td class="headtable" align="center">VIEW WEIGHT</td>
				</tr>
				<tr>
					<td>
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr align="left">
								<td class="normalText" align="left">DP&nbsp;No.</td>
								<td class="normalText" align="left"><IMG src="../images/red_arrow.gif" align="absMiddle">&nbsp;
									<asp:label id="Lb_dp_no" runat="server" CssClass="normalText"></asp:label></td>
							</tr>
							<tr>
								<td class="normalText" align="left">Total&nbsp;Material</td>
								<td class="normalText" align="left"><IMG src="../images/red_arrow.gif" align="absMiddle">&nbsp;
									<asp:label id="Lb_total" runat="server" CssClass="normalText"></asp:label></td>
							</tr>
							<tr>
								<td class="normalText" align="left" colSpan="2"><asp:datagrid id="Dg_Detail" runat="server" Width="100%" AllowPaging="True" ForeColor="White"
										ShowFooter="True" AutoGenerateColumns="False" CellSpacing="1" CellPadding="1" BackColor="White" BorderWidth="1px" BorderStyle="Solid"
										BorderColor="#E7E7FF" OnItemCommand="Dg_Detail_ItemCommand" OnItemDataBound="Dg_Detail_ItemDataBound" OnPageIndexChanged="Dg_Detail_PageIndexChanged">
										<FooterStyle Font-Bold="True" HorizontalAlign="Right" ForeColor="#4A3C8C" VerticalAlign="Middle"
											BackColor="#B5C7DE"></FooterStyle>
										<SelectedItemStyle Font-Bold="True" ForeColor="#F7F7F7" BackColor="#738A9C"></SelectedItemStyle>
										<AlternatingItemStyle BackColor="#F7F7F7"></AlternatingItemStyle>
										<ItemStyle HorizontalAlign="Right" ForeColor="#4A3C8C" VerticalAlign="Middle" BackColor="#E7E7FF"></ItemStyle>
										<HeaderStyle Font-Bold="True" HorizontalAlign="Center" ForeColor="#F7F7F7" VerticalAlign="Middle"
											BackColor="#4A3C8C"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn HeaderText="No." FooterText="Total">
												<HeaderStyle HorizontalAlign="Center" ForeColor="White" VerticalAlign="Middle"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
												<ItemTemplate>
													<asp:Label ID="Lb_no" Runat="server"></asp:Label>
												</ItemTemplate>
												<FooterStyle HorizontalAlign="Right" VerticalAlign="Middle"></FooterStyle>
											</asp:TemplateColumn>
											<asp:BoundColumn DataField="Material_No" HeaderText="Material">
												<HeaderStyle HorizontalAlign="Center" ForeColor="White" VerticalAlign="Middle"></HeaderStyle>
												<ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
											</asp:BoundColumn>
											<asp:TemplateColumn HeaderText="OR">
												<HeaderStyle HorizontalAlign="Center" ForeColor="White" VerticalAlign="Middle"></HeaderStyle>
												<ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
												<ItemTemplate>
													<asp:Label ID="Lb_mat_order" Runat="server"></asp:Label>
												</ItemTemplate>
												<FooterStyle HorizontalAlign="Center" VerticalAlign="Middle"></FooterStyle>
												<FooterTemplate>
													<%= total_order() %>
												</FooterTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="AO">
												<HeaderStyle HorizontalAlign="Center" ForeColor="White" VerticalAlign="Middle"></HeaderStyle>
												<ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
												<ItemTemplate>
													<asp:Label ID="Lb_ao" Runat="server"></asp:Label>
												</ItemTemplate>
												<FooterStyle HorizontalAlign="Center" VerticalAlign="Middle"></FooterStyle>
												<FooterTemplate>
													<%= total_ao() %>
												</FooterTemplate>
											</asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="SAP">
												<HeaderStyle HorizontalAlign="Center" ForeColor="White" VerticalAlign="Middle"></HeaderStyle>
												<ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
												<ItemTemplate>
													<asp:Label ID="Lb_pi" Runat="server"></asp:Label>
												</ItemTemplate>
												<FooterStyle HorizontalAlign="Center" VerticalAlign="Middle"></FooterStyle>
												<FooterTemplate>
													<%= total_pi() %>
												</FooterTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="H/H">
												<HeaderStyle HorizontalAlign="Center" ForeColor="White" VerticalAlign="Middle"></HeaderStyle>
												<ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
												<ItemTemplate>
													<asp:Label ID="Lb_pick_hh" Runat="server"></asp:Label>
												</ItemTemplate>
												<FooterStyle HorizontalAlign="Center" VerticalAlign="Middle"></FooterStyle>
												<FooterTemplate>
													<%= total_pick_hh() %>
												</FooterTemplate>
											</asp:TemplateColumn>
											<asp:ButtonColumn Text="Detail" HeaderText="Action" CommandName="Select">
												<HeaderStyle HorizontalAlign="Center" ForeColor="White" VerticalAlign="Middle"></HeaderStyle>
												<ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
											</asp:ButtonColumn>
										</Columns>
										<PagerStyle Font-Underline="True" Font-Bold="True" HorizontalAlign="Left" ForeColor="#4A3C8C"
											BackColor="#E7E7FF" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr class="tr">
					<td align="left"><asp:button id="Bt_change" runat="server" CssClass="buttonTag" Width="88px" Text="Change DP" OnClick="Bt_change_Click"></asp:button><asp:button id="Bt_Back" runat="server" CssClass="buttonTag" Width="70px" Text="GI Menu" OnClick="Bt_Back_Click"></asp:button></td>
				</tr>
			</table>
			<table>
				<tr>
					<td><asp:label id="Lb_id" Visible="False" Runat="server"></asp:label></td>
				</tr>
			</table>
		</form>
	</body>
</html>
