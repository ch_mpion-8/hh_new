﻿<%@ Register TagPrefix="uc1" TagName="header_control" Src="../header_control.ascx"   %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GI_SplitReam.aspx.cs" Inherits="hh_siamkraft.GI_SplitReam" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>GI_SplitReam</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C# .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"/>
		<LINK href="../css/style.css" type="text/css" rel="stylesheet"/>
	    <style type="text/css">
            .style1
            {
                height: 25px;
            }
        </style>
    <script language="javascript">
			function keyCheck(eventObj, obj){	// key number and dot
				var keyCode

				// Check For Browser Type
				if (document.all){ 
					keyCode=eventObj.keyCode
				}
				else{
					keyCode=eventObj.which
				}
				//alert(keyCode);

				var str=obj.value

				/*if(keyCode==46){ 
					if (str.indexOf(".")>0){
						return false
					}
				}*/

				if((keyCode<48 || keyCode >58) && (keyCode != 13)){ // Allow only integers
					return false
				}

				return true
			}
	
		</script>
</head>
 	<body style="MARGIN: 1px" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<table class="table_main" border="0" cellSpacing="0" cellPadding="0" width="225">
				<tr>
					<td class="headtable"><uc1:header_control id="Header_control1" runat="server"></uc1:header_control></td>
				</tr>
				<tr>
					<td class="headtable" align="center">GI - Split Ream</td>
				</tr>
				<tr height="3">
					<td class="smallerText"><asp:label id="Lb_msg" runat="server"></asp:label></td>
				</tr>
				<tr>
					<td>
						<table cellSpacing="0" cellPadding="0" width="100%">
							<tr>
								<td class="normalText" align="left">DP No.</td>
								<td class="normalText" align="left"><IMG align="absMiddle" src="../images/red_arrow.gif">&nbsp;
									<asp:label id="lblDPNo" runat="server" CssClass="normalText"></asp:label></td>
							</tr>
							<tr>
								<td class="normalText" align="left">Batch No.</td>
								<td align="left"><IMG align="absMiddle" src="../images/red_arrow.gif">&nbsp;
									<asp:textbox id="txtBatch" tabIndex="1" runat="server" CssClass="inputTag" AutoPostBack="True"
										MaxLength="10" Width="116px" OnTextChanged="txtBatch_TextChanged"></asp:textbox></td>
							</tr>
							<tr>
								<td class="normalText" align="left">Material</td>
								<td class="normalText" align="left"><IMG hspace="1" align="absMiddle" src="../images/red_arrow.gif">
									<asp:label id="lblMaterial" tabIndex="2" CssClass="normalText" Runat="server"></asp:label></td>
							</tr>
							<tr>
								<td class="normalText" align="left">Qty</td>
								<td class="normalText" align="left"><IMG hspace="1" align="absMiddle" src="../images/red_arrow.gif">
									<asp:label id="lblQty" tabIndex="3" CssClass="normalText" Runat="server"></asp:label></td>
							</tr>
							<tr>
								<td class="normalText" align="left">Confirm Qty</td>
								<td class="normalText" align="left"><IMG hspace="1" align="absMiddle" src="../images/red_arrow.gif">
									<asp:textbox id="txtConfirmQty" tabIndex="4" CssClass="inputTag" MaxLength="5" Width="80px" Runat="server"></asp:textbox></td>
							</tr>
							<tr style="PADDING-BOTTOM: 2px; PADDING-LEFT: 2px; PADDING-RIGHT: 2px; PADDING-TOP: 2px">
								<td class="normalText" colSpan="2"><IMG hspace="1" align="absMiddle" src="../images/red_arrow.gif">
									<asp:button id="btnTemp" tabIndex="2" CssClass="buttonTag" Width="0px" Runat="server" Text="Save"></asp:button>&nbsp;
									<asp:button id="btnSave" tabIndex="4" CssClass="buttonTag" Width="64px" Runat="server" Text="Save" OnClick="btnSave_Click"></asp:button>&nbsp;
									<asp:button id="btnBack" tabIndex="6" CssClass="buttonTag" Width="56px" Runat="server" Text="Back" OnClick="btnBack_Click"></asp:button></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr style="PADDING-BOTTOM: 2px; PADDING-LEFT: 2px; PADDING-RIGHT: 2px; PADDING-TOP: 2px"
					class="tr">
					<td align="left"></td>
				</tr>
			</table>
			<table>
				<tr>
					<td><asp:label id="Lb_id" Runat="server" Visible="False"></asp:label><asp:label id="Lb_plant" Runat="server" Visible="False"></asp:label></td>
				</tr>
			</table>
		</form>
	</body>
</html>
