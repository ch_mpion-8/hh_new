﻿using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using hh_siamkraft.GI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{
    public partial class GI_ScanDP : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();
        protected void Page_Init(object sender, EventArgs e)
        {
         
        }

        protected void Bt_Mainmenu_Click(object sender, EventArgs e)
        {
            Response.Redirect("../main_menu.aspx");

        }
        private void showMenu()
        {
            var permission = Convert.ToString(Session["Permission"]);
            var list = permission.Split(',').ToList();

            if (list.Where(o => o == "VIEWSTATUS").Count() > 0)
            {
                Bt_viewStatus.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user_id"] == null)
            {
                Session.RemoveAll();
                Response.Redirect("../index.aspx", false);
                return;
            }
          
            if(!(Page.IsPostBack))
            {
                //checkAuthorize(Session("user_id"), "HH")
                //showMenu("selectDP");
                //showMenu("601");
                //showMenu("viewstatus");

                this.Tb_DP_no.Attributes.Add("onkeypress", "javascript:return keyCheck(event,this);");
                script_func.script_focus(this.Tb_DP_no.ID, Page);
                if (!(Session["msg"] == null))
                {
                    this.Lb_msg.Text = Convert.ToString(Session["msg"]);
                    Session.Remove("msg");
                }
                showMenu();
                GetSocket();
            }
        }

        //private void showMenu(string str_function )
        //{
        //    switch (str_function)
        //    {
        //        case "selectDP":
        //            this.Bt_check_dp.Visible = true;
        //            break;
        //        case "601":
        //            this.Bt_gi.Visible = true;
        //            break;
        //        case "viewstatus":
        //            this.Bt_GI_ViewStatus.Visible = true;
        //            break;
        //    }
        //}

        protected void GetSocket()
        {
            var server_id = Convert.ToInt32(Session["Server_ID"]);
            //using (var session = new SkRepositorySession(connect))
            using (var session = new SkRepositorySession())
            {
                var repo = new GIRepository(session);
                var result = repo.GetSocket(server_id);

                foreach (var item in result.Value)
                {
                    Db_socket.Items.Add(new ListItem(Convert.ToString(item.Socket_ID) + " : " + Convert.ToString(item.Socket_Name), Convert.ToString(item.Socket_ID)));
                }
            }
        }

        protected void Bt_Mainmenu_Click1(object sender, EventArgs e)
        {
            Response.Redirect("GI_MainMenu.aspx");
        }

        private void GetDP()
        {
            try
            {
                if (Tb_DP_no.Text == "")
                {
                    script_func.script_alert("Please input 'DP No.'", Page);
                    script_func.script_focus(this.Tb_DP_no.ID, Page);
                    return;
                }

                string strdpNo = this.Tb_DP_no.Text.Trim().PadLeft(10, '0');
                if (check_DP(strdpNo) == true)
                {
                    //dp have in system
                    string DpNoStatus = Convert.ToString(Session["DpNoStatus"]);
                    switch (DpNoStatus)
                    {
                        case "N":
                            string str_script  = "<script language='javascript'>" +
                                                            "alert('DP No.-> " + strdpNo + " not select for scan " + "');" +
                                                            "document.location='GI_SelectDP.aspx';"  +
                                                            "</script>";
                            Response.Write(str_script);
                            return;
                            break;
                        case "C":
                            script_func.script_alert("DP No.-> " + strdpNo + "\r\n send to SAP complete ", Page);
                            script_func.script_focus(Tb_DP_no.ClientID, Page);
                            script_func.script_select(Tb_DP_no.ClientID, Page);
                            return;
                            break;
                        case "W":
                            script_func.script_alert("DP No.-> " + strdpNo + "\r\n waiting send to SAP ", Page);
                            script_func.script_focus(Tb_DP_no.ClientID, Page);
                            script_func.script_select(Tb_DP_no.ClientID, Page);
                            return;
                            break;
                    }

                    if (countDP(strdpNo) == "D")
                    {
                        Response.Redirect("GI_ConfirmDelete.aspx", false);
                        return;
                    }
                    else if (countDP(strdpNo) == "Y")
                    {
                        SaveSocket(strdpNo);
                        Response.Redirect("GI_Detail.aspx", false);
                    }
                    else
                    {
                        script_func.script_alert("DP No.-> " + strdpNo + "\r\n Not Found ", Page);
                        script_func.script_focus(Tb_DP_no.ClientID, Page);
                    }
                }
                else
                {
                    Tb_DP_no.Text = String.Empty;
                    script_func.script_focus(Tb_DP_no.ClientID, Page);
                }
            }
            catch (Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
            }
        }

        private void SaveSocket(string dp_no)
        {
            try
            {
                var socket_id = Convert.ToInt32(Db_socket.SelectedItem.Value);
                var server_id = Convert.ToInt32(Session["Server_ID"]);
                var user_id = Convert.ToString(Session["user_id"]);
                using (var session = new SkRepositorySession())
                {
                    var repo = new GIRepository(session);
                    var result = repo.SaveSocket(dp_no, socket_id, server_id, user_id);
                }
            }
            catch (Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
            }
        }

        private bool check_DP(string dp_no)
        {
            bool ret = false;
            
            var connect = Convert.ToString(Session["Connection_String"]);
            using (var session = new SkRepositorySession(connect))
            {
                var repo = new GIRepository(session);
                var tblDpNo = repo.GetDLHeadNotStatus(dp_no, "VW");
                if (tblDpNo.Value.Count == 0)
                {
                    script_func.script_alert("DP NO. -> " + dp_no + " not found!!", Page);
                    this.Tb_DP_no.Text = String.Empty;
                    script_func.script_focus(this.Tb_DP_no.ClientID, Page);
                    script_func.script_select(this.Tb_DP_no.ClientID, Page);
                    Session["DpNoStatus"] = null;
                    Session["DpRecordID"] = null;
                    ret = false;
                }
                else
                {
                    int x = 0;
                    var dt = tblDpNo.Value.FirstOrDefault();

                    Session["DpNoStatus"] = Convert.ToString(dt.Status).Trim().ToUpper();
                    Session["DpRecordID"] = Convert.ToString(dt.Record_ID).Trim().ToUpper();

                    ret = true;
                }
            }

            return ret;
        }

        private string countDP(string dp_no)
        {
            string ret = "N";

            var connect = Convert.ToString(Session["Connection_String"]);
            using (var session = new SkRepositorySession(connect))
            {
                var repo = new GIRepository(session);
                var tblGI = repo.GetHHGiItem(dp_no, Convert.ToString(Session["UserLogin_WHCode"]));
                if (tblGI.Value.Count > 0)
                {
                    Session.Remove("obj_gi");
                    variable_gi obj_gi = new variable_gi();
                    var dt = tblGI.Value.FirstOrDefault();
                    obj_gi.str_recID = Convert.ToString(dt.Record_ID);
                    obj_gi.str_dpNo = dp_no;
                    obj_gi.str_plant = Convert.ToString(dt.Plant);
                    Session["plant"] = Convert.ToString(dt.Plant);
                    Session["obj_gi"] = obj_gi;
                    obj_gi = null;
                    ret = "D";
                }
                else
                {
                    string strPlant  = "'" + Convert.ToString(Session["UserLogin_WHCode"]) + "','7551','7534'";
                    var dt_detail = repo.GetViewGICheckID(dp_no,Convert.ToInt32(Convert.ToString(Session["DpRecordID"])), strPlant);
                    if (dt_detail.Value.Count() > 0)
                    {
                        Session.Remove("obj_gi");
                        var dt = dt_detail.Value.FirstOrDefault();
                        variable_gi obj_gi = new variable_gi();
                        obj_gi.str_recID = Convert.ToString(dt.Record_ID);
                        obj_gi.str_dpNo = dp_no;
                        obj_gi.str_plant = Convert.ToString(dt.Plant);

                        if (!(string.IsNullOrEmpty(dt.Plant) || dt.Plant == null))
                        {
                            Session["txt"] = Convert.ToString(dt.Text);
                        }
                        Session["plant"] = Convert.ToString(dt.Plant);
                        Session["obj_gi"] = obj_gi;
                        obj_gi = null;
                        ret = "Y";
                    }
                    else
                    {
                        ret = "N";
                    }
                }
            }
         

           return ret;
        }

        protected void Bt_viewStatus_Click(object sender, EventArgs e)
        {
            Response.Redirect("GI_ViewStatus.aspx");
        }

        protected void Tb_DP_no_TextChanged(object sender, EventArgs e)
        {
            GetDP();
        }
    }
}