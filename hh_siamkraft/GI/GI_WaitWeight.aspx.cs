﻿using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using hh_siamkraft.Entities.GR;
using hh_siamkraft.GI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{
    public partial class GI_WaitWeight : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();
        protected void Page_Init(object sender, EventArgs e)
        {
         
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user_id"] == null)
            {
                Session.RemoveAll();
                Response.Redirect("../index.aspx", false);
                return;
            }

            if (Session["obj_gi"] == null)
            {
                Response.Redirect("GI_ViewStatus.aspx", false);
                return;
            }
            
            if (!Page.IsPostBack)
            {
                variable_gi obj_gi = new variable_gi();
                obj_gi = (variable_gi)Session["obj_gi"];

                this.Lb_dp_no.Text = obj_gi.str_dpNo;
                this.Lb_id.Text = obj_gi.str_recID;
                obj_gi = null;
                if (Session["time_out"] == null)
                {
                    var connect = Convert.ToString(Session["Connection_String"]);
                    using (var session = new SkRepositorySession(connect))
                    {
                        var repo = new GRRepository(session);
                        var Time_Out = repo.SelectTimeoutForSAP("GI");


                        if (Time_Out.Value != 0)
                        {
                            Session["time_out"] = Time_Out.Value;
                        }
                        else
                        {
                            Session["time_out"] = 10;
                        }
                    }
                }

                if (Session["refresh"] == null)
                {
                    Session["refresh"] = 1;
                }

                if (Convert.ToInt32(Session["refresh"]) > Convert.ToInt32(Session["time_out"]) )
                {
                    RemoveSession();
                    Session["view_weight"] = 1;
                    Response.Redirect("GI_TimeOut.aspx", false);
                    return;
                }

                if (checkStatus( Convert.ToInt32( this.Lb_id.Text.Trim())) == true)
                {
                    // get Data on success
                    RemoveSession();
                    Response.Redirect("GI_ResultWeight.aspx", false);
                }
                else
                {
                    // not found and refresh page on 5 seconds
                    Response.Write("<meta http-equiv=refresh content=5 url=\'GI_WaitWeight.aspx\'>");
                    Session["refresh"] = (Convert.ToInt32(Session["refresh"]) + 1);
                }

            }

        }

        private void RemoveSession()
        {
            Session.Remove("time_out");
            Session.Remove("refresh");
        }


        private bool checkStatus(int str_id)
        {
            try 
            {
                bool result = false;
                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession(connect))
                {
                    var repo = new GIRepository(session);
                    var Ctl = repo.GetControl(str_id, Convert.ToString(Session["HH_ID"]), "CheckDelivery", "N");

                    if (Ctl.Value.Count() == 0)
                    {
                        result =  false;
                    }
                    else
                    {
                        switch (Ctl.Value[0].Return_Code)
                        {
                            case "00":
                                result =  true;
                                break;
                            case "08":
                            case "13":
                                Session["view_weight"] = 1;
                                Session["error"] = Ctl.Value[0].Return_Code;
                                Response.Redirect("GI_ResultError.aspx", false);
                                break;
                        }
                    }
                }

                return result;
            }
            catch 
            {
                // script_func.script_alert(ex.Message.ToString(), Page)
                return false;
            }

        }


    }
}