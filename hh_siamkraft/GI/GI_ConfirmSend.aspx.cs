﻿using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using hh_siamkraft.GI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{
    public partial class GI_ConfirmSend : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();
        protected void Page_Init(object sender, EventArgs e)
        {
         
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user_id"] == null)
            {
                Session.RemoveAll();
                Response.Redirect("../index.aspx", false);
                return;
            }

            if (Session["obj_gi"] == null)
            {
                Response.Redirect("GI_ScanDP.aspx", false);
                return;
            }


            if (!(Page.IsPostBack))
            {

                variable_gi obj_gi = new variable_gi();
                obj_gi = (variable_gi)Session["obj_gi"];
                this.Lb_dp_no.Text = obj_gi.str_dpNo;
                this.Lb_id.Text = obj_gi.str_recID;
                this.Lb_plant.Text = obj_gi.str_plant;
                obj_gi = null;
            }

        }

        protected void BT_Save_Click(object sender, EventArgs e)
        {
            if ((this.Rb_yes.Checked == false) && (this.Rb_no.Checked == false))
            {
                script_func.script_alert("Please Select \'yes/no\'", Page);
                return;
            }

            if ((this.Rb_yes.Checked == true))
            {
                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession(connect))
                {
                    var repo = new GIRepository(session);
                    var record_id = repo.SaveGIToSap(Convert.ToInt32(Lb_id.Text), Lb_dp_no.Text, Convert.ToString(Session["UserLogin_HHID"]), Convert.ToString(Session["UserLogin_UserName"]), Convert.ToInt32( Session["UserLogin_WHID"]));

                    if ((record_id.Value != 0))
                    {
                        variable_gi obj_gi = new variable_gi();
                        obj_gi.str_recID = record_id.ToString();
                        obj_gi.str_dpNo = this.Lb_dp_no.Text;

                        Session["obj_gi"] = obj_gi;
                        obj_gi = null;
                        Session["msg"] = "<font color=lightgreen><b>SUCCESS !!</b></font> send to SAP on success.";
                        Response.Redirect("GI_ScanDP.aspx", false);
                    }
                    else
                    {
                        script_func.script_alert("Get Record ID on error", Page);
                        return;
                    }
                }

            }
            else
            {
                Response.Redirect("GI_ScanBatch.aspx", false);
            }
        }
    }
}