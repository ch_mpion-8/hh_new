﻿using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using hh_siamkraft.GI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{
    public partial class GI_ConfirmDelete : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();
        protected void Page_Init(object sender, EventArgs e)
        {
         
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user_id"] == null)
            {
                Session.RemoveAll();
                Response.Redirect("../index.aspx", false);
                return;
            }

            if (Session["obj_gi"] == null)
            {
                Response.Redirect("GI_ScanDP.aspx", false);
                return;
            }


            if (!(Page.IsPostBack))
            {

                variable_gi obj_gi = new variable_gi();
                obj_gi = (variable_gi)Session["obj_gi"];
                this.Lb_dp_no.Text = obj_gi.str_dpNo;
                this.Lb_id.Text = obj_gi.str_recID;
                this.Lb_plant.Text = obj_gi.str_plant;
                obj_gi = null;
            }

        }

        protected void Bt_ok_Click(object sender, EventArgs e)
        {
            if (this.Rb_yes.Checked == false && this.Rb_no.Checked == false)
            {
                script_func.script_alert("Please Select yes/no", Page);
                return;
            }

            var connect = Convert.ToString(Session["Connection_String"]);
            using (var session = new SkRepositorySession(connect))
            {
                var repo = new GIRepository(session);
                var dt_detail = repo.GetViewGICheckID(this.Lb_dp_no.Text.Trim());
                int intRow = 0;
                if (dt_detail.Value.Count() == 1 )
                {
                    intRow = 0;
                }
                else
                {
                    intRow = dt_detail.Value.Count() - 1;
                }

                variable_gi obj_gi = new variable_gi();
                obj_gi.str_recID = dt_detail.Value[intRow].Record_ID.ToString();
                obj_gi.str_dpNo = this.Lb_dp_no.Text;
                obj_gi.str_plant = dt_detail.Value[intRow].Plant.ToString();

                if( !(string.IsNullOrEmpty (dt_detail.Value[intRow].Text)))
                {
                    Session["txt"] = dt_detail.Value[intRow].Text;
                }
                Session["plant"] = dt_detail.Value[intRow].Plant.ToString();
                Session["obj_gi"] = obj_gi;
                obj_gi = null;

                if(this.Rb_yes.Checked == true)
                {
                    //yes confirm delete all data
                    repo.DeleteGiItem(this.Lb_dp_no.Text.Trim());
                    Response.Redirect("GI_Detail.aspx", false);
                }
                else
                {
                    //keep data
                    repo.UpdateGiItem(this.Lb_dp_no.Text.Trim(), dt_detail.Value[intRow].Record_ID);
                    Response.Redirect("GI_Detail.aspx", false);
                }
            }
        }
       
    }
}