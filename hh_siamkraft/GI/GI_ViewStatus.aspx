﻿<%@ Register TagPrefix="uc1" TagName="header_control" Src="../header_control.ascx"   %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GI_ViewStatus.aspx.cs" Inherits="hh_siamkraft.GI_ViewStatus" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>GI_ViewStatus</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C# .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"/>
		<LINK href="../css/style.css" type="text/css" rel="stylesheet"/>
	    <style type="text/css">
            .style1
            {
                height: 25px;
            }
        </style>
    <script language="javascript">
			function keyCheck(eventObj, obj){	// key number and dot
				var keyCode

				// Check For Browser Type
				if (document.all){ 
					keyCode=eventObj.keyCode
				}
				else{
					keyCode=eventObj.which
				}
				//alert(keyCode);

				var str=obj.value

				/*if(keyCode==46){ 
					if (str.indexOf(".")>0){
						return false
					}
				}*/

				if((keyCode<48 || keyCode >58) && (keyCode != 13)){ // Allow only integers
					return false
				}

				return true
			}
	
		</script>
</head>
 	<body leftMargin="1" topMargin="1" MS_POSITIONING="GridLayout">
		<form name="myForm" id="myForm" method="post" runat="server">
			<table class="table_main" cellSpacing="0" cellPadding="0" width="225" border="0">
				<tr>
					<td class="headtable"><uc1:header_control id="Header_control1" runat="server"></uc1:header_control></td>
				</tr>
				<tr>
					<td class="headtable" align="center">View Status</td>
				</tr>
				<tr>
					<td>
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="normalText" align="left">DP No.</td>
								<td align="left">&nbsp;<IMG src="../images/red_arrow.gif" align="absMiddle">&nbsp;
									<asp:textbox id="Tb_DP_no" runat="server" CssClass="inputTag" Width="100px" MaxLength="10" AutoPostBack="True" OnTextChanged="Tb_DP_no_TextChanged"></asp:textbox></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr class="tr">
					<td align="center">
                        <%--<asp:button id="Bt_ViewStatus" runat="server" Width="0px" CssClass="buttonTag" Text="View Status"></asp:button>&nbsp;--%>
                        <asp:button id="Bt_Mainmenu" runat="server" Text="GI Menu" CssClass="buttonTag" Width="72px" OnClick="Bt_Mainmenu_Click"></asp:button>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
