﻿using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{
    public partial class GI_SelectDP : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();
        protected void Page_Init(object sender, EventArgs e)
        {
         
        }

        protected void Bt_Mainmenu_Click(object sender, EventArgs e)
        {
            Response.Redirect("../main_menu.aspx");

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user_id"] == null)
            {
                Session.RemoveAll();
                Response.Redirect("../index.aspx", false);
                return;
            }
          
            if(!(Page.IsPostBack))
            {
                bindData();
                showMenu();
            }
        }
        private void showMenu()
        {
            var permission = Convert.ToString(Session["Permission"]);
            var list = permission.Split(',').ToList();

            if (list.Where(o => o == "601").Count() > 0)
            {
                Tb_goodIssue.Visible = true;
            }
        }

        private void bindData()
        {
            try
            {
                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession(connect))
                {
                    var repo = new GIRepository(session);
                    var result = repo.GetDLHeadStatus( "N");
                    this.Dg_select.DataSource = result.Value;
                    this.Dg_select.DataBind();
                }
            }
            catch
            {

            }
        }
       
        protected void Bt_mainMenu_Click1(object sender, EventArgs e)
        {
            Response.Redirect("GI_MainMenu.aspx", false);
        }

        protected void Tb_goodIssue_Click(object sender, EventArgs e)
        {
            Response.Redirect("GI_ScanDP.aspx", false);
        }

        private bool update_DP(DataGrid tmp_dg)
        {

            int chk_cnt = 0;
            int cnt_complete = 0;
            var connect = Convert.ToString(Session["Connection_String"]);
            using (var session = new SkRepositorySession(connect))
            {
                var repo = new GIRepository(session);
                for (int i = 0; i <= tmp_dg.Items.Count - 1; i++)
                {
                    CheckBox chk = (CheckBox)tmp_dg.Items[i].FindControl("chk_id");
                    if (chk.Checked)
                    {
                        chk_cnt++;
                        var result = repo.UpdateStatusDeliveryHead("NS", tmp_dg.Items[i].Cells[1].Text, Convert.ToInt32(tmp_dg.Items[i].Cells[2].Text));
                        if (result.Value) cnt_complete++;
                    }
                }
            }

            if(chk_cnt == cnt_complete)
            {
                this.Lb_msg.Text = "<font color=lightgreen><b>SUCCESS !!</b></font> save data on success.";
                return true;
            }
            else
            {
                return false;
            }
        }
   

        protected void Dg_select_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            this.Dg_select.CurrentPageIndex = e.NewPageIndex;
            bindData();
        }

        protected void Tb_save_Click(object sender, EventArgs e)
        {
            if (this.Dg_select.Items.Count == 0)
            {
                script_func.script_alert("No Record data", Page);
                return;
            }

            int chk = 1;

            for (int i = 0; i <= Dg_select.Items.Count - 1; i++)
            {
                CheckBox chkB = (CheckBox)Dg_select.Items[i].FindControl("chk_id");
                if (chkB.Checked)
                {
                    chk = 1;
                    break;
                }
                else
                {
                    chk = 0;
                }
            }

            if (chk == 0)
            {
                script_func.script_alert("Please Select DP No", Page);
                return;
            }

            update_DP(this.Dg_select);
            bindData();
        }
    }
}