﻿using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using hh_siamkraft.GI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{
    public partial class GI_Detail : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();
        private int rec_no = 1;
        protected void Page_Init(object sender, EventArgs e)
        {
         
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user_id"] == null)
            {
                Session.RemoveAll();
                Response.Redirect("../index.aspx", false);
                return;
            }

            if (!(Page.IsPostBack))
            {
                variable_gi obj_gi = new variable_gi();
                obj_gi = (variable_gi)Session["obj_gi"];
                this.Lb_dp_no.Text = obj_gi.str_dpNo;
                this.Lb_id.Text = obj_gi.str_recID;
                this.Lb_plant.Text = obj_gi.str_plant;
                obj_gi = null;

                bindData();
                script_func.script_focus(this.Bt_next.ClientID, Page);
            }
        }

        private void bindData()
        {
            var connect = Convert.ToString(Session["Connection_String"]);
            using (var session = new SkRepositorySession(connect))
            {
                var repo = new GIRepository(session);
                var result = repo.GetDeliveryOrder(this.Lb_dp_no.Text, Convert.ToInt32( this.Lb_id.Text));
                this.Dg_Detail.DataSource = result.Value;
                this.Dg_Detail.DataBind();


                this.Lb_total.Text = this.Dg_Detail.Items.Count.ToString();
                rec_no = 1;
            }
        }

        protected void Bt_back_Click(object sender, EventArgs e)
        {
            Response.Redirect("GI_ScanDP.aspx");
        }

        protected void Bt_next_Click(object sender, EventArgs e)
        {
            
            var connect = Convert.ToString(Session["Connection_String"]);
            using (var session = new SkRepositorySession(connect))
            {
                var repo = new GIRepository(session);
                var dt_detail = repo.GetDeliveryOrder(this.Lb_dp_no.Text, Convert.ToInt32(this.Lb_id.Text));

                variable_gi obj_gi = new variable_gi();
                obj_gi.str_recID = this.Lb_id.Text.Trim();
                obj_gi.str_dpNo = this.Lb_dp_no.Text.Trim();
                obj_gi.str_plant = this.Lb_plant.Text.Trim();
                obj_gi.str_ChkType = dt_detail.Value[0].Sales_unit;
                obj_gi.str_Total = dt_detail.Value[0].Material_No;
                
                Session["obj_gi"] = obj_gi;
                obj_gi = null;
            }
            Response.Redirect("GI_ScanBatch.aspx", false);
        }

        protected void Dg_Detail_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            Label lb_no = (Label)e.Item.FindControl("Lb_no");
            Label lb_order = (Label)e.Item.FindControl("lb_order");
            Label lb_ao = (Label)e.Item.FindControl("Lb_ao");
            Label lb_hh_qty = (Label)e.Item.FindControl("lb_hh_qty");

            if (lb_no != null) lb_no.Text = Convert.ToString(this.Dg_Detail.Items.Count + 1);
            if (lb_order != null) lb_order.Text = DataBinder.Eval(e.Item.DataItem, "Mat_Order").ToString(); 
            if (lb_ao != null)
            {
                if (DataBinder.Eval(e.Item.DataItem, "AO") == DBNull.Value)
                {
                    lb_ao.Text = "0";
                }
                else
                {
                    lb_ao.Text = DataBinder.Eval(e.Item.DataItem, "AO").ToString();
                }
            }

            if (lb_hh_qty != null)
            {
                if (DataBinder.Eval(e.Item.DataItem, "HH_Qty") == DBNull.Value)
                {
                    lb_hh_qty.Text = "0.000";
                }
                else
                {
                    lb_hh_qty.Text = DataBinder.Eval(e.Item.DataItem, "HH_Qty").ToString();
                    if (Convert.ToString(Session["DB_Description"]).Contains("17"))
                    {
                        lb_hh_qty.Text = (Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "HH_Qty")) / 1000).ToString("0.000");
                    }
                }
            }
        }

        public decimal total_order()
        {
            int i;
            Label lbTotal;
            decimal TotalOrder = 0;
            string[] roll_split;
            for (i = 0; i <= (Dg_Detail.Items.Count - 1); i++)
            {
                lbTotal = (Label) Dg_Detail.Items[i].FindControl("Lb_order");
                TotalOrder = (Decimal.Parse(lbTotal.Text) + TotalOrder);
            }

            //roll_split = TotalOrder.ToString().Split('.');
            //if ((roll_split.Length == 2))
            //{
            //    // have dot
            //    if ((int.Parse(roll_split[1].ToCharArray(0, 1).ToString()) >= 5))
            //    {
            //        roll_split[0] = roll_split[0 + 1];
            //    }

            //    this.Lb_total_roll.Text = roll_split[0].ToString();
            //}
            //else
            //{
                this.Lb_total_roll.Text = Math.Round(TotalOrder).ToString();
            //}

            return TotalOrder;
        }


        public string total_qty()
        {
            int i;
            Label lbTotal;
            double TotalQty = 0;
            for (i = 0; (i
                        <= (Dg_Detail.Items.Count - 1)); i++)
            {
                lbTotal = (Label)Dg_Detail.Items[i].FindControl("Lb_hh_qty");
                TotalQty = (double.Parse(lbTotal.Text) + TotalQty);
            }

            return TotalQty.ToString("0.000");
        }


        public decimal total_ao()
        {
            int i;
            Label lbTotal;
            decimal TotalAO = 0;
            for (i = 0; (i
                        <= (Dg_Detail.Items.Count - 1)); i++)
            {
                lbTotal = (Label)Dg_Detail.Items[i].FindControl("Lb_ao");
                TotalAO = (int.Parse(lbTotal.Text) + TotalAO);
            }

            return TotalAO;
        }

        public string retActual(decimal qty)
        {
            string[] dot;
            dot = qty.ToString().Split('.');
            if ((dot.Length == 2))
            {
                if ((Decimal.Parse(dot[1].ToCharArray(0, 1).ToString()) >= 5))
                {
                    dot[0] = dot[0 + 1];
                }

            }

            return dot[0];
        }

    }
}