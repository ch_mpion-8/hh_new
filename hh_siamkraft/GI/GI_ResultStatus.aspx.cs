﻿using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using hh_siamkraft.Entities.GR;
using hh_siamkraft.GI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{
    public partial class GI_ResultStatus : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();
        protected void Page_Init(object sender, EventArgs e)
        {
         
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user_id"] == null)
            {
                Session.RemoveAll();
                Response.Redirect("../index.aspx", false);
                return;
            }

            if (Session["dt_status"] == null)
            {
                Response.Redirect("GI_ViewStatus.aspx", false);
                return;
            }


            if (!Page.IsPostBack)
            {
                this.tr_view.Visible = false;
                variable_gi obj_gi = new variable_gi();
                obj_gi = (variable_gi)Session["obj_gi"];
                this.Lb_dp_no.Text = obj_gi.str_dpNo;
                obj_gi = null;

                List<TblControl> dt_status = (List<TblControl>)Session["dt_status"];

                var dr = dt_status.FirstOrDefault();
                if (dt_status.Count != 1)
                {
                    dr = dt_status.Last();
                }

                switch (dr.Return_Code) 
                {
                    case "N":
                        this.Tb_msg.Text = "Waiting... send to SAP";
                        break;
                    case "08":
                    case "13":
                        this.Tb_msg.Text = dr.Return_Msg;
                        break;
                    case "00":
                        this.Tb_msg.Text = dr.Return_Msg;
                        this.tr_view.Visible = true;
                        break;
                }
            }

        }

        protected void Bt_Back_Click(object sender, EventArgs e)
        {
            Response.Redirect("GI_ViewStatus.aspx");
        }

        protected void Bt_view_Click(object sender, EventArgs e)
        {
            int record_id;
            record_id = getID();
            if ((record_id != 0))
            {
                variable_gi obj_gi = new variable_gi();
                obj_gi.str_recID = record_id.ToString();
                obj_gi.str_dpNo = this.Lb_dp_no.Text;
                Session["obj_gi"] = obj_gi;
                obj_gi = null;
                Response.Redirect("GI_WaitWeight.aspx", false);
            }
            else
            {
                script_func.script_alert("Get Record ID on error", Page);
            }
        }

        private int getID()
        {
            try
            {
                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession(connect))
                {
                    var repo = new GIRepository(session);
                    var result = repo.GetGIViewWeight(Convert.ToString(Session["HH_ID"]), Convert.ToString(Session["user_id"]), this.Lb_dp_no.Text.Trim());
                    return result.Value;
                }
            }
            catch 
            {
                return 0;
            }

        }

    }
}