﻿using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using hh_siamkraft.Entities.GR;
using hh_siamkraft.GI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{
    public partial class GI_ResultWeight : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();
        protected void Page_Init(object sender, EventArgs e)
        {
         
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user_id"] == null)
            {
                Session.RemoveAll();
                Response.Redirect("../index.aspx", false);
                return;
            }


            if (!Page.IsPostBack)
            {
                variable_gi obj_gi = new variable_gi();
                obj_gi = (variable_gi)Session["obj_gi"];
                this.Lb_dp_no.Text = obj_gi.str_dpNo;
                this.Lb_id.Text = obj_gi.str_recID;
                obj_gi = null;
                bindData();

            }

        }


        private void bindData()
        {
            var connect = Convert.ToString(Session["Connection_String"]);
            using (var session = new SkRepositorySession(connect))
            {
                var repo = new GIRepository(session);
                var tbl = repo.GetVwMatDetail(this.Lb_dp_no.Text,  Convert.ToInt32(this.Lb_id.Text));
                if (tbl.Value.Count() != 0)
                {
                    this.Dg_Detail.DataSource = tbl.Value;
                    this.Dg_Detail.DataBind();
                    this.Lb_total.Text = tbl.Value.Count.ToString();
                }
            }
        }

        protected void Dg_Detail_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            Label lb_no = (Label)e.Item.FindControl("Lb_no");
            Label lb_mat_order = (Label)e.Item.FindControl("Lb_mat_order");
            Label lb_ao = (Label)e.Item.FindControl("Lb_ao");
            Label lb_pick_hh = (Label)e.Item.FindControl("Lb_pick_hh");
            Label lb_pi = (Label)e.Item.FindControl("Lb_pi");
            if (!(lb_no == null))
            {
                lb_no.Text = ((Dg_Detail.CurrentPageIndex * 10)  + (this.Dg_Detail.Items.Count + 1)).ToString();
            }

            if (!(lb_mat_order == null))
            {
                lb_mat_order.Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "Mat_Order")); 
            }

            if (!(lb_ao == null))
            {
                lb_ao.Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "AO")); 
            }

            if (!(lb_pick_hh == null))
            {
                lb_pick_hh.Text = (Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "Pick_HH"))).ToString("0.000");
            }

            if (!(lb_pi == null))
            {
                lb_pi.Text = (Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "PI"))).ToString("0.000");
            }
        }


        public string total_order()
        {
            int i;
            Label lbTotal;
            int TotalOrder = 0;
            for (i = 0; i  <= (Dg_Detail.Items.Count - 1); i++)
            {
                lbTotal = (Label)Dg_Detail.Items[i].FindControl("Lb_mat_order");
                TotalOrder = (int.Parse(lbTotal.Text) + TotalOrder);
            }

            return TotalOrder.ToString();
        }

        public string total_ao()
        {
            int i;
            Label lbTotal;
            int TotalAO = 0;
            for (i = 0; i <= (Dg_Detail.Items.Count - 1); i++)
            {
                lbTotal = (Label)Dg_Detail.Items[i].FindControl("Lb_ao");
                TotalAO = (int.Parse(lbTotal.Text) + TotalAO);
            }

            return TotalAO.ToString();
        }

        public string total_pick_hh()
        {
            int i;
            Label lbTotal;
            decimal TotalPick = 0;
            for (i = 0; i <= (Dg_Detail.Items.Count - 1); i++)
            {
                lbTotal = (Label)Dg_Detail.Items[i].FindControl("Lb_pick_hh");
                TotalPick = (Decimal.Parse(lbTotal.Text) + TotalPick);
            }

            return TotalPick.ToString("0.000");
        }

        public string total_pi()
        {
            int i;
            Label lbTotal;
            decimal TotalPI = 0;
            for (i = 0; (i
                        <= (Dg_Detail.Items.Count - 1)); i++)
            {
                lbTotal = (Label)Dg_Detail.Items[i].FindControl("Lb_pi");
                TotalPI = (Decimal.Parse(lbTotal.Text) + TotalPI);
            }

            return TotalPI.ToString("0.000");
        }

        protected void Dg_Detail_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName.Equals("Select"))
            {
                Session["material_no"] = e.Item.Cells[1].Text;
            }
            Response.Redirect("GI_BatchDetail.aspx", false);
        }

        protected void Bt_change_Click(object sender, EventArgs e)
        {
            Response.Redirect("GI_ViewStatus.aspx", false);
        }

        protected void Bt_Back_Click(object sender, EventArgs e)
        {
            Response.Redirect("GI_MainMenu.aspx", false);
        }

        protected void Dg_Detail_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            Dg_Detail.CurrentPageIndex = e.NewPageIndex;
            bindData();
        }
    }
}