﻿using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using hh_siamkraft.GI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{
    public partial class GI_ViewStatus : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();
        protected void Page_Init(object sender, EventArgs e)
        {
         
        }

        protected void Bt_Mainmenu_Click(object sender, EventArgs e)
        {
            Response.Redirect("GI_MainMenu.aspx");

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user_id"] == null)
            {
                Session.RemoveAll();
                Response.Redirect("../index.aspx", false);
                return;
            }
          
            if(!(Page.IsPostBack))
            {
                script_func.script_focus("Tb_DP_no", Page);
                this.Tb_DP_no.Attributes.Add("onkeypress", "javascript:return keyCheck(event,this);");
            }

        }

        private void ViewStatus()
        {
            try
            {
                if (this.Tb_DP_no.Text == "")
                {
                    script_func.script_alert("Please Insert 'DP No.'", Page);
                    script_func.script_focus(this.Tb_DP_no.ClientID, Page);
                    return;
                }

                if ( check_Status() == true )
                {
                    Response.Redirect("GI_ResultStatus.aspx", false);
                }
                else
                {
                    script_func.script_select(this.Tb_DP_no.ClientID, Page);
                }
            }
            catch
            {

            }
        }

        private bool check_Status()
        {
            try
            {
                bool ret = false;
                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession(connect))
                {
                    var repo = new GIRepository(session);
                    var result = repo.GetPickingControl(this.Tb_DP_no.Text.Trim().PadLeft(10, '0'), "Picking");
                    if (result.Value.Count == 0)
                    {
                        //not status
                        script_func.script_alert("DP-> " + this.Tb_DP_no.Text.Trim().PadLeft(10, '0') + " not found", Page);
                        ret =  false;
                    }
                    else
                    {
                        //have status
                        variable_gi obj_gi = new variable_gi();
                        obj_gi.str_dpNo = Tb_DP_no.Text.Trim().PadLeft(10, '0');
                       
                        Session["obj_gi"] = obj_gi;
                        Session["dt_status"] = result.Value;
                        obj_gi = null;
                        ret = true;
                    }
                }

                return ret;
            }
            catch
            {
                script_func.script_alert("Get Status on error", Page);
                return false;
            }
        }

        protected void Tb_DP_no_TextChanged(object sender, EventArgs e)
        {
            ViewStatus();
        }
    }
}