﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{
    public partial class main_menu : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
         
        }

        private void showMenu()
        {
            var permission = Convert.ToString(Session["Permission"]);
            var list = permission.Split(',').ToList();
            if (list.Where(o => o == "VIEWSTOCK" || o == "525").Count() > 0)
            {
                tr_gr.Visible = true;
            }

            if (list.Where(o => o == "311" || o == "312" || o == "321" || o == "322" || o == "343" || o == "344").Count() > 0)
            {
                tr_gt.Visible = true;
            }

            if (list.Where(o => o == "601" || o == "VIEWSTATUS" || o == "SELECTDP").Count() > 0)
            {
                tr_gi.Visible = true;
            }

            if (list.Where(o => o == "SCANREWINDBATCHINSAP" || o == "SCANREWINDBATCHNOTINSAP").Count() > 0)
            {
                tr_rewind.Visible = true;
            }

            if(Convert.ToString(Session["MultiPlant"]) == "Y")
            {
                tr_Auth.Visible = true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                showMenu();
            }
        }

        protected void Bt_gr_Click(object sender, EventArgs e)
        {
            Response.Redirect("GR/GR_MainMenu.aspx");

        }

        protected void Bt_gt_Click(object sender, EventArgs e)
        {
            Response.Redirect("GT/GT_MainMenu.aspx");

        }

        protected void Bt_gi_Click(object sender, EventArgs e)
        {

            Response.Redirect("GI/GI_MainMenu.aspx");

        }

        protected void Bt_logout_Click(object sender, EventArgs e)
        {
            Session.RemoveAll();
            string msg = "SUCCESS !! log out on success.";
            Response.Redirect("index.aspx?msg=" + msg);
        }

        protected void btnRewind_Click(object sender, EventArgs e)
        {
            Response.Redirect("DPRewind/MainMenu.aspx");
        }

        protected void btnAuth_Click(object sender, EventArgs e)
        {
            Response.Redirect("ChangePlant.aspx");
        }
    }
}