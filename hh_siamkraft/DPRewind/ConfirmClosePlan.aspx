﻿<%@ Register TagPrefix="uc1" TagName="header_control" Src="../header_control.ascx"   %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConfirmClosePlan.aspx.cs" Inherits="hh_siamkraft.ConfirmClosePlan" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>ConfirmClosePlan</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C# .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"/>
		<LINK href="../css/style.css" type="text/css" rel="stylesheet"/>
	    <style type="text/css">
            .style1
            {
                height: 25px;
            }
        </style>
</head>
<body leftMargin="1" topMargin="1" rightmargin="1" bottommargin="1" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<table class="table_main" cellSpacing="0" cellPadding="0" width="225" border="0">
				<tr>
					<td class="headtable"><uc1:header_control id="Header_control1" runat="server"></uc1:header_control></td>
				</tr>
				<tr>
					<td class="headtable" align="center">DP Rewind</td>
				</tr>
				<tr>
					<td>
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="normalText" align="left">DP</td>
								<td align="left" class="normalText"><img src="../images/red_arrow.gif" align="absMiddle">&nbsp;
									<asp:Label id="lblDocNo" runat="server" CssClass="normalText"></asp:Label></td>
							</tr>
							<tr>
								<td align="left" class="normalText" colspan="2" nowrap>&nbsp;<img src="../images/question.gif" align="absMiddle">&nbsp;<asp:Label ID="Lb_info" Runat="server" ForeColor="red" CssClass="normalText">Goods Issue not enough ?</asp:Label><font color="red"></font></td>
							</tr>
							<tr>
								<td align="left" colspan="2" class="normalText">
									<asp:radiobutton id="rdoConfirm" GroupName="confirm" Text="Yes , send to SAP" Runat="server"></asp:radiobutton></td>
							</tr>
							<tr>
								<td align="left" colspan="2" class="normalText">
									<asp:radiobutton id="rdoCancel" GroupName="confirm" Text="No ,  send to SAP" Runat="server" tabIndex="1"></asp:radiobutton></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr class="tr" style="PADDING-BOTTOM: 2px; PADDING-LEFT: 2px; PADDING-RIGHT: 2px; PADDING-TOP: 2px">
					<td align="center">
						<P>
							<asp:button id="btnSelect" runat="server" Width="50px" Text="OK" CssClass="buttonTag" tabIndex="2" OnClick="btnSelect_Click"></asp:button>

						</P>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
