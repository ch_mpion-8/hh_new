﻿using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using hh_siamkraft.Entities.GR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{
    public partial class ScanManual : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();

        private int IsSAP
        {
            get
            {
                return Convert.ToInt32(ViewState["IsSAP"]);
            }
            set
            {
                ViewState["IsSAP"] = value;
            }
        }
        protected void Page_Init(object sender, EventArgs e)
        {
           
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(Page.IsPostBack))
            {
                try
                {
                    if (Request.QueryString["docno"] == null)
                    {
                        Response.Redirect("SelectDP.aspx", true);
                    }
                    btnConfirm.Attributes.Add("onclick", "javascript:if(confirm('Do you want confirm rewind ?')== false){return false;}");
                    txtQty.Attributes.Add("onkeypress", "return keyCheck(event,this)");

                    lblDocNo.Text = Request.QueryString["docno"];
                    IsSAP = Convert.ToInt32(Request.QueryString["IsSAP"]);

                    SelectTotalScanQty();
                    SelectScanDetail();

                    script_func.script_focus(txtBatch.ClientID, Page);
                }
                catch (Exception ex)
                {
                    script_func.script_alert(ex.Message, Page);
                }
            }
        }
        protected void SelectTotalScanQty()
        {
            try
            {
                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession(connect))
                {
                    var repo = new DPRewindRepository(session);
                    var result = repo.SelectTotalScanQty(lblDocNo.Text);
                    if (result.Value.Count() > 0)
                    {
                        var dt = result.Value.FirstOrDefault();
                        lblScanQty.Text = dt.TotalScanQty.ToString();
                        lblOrderQty.Text = dt.TotalOrderQty.ToString();
                    }

                }
            }
            catch (Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
            }
        }

        protected void SelectScanDetail()
        {
            try
            {
                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession(connect))
                {
                    var repo = new DPRewindRepository(session);
                    var result = repo.SelectScanDetail(lblDocNo.Text, null);
                    if (result.Value.Count() > 0)
                    {
                        dgData.DataSource = result.Value;
                        dgData.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
            }
        }

        protected void dgData_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblNo = e.Item.FindControl("lblNo") as Label;
                if (lblNo != null)
                {
                    lblNo.Text = Convert.ToString(dgData.Items.Count + 1);
                }
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            string url = String.Format("SelectDP.aspx?docno={0}&IsSAP={1}", lblDocNo.Text, IsSAP.ToString());
            Response.Redirect(url);
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            ConfirmData();
        }

        protected void ConfirmData()
        {
            try
            {
                if (dgData.Items.Count == 0)
                {
                    script_func.script_alert("Please scan barcode at least 1 item.", Page);
                    script_func.script_focus(txtBatch.ClientID, Page);
                    return;
                }

                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession(connect))
                {
                    var repo = new DPRewindRepository(session);
                    var result = repo.SelectTotalScanQty(lblDocNo.Text);
                    if (result.Value.Count() > 0)
                    {
                        var dt = result.Value.FirstOrDefault();
                        if (Convert.ToInt32(dt.TotalOrderQty) - Convert.ToInt32(dt.TotalScanQty) == 0)
                        {
                            var result2 = repo.ConfirmScanRewindManual(lblDocNo.Text, Convert.ToString(Session["UserLogin_UserName"]));
                            if(result2.Value)
                            {
                                string msgAlert = "DP : {0} close plan completed.";
                                msgAlert = String.Format(msgAlert, lblDocNo.Text);
                                Page.RegisterStartupScript("location", "<script language='javascript'>alert('" + msgAlert + "');window.location.href='MainMenu.aspx';</script>");
                                return;
                            }
                            else
                            {
                                script_func.script_alert("Can not confirm this DP.", Page);
                                return;
                            }
                        }
                        else
                        {
                            string url = String.Format("ConfirmClosePlan.aspx?docno={0}&IsSAP={1}", lblDocNo.Text, IsSAP.ToString());
                            Response.Redirect(url, true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
            }
        }

        protected void txtBatch_TextChanged(object sender, EventArgs e)
        {
            SaveScan();
        }

        protected void txtQty_TextChanged(object sender, EventArgs e)
        {
            SaveScan();
        }

        protected void txtMaterial_TextChanged(object sender, EventArgs e)
        {
            SaveScan();
        }

        protected void ClearScanDetail()
        {
            txtBatch.Text = String.Empty;
            txtQty.Text = String.Empty;
            txtMaterial.Text = String.Empty;
        }

        protected void SaveScan()
        {
            try
            {
                if (ValidateSave())
                {
                    txtBatch.Text = txtBatch.Text.Trim().ToUpper();
                    txtMaterial.Text = txtMaterial.Text.Trim().ToUpper();

                    var connect = Convert.ToString(Session["Connection_String"]);
                    using (var session = new SkRepositorySession(connect))
                    {
                        var repo = new DPRewindRepository(session);
                        var result = repo.SelectScanDetail(lblDocNo.Text, txtBatch.Text);
                        if (result.Value.Count() > 0)
                        {
                            string url = String.Format("DeleteBatch.aspx?docno={0}&IsSAP={1}", lblDocNo.Text, IsSAP.ToString());
                            Session["BatchDetail"] = result.Value;
                            Response.Redirect(url);
                            return;
                        }

                        var result2 = repo.SaveScanBarcode(Convert.ToString(Session["UserLogin_WHCode"]), lblDocNo.Text, txtBatch.Text, Convert.ToDecimal(txtQty.Text), txtMaterial.Text, false, Convert.ToString(Session["UserLogin_UserName"]));
                        if (result2.Value)
                        {
                            SelectTotalScanQty();
                            SelectScanDetail();
                        }
                        else
                        {
                            script_func.script_alert(result2.Error.InnerException.ToString(), Page);
                        }
                        ClearScanDetail();
                        script_func.script_focus(txtBatch.ClientID, Page);
                    }
                }
            }
            catch (Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
            }
        }

        protected bool ValidateSave()
        {
            if(txtBatch.Text.Trim().Length == 0)
            {
                script_func.script_focus(txtBatch.ClientID, Page);
                return false;
            }

            if(txtBatch.Text.Trim().Length != 10) 
            {
                script_func.script_alert("Scan batch invalid.", Page);
                txtBatch.Text = String.Empty;
                script_func.script_focus(txtBatch.ClientID, Page);
                return false;
            }

            if (txtQty.Text.Trim().Length == 0)
            {
                script_func.script_focus(txtQty.ClientID, Page);
                return false;
            }


            if (Convert.ToDecimal(txtQty.Text) == 0)
            {
                script_func.script_alert("Scan qty invalid.", Page);
                txtQty.Text = String.Empty;
                script_func.script_focus(txtQty.ClientID, Page);
                return false;
            }

            if (txtMaterial.Text.Trim().Length == 0)
            {
                script_func.script_focus(txtMaterial.ClientID, Page);
                return false;
            }

            if (txtMaterial.Text.Trim().Length != 18)
            {
                script_func.script_alert("Scan material invalid.", Page);
                txtMaterial.Text = String.Empty;
                script_func.script_focus(txtMaterial.ClientID, Page);
                return false;
            }

            return true;
        }
    }
}