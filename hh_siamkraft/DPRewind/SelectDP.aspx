﻿<%@ Register TagPrefix="uc1" TagName="header_control" Src="../header_control.ascx"   %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SelectDP.aspx.cs" Inherits="hh_siamkraft.SelectDP" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>SelectDP</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C# .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"/>
		<LINK href="../css/style.css" type="text/css" rel="stylesheet"/>
	    <style type="text/css">
            .style1
            {
                height: 25px;
            }
        </style>
</head>
<body leftMargin="1" topMargin="1" rightmargin="1" bottommargin="1" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<asp:panel id="pnlHeader" Runat="server">
				<TABLE class="table_main" border="0" cellSpacing="2" cellPadding="2" width="225">
					<TR>
						<TD class="headtable">
							<uc1:header_control id="Header_control1" runat="server"></uc1:header_control></TD>
					</TR>
					<TR>
						<TD class="headtable" align="center">DP Rewind</TD>
					</TR>
					<TR>
						<TD>
							<TABLE border="0" cellSpacing="2" cellPadding="2" width="100%">
								<TR>
									<TD class="smallerText" colSpan="2" align="left">
										<asp:Label id="Lb_msg" Runat="server"></asp:Label></TD>
								</TR>
								<TR>
									<TD class="normalText" align="left">DP No.</TD>
									<TD align="left">&nbsp;<IMG align="absMiddle" src="../images/red_arrow.gif">&nbsp;
										<asp:textbox id="txtDocNo" runat="server" AutoPostBack="True" Width="100px" CssClass="inputTag" OnTextChanged="txtDocNo_TextChanged"></asp:textbox></TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
					<TR style="PADDING-BOTTOM: 2px; PADDING-LEFT: 2px; PADDING-RIGHT: 2px; PADDING-TOP: 2px"
						class="tr">
						<TD align="center">
							<asp:button id="btnMainMenu" runat="server" Width="88px" CssClass="buttonTag" usesubmitbehavior="false"
								Text="Rewind Menu" OnClick="btnMainMenu_Click"></asp:button>

						</TD>
					</TR>
				</TABLE>
			</asp:panel>
            <asp:panel id="pnlDetail" Runat="server">
				<TABLE class="table_main" border="0" cellSpacing="2" cellPadding="2" width="225">
					<TR>
						<TD class="headtable">
							<uc1:header_control id="Header_control2" runat="server"></uc1:header_control></TD>
					</TR>
					<TR>
						<TD class="headtable" align="center">DP Rewind</TD>
					</TR>
					<TR>
						<TD>
							<TABLE border="0" cellSpacing="2" cellPadding="2" width="100%">
								<TR>
									<TD class="smallerText" colSpan="2" align="left">
										<asp:Label id="Label1" Runat="server"></asp:Label></TD>
								</TR>
								<TR>
									<TD class="normalText" align="left">DP No.</TD>
									<TD align="left">&nbsp;<IMG align="absMiddle" src="../images/red_arrow.gif">&nbsp;
										<asp:Label style="Z-INDEX: 0" id="lblDocNo" runat="server"></asp:Label></TD>
								</TR>
								<TR>
									<TD class="normalText" align="left">Total Material/Total Roll</TD>
									<TD align="left">&nbsp;<IMG align="absMiddle" src="../images/red_arrow.gif">
										<asp:label id="lblTotalMaterial" runat="server" CssClass="normalText"></asp:label>/
										<asp:Label id="lblTotalOrderQty" Runat="server" CssClass="normalText"></asp:Label></TD>
								</TR>
								<TR>
									<TD colSpan="2" align="center"><IMG hspace="1" align="absMiddle" src="../images/red_arrow.gif">&nbsp;
										<asp:button id="btnScanBarcode" runat="server" Width="72px" CssClass="buttonTag" usesubmitbehavior="false"
											Text="Scan Data" OnClick="btnScanBarcode_Click"></asp:button>&nbsp;
										<asp:button id="btnBack" runat="server" Width="50px" CssClass="buttonTag" usesubmitbehavior="false"
											Text="Back" OnClick="btnBack_Click"></asp:button></TD>
								</TR>
								<TR>
									<TD colSpan="2">
										<asp:datagrid id="dgData" runat="server" Width="100%" BorderColor="#E7E7FF" BorderStyle="Solid"
											BorderWidth="1px" BackColor="White" CellPadding="1" CellSpacing="1" AutoGenerateColumns="False"
											ShowFooter="True" OnItemDataBound="dgData_ItemDataBound">
											<FooterStyle HorizontalAlign="Right" ForeColor="#4A3C8C" VerticalAlign="Middle" BackColor="#B5C7DE"></FooterStyle>
											<SelectedItemStyle Font-Bold="True" ForeColor="#F7F7F7" BackColor="#738A9C"></SelectedItemStyle>
											<AlternatingItemStyle BackColor="#F7F7F7"></AlternatingItemStyle>
											<ItemStyle HorizontalAlign="Right" ForeColor="#4A3C8C" VerticalAlign="Middle" BackColor="#E7E7FF"></ItemStyle>
											<HeaderStyle Font-Bold="True" ForeColor="#F7F7F7" BackColor="#4A3C8C"></HeaderStyle>
											<Columns>
												<asp:TemplateColumn HeaderText="No" FooterText="Total :">
													<HeaderStyle HorizontalAlign="Center" ForeColor="White" VerticalAlign="Middle"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
													<ItemTemplate>
														<asp:Label ID="lblNo" Runat="server"></asp:Label>
													</ItemTemplate>
													<FooterStyle Font-Bold="True" HorizontalAlign="Right" VerticalAlign="Middle"></FooterStyle>
												</asp:TemplateColumn>
												<asp:BoundColumn DataField="Material" HeaderText="Material">
													<HeaderStyle HorizontalAlign="Center" ForeColor="White" VerticalAlign="Middle"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="OrderQty" HeaderText="Order Qty">
													<HeaderStyle Font-Bold="True" HorizontalAlign="Center" ForeColor="#FFFFFF"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="RewindQty" HeaderText="Scan Qty">
													<HeaderStyle Font-Bold="True" HorizontalAlign="Center" ForeColor="#FFFFFF"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="TotalWeight" HeaderText="Total Weight" DataFormatString="{0:#,##0.000}">
													<HeaderStyle Font-Bold="True" HorizontalAlign="Center" ForeColor="#FFFFFF"></HeaderStyle>
												</asp:BoundColumn>
											</Columns>
											<PagerStyle HorizontalAlign="Right" ForeColor="#4A3C8C" BackColor="#E7E7FF" Mode="NumericPages"></PagerStyle>
										</asp:datagrid></TD>
								</TR>
							</TABLE>
						</TD>
					</TR>
					<TR style="PADDING-BOTTOM: 2px; PADDING-LEFT: 2px; PADDING-RIGHT: 2px; PADDING-TOP: 2px"
						class="tr">
						<TD align="left">&nbsp;
						</TD>
					</TR>
				</TABLE>
			</asp:panel></form>
	</body>
</html>
