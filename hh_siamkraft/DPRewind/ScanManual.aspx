﻿<%@ Register TagPrefix="uc1" TagName="header_control" Src="../header_control.ascx"   %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ScanManual.aspx.cs" Inherits="hh_siamkraft.ScanManual" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>ScanManual</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C# .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"/>
		<LINK href="../css/style.css" type="text/css" rel="stylesheet"/>
	    <style type="text/css">
            .style1
            {
                height: 25px;
            }
        </style>
</head>
<body leftMargin="1" topMargin="1" rightmargin="1" bottommargin="1" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<table class="table_main" border="0" cellSpacing="0" cellPadding="0" width="225">
				<tr>
					<td class="headtable"><uc1:header_control id="Header_control1" runat="server"></uc1:header_control></td>
				</tr>
				<tr>
					<td class="headtable" align="center">DP Rewind</td>
				</tr>
				<tr height="3">
					<td class="smallerText"><asp:label id="Lb_msg" runat="server"></asp:label></td>
				</tr>
				<tr>
					<td>
						<table cellSpacing="0" cellPadding="0" width="100%">
							<tr>
								<td class="normalText" align="left">DP&nbsp;No.</td>
								<td class="normalText" align="left"><IMG align="absMiddle" src="../images/red_arrow.gif">&nbsp;
									<asp:label id="lblDocNo" runat="server" CssClass="normalText"></asp:label></td>
							</tr>
							<tr>
								<td class="normalText" align="left">Batch&nbsp;No.</td>
								<td align="left"><IMG align="absMiddle" src="../images/red_arrow.gif">&nbsp;
									<asp:textbox id="txtBatch" runat="server" CssClass="inputTag" Width="100px" MaxLength="10" AutoPostBack="True" OnTextChanged="txtBatch_TextChanged"></asp:textbox></td>
							</tr>
							<tr>
								<td class="normalText" align="left">Qty</td>
								<td align="left"><IMG align="absMiddle" src="../images/red_arrow.gif">&nbsp;
									<asp:textbox id="txtQty" runat="server" CssClass="inputTag" Width="80px" MaxLength="10" AutoPostBack="True" OnTextChanged="txtQty_TextChanged"></asp:textbox></td>
							</tr>
							<tr>
								<td class="normalText" align="left">Material</td>
								<td align="left"><IMG align="absMiddle" src="../images/red_arrow.gif">&nbsp;
									<asp:textbox id="txtMaterial" runat="server" CssClass="inputTag" Width="118px" MaxLength="18"
										AutoPostBack="True" OnTextChanged="txtMaterial_TextChanged"></asp:textbox></td>
							</tr>
							<tr>
								<td class="normalText" align="left">Scan&nbsp;Total</td>
								<td class="normalText" align="left"><IMG hspace="1" align="absMiddle" src="../images/red_arrow.gif">&nbsp;
									<asp:label id="lblScanQty" runat="server" CssClass="normalText"></asp:label>&nbsp;/&nbsp;
									<asp:label id="lblOrderQty" runat="server" CssClass="normalText"></asp:label></td>
							</tr>
							<tr style="PADDING-BOTTOM: 2px; PADDING-LEFT: 2px; PADDING-RIGHT: 2px; PADDING-TOP: 2px"
								id="tr_send" runat="server">
								<td class="normalText" colSpan="2" align="center">
                                    <asp:button id="btnConfirm" runat="server" CssClass="buttonTag" Width="80px" Text="Confirm GI" OnClick="btnConfirm_Click"></asp:button>&nbsp;
                                    <asp:button id="btnBack" runat="server" CssClass="buttonTag" Width="56px" Text="Back" OnClick="btnBack_Click"></asp:button>

								</td>
							</tr>
							<tr>
								<td colSpan="2" align="left"><asp:datagrid id="dgData" runat="server" Width="100%" CellSpacing="1" AutoGenerateColumns="False"
										CellPadding="1" BorderWidth="1px" BorderStyle="Solid" BorderColor="#E7E7FF" OnItemDataBound="dgData_ItemDataBound">
										<FooterStyle ForeColor="#4A3C8C" BackColor="#B5C7DE"></FooterStyle>
										<SelectedItemStyle Font-Bold="True" ForeColor="#F7F7F7" BackColor="#738A9C"></SelectedItemStyle>
										<AlternatingItemStyle BackColor="#F7F7F7"></AlternatingItemStyle>
										<ItemStyle HorizontalAlign="Center" ForeColor="#4A3C8C" VerticalAlign="Middle" BackColor="#E7E7FF"></ItemStyle>
										<HeaderStyle Font-Bold="True" ForeColor="#F7F7F7" BackColor="#4A3C8C"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn HeaderText="No.">
												<HeaderStyle HorizontalAlign="Center" ForeColor="White" VerticalAlign="Middle"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
												<ItemTemplate>
													<asp:Label ID="lblNo" Runat="server"></asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn DataField="Batch" HeaderText="Batch No.">
												<HeaderStyle HorizontalAlign="Center" ForeColor="White" VerticalAlign="Middle"></HeaderStyle>
												<ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="Qty" HeaderText="Qty" DataFormatString="{0:0.000}">
												<HeaderStyle Font-Bold="True" HorizontalAlign="Center" ForeColor="White"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="Material" HeaderText="Material">
												<HeaderStyle Wrap="False" HorizontalAlign="Center" ForeColor="White" VerticalAlign="Middle"></HeaderStyle>
												<ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
											</asp:BoundColumn>
										</Columns>
										<PagerStyle VerticalAlign="Middle" Font-Underline="True" Font-Bold="True" HorizontalAlign="Left"
											ForeColor="#4A3C8C" BackColor="#E7E7FF" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr style="PADDING-BOTTOM: 2px; PADDING-LEFT: 2px; PADDING-RIGHT: 2px; PADDING-TOP: 2px"
					class="tr">
					<td align="left"></td>
				</tr>
			</table>
		</form>
	</body>
</html>
