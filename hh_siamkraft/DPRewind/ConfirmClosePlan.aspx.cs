﻿using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using hh_siamkraft.Entities.GR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{
    public partial class ConfirmClosePlan : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();

        private int IsSAP
        {
            get
            {
                return Convert.ToInt32(ViewState["IsSAP"]);
            }
            set
            {
                ViewState["IsSAP"] = value;
            }
        }
        
        protected void Page_Init(object sender, EventArgs e)
        {
            
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(Page.IsPostBack))
            {
                try
                {
                    if (Request.QueryString["docno"] == null)
                    {
                        Response.Redirect("SelectDP.aspx", true);
                    }

                    lblDocNo.Text = Request.QueryString["docno"];
                    IsSAP = Convert.ToInt32(Request.QueryString["IsSAP"]);

                    if (IsSAP == 0)
                    {
                        rdoConfirm.Text = "Yes, confirm data";
                        rdoCancel.Text = "No, confirm data";
                    }
                }
                catch (Exception ex)
                {
                    script_func.script_alert(ex.Message, Page);
                }
            }
        }

        protected void btnSelect_Click(object sender, EventArgs e)
        {
            ConfirmData();
        }

        protected void ConfirmData()
        {
            try
            {
                if(!rdoConfirm.Checked && !rdoCancel.Checked)
                {
                    script_func.script_alert("Please select confirm/no ", Page);
                    return;
                }

                string url = "";
                if (rdoConfirm.Checked)
                {
                    if(IsSAP==1)
                    {
                        var connect = Convert.ToString(Session["Connection_String"]);
                        using (var session = new SkRepositorySession(connect))
                        {
                            var repo = new DPRewindRepository(session);
                            var result = repo.ConfirmSendToSAP(Convert.ToString(Session["UserLogin_WHCode"]), lblDocNo.Text, Convert.ToString(Session["UserLogin_HHID"]), Convert.ToString(Session["UserLogin_UserName"]), DateTime.Now.ToString("dd.MM.yyyy"), DateTime.Now.ToString("dd.MM.yyyy"), String.Empty);
                            if(result.Value > 0)
                            {
                                url = String.Format("WaitData.aspx?docno={0}&IsSAP={1}&interfaceid={2}", lblDocNo.Text, IsSAP.ToString(), result.Value.ToString());
                            }
                            else
                            {
                                script_func.script_alert("Can not select Interface ID.", Page);
                                return;
                            }

                        }
                    }
                    else
                    {
                        var connect = Convert.ToString(Session["Connection_String"]);
                        using (var session = new SkRepositorySession(connect))
                        {
                            var repo = new DPRewindRepository(session);
                            var result = repo.ConfirmScanRewindManual(lblDocNo.Text, Convert.ToString(Session["UserLogin_UserName"]));
                            if (result.Value)
                            {
                                string msgAlert = "DP : {0} close plan completed.";
                                msgAlert = String.Format(msgAlert, lblDocNo.Text);
                                Page.RegisterStartupScript("location", "<script language='javascript'>alert('" + msgAlert + "');window.location.href='MainMenu.aspx';</script>");
                                return;
                            }
                            else
                            {
                                script_func.script_alert("Can not confirm this DP.", Page);
                                return;
                            }

                        }
                    }
                }
                else
                {
                    if (IsSAP == 1)
                    {
                        url = String.Format("ScanBarcode.aspx?docno={0}&IsSAP={1}", lblDocNo.Text, IsSAP.ToString());
                    }
                    else
                    {
                        url = String.Format("ScanManual.aspx?docno={0}&IsSAP={1}", lblDocNo.Text, IsSAP.ToString());
                    }
                }
                Response.Redirect(url);
            }
            catch (Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
            }
        }
    }
}