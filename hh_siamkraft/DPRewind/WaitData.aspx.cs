﻿using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using hh_siamkraft.Entities.GR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{
    public partial class WaitData : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();

        private int InterfaceID
        {
            get
            {
                return Convert.ToInt32(ViewState["InterfaceID"]);
            }
            set
            {
                ViewState["InterfaceID"] = value;
            }
        }
        private int IsSAP
        {
            get
            {
                return Convert.ToInt32(ViewState["IsSAP"]);
            }
            set
            {
                ViewState["IsSAP"] = value;
            }
        }
        
        protected void Page_Init(object sender, EventArgs e)
        {
            
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(Page.IsPostBack))
            {
                try
                {
                    if (Request.QueryString["docno"] == null)
                    {
                        Response.Redirect("SelectDP.aspx", true);
                    }

                    txtDocNo.Text = Request.QueryString["docno"];
                    IsSAP = Convert.ToInt32(Request.QueryString["IsSAP"]);
                    InterfaceID = Convert.ToInt32(Request.QueryString["interfaceid"]);

                    pnlLoad.Visible = true;
                    pnlError.Visible = false;
                    pnlComplete.Visible = false;

                    SelectInterfaceStatus();
                }
                catch (Exception ex)
                {
                    script_func.script_alert(ex.Message, Page);
                }
            }
        }

        protected void SelectInterfaceStatus()
        {
            try
            {
                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession(connect))
                {
                    var repo = new GRRepository(session);
                    var result = repo.SelectResultControl(InterfaceID);
                    if (result.Value.Count() > 0)
                    {
                        var dt = result.Value.FirstOrDefault();
                        if(dt.Return_Code == "00")
                        {
                            DisplayMatDoc(dt);
                        }
                        else if (dt.Return_Code == "N")
                        {
                            string url = String.Format("docno={0}&IsSAP={1}&interfaceid={2}", txtDocNo.Text, IsSAP.ToString(), InterfaceID.ToString());
                            Response.Write("<meta http-equiv=refresh content=5 url='WaitData.aspx?'" + url);
                        }
                        else
                        {
                            DisplayError(dt);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
            }
        }

        protected void DisplayMatDoc(TblControl dt)
        {
            try
            {
                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession(connect))
                {
                    var repo = new DPRewindRepository(session);
                    var result = repo.UpdateStausComplete(txtDocNo.Text, dt.Return_Msg, Convert.ToString(Session["UserLogin_UserName"]));
                    if (result.Value)
                    {
                        pnlLoad.Visible = false;
                        pnlError.Visible = false;
                        pnlComplete.Visible = true;
                        lblMatDoc.Text = dt.Return_Msg;
                    }
                }
            }
            catch (Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
            }
        }
        protected void DisplayError(TblControl dt)
        {
            try
            {
                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession(connect))
                {
                    var repo = new DPRewindRepository(session);
                    var result = repo.UpdateStatusError(txtDocNo.Text, dt.Return_Msg);
                    if (result.Value)
                    {
                        pnlLoad.Visible = false;
                        pnlComplete.Visible = false;
                        pnlError.Visible = true;
                        lblError.Text = dt.Return_Msg;
                    }
                }
            }
            catch (Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
            }
        }

        protected void btnCloseComplete_Click(object sender, EventArgs e)
        {
            Response.Redirect("MainMenu.aspx", true);
        }

        protected void btnCloseError_Click(object sender, EventArgs e)
        {
            Response.Redirect("MainMenu.aspx", true);
        }
    }
}