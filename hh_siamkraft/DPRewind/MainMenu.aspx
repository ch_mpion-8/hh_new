﻿<%@ Register TagPrefix="uc1" TagName="header_control" Src="../header_control.ascx"   %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MainMenu.aspx.cs" Inherits="hh_siamkraft.MainMenu" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>MainMenu</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C# .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"/>
		<LINK href="../css/style.css" type="text/css" rel="stylesheet"/>
	    <style type="text/css">
            .style1
            {
                height: 25px;
            }
        </style>
</head>
<body leftMargin="1" topMargin="1" rightmargin="1" bottommargin="1" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<table class="table_main" border="0" cellSpacing="0" cellPadding="0" width="225">
				<tr>
					<td class="headtable"><uc1:header_control id="Header_control1" runat="server"></uc1:header_control></td>
				</tr>
				<tr>
					<td class="headtable" align="center"><font color="white">DP Rewind Main Menu</font></td>
				</tr>
				<tr>
					<td>
						<table border="0" cellSpacing="2" cellPadding="2" width="100%">
							<tr>
								<td align="center"><asp:button id="btnBatchSAP" runat="server" Text="Scan Batch in SAP" Width="210px" CssClass="buttonTag" Visible="false"
										Height="22px" OnClick="btnBatchSAP_Click"></asp:button></td>
							</tr>
							<tr>
								<td align="center"><asp:button id="btnBatchNotSAP" runat="server" Text="Scan Batch not in SAP" Width="210px" CssClass="buttonTag" Visible="false"
										Height="22px" OnClick="btnBatchNotSAP_Click"></asp:button></td>
							</tr>
							<tr>
								<td align="center"><asp:button id="Bt_MainMenu" runat="server" Text="MAIN MENU" Width="210px" CssClass="buttonTag"
										Height="22px" OnClick="Bt_MainMenu_Click"></asp:button></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
