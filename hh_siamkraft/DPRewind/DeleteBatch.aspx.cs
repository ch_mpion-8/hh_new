﻿using Entities;
using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using hh_siamkraft.Entities.GR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{
    public partial class DeleteBatch : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();

        private string DocNo
        {
            get
            {
                return Convert.ToString(ViewState["DocNo"]);
            }
            set
            {
                ViewState["DocNo"] = value;
            }
        }
        private int IsSAP
        {
            get
            {
                return Convert.ToInt32(ViewState["IsSAP"]);
            }
            set
            {
                ViewState["IsSAP"] = value;
            }
        }
        
        protected void Page_Init(object sender, EventArgs e)
        {
            
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(Page.IsPostBack))
            {
                try
                {
                    if (Request.QueryString["docno"] == null)
                    {
                        Response.Redirect("SelectDP.aspx", true);
                    }

                    DocNo = Request.QueryString["docno"];
                    IsSAP = Convert.ToInt32(Request.QueryString["IsSAP"]);

                    IList<RewindScan> rw = (IList<RewindScan>)Session["BatchDetail"];
                    var dt = rw.FirstOrDefault();
                    lblBatch.Text = dt.Batch;
                    lblQty.Text = dt.Qty.ToString();
                    lblMaterial.Text = dt.Material;
                }
                catch (Exception ex)
                {
                    script_func.script_alert(ex.Message, Page);
                }
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteScan();
        }

        protected void DeleteScan()
        {

            try
            {
                if(!rdoDelete.Checked && !rdoCancel.Checked)
                {
                    script_func.script_alert("Please select delete/no condition", Page);
                    return;
                }

                string url = "";
                if (rdoDelete.Checked)
                {
                    var connect = Convert.ToString(Session["Connection_String"]);
                    using (var session = new SkRepositorySession(connect))
                    {
                        var repo = new DPRewindRepository(session);
                        var result = repo.DeleteScanBarcode(Convert.ToString(Session["UserLogin_WHCode"]), DocNo, lblMaterial.Text, lblBatch.Text, Convert.ToString(Session["UserLogin_UserName"]));
                        if (result.HasError)
                        {
                            script_func.script_alert(result.Error.ToString(), Page);
                            return;
                        }
                        else
                        {

                            if (IsSAP == 1)
                            {
                                url = String.Format("ScanBarcode.aspx?docno={0}&IsSAP={1}", DocNo, IsSAP.ToString());
                            }
                            else
                            {
                                url = String.Format("ScanManual.aspx?docno={0}&IsSAP={1}", DocNo, IsSAP.ToString());
                            }
                        }
                    }
                }
                else
                {
                    if (IsSAP == 1)
                    {
                        url = String.Format("ScanBarcode.aspx?docno={0}&IsSAP={1}", DocNo, IsSAP.ToString());
                    }
                    else
                    {
                        url = String.Format("ScanManual.aspx?docno={0}&IsSAP={1}", DocNo, IsSAP.ToString());
                    }
                }
                Response.Redirect(url, true);
            }
            catch (Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
            }
        }
    }
}