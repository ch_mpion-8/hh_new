﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{
    public partial class MainMenu : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
         
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(Page.IsPostBack))
            {
                showMenu();
            }
        }
        private void showMenu()
        {
            var permission = Convert.ToString(Session["Permission"]);
            var list = permission.Split(',').ToList();

            if (list.Where(o => o == "SCANREWINDBATCHINSAP").Count() > 0)
            {
                btnBatchSAP.Visible = true;
            }

            if (list.Where(o => o == "SCANREWINDBATCHNOTINSAP").Count() > 0)
            {
                btnBatchNotSAP.Visible = true;
            }
            
        }
        protected void btnBatchSAP_Click(object sender, EventArgs e)
        {
            Response.Redirect("SelectDP.aspx?IsSAP=1");
        }

        protected void btnBatchNotSAP_Click(object sender, EventArgs e)
        {
            Response.Redirect("SelectDP.aspx?IsSAP=0");
        }

        protected void Bt_MainMenu_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/main_menu.aspx");
        }
    }
}