﻿<%@ Register TagPrefix="uc1" TagName="header_control" Src="../header_control.ascx"   %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WaitData.aspx.cs" Inherits="hh_siamkraft.WaitData" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>WaitData</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C# .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"/>
		<LINK href="../css/style.css" type="text/css" rel="stylesheet"/>
	    <style type="text/css">
            .style1
            {
                height: 25px;
            }
        </style>
</head>
<body leftMargin="1" topMargin="1" rightmargin="1" bottommargin="1" MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<table class="table_main" cellSpacing="0" cellPadding="0" width="225" border="0">
				<tr>
					<td class="headtable"><uc1:header_control id="Header_control1" runat="server"></uc1:header_control></td>
				</tr>
				<tr>
					<td class="headtable" align="center">DP Rewind</td>
				</tr>
				<tr>
					<td>
						<table cellpadding="2" cellspacing="2">
							<tr>
								<td class="normalText">
									<asp:Label style="Z-INDEX: 0" id="Label1" runat="server">DP :</asp:Label>
								</td>
								<td>
									<asp:TextBox id="txtDocNo" Runat="server" CssClass="TextReadOnly" Width="120px"></asp:TextBox>
								</td>
							</tr>
						</table>
						<asp:Panel ID="pnlLoad" Runat="server" Width="100%">
							<TABLE border="0" cellSpacing="2" cellPadding="2" width="100%">
								<TR>
									<TD style="PADDING-BOTTOM: 5px; PADDING-LEFT: 5px; PADDING-RIGHT: 5px; PADDING-TOP: 5px"><IMG align="absMiddle" src="../images/loading.gif">&nbsp;
										<asp:label id="Lb_msg" runat="server" CssClass="normalText">Waiting... Mat Doc From SAP</asp:label></TD>
								</TR>
							</TABLE>
						</asp:Panel>
						<asp:Panel ID="pnlComplete" Runat="server" Width="100%">
							<TABLE border="0" cellSpacing="2" cellPadding="2" width="100%">
								<TR>
									<TD style="PADDING-BOTTOM: 5px; PADDING-LEFT: 5px; PADDING-RIGHT: 5px; PADDING-TOP: 5px"><IMG align="absMiddle" src="../images/text_ok.png">&nbsp;
										<asp:label id="lblMatDoc" runat="server" CssClass="normalText"></asp:label></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:Button id="btnCloseComplete" Width="48px" CssClass="buttonTag" Runat="server" Text="Exit" OnClick="btnCloseComplete_Click"></asp:Button>

									</TD>
								</TR>
							</TABLE>
						</asp:Panel>
						<asp:Panel ID="pnlError" Runat="server" Width="100%">
							<TABLE border="0" cellSpacing="2" cellPadding="2" width="100%">
								<TR>
									<TD style="PADDING-BOTTOM: 5px; PADDING-LEFT: 5px; PADDING-RIGHT: 5px; PADDING-TOP: 5px"><IMG align="absMiddle" src="../images/saperror.gif">&nbsp;
										<asp:label id="lblError" runat="server" CssClass="normalText"></asp:label></TD>
								</TR>
								<TR>
									<TD align="center">
										<asp:Button id="btnCloseError" Width="48px" CssClass="buttonTag" Runat="server" Text="Exit" OnClick="btnCloseError_Click"></asp:Button>

									</TD>
								</TR>
							</TABLE>
						</asp:Panel>
					</td>
				</tr>
				<tr class="tr" style="PADDING-BOTTOM: 2px; PADDING-LEFT: 2px; PADDING-RIGHT: 2px; PADDING-TOP: 2px">
					<td align="left">&nbsp;
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
