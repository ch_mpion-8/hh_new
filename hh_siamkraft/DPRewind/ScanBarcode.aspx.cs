﻿using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using hh_siamkraft.Entities.GR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{
    public partial class ScanBarcode : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();
        
        private int IsSAP
        {
            get
            {
                return Convert.ToInt32(ViewState["IsSAP"]);
            }
            set
            {
                ViewState["IsSAP"] = value;
            }
        }
        
        protected void Page_Init(object sender, EventArgs e)
        {
           
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(Page.IsPostBack))
            {
                try
                {
                    btnSendSAP.Attributes.Add("onclick", "javascript:if(confirm('ต้องการ Send to SAP ใช่หรือไม่ ?')== false){return false;}");
                    if (Request.QueryString["docno"] == null)
                    {
                        Response.Redirect("SelectDP.aspx", true);
                        return;
                    }
                    lblDocNo.Text = Request.QueryString["docno"];
                    IsSAP = Convert.ToInt32(Request.QueryString["IsSAP"]);

                    SelectTotalScanQty();
                    SelectScanDetail();
                }
                catch (Exception ex)
                {
                    script_func.script_alert(ex.Message, Page);
                }
            }
        }

        protected void SelectTotalScanQty()
        {
            try
            {
                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession(connect))
                {
                    var repo = new DPRewindRepository(session);
                    var result = repo.SelectTotalScanQty(lblDocNo.Text);
                    if (result.Value.Count() > 0)
                    {
                        var dt = result.Value.FirstOrDefault();
                        lblScanQty.Text = dt.TotalScanQty.ToString();
                        lblOrderQty.Text = dt.TotalOrderQty.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
            }
        }
        protected void SelectScanDetail()
        {
            try
            {
                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession(connect))
                {
                    var repo = new DPRewindRepository(session);
                    var result = repo.SelectScanDetail(lblDocNo.Text, "-");
                    if (result.Value.Count() > 0)
                    {
                        dgData.DataSource = result.Value;
                        dgData.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
            }
        }

        protected void dgData_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblNo = e.Item.FindControl("lblNo") as Label;
                if(lblNo != null)
                {
                    lblNo.Text = Convert.ToString(dgData.Items.Count + 1);
                }
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            var url = String.Format("SelectDP.aspx?docno={0}&IsSAP={1}", lblDocNo.Text, IsSAP.ToString());
            Response.Redirect(url);
        }

        protected void txtBatch_TextChanged(object sender, EventArgs e)
        {
            SaveScan();
        }

        protected void SaveScan()
        {
            try
            {
                if(txtBatch.Text.Trim().Length == 0)
                {
                    script_func.script_focus(txtBatch.ClientID, Page);
                    return;
                }

                if (txtBatch.Text.Trim().Length != 10)
                {
                    script_func.script_alert("สแกน Batch ไม่ถูกต้อง", Page);
                    txtBatch.Text = String.Empty;
                    script_func.script_focus(txtBatch.ClientID, Page);
                    return;
                }
                txtBatch.Text = txtBatch.Text.Trim().ToUpper();

                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession(connect))
                {
                    var repo = new DPRewindRepository(session);
                    var result = repo.SelectScanDetail(lblDocNo.Text, txtBatch.Text);
                    if (result.Value.Count() > 0)
                    {
                        var url = String.Format("DeleteBatch.aspx?docno={0}&IsSAP={1}", lblDocNo.Text, IsSAP.ToString());
                        Session["BatchDetail"] = result.Value;
                        Response.Redirect(url);
                    }

                    var result2 = repo.SaveScanBarcode(Convert.ToString(Session["UserLogin_WHCode"]), lblDocNo.Text, txtBatch.Text, 0, String.Empty, true, Convert.ToString(Session["UserLogin_UserName"]));
                    if(result2.Value)
                    {
                        SelectTotalScanQty();
                        SelectScanDetail();
                    }
                    else
                    {
                        script_func.script_alert(result.Error.Message, Page);
                    }
                    txtBatch.Text = String.Empty;
                    script_func.script_focus(txtBatch.ClientID, Page);
                }
            }
            catch (Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
            }
        }

        protected void SendSAP()
        {
            try
            {
                if (dgData.Items.Count == 0)
                {
                    script_func.script_alert("Please scan barcode at least 1 item.", Page);
                    script_func.script_focus(txtBatch.ClientID, Page);
                    return;
                }

                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession(connect))
                {
                    var repo = new DPRewindRepository(session);
                    var result = repo.SelectTotalScanQty(lblDocNo.Text);
                    if (result.Value.Count() > 0)
                    {
                        var dt = result.Value.FirstOrDefault();
                        if(dt.TotalOrderQty - dt.TotalScanQty == 0)
                        {
                            var result2 = repo.ConfirmSendToSAP(Convert.ToString(Session["UserLogin_WHCode"]), lblDocNo.Text, Convert.ToString(Session["UserLogin_HHID"]), Convert.ToString(Session["UserLogin_UserName"]), DateTime.Now.ToString("dd.MM.yyyy"), DateTime.Now.ToString("dd.MM.yyyy")
                                            , String.Empty);
                            if(result2.Value > 0)
                            {
                                string url = String.Format("WaitData.aspx?docno={0}&issap={1}&interfaceid={2}", lblDocNo.Text, IsSAP.ToString(), result2.Value.ToString());
                                Response.Redirect(url);
                            }
                        }
                        else
                        {
                            string url = String.Format("ConfirmClosePlan.aspx?docno={0}&issap={1}", lblDocNo.Text, IsSAP.ToString());
                            Response.Redirect(url);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
            }
        }

        protected void btnSendSAP_Click(object sender, EventArgs e)
        {
            SendSAP();
        }
    }
}