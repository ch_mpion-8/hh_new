﻿using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{
    public partial class SelectDP : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();
        private int IsSAP
        {
            get
            {
                return Convert.ToInt32(ViewState["IsSAP"]);
            }
            set
            {
                ViewState["IsSAP"] = value;
            }
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(Page.IsPostBack))
            {
                try
                {
                    if (Request.QueryString["IsSAP"] != null)
                    {
                        IsSAP = Convert.ToInt32(Request.QueryString["IsSAP"]);
                        pnlDetail.Visible = false;
                        script_func.script_focus(txtDocNo.ClientID, Page);
                    }

                    if (Request.QueryString["docno"] != null)
                    {
                        txtDocNo.Text = Convert.ToString(Request.QueryString["docno"]);
                        SelectRewindDetail();
                    }
                    else
                    {
                        if (IsSAP == 1)
                        {
                            var connect = Convert.ToString(Session["Connection_String"]);
                            using (var session = new SkRepositorySession(connect))
                            {
                                var repo = new DPRewindRepository(session);
                                var result = repo.SelectInterfaceDisconnect(Convert.ToString(Session["UserLogin_WHCode"]), Convert.ToString(Session["UserLogin_UserName"]));
                                if (result.Value.Count() > 0)
                                {
                                    var dt = result.Value.FirstOrDefault();
                                    string url = String.Format("WaitData.aspx?docno={0}&IsSAP={1}&interfaceid={2}", dt.DocNo, IsSAP.ToString(), dt.InterfaceID.ToString());
                                    Response.Redirect(url, true);
                                }

                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    script_func.script_alert(ex.Message, Page);
                }
            }
        }

        protected void btnMainMenu_Click(object sender, EventArgs e)
        {
            Response.Redirect("MainMenu.aspx");
        }

        protected void SelectRewindDetail()
        {
            try
            {
                if (txtDocNo.Text.Trim().Length == 0)
                {
                    script_func.script_focus(txtDocNo.ClientID, Page);
                }

                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession(connect))
                {
                    var repo = new DPRewindRepository(session);
                    var result = repo.SelectRewindDetailByDocNo(Convert.ToString(Session["UserLogin_WHCode"]), txtDocNo.Text.Trim().ToUpper(), Convert.ToBoolean(IsSAP));
                    if (result.Value.Count() > 0)
                    {
                        pnlHeader.Visible = false;
                        pnlDetail.Visible = true;
                        lblDocNo.Text = txtDocNo.Text.Trim().ToUpper();
                        dgData.DataSource = result.Value;
                        dgData.DataBind();

                        lblTotalMaterial.Text = result.Value.Count().ToString();
                        lblTotalOrderQty.Text = result.Value.Select(o => o.OrderQty).Sum().ToString();
                    }
                    else
                    {
                        script_func.script_alert("ไม่มีข้อมูล DP No : " + txtDocNo.Text.Trim().ToUpper() + " ในระบบ", Page);
                        txtDocNo.Text = String.Empty;
                        script_func.script_focus(txtDocNo.ClientID, Page);
                    }
                }
            }
            catch (Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
            }
        }

        int runNo = 0, orderQty = 0, rewindQty = 0;

        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                //((DataTable)dgData.DataSource).Clear();
                dgData.DataSource = null;
                dgData.DataBind();
                lblDocNo.Text = String.Empty;
                lblTotalMaterial.Text = String.Empty;
                lblTotalOrderQty.Text = String.Empty;
                pnlDetail.Visible = false;
                pnlHeader.Visible = true;
                txtDocNo.Text = String.Empty;
                script_func.script_focus(txtDocNo.ClientID, Page);
            }
            catch (Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
            }
        }

        protected void txtDocNo_TextChanged(object sender, EventArgs e)
        {
            SelectRewindDetail();
        }

        protected void btnScanBarcode_Click(object sender, EventArgs e)
        {
            string url;
            url = String.Format("ScanBarcode.aspx?docno={0}&IsSAP={1}", lblDocNo.Text, IsSAP.ToString());
            Response.Redirect(url);
        }

        decimal totalWeight = 0;
        protected void dgData_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if(e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblNo = e.Item.FindControl("lblNo") as Label;
                runNo++;
                orderQty = orderQty + Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "OrderQty"));
                rewindQty = rewindQty + Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "RewindQty"));
                totalWeight = totalWeight + Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "TotalWeight"));

                try
                {
                    lblNo.Text = Convert.ToString(dgData.Items.Count + 1);
                }
                catch
                {

                }
            }
            else if(e.Item.ItemType == ListItemType.Footer)
            {
                e.Item.Cells[2].Text = orderQty.ToString();
                e.Item.Cells[3].Text = rewindQty.ToString();
                e.Item.Cells[4].Text = totalWeight.ToString("#,##0.000");
            }
        }
    }
}