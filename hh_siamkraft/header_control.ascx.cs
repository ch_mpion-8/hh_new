﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{
    public partial class header_control : System.Web.UI.UserControl
    {
        [System.Diagnostics.DebuggerStepThrough()]
        protected void InitializeComponent()
        {
        }
        protected void Page_Load(object sender, EventArgs e)
        {   
            if(Session["user_id"] == null)
            {
                Response.Redirect(@"~/index.aspx", true);  
            }
            this.Lb_user_login.Text = Session["user_id"] != null ? Convert.ToString(Session["user_id"]) : "";
            this.Lb_hh.Text = Session["HH_ID"] != null ? Convert.ToString(Session["HH_ID"]) : "";
            this.Lb_Server2.Text = Session["DB_Description"] != null ? Convert.ToString(Session["DB_Description"]) : "";
            this.Lb_Plant2.Text = Session["UserLogin_WHCode"] != null ? Convert.ToString(Session["UserLogin_WHCode"]) : "";

            if (Convert.ToString(Session["MultiPlant"]) == "Y")
            {
                this.Lb_Server2.NavigateUrl = "ChangePlant.aspx";
                this.Lb_Plant2.NavigateUrl = "ChangePlant.aspx";
            }
            
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            InitializeComponent();
        }
    }
}