﻿using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using hh_siamkraft.Entities.Common;
using hh_siamkraft.Infrastructure.Configurations;
using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{
    public partial class index : System.Web.UI.Page
    {

        ScriptFunction script_func = new ScriptFunction();

        //private UserLogin UserLoginData
        //{
        //    get
        //    {
        //        var user = (UserLogin)ViewState["UserLogin"];
        //        return user;
        //    }
        //    set
        //    {
        //        ViewState["UserLogin"] = value;
        //    }
        //}

        // This call is required by the Web Form Designer.
        [System.Diagnostics.DebuggerStepThrough()]
        protected void InitializeComponent()
        {
        }



        protected void Page_Init(object sender, System.EventArgs e)
        {
            InitializeComponent();
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            if (!IsPostBack)
            {
                this.initialForm();
            }
            if (string.IsNullOrEmpty(txtUserName.Text))
            {
                script_func.script_focus(txtUserName.ID, Page);
            }
            else if (string.IsNullOrEmpty(txtPassword.Text))
            {
                script_func.script_focus(txtPassword.ID, Page);
            }

            var msg = Request.Params["msg"] == null ? string.Empty : Request.Params["msg"].ToString();
            if ((msg != ""))
            {
                lblInfo.Text = msg;
            }

            //this.txtUserName.Text = "admin";
            //this.txtPassword.Text = "pass";
        }

        private void initialForm()
        {
            trAuth.Visible = true;
            trUser.Visible = true;
            trPass.Visible = true;

            trPlant.Visible = false;
            trServer.Visible = false;
            txtUserName.Text = "";
            txtPassword.Text = "";
            script_func.script_focus(txtUserName.ID, Page);
        }

        private void ClearScreen()
        {
            try
            {
                txtUserName.Enabled = true;
                txtUserName.Text = string.Empty;
                txtPassword.Enabled = true;
                txtPassword.Text = string.Empty;
                trAuth.Visible = true;
                trUser.Visible = true;
                trPass.Visible = true;
                trPlant.Visible = false;
                trServer.Visible = false;
                script_func.script_focus(txtUserName.ClientID, Page);
            }
            catch (Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
            }

        }

        protected void btnLogin_Click(object sender, System.EventArgs e)
        {
            Login();
        }

        protected void btnReset_Click(object sender, System.EventArgs e)
        {
            ClearScreen();
        }

        private void txtUserName_TextChanged(object sender, System.EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtUserName.Text))
            {
                script_func.script_focus(txtPassword.ID, Page);
            }
        }

        protected void txtPassword_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtUserName.Text))
            {
                script_func.script_focus(btnLogin.ID, Page);
            }
        }

        protected void ddlAuthorize_DataBinding(object sender, EventArgs e)
        {
            Helper.Control.EnumToListBox(typeof(AuthorizeType), ddlAuthorize);
        }

        private void Login()
        {
            try
            {
                var userName = txtUserName.Text;
                var password = txtPassword.Text;

                if (string.IsNullOrEmpty(userName) || userName.Trim() == string.Empty)
                {
                    script_func.script_alert("กรุณากรอก UserName", Page);
                    script_func.script_focus(txtUserName.ID, Page);
                    return;
                }

                if (trPlant.Visible || trServer.Visible)
                {
                    //if (ddlWarehouse.SelectedIndex == 0)
                    //{
                    //    script_func.script_alert("กรุณาเลือก Plant ที่ต้องการใช้งาน", Page);
                    //    return;
                    //}
                    if (trPlant.Visible)
                    {
                        var wh_no = Convert.ToInt32(ddlWarehouse.SelectedItem.Value);
                        using (var session = new SkRepositorySession())
                        {
                            var repo = new CommonRepository(session);
                            var data = repo.GetWarehouseDetail(wh_no);
                            if (data.HasError)
                            {
                                script_func.script_alert(data.Error.Message, Page);
                                this.lblInfo.Text = "<font color=\'#ff0000\'>Error!!</font> Invalid Warehouse!";
                                this.initialForm();
                                return;
                            }
                            var warehouse = data.Value;
                            if (warehouse != null)
                            {
                                Session["HH_Plant"] = warehouse.WHCode;
                                Session["WH_No"] = wh_no;
                                //this.UserLoginData.WHCode = dt.Rows[0]["WHCode"].ToString();
                                //this.UserLoginData.WHDescription = dt.Rows[0]["WH_Description"].ToString();
                                //this.UserLoginData.ShippingPoint = dt.Rows[0]["ShippingPoint"].ToString();
                                //this.UserLoginData.WHID = wh_no;
                                //Session["UserLogin"] = user;

                                Session["UserLogin_WHCode"] = warehouse.WHCode;
                                Session["UserLogin_WHDescription"] = warehouse.WH_Description;
                                Session["UserLogin_ShippingPoint"] = warehouse.ShippingPoint;
                                Session["UserLogin_WHID"] = warehouse.WH_No;
                            }
                            else
                            {

                                script_func.script_alert("Invalid Warehouse!", Page);
                                this.lblInfo.Text = "<font color=\'#ff0000\'>Error!!</font> Invalid Warehouse!";
                                this.initialForm();
                                return;
                            }
                        }
                    }
                    if (trServer.Visible)
                    {
                        var server_id = Convert.ToInt32(ddlServer.SelectedItem.Value);
                        using (var session = new SkRepositorySession())
                        {
                            var repo = new CommonRepository(session);
                            var data = repo.GetServer(server_id);
                            if (data.HasError)
                            {
                                script_func.script_alert(data.Error.Message, Page);
                                this.lblInfo.Text = "<font color=\'#ff0000\'>Error!!</font> Invalid Warehouse!";
                                this.initialForm();
                                return;
                            }
                            var server = data.Value;
                            if (server != null)
                            {
                                Session["Server_ID"] = Convert.ToString(server.ServerID);
                                Session["DB_Description"] = server.Description;
                                Session["Connection_String"] = server.ConnectString;
                            }
                            else
                            {

                                script_func.script_alert("Invalid PM!", Page);
                                this.lblInfo.Text = "<font color=\'#ff0000\'>Error!!</font> Invalid PM!";
                                this.initialForm();
                                return;
                            }
                        }
                    }

                    Response.Redirect("main_menu.aspx", false);
                }
                else
                {
                    var ip_address = GetIP();
                    var hhID = string.Empty;
                    if (string.IsNullOrEmpty(password) || password.Trim() == string.Empty)
                    {
                        script_func.script_alert("กรุณากรอก Password", Page);
                        script_func.script_focus(txtPassword.ID, Page);
                        return;
                    }
                    userName = userName.Trim().ToLower();
                    password = password.Trim().ToLower();

                    if (ddlAuthorize.SelectedValue == AuthorizeType.Domain.ToString() && !CheckJoinDomain())
                    {

                        script_func.script_alert("Domain Not Found!", Page);
                        this.lblInfo.Text = "<font color=\'#ff0000\'>Error!!</font> Username or Password invalid";
                        this.initialForm();
                        return;
                    }
                    var authenType = "P";
                    if(ddlAuthorize.SelectedValue == "1")
                    {
                        authenType = "D";
                    }
                    var commonRepo = new CommonRepository(new SkRepositorySession());
                    var user = commonRepo.GetUser(userName, password, ip_address, authenType).Value;
                    if (user == null || user.UserDetail == null)
                    {
                        script_func.script_alert("UserName or Password invalid", Page);
                        this.lblInfo.Text = "<font color=\'#ff0000\'>Error!!</font> Username or Password invalid";
                        this.initialForm();
                        return;
                    }
                    else
                    {
                        if (user.UserDetail.User_Status == 0)
                        {
                            script_func.script_alert("This username is inactive.", Page);
                            this.lblInfo.Text = "<font color=\'#ff0000\'>Error!!</font> This username is inactive.";
                            this.initialForm();
                            return;
                        }

                        var func = string.Join(",", user.Permission.Where(o => o.Platform == "HH").Select(o => o.Function_Name).ToList());
                        Session["Permission"] = func;

          
                        // Check Handheld ID
                        if (user.Handheld.Count > 0)
                        {
                            hhID = user.Handheld.Select(o => o.HH_ID).FirstOrDefault();
                        }
                        if (string.IsNullOrEmpty(hhID) && user.ConfigValue.Count > 0)
                        {
                            hhID = user.ConfigValue.FirstOrDefault();
                        }
                        if (string.IsNullOrEmpty(hhID))
                        {
                            script_func.script_alert(("HH ID Not found : "
                                             + (ip_address + " or Default HH ID does not exists.")), Page);
                            script_func.script_focus(txtUserName.ClientID, Page);
                            return;
                        }

                        if (user.ServerDetail != null)
                        {
                            if (user.ServerDetail.Count == 1)
                            {
                                Session["Server_ID"] = user.ServerDetail.FirstOrDefault().ServerID.ToString();
                                Session["DB_Description"] = user.ServerDetail.FirstOrDefault().Description;
                                Session["Connection_String"] = user.ServerDetail.FirstOrDefault().ConnectString;
                            }
                        }
                        else
                        {
                            script_func.script_alert(("HH ID Not found : "
                                             + (ip_address + " or Default HH ID does not exists.")), Page);
                            script_func.script_focus(txtUserName.ClientID, Page);
                            return;
                        }


                        Session["UserLogin_UserName"] = txtUserName.Text;
                        Session["UserLogin_HHID"] = hhID;
                        Session["UserLogin_IPAddress"] = ip_address;

                        Session["user_id"] = user.UserDetail.User_ID;
                        Session["IPAddress"] = ip_address;
                        Session["HH_ID"] = hhID;
                        if (user.WarehouseDetail.Count == 1)
                        {
                            var warehouse = user.WarehouseDetail.FirstOrDefault();
                            //Session["company"] = user.UserDetail.User_Company;
                            Session["HH_Plant"] = warehouse.WHCode;
                            Session["WH_No"] = warehouse.WH_No;
                            
                            user.UserLogin.WHCode = warehouse.WHCode;
                            user.UserLogin.WHDescription = warehouse.WH_Description;
                            user.UserLogin.ShippingPoint = warehouse.ShippingPoint;
                            user.UserLogin.HHID = hhID;
                            user.UserLogin.IPAddress = ip_address;
                            user.UserLogin.WHID = warehouse.WH_No;
                            Session["UserLogin"] = user;


                            //Session["UserLogin_UserName"] = txtUserName.Text;
                            Session["UserLogin_WHCode"] = warehouse.WHCode;
                            Session["UserLogin_WHDescription"] = warehouse.WH_Description;
                            Session["UserLogin_ShippingPoint"] = warehouse.ShippingPoint;
                            //Session["UserLogin_HHID"] = hhID;
                            //Session["UserLogin_IPAddress"] = ip_address;
                            Session["UserLogin_WHID"] = warehouse.WH_No;

                            
                        }
                        

                        if(user.WarehouseDetail.Count> 1 || user.ServerDetail.Count > 1)
                        {
                            Session["MultiPlant"] = "Y";
                            if (user.ServerDetail.Count > 1)
                            {
                                this.trServer.Visible = true;
                            }
                            Helper.Control.SetServerDropDownList(ddlServer, user.ServerDetail);

                            if (user.WarehouseDetail.Count > 1)
                            {
                                this.trPlant.Visible = true;
                                GetWH();
                                //Helper.Control.SetWarehouseDropDownList(ddlWarehouse, user.WarehouseDetail);

                            }

                            trAuth.Visible = false;
                            trUser.Visible = false;
                            trPass.Visible = false;
                            this.txtUserName.Enabled = false;
                            this.txtPassword.Enabled = false;
                        }
                       else
                        {
                            Session["MultiPlant"] = "N";
                            Response.Redirect("main_menu.aspx", false);
                        }

                    }

                }



            }
            catch (Exception ex)
            {

                script_func.script_alert(ex.Message, Page);
                return;
            }
        }

        private bool CheckJoinDomain()
        {
            if ((string.Equals(txtUserName.Text.Trim(), "admin") && string.Equals(txtPassword.Text.Trim(), "pass")))
            {
                return true;
            }

            try
            {
                var rootEntry = new DirectoryEntry("LDAP://cementhai.com", txtUserName.Text.Trim(), txtPassword.Text.Trim());
                var obj = rootEntry.NativeObject;
                var Search = new DirectorySearcher(rootEntry);
                Search.Filter = ("(&(SAMAccountName="
                            + (txtUserName.Text.Trim() + ")(sAMAccountType=*))"));
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }


        private string GetIP()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }

            //IPAddress[] ipAdr = Dns.GetHostByName(Dns.GetHostName()).AddressList;
            //foreach (IPAddress _ip in ipAdr)
            //{
            //    try
            //    {
            //        if ((_ip.ToString().Length <= 15))
            //        {
            //            string[] ipArr = _ip.ToString().Split('.');
            //            if ((ipArr.Length == 4))
            //            {
            //                return _ip.ToString();
            //            }

            //        }

            //    }
            //    catch (Exception ex)
            //    {

            //    }

            //}

            return string.Empty;

        }

        private void GetWH()
        {
            var commonRepo = new CommonRepository(new SkRepositorySession());
            var username = Convert.ToString(Session["UserLogin_UserName"]);
            var wh = commonRepo.GetUserWH(username, ddlServer.SelectedValue).Value;
            Helper.Control.SetWarehouseDropDownList(ddlWarehouse, wh);
        }
        protected void ddlServer_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetWH();
        }
    }
}

/*
      Example For Connect Different DB  and use transaction       
           
       using (var session = new SkRepositorySession(SkConfiguration.Current.ConnectionStrings.Logging))
            {
                session.OpenTransaction(System.Data.IsolationLevel.ReadUncommitted);
                var repo = new CommonRepository(session);
                var data = repo.GetWarehouseDetail(wh_no);
               
                var warehouse = data.Value;
                session.RollbackTransaction();
                session.CommitTransaction();
            }
         
     
 
     
     
     
     
     */
