﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{
    public class ScriptFunction
    {

        public void ConfirmButton(Button btnConfirm, string message)
        {
            btnConfirm.Attributes.Add("onclick", "javascript:if(confirm(\"" + message + "\") == false) return false;");
        }

        public void script_alert(string alert, Page targetPage)
        {
            alert = alert.Replace("'", "\\'").Replace("\r", "\\\r").Replace("\n", "\\\n").Replace("\"", "\\\\\"");
            string str_Script = "<script language='javascript'>alert(\"" + alert + "\");</script>";
            //targetPage.RegisterStartupScript("alert", str_Script);
            targetPage.ClientScript.RegisterStartupScript(targetPage.GetType(), "alert", str_Script);
        }

        public void script_focus(string str_focus, Page targetPage)
        {
            string str_Script = "<script language=\"javascript\">document.forms[0]." + str_focus + ".focus();</script>";
            //targetPage.RegisterStartupScript("focus", str_Script);
            targetPage.ClientScript.RegisterStartupScript(targetPage.GetType(), "focus", str_Script);
        }

        public void script_select(string str_select, Page targetPage)
        {
            string str_Script = "<script language='javascript'>document.forms[0]." + str_select + ".select();</script>";
            //targetPage.RegisterStartupScript("select", str_Script);
            targetPage.ClientScript.RegisterStartupScript(targetPage.GetType(), "select", str_Script);
        }

        public string EscapeSingleQuotes(string textToEscape)
        {
            Regex re = new Regex("(\\w'|\\s'|!'|@'|#'|\\$'|%'|\\^'|&'|\\*'|\\('|\\)'|\\-'|\\+'|='|:'|;'|\"'|,'|<'|\\.'|>'|/'|\\?'|\\['|\\]'|\\{'|\\}'|'')[^\\']");
            System.Text.RegularExpressions.MatchCollection reMatches = re.Matches(textToEscape);
            for (int i = 0; i <= reMatches.Count - 1; i++)
                textToEscape = textToEscape.Replace(reMatches[i].Value, reMatches[i].Value.Replace("'", "\\'"));
            textToEscape = textToEscape.Replace("\r", "\\r").Replace("\n", "\\n");
            return textToEscape;
        }

        public string Return_VarChar(object obj, bool SingleQuote)
        {
            string ret;
            string tmp;
            if (obj == DBNull.Value)
                if (SingleQuote == true)
                    ret = "''";
                else
                    ret = "";
            else
            {
                tmp = obj.ToString();
                if (SingleQuote == true)
                {
                    ret = tmp.Replace("'", "''");
                    ret = "'" + ret + "'";
                }
                else
                    ret = tmp;
            }

            return ret;
        }
    }


}