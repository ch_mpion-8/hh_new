﻿using hh_siamkraft.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace hh_siamkraft.Helper
{
    public class Control
    {
        public static void SetDropDownList(DropDownList ddlSource, DataTable source, string valueMember, string displayMember)
        {
            if (!(source == null))
            {
                ddlSource.DataValueField = valueMember;
                ddlSource.DataTextField = displayMember;
                ddlSource.DataSource = source;
                ddlSource.DataBind();
                if ((ddlSource.Items.Count > 0))
                {
                    ddlSource.Items.Insert(0, "Select");
                }
                else
                {
                    ddlSource.Items.Insert(0, "Not found");
                }

            }
            else
            {
                ddlSource.Items.Insert(0, "Not found");
            }

            ddlSource.SelectedIndex = 0;
        }

        public static void SetWarehouseDropDownList(DropDownList ddlSource, IList<WarehouseDetail> source)
        {
            ddlSource.Items.Clear();
            if (source != null && source.Count > 0)
            {
                
                foreach (var wh in source)
                {
                    ddlSource.Items.Add(new ListItem(wh.WHCode+" : "+ wh.WH_Description, wh.WH_No.ToString()));
                }
                //ddlSource.Items.Insert(0, "Select");

            }
            else
            {
                ddlSource.Items.Insert(0, "Not found");
            }

            ddlSource.SelectedIndex = 0;
        }

        public static void SetServerDropDownList(DropDownList ddlSource, IList<ServerDetail> source)
        {
            ddlSource.Items.Clear();
            if (source != null && source.Count > 0)
            {

                foreach (var wh in source)
                {
                    ddlSource.Items.Add(new ListItem(wh.Description, wh.ServerID.ToString()));
                }
                //ddlSource.Items.Insert(0, "Select");
            }
            else
            {
                ddlSource.Items.Insert(0, "Not found");
            }

            ddlSource.SelectedIndex = 0;
        }

        public static void EnumToListBox(Type enumType, DropDownList control)
        {
            var Values = System.Enum.GetValues(enumType);

            foreach (int Value in Values)
            {
                string Display = Enum.GetName(enumType, Value);
                ListItem Item = new ListItem(Display, Value.ToString());
                control.Items.Add(Item);
            }
        }
    }
}