﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using hh_siamkraft.Entities.Common;

namespace hh_siamkraft.Helper
{
    public class GetData
    {
        public bool IsSessionLogin()
        {
            if ((HttpContext.Current.Session["UserLogin"] == null))
            {
                return false;
            }
            else
            {
                return true;
            }

        }

        public User GetUserLogin()
        {
            User userLogin = ((User)(HttpContext.Current.Session["UserLogin"]));
            if ((userLogin == null))
            {
                throw new Exception("Session Timeout\\r\\nกรุณา Login เข้าระบบใหม่อีกครั้ง");
            }

            return userLogin;
        }

        public string UserName()
        {
            return this.GetUserLogin().UserLogin.UserName;
        }

        public Int32 WHID()
        {
            return this.GetUserLogin().UserLogin.WHID;
        }

        public string WHCode()
        {
            return this.GetUserLogin().UserLogin.WHCode;
        }

        public string WHDesc()
        {
            return this.GetUserLogin().UserLogin.WHDescription;
        }

        public string ShippingPoint()
        {
            return this.GetUserLogin().UserLogin.ShippingPoint;
        }

        public string CompanyCode()
        {
            return this.GetUserLogin().UserLogin.CompanyCode;
        }

        public string HHID()
        {
            return this.GetUserLogin().UserLogin.HHID;
        }

        private static GetData _instance;

        public static GetData Instance
        {
            get
            {
                if ((_instance == null))
                {
                    _instance = new GetData();
                }

                return _instance;
            }
        }


    }
}