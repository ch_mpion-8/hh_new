﻿function keyCheck(eventObj, obj) {	// key number and dot
    var keyCode

    // Check For Browser Type
    if (document.all) {
        keyCode = eventObj.keyCode
    }
    else {
        keyCode = eventObj.which
    }

    var str = obj.value
    if (keyCode == 46) {
        if (str.indexOf(".") > 0) {
            return false
        }
    }

    if ((keyCode < 48 || keyCode > 58) && (keyCode != 46) && (keyCode != 13)) { // Allow only integers
        return false
    }

    return true
}


function focusText(eventObj, obj) {
    var keyCode

    // Check For Browser Type
    if (document.all) {
        keyCode = eventObj.keyCode
    }
    else {
        keyCode = eventObj.which
    }

    if (keyCode == 13) {
        obj.focus();
    }
}