﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{
    public partial class GR_MainMenu : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
         
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                showMenu();
            }
        }

        private void showMenu()
        {
            var permission = Convert.ToString(Session["Permission"]);
            var list = permission.Split(',').ToList();


            if (list.Where(o => o == "525").Count() > 0)
            {
                tr_grprod.Visible = true;
            }

            if (list.Where(o => o == "VIEWSTOCK").Count() > 0)
            {
                tr_viewstock.Visible = true;
            }
        }

        protected void Bt_gr_prod_Click(object sender, EventArgs e)
        {
            Response.Redirect("525/525_ScanID.aspx");

        }
        protected void Bt_gr_outsource_Click(object sender, EventArgs e)
        {
            Response.Redirect("101/101_ScanPO.aspx");

        }
        protected void Bt_viewStock_Click(object sender, EventArgs e)
        {
            Response.Redirect("525/525_ViewStock.aspx");

        }

        protected void Bt_mainmenu_Click(object sender, EventArgs e)
        {
            Response.Redirect("../main_menu.aspx");

        }
    }
}