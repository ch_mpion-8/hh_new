﻿<%@ Register TagPrefix="uc1" TagName="header_control" Src="../../header_control.ascx"   %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="525_ScanDetail.aspx.cs" Inherits="hh_siamkraft._525_ScanDetail" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>525_ScanDetail</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C# .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"/>
		<LINK href="../../css/style.css" type="text/css" rel="stylesheet"/>
        <script src="../../javascript/check_func.js"></script>
	    <style type="text/css">
            .style1
            {
                height: 25px;
            }
        </style>
</head>
	<body style="MARGIN: 1px" MS_POSITIONING="GridLayout">
		<form id="myForm" method="post" name="myForm" runat="server">
			<table class="table_main" border="0" cellSpacing="0" cellPadding="0" width="225">
				<tr>
					<td class="headtable"><uc1:header_control id="Header_control1" runat="server"></uc1:header_control></td>
				</tr>
				<tr>
					<td class="headtable" align="center"><font color="#ffffff"><FONT color="#ffffff"><FONT color="#ffffff">Goods 
									Receipt -&nbsp;Products</FONT></FONT></font></td>
				</tr>
				<tr>
					<td class="smallerText" Runat="server" id="Td_msg"><asp:label id="Lb_msg" Runat="server"></asp:label></td>
				</tr>
				<tr>
					<td>
						<table border="0" cellSpacing="0" cellPadding="0" width="100%">
							<tr>
								<td class="normalText" align="left">ID</td>
								<td class="normalText" align="left">
									<asp:label id="Lb_id" runat="server"></asp:label></td>
							</tr>
							<tr>
								<td class="normalText" align="left">Batch</td>
								<td align="left">
									<asp:textbox style="Z-INDEX: 0" id="Tb_batch" runat="server" AutoPostBack="True" CssClass="inputTag"
										MaxLength="10"  OnTextChanged="Tb_batch_TextChanged"></asp:textbox></td>
							</tr>
							<tr>
								<td class="normalText" align="left">Qty</td>
								<td align="left">
									<asp:textbox style="Z-INDEX: 0" id="Tb_qty" runat="server" AutoPostBack="True" CssClass="inputTag"
										MaxLength="6" OnTextChanged="Tb_qty_TextChanged"></asp:textbox></td>
							</tr>
							<tr>
								<td class="normalText" align="left">Material</td>
								<td align="left">
									<asp:textbox style="Z-INDEX: 0" id="Tb_material" runat="server" AutoPostBack="True" CssClass="inputTag"
										MaxLength="18" OnTextChanged="Tb_material_TextChanged"></asp:textbox>&nbsp;
                                    
							</tr>
                            <tr>
								<td class="normalText" align="left">Container No.</td>
								<td align="left">
									<asp:label id="Lb_Container" runat="server"></asp:label></td>
                                    
							</tr>
							
							<tr style="PADDING-BOTTOM: 4px; PADDING-LEFT: 2px; PADDING-RIGHT: 2px; PADDING-TOP: 4px"
					class="tr">
								<td class="normalText" colSpan="2" noWrap align="center">
                                    <asp:Button runat="server" ID="Btn_Save" CssClass="buttonTag" Text="Save" OnClick="Btn_Save_Click" />
								</td>
							</TR>
						</table>
						<asp:datagrid id="Dg_detail" runat="server" Width="100%" CellSpacing="1" AutoGenerateColumns="False"
							BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" BackColor="White" CellPadding="1">
							<FooterStyle ForeColor="#4A3C8C" BackColor="#B5C7DE"></FooterStyle>
							<SelectedItemStyle Font-Bold="True" ForeColor="#F7F7F7" BackColor="#738A9C"></SelectedItemStyle>
							<AlternatingItemStyle BackColor="#F7F7F7"></AlternatingItemStyle>
							<ItemStyle ForeColor="#4A3C8C" BackColor="#E7E7FF"></ItemStyle>
							<HeaderStyle Font-Bold="True" ForeColor="#F7F7F7" BackColor="#4A3C8C"></HeaderStyle>
							<Columns>
								<asp:BoundColumn DataField="Batch" HeaderText="Batch">
									<HeaderStyle HorizontalAlign="Center" ForeColor="White" VerticalAlign="Middle"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="Qty" HeaderText="Qty" DataFormatString="{0:0.000}">
									<HeaderStyle HorizontalAlign="Center" ForeColor="White" VerticalAlign="Middle"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
								</asp:BoundColumn>
								<asp:BoundColumn DataField="Material" HeaderText="Material">
									<HeaderStyle HorizontalAlign="Center" ForeColor="White" VerticalAlign="Middle"></HeaderStyle>
									<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
								</asp:BoundColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Right" ForeColor="#4A3C8C" BackColor="#E7E7FF" Mode="NumericPages"></PagerStyle>
						</asp:datagrid>&nbsp;&nbsp;
						<asp:label id="Label1" runat="server" CssClass="normalText">RecordCount :</asp:label>&nbsp;&nbsp;
						<asp:label id="Lb_Count" runat="server" CssClass="normalText" Width="32px"></asp:label></td>
				</tr>
				<tr style="PADDING-BOTTOM: 2px; PADDING-LEFT: 2px; PADDING-RIGHT: 2px; PADDING-TOP: 2px"
					class="tr">
					<td align="center">
						<asp:button id="BT_Send2SAP" runat="server" CssClass="buttonTag" Text="Send To SAP" Width="80px" OnClick="BT_Send2SAP_Click"></asp:button>
						<asp:button id="Bt_Mainmenu" runat="server" CssClass="buttonTag" Text="Back" Width="55px" OnClick="Bt_Mainmenu_Click"></asp:button>
						<asp:button id="Bt_Clear" runat="server" CssClass="buttonTag" Text="Clear" Width="50px" OnClick="Bt_Clear_Click"></asp:button>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>