﻿using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{

    public partial class GR_MatDoc : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();
        protected void Page_Init(object sender, EventArgs e)
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {

             if (Session["user_id"] == null)
            {
                Response.Redirect("../../index.aspx", false);
                return;
            }
            if (!(Page.IsPostBack))
            {
                if (Session["matDoc"] != null)
                {
                    this.Lb_info.Text = "Message";
                    this.Label1.Text = Convert.ToString(Session["matDoc"]);
                }
                else if (Session["error"] != null)
                {
                    this.Lb_info.Text = "ERROR";
                    this.Label1.Text =Convert.ToString( Session["error"]);
                }
            }
        }

        protected void Bt_ok_Click(object sender, EventArgs e)
        {
           if (Session["matDoc"] != null)
            {
                Session.Remove("matDoc");
            }
             else
            {
                Session.Remove("error");
            }
            Response.Redirect("../GR_MainMenu.aspx", false);
        }
    }
}