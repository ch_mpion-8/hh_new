﻿<%@ Register TagPrefix="uc1" TagName="header_control" Src="../../header_control.ascx"   %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="525_QtyDiff.aspx.cs" Inherits="hh_siamkraft._525_QtyDiff" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>525_QtyDiff</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C# .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"/>
		<LINK href="../../css/style.css" type="text/css" rel="stylesheet"/>
	    <style type="text/css">
            .style1
            {
                height: 25px;
            }
        </style>
</head>
		<body style="MARGIN:1px" MS_POSITIONING="GridLayout">
		<form id="myForm" name="myForm" method="post" runat="server">
			<table class="table_main" cellSpacing="0" cellPadding="0" width="225" border="0">
				<tr>
					<td class="headtable"><uc1:header_control id="Header_control1" runat="server"></uc1:header_control></td>
				</tr>
				<tr>
					<td class="headtable" align="center"><font color="#ffffff"><FONT color="#ffffff">Goods 
								Receipt -&nbsp;Products </FONT></font>
					</td>
				</tr>
				<tr>
					<td>
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="normalText" align="left">ID</td>
								<td class="normalText" align="left"><IMG hspace="1" src="../../images/red_arrow.gif" align="absMiddle">&nbsp;
									<asp:Label id="lb_ID" runat="server" Width="8px" CssClass="normalText"></asp:Label></td>
							</tr>
							<tr>
								<td class="normalText" align="left">Batch</td>
								<td class="normalText" align="left"><IMG hspace="1" src="../../images/red_arrow.gif" align="absMiddle">&nbsp;
									<asp:Label id="lb_batch" runat="server" CssClass="normalText"></asp:Label></td>
							</tr>
							<tr>
								<td class="normalText" align="left">Qty</td>
								<td class="normalText" align="left"><IMG hspace="1" src="../../images/red_arrow.gif" align="absMiddle">&nbsp;
									<asp:Label id="lb_qty" runat="server" CssClass="normalText"></asp:Label></td>
							</tr>
							<tr>
								<td class="normalText" align="left">Material</td>
								<td class="normalText" align="left"><IMG hspace="1" src="../../images/red_arrow.gif" align="absMiddle">&nbsp;
									<asp:Label id="lb_material" runat="server" CssClass="normalText"></asp:Label></td>
							</tr>
						</table>
						<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD class="normalText" nowrap><img src="../../images/question.gif" align="absMiddle">&nbsp;<asp:Label ID="Lb_info" Runat="server" ForeColor="Red" CssClass="normalText">Error weight diff Do you want save data ?</asp:Label></FONT></TD>
							</TR>
							<TR>
								<TD class="normalText">&nbsp;
									<asp:radiobutton id="RdoNo" runat="server" ForeColor="Black" Text="No" GroupName="confirm"></asp:radiobutton>
									<asp:radiobutton id="RdoYes" runat="server" ForeColor="Black" Text="Yes" GroupName="confirm"></asp:radiobutton></TD>
							</TR>
						</TABLE>
						&nbsp;&nbsp;&nbsp; </FONT>
					</td>
				</tr>
				<TR class="tr" style="PADDING-BOTTOM: 2px; PADDING-LEFT: 2px; PADDING-RIGHT: 2px; PADDING-TOP: 2px">
					<TD align="left">
						<P><asp:button id="BT_Save" runat="server" CssClass="buttonTag" Width="50px" Text="OK" OnClick="BT_Save_Click"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</P>
					</TD>
				</TR>
			</table>
			</TR></TABLE></form>
	</body>
</html>
