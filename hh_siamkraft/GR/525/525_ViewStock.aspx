﻿<%@ Register TagPrefix="uc1" TagName="header_control" Src="../../header_control.ascx"   %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="525_ViewStock.aspx.cs" Inherits="hh_siamkraft._525_ViewStock" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>525_ViewStock</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C# .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"/>
		<LINK href="../../css/style.css" type="text/css" rel="stylesheet"/>
	    <style type="text/css">
            .style1
            {
                height: 25px;
            }
        </style>
</head>
<body style="MARGIN:1px" MS_POSITIONING="GridLayout">
		<form id="myForm" method="post" runat="server">
			<table class="table_main" cellSpacing="0" cellPadding="0" width="225" border="0">
				<tr>
					<td align="left" class="headtable"><uc1:header_control id="Header_control1" runat="server"></uc1:header_control></td>
				</tr>
				<tr>
					<td class="headtable" align="center"><font color="#ffffff">View Stock 525</font>
					</td>
				</tr>
				<tr>
					<td>
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TBODY>
								<tr>
									<td class="normalText" align="left">Storage</td>
									<td class="normalText" align="left"><IMG hspace="1" src="../../images/red_arrow.gif" align="absMiddle">&nbsp;
										<asp:dropdownlist id="drp_PM" runat="server" AutoPostBack="True" CssClass="colorBox" Width="100px" OnSelectedIndexChanged="drp_PM_SelectedIndexChanged"></asp:dropdownlist></td>
								</tr>
								<tr>
									<td class="normalText" align="left"><asp:label id="Label1" runat="server">Count :</asp:label>&nbsp;<asp:label id="lb_Count" runat="server"></asp:label></td>
								</tr>
								<tr>
									<td align="left" colSpan="2">
										<asp:datagrid id="Dg_detail" runat="server" Width="100%" AutoGenerateColumns="False" CellSpacing="1"
											CellPadding="1" BackColor="White" BorderWidth="1px" BorderStyle="None" BorderColor="#E7E7FF"
											AllowPaging="True" OnPageIndexChanged="Dg_detail_PageIndexChanged">
											<FooterStyle ForeColor="#4A3C8C" BackColor="#B5C7DE"></FooterStyle>
											<SelectedItemStyle Font-Bold="True" ForeColor="#F7F7F7" BackColor="#738A9C"></SelectedItemStyle>
											<AlternatingItemStyle BackColor="#F7F7F7"></AlternatingItemStyle>
											<ItemStyle ForeColor="#4A3C8C" BackColor="#E7E7FF"></ItemStyle>
											<HeaderStyle Font-Bold="True" ForeColor="#F7F7F7" BackColor="#4A3C8C"></HeaderStyle>
											<Columns>
												<asp:BoundColumn DataField="batch" HeaderText="Batch">
													<HeaderStyle HorizontalAlign="Center" ForeColor="White" VerticalAlign="Middle"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="qty" HeaderText="Qty" DataFormatString="{0:0.000}">
													<HeaderStyle HorizontalAlign="Center" ForeColor="White" VerticalAlign="Middle"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="material" HeaderText="Material">
													<HeaderStyle HorizontalAlign="Center" ForeColor="White" VerticalAlign="Middle"></HeaderStyle>
													<ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
												</asp:BoundColumn>
											</Columns>
											<PagerStyle Font-Underline="True" Font-Bold="True" HorizontalAlign="Left" ForeColor="#4A3C8C"
												BackColor="#E7E7FF" Mode="NumericPages"></PagerStyle>
										</asp:datagrid></td>
								</tr>
							</TBODY>
						</table>
					</td>
				</tr>
				<tr class="tr" style="PADDING-BOTTOM: 2px; PADDING-LEFT: 2px; PADDING-RIGHT: 2px; PADDING-TOP: 2px">
					<td align="center">
                        <asp:button id="Bt_Ok" runat="server" CssClass="buttonTag" Width="56px" Text="Back" OnClick="Bt_Ok_Click"></asp:button>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
