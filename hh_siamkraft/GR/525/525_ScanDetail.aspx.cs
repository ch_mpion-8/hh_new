﻿using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{

    public partial class _525_ScanDetail : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();
        protected void Page_Init(object sender, EventArgs e)
        {

        }

        protected void Tb_batch_Key(object sender, EventArgs e)
        {
            script_func.script_focus(this.Tb_qty.ID, Page);
        }
        protected void Tb_qty_Key(object sender, EventArgs e)
        {
            script_func.script_focus(this.Tb_material.ID, Page);
        }
        protected void Tb_material_Key(object sender, EventArgs e)
        {
            SaveScanBarcode();
        }

        #region Form_Load
        protected void Page_Load(object sender, EventArgs e)
        {
            string target = Request["__EVENTTARGET"];
            if(target == "Tb_batch")
            {
                Tb_batch_Key(sender, e);
            }
            else if (target == "Tb_qty")
            {
                Tb_qty_Key(sender, e);
            }
            else if (target == "Tb_material")
            {
                Tb_material_Key(sender, e);
            }

            if (!(Page.IsPostBack))
            {
                if (Session["gr_525"] != null)
                { 
                    this.Lb_msg.Text = Convert.ToString(Session["gr_525"]);
                }
            

                clear();
                script_func.script_focus(this.Tb_batch.ID, Page);
                this.Tb_qty.Attributes.Add("onkeypress", "return keyCheck(event,this)");
                string a = Convert.ToString(Session["GrId"]);
                if (a == "" )//ไม่ส่งค่า  ID
                { 

                    var connect = Convert.ToString(Session["Connection_String"]);
                    using (var session = new SkRepositorySession(connect))
                    {
                        var repo = new GRRepository(session);
                        var result = repo.GetRunningGRID();
                        if (result.Value == 0)
                        {
                            script_func.script_focus("ID Not found !!", Page);
                        }
                        else
                        {
                            Lb_id.Text = result.Value.ToString();
                        }
                    }
                }
                else
                {
                    Lb_id.Text = Convert.ToString(Session["GrId"]);
                    insertGrid();
                }
            }

        }
        #endregion

        private void clear()
        {
            Tb_batch.Text = "";
            Tb_qty.Text = "";
            Tb_material.Text = "";
        }

        private void insertGrid()
        {
            var connect = Convert.ToString(Session["Connection_String"]);
            using (var session = new SkRepositorySession(connect))
            {
                var repo = new GRRepository(session);
                var result = repo.GetGRProd(Lb_id.Text);

                Dg_detail.DataSource = result.Value;
                Dg_detail.DataBind();
                Lb_Count.Text = Dg_detail.Items.Count.ToString();
            }
        }

        protected void Bt_Clear_Click(object sender, EventArgs e)
        {
            clear();
            //this.clear_str(this);
            script_func.script_focus(this.Tb_batch.ID, Page);
        }

        private void clear_str(Control tmp)
        {
            foreach(Control con in tmp.Controls)
            {
                if (con.GetType() == typeof( HtmlControl))
                {
                    //clear_str(con)
                }
                else if (con.GetType() == typeof(TextBox))
                {
                    ((TextBox)con).Text = string.Empty;
                }
            }
        }

        private void ClearScreen()
        {
            this.Tb_batch.Text = string.Empty;
            this.Tb_qty.Text = string.Empty;
            this.Tb_material.Text = string.Empty;
        }

        private void check_Value()
        {
            if (Tb_batch.Text == "")
            {
                script_func.script_focus("txtBatch", Page);
            }   
            else if (Tb_batch.Text != "" && Tb_qty.Text == "")
            {
                if (Tb_batch.Text.Length == 10) script_func.script_focus(this.Tb_qty.ID, Page);
                else
                {
                    script_func.script_alert("digit batch <> 10", Page);
                    Tb_batch.Text = "";
                    script_func.script_focus(this.Tb_batch.ID, Page);
                }
            }
            else if (Tb_batch.Text != "" && Tb_qty.Text != "" && Tb_material.Text == "")
            {
                script_func.script_focus(this.Tb_batch.ID, Page);
            }
            else
            {
                if (Tb_material.Text.Length != 18)
                {
                    script_func.script_alert("digit material <> 18", Page);
                    script_func.script_focus(this.Tb_material.ID, Page);
                }
            }
        }

        protected void Tb_batch_TextChanged(object sender, EventArgs e)
        {
            script_func.script_focus(this.Tb_qty.ID, Page);
            GetBatchDetail();
        }

        protected void Tb_qty_TextChanged(object sender, EventArgs e)
        {
            if(Tb_qty.Text.IndexOf(".")<0)
            {
                Tb_qty.Text = (Convert.ToInt32(Tb_qty.Text.Trim()) / 1000.0).ToString();
            }
            script_func.script_focus(this.Tb_material.ID, Page);
        }

        protected void Tb_material_TextChanged(object sender, EventArgs e)
        {
            SaveScanBarcode();
        }

        private void GetBatchDetail()
        {
            this.Lb_msg.Text = string.Empty;
            this.Td_msg.Style.Value = "";

            if (this.Tb_batch.Text == "")
            {
                script_func.script_focus(this.Tb_batch.ID, Page);
                return;
            }
            else if (this.Tb_batch.Text.Length != 10)
            {
                script_func.script_alert("Scan 'Batch' Incorrect !!", Page);
                script_func.script_focus(this.Tb_batch.ID, Page);
                script_func.script_select(this.Tb_batch.ID, Page);
                return;
            }
            
            var connect = Convert.ToString(Session["Connection_String"]);
            using (var session3 = new SkRepositorySession(connect))
            {
                var repo3 = new GRRepository(session3);
                var result3 = repo3.Get521(this.Tb_batch.Text);
                if (result3.Value.Count > 0)
                {
                    //script_func.script_alert("Batch -> " + this.Tb_batch.Text.ToUpper() + "\r\n Receive Completed", Page);
                    this.Lb_msg.Text = "<font color=red><b>ERROR !!</b></font> Batch -> " + this.Tb_batch.Text.ToUpper() + "\r\n Receive Completed";
                    //this.Td_msg.Style.Value = "border: 2px solid yellow";
                    this.Td_msg.Style.Value = "background-color: yellow";
                    this.ClearScreen();
                    script_func.script_focus(this.Lb_msg.ID, Page);
                    script_func.script_focus(this.Tb_batch.ID, Page);
                    script_func.script_select(this.Tb_batch.ID, Page);
                    return;
                }
            }


            using (var session2 = new SkRepositorySession(connect))
            {
                var repo2 = new GRRepository(session2);
                var result2 = repo2.GetBatchDup(this.Tb_batch.Text);
                if (result2.Value.Count > 0)
                {
                    var data = result2.Value.FirstOrDefault();
                    this.Lb_msg.Text = "<font color=red><b>ERROR !!</b></font> Batch-> " + Tb_batch.Text.ToUpper() + "\r\n found in ID-> " + data.GR_ID + "\r\n and duplicated in this date";
                    //this.Td_msg.Style.Value = "border: 2px solid yellow";
                    this.Td_msg.Style.Value = "background-color: yellow";
                    this.ClearScreen();
                    //script_func.script_alert("Batch-> " + Tb_batch.Text.ToUpper() + "\r\n found in ID-> " + data.GR_ID + "\r\n and duplicated in this date", Page);
                    script_func.script_focus(this.Lb_msg.ID, Page);
                    script_func.script_focus(this.Tb_batch.ID, Page);
                    script_func.script_select(this.Tb_batch.ID, Page);
                    return;
                }
            }

            using (var session = new SkRepositorySession(connect))
            {
                var repo = new GRRepository(session);
                var result = repo.GetBatchDetail(Tb_batch.Text);
                if (result.Value != null)
                {
                    var dt = result.Value;
                    Tb_qty.Text = Convert.ToString(dt.Weight / 1000);
                    Tb_material.Text = dt.Material_number;
                    Lb_Container.Text = dt.Container_Item;
                }
                else
                {
                    Tb_qty.Text = String.Empty;
                    Tb_material.Text = String.Empty;
                    Lb_Container.Text = String.Empty;
                }
            }

        }
        private void SaveScanBarcode()
        {
            try
            {
                
                //-----------------Check Batch ซ้ำใน ID เดียวกัน
                var connect = Convert.ToString(Session["Connection_String"]);
                

                //-----------------Check Batch ซ้ำ ในวันเดียวกันและส่งไป sap สำเร็จ
                using (var session2 = new SkRepositorySession(connect))
                {
                   var repo2 = new GRRepository(session2);
                    var result2 = repo2.GetBatchDup(this.Tb_batch.Text);
                    if (result2.Value.Count > 0)
                    {
                        var data = result2.Value.FirstOrDefault();
                        //string[] strAry = (result2.Value.FirstOrDefault().Return_Msg.ToString()).Split(',');
                        //if (strAry.Length > 1)
                        //{
                        //    if (strAry[1].ToString().Trim() == "S")
                        //    {
                        //script_func.script_alert("Batch -> " + Tb_batch.Text.ToUpper() + "\r\n ได้ส่งไป SAP แล้ว ที่ MatDoc -> " + "\r\n " + strAry[4] + "", Page);
                        this.Lb_msg.Text = "<font color=red><b>ERROR !!</b></font> Batch-> " + Tb_batch.Text.ToUpper() + "\r\n found in ID-> " + data.GR_ID + "\r\n and duplicated in this date";
                        //script_func.script_alert("Batch-> " + Tb_batch.Text.ToUpper() + "\r\n found in ID-> " + data.GR_ID + "\r\n and duplicated in this date", Page);
                        //this.Td_msg.Style.Value = "border: 2px solid yellow";
                        script_func.script_focus(this.Lb_msg.ID, Page);
                        this.Td_msg.Style.Value = "background-color: yellow";
                        this.ClearScreen();
                        return;
                        //    }
                        //}
                    }
                }
                //---------------------------------------

                using (var session = new SkRepositorySession(connect))
                {
                    var repo = new GRRepository(session);
                    var result = repo.CheckDupBatch(this.Tb_batch.Text, this.Lb_id.Text);
                    if (result.Value == true)
                    {
                        Session["GrId"] = this.Lb_id.Text;
                        Session["Batch525"] = this.Tb_batch.Text;
                        Response.Redirect("ConfirmDelete525.aspx");
                    }
                }


                if (this.Tb_qty.Text == "")
                {
                    script_func.script_focus(this.Tb_qty.ID, Page);
                    return;
                }

                if (this.Tb_material.Text == "")
                {
                    script_func.script_focus(this.Tb_material.ID, Page);
                    return;
                 }

                //have data in SAP_GR filter by Material, Batch, Qty and MovementType in 525,000
                int? container_No = 0;
                if(Lb_Container.Text.Trim().Length > 0)
                {
                    try
                    {
                        container_No = Convert.ToInt32(Lb_Container.Text);
                    }
                    catch(Exception ex)
                    {
                        container_No = 0;
                    }
                    
                }
                string matType = string.Empty;
                matType = "KRAFT";
                using (var session3 = new SkRepositorySession(connect))
                {
                    var repo3 = new GRRepository(session3);
                    var result3 = repo3.InsertGRProd(this.Lb_id.Text, this.Tb_batch.Text.ToUpper(), this.Tb_qty.Text.Trim(), this.Tb_material.Text.ToUpper(), container_No,
                       Convert.ToString(Session["UserLogin_WHID"])
                       , Convert.ToString(Session["UserLogin_WHCode"]), matType, Tb_batch.Text.Substring(0, 1), Convert.ToString(Session["user_id"]));
                    if (result3.Value == true)
                    {
                        this.Lb_msg.Text = "<font color=lightgreen><b>SUCCESS !!</b></font> save data on success.";
                        this.insertGrid();
                        this.ClearScreen();
                        script_func.script_focus(this.Tb_batch.ClientID, Page);
                        return;
                    }
                }

            }
            catch (Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
            }
        }

        protected void BT_Send2SAP_Click(object sender, EventArgs e)
        {
            string msg = "";
            var connect = Convert.ToString(Session["Connection_String"]);
            using (var session = new SkRepositorySession(connect))
            {
                var repo = new GRRepository(session);
                var result = repo.GetGRProd(this.Lb_id.Text);
                if (result.Value.Count() > 0)
                {
                    //วนลูป Update MMT ตาม ID ใน SAP_GR
                    for (int i = 0; i < result.Value.Count(); i++)
                    {
                        repo.UpdateSapGrMovementType(result.Value[i].Batch.ToString());
                        repo.UpdateGrProductStatus(result.Value[i].Batch.ToString());
                        clear();
                    }
                    //ปิด if Loop Update MMT
                }
                else
                {
                    script_func.script_focus(this.Tb_batch.ID, Page);
                    script_func.script_alert("No Record Send to SAP", Page);
                    return;
                }

                Session["gr_525"] = "<font color=lightgreen><b>SUCCESS !!</b></font> send data to SAP on success.";
                Response.Redirect("525_ScanID.aspx", false);

            }
        }

        protected void Bt_Mainmenu_Click(object sender, EventArgs e)
        {
            Session.Remove("gr_525");
            Response.Redirect("525_ScanID.aspx");
        }

        protected void Btn_Save_Click(object sender, EventArgs e)
        {
            SaveScanBarcode();
        }
    }
}