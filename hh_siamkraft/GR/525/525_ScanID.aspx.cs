﻿using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{
    
    public partial class _525_ScanID : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();
        protected void Page_Init(object sender, EventArgs e)
        {
         
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(Page.IsPostBack))
            {
                this.Tb_id.Attributes.Add("onkeypress", "return keyCheck(event,this)");
                script_func.script_focus(this.Tb_id.ClientID, Page);
                string msg = Request.Params["msg"];
                if (msg != "")
                { 
                    this.lb_msg.Text = msg;
                }
                else
                {
                    this.lb_msg.Visible = true;
                }
                    

                if (Session["gr_525"] != null)
                {
                    this.lb_msg.Text = Convert.ToString(Session["gr_525"]);
                }
                    
            }

            if (this.Tb_id.Text == "")
            {
                script_func.script_focus(this.Tb_id.ClientID, Page);
            }
            else
            { 
                Bt_Ok_Click(sender, e);
            }
        }

        protected void Bt_Ok_Click(object sender, EventArgs e)
        {
            GetGRID();
        }

        private void GetGRID()
        {
            try
            {
                if (Tb_id.Text != "")
                {
                    var connect = Convert.ToString(Session["Connection_String"]);
                    using (var session = new SkRepositorySession(connect))
                    {
                        var repo = new GRRepository(session);
                        var result = repo.GetGRID(Tb_id.Text);
                        if (result.Value)
                        {
                            Session["GrId"] = Tb_id.Text;
                        }
                        else
                        {
                            script_func.script_alert("Invalid ID", Page);
                            Tb_id.Text = "";
                            return;
                        }
                    }
                }
                else
                {
                    var connect = Convert.ToString(Session["Connection_String"]);
                    using (var session = new SkRepositorySession(connect))
                    {
                        var repo = new GRRepository(session);
                        Session["GrId"] = repo.GetNewGRID().Value;
                    }
                }

                Session.Remove("gr_525");
                Response.Redirect("525_ScanDetail.aspx");
            }
            catch (Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
            }
        }

        protected void Bt_Mainmenu_Click(object sender, EventArgs e)
        {
            Session.Remove("gr_525");
            Response.Redirect("../GR_MainMenu.aspx");
        }

        protected void Bt_viewid_Click(object sender, EventArgs e)
        {
            try
            {
                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession(connect))
                {
                    var repo = new GRRepository(session);
                    var result = repo.GetRunningGRID();
                    if ( result.Value == 0)
                    {
                        script_func.script_focus("ID Not found !!", Page);
                    }
                    else
                    {

                        Lb_id.Text = result.Value.ToString();
                    }
 
                }
            }
            catch (Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
            }
        }
    }
}