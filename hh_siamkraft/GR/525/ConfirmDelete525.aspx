﻿<%@ Register TagPrefix="uc1" TagName="header_control" Src="../../header_control.ascx"   %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConfirmDelete525.aspx.cs" Inherits="hh_siamkraft.ConfirmDelete525" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>ConfirmDelete525</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C# .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"/>
		<LINK href="../../css/style.css" type="text/css" rel="stylesheet"/>
        <script src="../javascript/check_func.js"></script>

	    <style type="text/css">
            .style1
            {
                height: 25px;
            }
        </style>
</head>
<body MS_POSITIONING="GridLayout" style="MARGIN:1px">
		<form id="myForm" method="post" runat="server">
			<table class="table_main" cellSpacing="0" cellPadding="0" width="225" border="0">
				<TBODY>
					<tr>
						<td class="headtable"><uc1:header_control id="Header_control2" runat="server"></uc1:header_control></td>
					</tr>
					<tr>
						<td class="headtable" align="center"><font color="#ffffff"><FONT color="#ffffff">Confirm 
									Delete</FONT></font>
						</td>
					</tr>
					<tr>
						<td>
							<table cellSpacing="0" cellPadding="0" width="100%" border="0">
								<tr>
									<td class="normalText" style="WIDTH: 92px" align="left">ID</td>
									<td align="left"><asp:label id="lb_ID" runat="server" CssClass="normalText" Width="8px"></asp:label></td>
								</tr>
							</table>
							<table cellSpacing="0" cellPadding="0" width="100%" border="0">
								<tr>
									<td class="normalText" style="WIDTH: 92px" align="left">Batch</td>
									<td align="left"><asp:label id="lb_batch" runat="server" CssClass="normalText"></asp:label></td>
								</tr>
							</table>
							<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
								<TR>
									<TD align="left" class="normalText" nowrap>&nbsp;<img src="../../images/question.gif" align="absMiddle">&nbsp;<asp:Label ID="Lb_info" Runat="server" ForeColor="red" CssClass="normalText">Batch duplicated , Do you want delete data ?</asp:Label></TD>
								</TR>
								<TR height="10">
									<TD class="normalText" align="center">&nbsp;
										<asp:radiobutton id="RdoNo" runat="server" ForeColor="Black" GroupName="confirm" Text="No"></asp:radiobutton><asp:radiobutton id="RdoYes" runat="server" ForeColor="Black" GroupName="confirm" Text="Yes"></asp:radiobutton></TD>
								</TR>
							</TABLE>
						</td>
					</tr>
					<TR class="tr" style="PADDING-RIGHT: 2px; PADDING-LEFT: 2px; PADDING-BOTTOM: 2px; PADDING-TOP: 2px">
						<TD align="center">
							<P><asp:button id="BT_Save" runat="server" CssClass="buttonTag" Width="50px" Text="OK" OnClick="BT_Save_Click"></asp:button></P>
						</TD>
					</TR>
				</TBODY>
			</table>

		</form>
	</body>

</html>
