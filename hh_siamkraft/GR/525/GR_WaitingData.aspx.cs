﻿using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{

    public partial class GR_WaitingData : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();

        protected void Page_Init(object sender, EventArgs e)
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if( Session["user_id"] == null)
            {
                Session.RemoveAll();
                Response.Redirect("../../index.aspx", false);
                return;
            }

            if (!(Page.IsPostBack))
            {
                this.InitializeForm();
            }
        }

        private void InitializeForm()
        {
            try
            {
                if (Session["Record_ID_GR"] != null)
                {
                    lblRecordID.Text = Convert.ToString(Session["Record_ID_GR"]);
                }
                else
                {
                    Response.Redirect("../GR_MainMenu.aspx");
                    return;
                }
                this.GetProcessTimeout();

                if (Session["refresh"] == null) Session["refresh"] = 1;
                if (Convert.ToInt32(Session["refresh"]) > Convert.ToInt32(Session["time_out"]))
                {
                    Session["Record_ID"] = lblRecordID.Text;
                    Session.Remove("time_out");
                    Session.Remove("refresh");
                    Response.Redirect("GR_TimeOut.aspx", false);
                    return;
                }

                this.CheckResultControl();
            }
            catch (Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
            }
        }

        private void GetProcessTimeout()
        {
            try
            {
                if (Session["time_out"] == null)
                {
                    var connect = Convert.ToString(Session["Connection_String"]);
                    using (var session = new SkRepositorySession(connect))
                    {
                        var repo = new GRRepository(session);
                        var result = repo.SelectTimeoutForSAP("GT");
                        int timeOut = result.Value;
                        if (timeOut <= 0) timeOut = 10;
                        Session["time_out"] = timeOut;
                    }
                }
            }
            catch (Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
            }
        }

        private void CheckResultControl()
        {
            try
            {
                bool result;

                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession(connect))
                {
                    var repo = new GRRepository(session);
                    var ctl = repo.SelectResultControl(Convert.ToInt32(lblRecordID.Text));
                    if (ctl.Value.Count() > 0)
                    {
                        if (Convert.ToString(ctl.Value[0].Return_Code) == "N")
                        {
                            //not found and refresh page on 5 seconds
                            Session["refresh"] = Convert.ToInt32(Session["refresh"]) + 1;
                            Response.Write("<meta http-equiv=refresh content=5 url='GR_WaitingData.aspx'");
                        }
                        else
                        {
                            if (Convert.ToString(ctl.Value[0].Return_Code) == "00")
                            {
                                //Complete
                                var updComplete = repo.UpdateStatusCompleteSAP_GR(Convert.ToInt32(lblRecordID.Text), Convert.ToString(ctl.Value[0].Return_Msg));
                                this.RemoveSession();
                                Session["matDoc"] = ctl.Value[0].Return_Msg.ToString();
                                Response.Redirect("GR_Matdoc.aspx", false);
                            }
                            else
                            {
                                //Error from SAP
                                var updComplete = repo.UpdateStatusErrorSAP_GR(Convert.ToInt32(lblRecordID.Text));
                                Session["error"] = ctl.Value[0].Return_Msg.ToString();
                                Response.Redirect("GR_Matdoc.aspx", false);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void RemoveSession()
        {
            Session.Remove("time_out");
            Session.Remove("refresh");
            Session.Remove("Record_ID");
            Session.Remove("MovementType");
            Session.Remove("ScanID");
        }

    }
}