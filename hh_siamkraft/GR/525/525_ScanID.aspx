﻿<%@ Register TagPrefix="uc1" TagName="header_control" Src="../../header_control.ascx"   %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="525_ScanID.aspx.cs" Inherits="hh_siamkraft._525_ScanID" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>MAIN MENU</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C# .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"/>
		<LINK href="../../css/style.css" type="text/css" rel="stylesheet"/>
        <script src="../../javascript/check_func.js"></script>
	    <style type="text/css">
            .style1
            {
                height: 25px;
            }
        </style>
</head>
<body leftMargin="1" topMargin="1" rightMargin="1" MS_POSITIONING="GridLayout">
		<form id="myForm" name="myForm" method="post" runat="server">
			<table class="table_main" cellSpacing="0" cellPadding="0" width="225" border="0">
				<tr>
					<td class="headtable"><uc1:header_control id="Header_control1" runat="server"></uc1:header_control></td>
				</tr>
				<tr>
					<td class="headtable" align="center"><font color="#ffffff">&nbsp;<FONT color="#ffffff"><FONT color="#ffffff">Goods 
									Receipt -&nbsp;Products</FONT></FONT> </font>
					</td>
				</tr>
				<tr height="3">
					<td align="left" class="normalText">
						<asp:Label id="lb_msg" runat="server"></asp:Label></td>
				</tr>
				<tr>
					<td>
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TBODY>
								<tr>
									<td class="normalText" align="left"><asp:button id="Bt_viewid" runat="server" Text="View ID" Width="60px" CssClass="buttonTag" OnClick="Bt_viewid_Click"></asp:button></td>
									<td class="normalText" align="left">
										<asp:label id="Lb_id" runat="server" Width="24px"></asp:label></td>
								</tr>
								<tr>
									<td class="normalText" align="left">KeyIn&nbsp;ID</td>
									<td class="normalText" align="left"><asp:textbox id="Tb_id" CssClass="inputTag" Runat="server" MaxLength="10"></asp:textbox></td>
								</tr>
							</TBODY>
						</table>
					</td>
				</tr>
				<tr class="tr" style="PADDING-BOTTOM: 2px; PADDING-LEFT: 2px; PADDING-RIGHT: 2px; PADDING-TOP: 2px">
					<td align="center"><asp:button id="Bt_Ok" runat="server" Text="OK" Width="50px" CssClass="buttonTag" OnClick="Bt_Ok_Click"></asp:button>&nbsp;
						<asp:button id="Bt_Mainmenu" runat="server" Text="GR Menu" Width="72px" CssClass="buttonTag" OnClick="Bt_Mainmenu_Click"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
			</table>
		</form>
	</body>
</html>
