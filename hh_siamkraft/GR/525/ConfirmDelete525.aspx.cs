﻿using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{

    public partial class ConfirmDelete525 : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();
        protected void Page_Init(object sender, EventArgs e)
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Put user code to initialize the page here
            if (!(Page.IsPostBack))
            {
                lb_ID.Text = Convert.ToString(Session["GrId"]);
                lb_batch.Text = Convert.ToString(Session["Batch525"]);
            }

        }

        protected void BT_Save_Click(object sender, EventArgs e)
        {
            if (RdoYes.Checked == true)
            {
                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession(connect))
                {
                    var repo = new GRRepository(session);
                    var result = repo.DeleteGRProd(lb_batch.Text, lb_ID.Text);

                    Session["Yes"] = 1;
                    Session["GrId"] = lb_ID.Text;
                    Response.Redirect("525_ScanDetail.aspx");
                }
            }

            if(RdoNo.Checked == true)
            {
                Session["GrId"] = lb_ID.Text;
                Response.Redirect("525_ScanDetail.aspx");
            }
        }
    }
}