﻿using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{

    public partial class _525_QtyDiff : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();
        protected void Page_Init(object sender, EventArgs e)
        {

        }

        #region Form_Load
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(Page.IsPostBack))
            {
                lb_ID.Text = Convert.ToString( Session["GrId"]);
                lb_batch.Text = Convert.ToString(Session["Batch"]);
                lb_qty.Text = Convert.ToString(Session["Qty"]);
                lb_material.Text = Convert.ToString(Session["Material"]);
                Lb_info.Text = Convert.ToString(Session["Info"]);
            }

        }

        #endregion

        protected void BT_Save_Click(object sender, EventArgs e)
        {
            if (RdoYes.Checked)
            {
                int container_No = 0;
                if (Convert.ToString(Session["ContainerNO"]) != "00" && Convert.ToString(Session["ContainerNO"]) != "")
                {
                    try
                    {
                        container_No = Convert.ToInt32(Convert.ToString(Session["ContainerNO"]).PadLeft(2, '0'));
                    }
                    catch(Exception ex)
                    {
                        container_No = 0;
                    }
                    
                }

                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession(connect))
                {
                    var repo = new GRRepository(session);
                    var result = repo.InsertGRProd(this.lb_ID.Text, this.lb_batch.Text.ToUpper(), this.lb_qty.Text.Trim(), this.lb_material.Text.ToUpper(), container_No,
                       Convert.ToString(Session["UserLogin_WHID"])
                       , Convert.ToString(Session["UserLogin_WHCode"])
                       , Convert.ToString(Session["MatType"]), "", Convert.ToString(Session["user_id"]));
                    if (result.Value == true)
                    {
                        Session["gr_525"] = "<font color=lightgreen><b>SUCCESS !!</b></font> save data on success.";
                        Session["Yes"] = 1;
                        Session["id"] = lb_ID.Text;
                        Response.Redirect("525_ScanDetail.aspx");
                    }

                }
            }
            if (RdoNo.Checked)
            {
                Response.Redirect("525_ScanDetail.aspx");
            }
        }
    }
}