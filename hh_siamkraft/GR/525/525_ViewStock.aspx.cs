﻿using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{
    
    public partial class _525_ViewStock : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();
        protected void Page_Init(object sender, EventArgs e)
        {
         
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(Page.IsPostBack))
            {
                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession())
                {
                    var repo = new GRRepository(session);
                    var result = repo.GetHHSloc(Convert.ToString(Session["UserLogin_WHCode"]));

                    if (result.Value != null && result.Value.Count() > 0)
                    {
                        foreach (var pm in result.Value.OrderBy(o => o.StorageLocation).ToList())
                        {
                            drp_PM.Items.Add(new ListItem(pm.StorageLocation, pm.StorageLocation));
                        }
                        drp_PM.Items.Insert(0, "--Select Data--");
                    }
                    else
                    {
                        drp_PM.Items.Insert(0, "Not found");
                    }

                    drp_PM.SelectedIndex = 0;
                }
            }
        }

        private void bindData()
        {
            var connect = Convert.ToString(Session["Connection_String"]);
            using (var session = new SkRepositorySession(connect))
            {
                var repo = new GRRepository(session);
                var result = repo.GetSapGr(this.drp_PM.SelectedItem.Text.Trim());

                if (result.Value.Count() > 0)
                {
                    this.Dg_detail.DataSource = result.Value;
                    this.Dg_detail.DataBind();
                    this.lb_Count.Text = result.Value.Count().ToString();
                }
                else
                {
                    this.Dg_detail.DataSource = null;
                    this.Dg_detail.DataBind();
                    this.lb_Count.Text = "0";
                }
            }
        }

        protected void drp_PM_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.drp_PM.SelectedIndex != 0 || this.drp_PM.SelectedItem.Text != "--Select Data--")
            {
                this.Dg_detail.CurrentPageIndex = 0;
                bindData();
            }
            else
            {
                script_func.script_focus(this.drp_PM.ClientID, Page);
                script_func.script_select(this.drp_PM.ClientID, Page);
            }
        }

        protected void Dg_detail_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            this.Dg_detail.CurrentPageIndex = e.NewPageIndex;
            bindData();
        }

        protected void Bt_Ok_Click(object sender, EventArgs e)
        {
            Response.Redirect("../GR_MainMenu.aspx");
        }
    }
}