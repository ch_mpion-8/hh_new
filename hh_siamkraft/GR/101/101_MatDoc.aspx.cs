﻿using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using hh_siamkraft.GR._101;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{

    public partial class _101_MatDoc : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();
        protected void Page_Init(object sender, EventArgs e)
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Put user code to initialize the page here
            if( Session["user_id"] == null)
            {
                Session.RemoveAll();
                Response.Redirect("../../index.aspx", false);
                return;
            }

            if (Session["matDoc"] == null)
            {
                Response.Redirect("101_ScanPO.aspx", false);
                return;
            }

            if (!(Page.IsPostBack))
            {
                this.Tb_msg.Text = Convert.ToString( Session["matDoc"]);
            }
        }

        protected void Bt_next_Click(object sender, EventArgs e)
        {
            Session.Remove("matDoc");
            Response.Redirect("101_ScanPO.aspx", false);
        }
    }
}