﻿using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using hh_siamkraft.GR._101;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{

    public partial class _101_ConfirmDelete : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();
        protected void Page_Init(object sender, EventArgs e)
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user_id"] == null)
            {
                Session.RemoveAll();
                Response.Redirect("../../index.aspx");
                return;
            }

            if (Session["obj_var"] == null)
            {
                Response.Redirect("101_ScanPO.aspx", false);
                return;
            }

            if (!(Page.IsPostBack))
            {
                var obj_var = new variable_101();
                obj_var = (variable_101)Session["obj_var"];
                Lb_po.Text = obj_var.str_po.Trim();
                obj_var = null;

            }

        }

        protected void Bt_ok_Click(object sender, EventArgs e)
        {
            if (this.Rb_yes.Checked == false && this.Rb_yes.Checked == false)
            {
                script_func.script_alert("Please Select 'yes/no'.", Page);
                return;
            }

            bool ret = false;
            if(this.Rb_yes.Checked)
            {
                //yes delete all
                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession(connect))
                {
                    //select data from HH_GR_Outsourec have not send to sap
                    var repo = new GRRepository(session);
                    var result = repo.DeleteOutSourceAll(this.Lb_po.Text.Trim());
                    if (result.Value) ret = true;
                    else
                    {
                        script_func.script_alert("Delete data on error", Page);
                        return;
                    }
                }
            }
            else
            {
                //keep data
                ret = true;
            }

            if(ret == true)
            {
                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession(connect))
                {
                    var repo = new GRRepository(session);
                    var result = repo.getID(Convert.ToString(Session["HH_ID"]), Convert.ToString(Session["user_id"]), this.Lb_po.Text.Trim());
                    Session["id_page"] = result.Value;
                }

                if (Convert.ToString(Session["id_page"]) != "0")
                {
                    var obj_var = new variable_101();
                    obj_var.str_id = Convert.ToString(Session["id_page"]);
                    obj_var.str_po = this.Lb_po.Text.Trim();

                    Session["obj_var"] = obj_var;
                    obj_var = null;
                    Response.Redirect("101_WaitData.aspx", false);
                }
                else
                {
                    script_func.script_alert("Get ID On Error", Page);
                    return;
                }
            }
        }
    }
}