﻿<%@ Register TagPrefix="uc1" TagName="header_control" Src="../../header_control.ascx"   %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="101_ScanDetail.aspx.cs" Inherits="hh_siamkraft._101_ScanDetail" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>101_ScanDetail</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C# .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"/>
		<LINK href="../../css/style.css" type="text/css" rel="stylesheet"/>
        <script src="../../javascript/check_func.js"></script>
</head>
<body bottomMargin="1" leftMargin="1" topMargin="1" rightMargin="1" MS_POSITIONING="GridLayout">
		<form id="myForm" method="post" runat="server">
			<table class="table_main" cellSpacing="0" cellPadding="0" width="225" border="0">
				<tr>
					<td class="headtable"><uc1:header_control id="Header_control1" runat="server" width="100%"></uc1:header_control></td>
				</tr>
				<tr>
					<td class="headtable" align="center">Goods Receipt&nbsp;- Outsource</td>
				</tr>
				<tr height="1">
					<td class="smallerText" noWrap align="left"><asp:label id="Lb_msg" runat="server"></asp:label></td>
				</tr>
				<tr>
					<td>
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TBODY>
								<tr>
									<td class="normalText" align="left">PO No.</td>
									<td class="normalText" align="left">&nbsp;<IMG hspace="1" src="../../images/red_arrow.gif" align="absMiddle">
										<asp:label id="Lb_po_no" runat="server" Width="128px" CssClass="normalText"></asp:label></td>
								</tr>
								<tr>
									<td class="normalText" align="left">Set</td>
									<td class="normalText" align="left">&nbsp;<IMG hspace="1" src="../../images/red_arrow.gif" align="absMiddle">
										<asp:label id="Lb_set" runat="server" CssClass="normalText"></asp:label></td>
								</tr>
								<tr>
									<td class="normalText" align="left">Batch&nbsp;Sup</td>
									<td class="normalText" align="left">&nbsp;<IMG hspace="1" src="../../images/red_arrow.gif" align="absMiddle">
										&nbsp;<asp:TextBox style="Z-INDEX: 0" id="Tb_batch" runat="server" CssClass="inputTag" AutoPostBack="True" OnTextChanged="Tb_batch_TextChanged"></asp:TextBox></td>
								</tr>
								<tr>
									<td class="normalText" align="left">Qty</td>
									<td class="normalText" align="left">&nbsp;<IMG hspace="1" src="../../images/red_arrow.gif" align="absMiddle">
										&nbsp;<asp:TextBox style="Z-INDEX: 0" id="Tb_qty" runat="server" CssClass="inputTag" AutoPostBack="True" OnTextChanged="Tb_qty_TextChanged"></asp:TextBox></td>
								</tr>
								<tr>
									<td class="testText" align="left">Material&nbsp;Sup</td>
									<td class="normalText" align="left">&nbsp;<IMG hspace="1" src="../../images/red_arrow.gif" align="absMiddle">
										&nbsp;<asp:TextBox style="Z-INDEX: 0" id="Tb_mat" runat="server" CssClass="inputTag" AutoPostBack="True" OnTextChanged="Tb_mat_TextChanged"></asp:TextBox></td>
								</tr>
								<tr height="2">
									<td colSpan="2"><IMG hspace="1" src="../../images/red_arrow.gif" align="absMiddle">
										<asp:button id="Bt_save" Width="0px" Runat="server" CssClass="buttonTag" Text="Save Data"></asp:button></td>
								</tr>
								<TR>
									<td colSpan="2" height="5"><asp:datagrid id="Dg_detail" runat="server" Width="100%" Font-Size="2pt" Font-Names="Arial" CellSpacing="1"
											AutoGenerateColumns="False" CellPadding="1" BackColor="White" BorderWidth="1px" BorderStyle="None" BorderColor="#E7E7FF"
											AllowPaging="True">
											<FooterStyle ForeColor="#4A3C8C" BackColor="#B5C7DE"></FooterStyle>
											<SelectedItemStyle Font-Bold="True" ForeColor="#F7F7F7" BackColor="#738A9C"></SelectedItemStyle>
											<AlternatingItemStyle BackColor="#F7F7F7"></AlternatingItemStyle>
											<ItemStyle Font-Size="XX-Small" ForeColor="#4A3C8C" BackColor="#E7E7FF"></ItemStyle>
											<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#4A3C8C"></HeaderStyle>
											<Columns>
												<asp:TemplateColumn HeaderText="No.">
													<HeaderStyle HorizontalAlign="Center" ForeColor="White" VerticalAlign="Middle"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
													<ItemTemplate>
														<asp:Label ID="Lb_no" Runat="server"></asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn DataField="Material" HeaderText="Material">
													<HeaderStyle HorizontalAlign="Center" ForeColor="White" VerticalAlign="Middle"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="Sup_Batch" HeaderText="BatchSup">
													<HeaderStyle HorizontalAlign="Center" ForeColor="White" VerticalAlign="Middle"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
													<FooterStyle ForeColor="White"></FooterStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="Qty" HeaderText="Qty" DataFormatString="{0:0.000}">
													<HeaderStyle HorizontalAlign="Center" ForeColor="White" VerticalAlign="Middle"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="Batch" HeaderText="Batch No.">
													<HeaderStyle HorizontalAlign="Center" ForeColor="White" VerticalAlign="Middle"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="Plant" HeaderText="Plant">
													<HeaderStyle HorizontalAlign="Center" ForeColor="White" VerticalAlign="Middle"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
												</asp:BoundColumn>
											</Columns>
											<PagerStyle Font-Overline="True" Font-Bold="True" HorizontalAlign="Left" ForeColor="#4A3C8C"
												BackColor="#E7E7FF" Mode="NumericPages"></PagerStyle>
										</asp:datagrid>&nbsp;</td>
								</TR>
							</TBODY>
						</table>
					</td>
				<tr class="tr" style="PADDING-BOTTOM: 2px; PADDING-LEFT: 2px; PADDING-RIGHT: 2px; PADDING-TOP: 2px">
					<td align="left"><asp:button id="Bt_sendToSap" Width="88px" Runat="server" CssClass="buttonTag" Text="Send To SAP" OnClick="Bt_sendToSap_Click"></asp:button>&nbsp;
						<asp:button id="Bt_reset" Width="80px" Runat="server" CssClass="buttonTag" Text="Clear Screen" OnClick="Bt_reset_Click"></asp:button>&nbsp;
						<asp:button id="Bt_back" Width="50px" Runat="server" CssClass="buttonTag" Text="Back" OnClick="Bt_back_Click"></asp:button></td>
				</tr>
			</table>
			</TR></TBODY></TABLE>
			<table>
				<tr>
					<td><asp:label id="Lb_id" runat="server" Width="32px" Height="8px" Visible="False"></asp:label><asp:label id="Lb_plant" Runat="server" Visible="False"></asp:label><asp:label id="Lb_vendor" Runat="server" Visible="False"></asp:label></td>
				</tr>
			</table>
		</form>
	</body>
</html>
