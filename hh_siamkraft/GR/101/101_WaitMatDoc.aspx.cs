﻿using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using hh_siamkraft.GR._101;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{

    public partial class _101_WaitMatDoc : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();
        protected void Page_Init(object sender, EventArgs e)
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user_id"] == null)
            {
                Session.RemoveAll();
                Response.Redirect("../../index.aspx", false);
                return;
            }

            if (Session["obj_var"] == null)
            {
                Response.Redirect("101_ScanPO.aspx", false);
                return;
            }

            if (!(Page.IsPostBack))
            {
                if (Convert.ToString(Session["flag"]) == "1")
                {
                    this.Lb_txt.Text = "Please Check Text file";
                }

                variable_101 obj_var = new variable_101();
                obj_var = (variable_101)Session["obj_var"];
                Lb_id.Text = obj_var.str_id.Trim();
                Lb_po_no.Text = obj_var.str_po.Trim();
                obj_var = null;

                if (Session["time_out"] == null)
                {
                    var connect = Convert.ToString(Session["Connection_String"]);
                    using (var session = new SkRepositorySession(connect))
                    {
                        var repo = new GRRepository(session);
                        var result = repo.GetTimeOut();
                        if (result.Value != 0)
                        {
                            Session["time_out"] = result.Value;
                        }
                        else
                        {
                            Session["time_out"] = 10;
                        }
                    }
                }

                if (Session["refresh"] == null)
                {
                    Session["refresh"] = 1;
                }


                if (Convert.ToInt32(Session["refresh"]) > Convert.ToInt32(Session["time_out"]))
                {
                    RemoveSession();
                    Session["matDoc_timeout"] = 1;
                    Response.Redirect("101_TimeOut.aspx", false);
                    return;
                }


                if (checkStatus(Convert.ToInt32( this.Lb_id.Text.Trim())) == true)
                {
                    //get Data on success
                    RemoveSession();
                    Response.Redirect("101_MatDoc.aspx", false);
                }
                else
                {
                    //not found and refresh page on 5 seconds   
                    Response.Write("<meta http-equiv=refresh content=5 url='101_WaitMatDoc.aspx'>");
                    Session["refresh"] = Convert.ToInt32(Session["refresh"]) + 1;
                }
            }     
        }



        private void RemoveSession()
        {
            Session.Remove("time_out");
            Session.Remove("refresh");
            return;
        }


        private bool checkStatus(int str_id)
        {
            try
            {
                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession(connect))
                {
                    var repo = new GRRepository(session);
                    var resultStatus = repo.GetTblControl(str_id, Convert.ToString(Session["HH_ID"]));
                    if (resultStatus.Value.Count() == 0)
                    {
                        return false;
                    }
                    else
                    {
                        switch (resultStatus.Value[0].Return_Code.ToString())
                        {
                            case "00": //complete
                                var resultetail = repo.GetViewItem(str_id);
                                if (resultetail.Value.Count() > 0)
                                {
                                    Session["plant"] = resultetail.Value[0].Plant.ToString().Trim();
                                    Session["vendor"] = Convert.ToInt64(resultetail.Value[0].VendorCode.ToString().Trim());
                                }
                                variable_101 obj_var = new variable_101();
                                obj_var.str_id = this.Lb_id.Text;
                                obj_var.str_po = this.Lb_po_no.Text;
                                obj_var.str_plant = resultetail.Value[0].Plant.ToString().Trim();
                                obj_var.str_vendor = Convert.ToString(Session["vendor"]);

                                Session["obj_var"] = obj_var;
                                obj_var = null;
                                return true;

                            case "08":
                                Session["error"] = resultStatus.Value[0].Return_Msg;
                                Response.Redirect("101_ResultError.aspx", false);
                                return true;

                            case "13":
                                Session["error"] = resultStatus.Value[0].Return_Msg;
                                Response.Redirect("101_ResultError.aspx", false);
                                return true;
                        }
                    }
                    return true;
                }
            }
            catch(Exception ex)
            {
                string str_script = "<script>" +
                                        " alert('Get Data on error'); " +
                                        " document.location('101_ScanPO.aspx'); "  +
                                        " </script>";
                this.Page.RegisterStartupScript("alert", str_script);
                return false;
            }
        }

    }
}