﻿using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using hh_siamkraft.GR._101;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace hh_siamkraft
{

    public partial class _101_DeleteBatch : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();
        protected void Page_Init(object sender, EventArgs e)
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if(!(Page.IsPostBack))
            {
                //Put user code to initialize the page here
                if (Session["user_id"] == null)
                {
                    Session.RemoveAll();
                    Response.Redirect("../../index.aspx", false);
                    return;
                }

                if (Session["obj_var"] == null || Session["101_dup"] == null)
                {
                    Response.Redirect("101_ScanPO.aspx", false);
                    return;
                }

                if(!(Page.IsPostBack))
                {
                    variable_101 obj_var = new variable_101();
                    obj_var = (variable_101)Session["obj_var"];
                    this.Lb_id.Text = Convert.ToString( obj_var.str_id);
                    this.Lb_po.Text = Convert.ToString(obj_var.str_po);
                    this.Lb_set.Text = Convert.ToString(obj_var.str_set);
                    this.Lb_plant.Text = Convert.ToString(obj_var.str_plant);
                    this.Lb_vendor.Text = Convert.ToString(obj_var.str_vendor);
                    obj_var = null;

                    //DataTable dt_dup = (DataTable)Session["101_dup"];
                    //this.Lb_batch.Text = dt_dup.Rows[0]["Sup_Batch"];
                //        this.Lb_material.Text = .Rows(0)("Sup_Material")
                //        this.Lb_qty.Text = .Rows(0)("Qty")
                }


                //    obj_var = Nothing
                //    Dim dt_dup As DataTable = Session("101_dup")
                //    With dt_dup
                //        Me.Lb_batch.Text = .Rows(0)("Sup_Batch")
                //        Me.Lb_material.Text = .Rows(0)("Sup_Material")
                //        Me.Lb_qty.Text = .Rows(0)("Qty")
                //    End With
                //End If
            }


        }

      

    }
}