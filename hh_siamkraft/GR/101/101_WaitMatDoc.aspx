﻿<%@ Register TagPrefix="uc1" TagName="header_control" Src="../../header_control.ascx"   %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="101_WaitMatDoc.aspx.cs" Inherits="hh_siamkraft._101_WaitMatDoc" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>101_WaitMatDoc</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C# .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"/>
		<LINK href="../../css/style.css" type="text/css" rel="stylesheet"/>
  
</head>
 	<body bottomMargin="1" leftMargin="1" topMargin="1" rightMargin="1" MS_POSITIONING="GridLayout">
		<form id="myForm" method="post" runat="server">
			<table class="table_main" id="tbl_waitdata" cellSpacing="0" cellPadding="0" width="225"
				border="0" runat="server">
				<tr>
					<td class="headtable"><uc1:header_control id="Header_control1" runat="server"></uc1:header_control></td>
				</tr>
				<tr>
					<td class="headtable" align="center">Goods Receipt&nbsp;- Outsource</td>
				</tr>
				<tr height="2">
					<td class="smallerText"><asp:label id="Lb_txt" runat="server"></asp:label></td>
				</tr>
				<tr>
					<td>
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<th class="normalText" align="left" colSpan="2">
									&nbsp;<img src="../../images/loading.gif" align="absMiddle">&nbsp;
									<asp:label id="Lb_msg" runat="server" CssClass="normalText"> Waiting... Mat Doc</asp:label></th></tr>
							<tr>
								<td class="normalText" align="left">PO No.</td>
								<td class="normalText" align="left">&nbsp;<IMG hspace="1" src="../../images/red_arrow.gif" align="absMiddle">&nbsp;
									<asp:label id="Lb_po_no" runat="server" Width="128px" CssClass="normalText"></asp:label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr class="tr" style="PADDING-BOTTOM: 2px; PADDING-LEFT: 2px; PADDING-RIGHT: 2px; PADDING-TOP: 2px">
					<td align="left">
					</td>
				</tr>
			</table>
			<table>
				<tr>
					<td><asp:label id="Lb_id" Runat="server" Visible="False"></asp:label></td>
				</tr>
			</table>
		</form>
	</body>
</html>
