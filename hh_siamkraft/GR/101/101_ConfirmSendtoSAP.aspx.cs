﻿using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using hh_siamkraft.GR._101;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using hh_siamkraft.Entities.GR;

namespace hh_siamkraft
{

    public partial class _101_ConfirmSendtoSAP : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();
        private DataTable dt_detail = new DataTable();
        protected void Page_Init(object sender, EventArgs e)
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user_id"] == null)
            {
                Session.RemoveAll();
                Response.Redirect("../../index.aspx", false);
                return;
            }


            if (!(Page.IsPostBack))
            {
                dt_detail = (DataTable)Session["Dg_report"];
                this.Dg_report.DataSource = dt_detail;
                this.Dg_report.DataBind();

                var obj_var = new variable_101();
                obj_var = (variable_101)Session["obj_var"];
                this.Lb_id.Text = obj_var.str_id;
                this.Lb_po.Text = obj_var.str_po;
                this.Lb_set_no.Text = obj_var.str_set;
                this.Lb_plant.Text = obj_var.str_plant;
                this.Lb_vendor.Text = obj_var.str_vendor;

            }
        }

        protected void Bt_ok_Click(object sender, EventArgs e)
        {
            if (this.Rb_yes.Checked == false && this.Rb_no.Checked == false)
            {
                script_func.script_alert("Please Select yes/no", Page);
                return;
            }
        
            if(this.Rb_yes.Checked)
            {
                //yes send to sap
                if (this.Dg_report.Items.Count != 0)
                {
                    var str = string.Join(",", this.dt_detail.AsEnumerable().Select(o => "'" + o["Material"] + "'")); // 'A','B'
                    var connect = Convert.ToString(Session["Connection_String"]);
                    using (var session = new SkRepositorySession(connect))
                    {
                        var repo = new GRRepository(session);
                        var result = repo.GetOutsourceNoMaterial(str, this.Lb_po.Text.Trim());
                        if (result.Value.Count() > 0)
                        {
                            int rec_id = this.insertTableControl();
                            for (int i = 0; i <= result.Value.Count() - 1; i++)
                            {
                                repo.InsertSap_grwithPO_item(rec_id, this.Lb_po.Text.Trim(), result.Value[i].Item_No, result.Value[i].Plant, result.Value[i].Storage
                                   , result.Value[i].Material, result.Value[i].Batch, result.Value[i].Qty, result.Value[i].ProdDate, result.Value[i].Length);
                            }

                            var rec = repo.get_RecordID(rec_id, Convert.ToString(Session["HH_ID"]), Convert.ToString(Session["user_id"]), this.Lb_po.Text.Trim(), sp_GroupType.some_Mat);
                            if (rec.Value == 0)
                            {
                                string script = "<script>" +
                                                    " alert('No Data send to SAP'); " +
                                                    " document.location = '101_ScanDetail.aspx';" +
                                                    " </script>";
                                Page.RegisterStartupScript("alert", script);
                                return;
                            }
                            else
                            {
                                var obj_var = new variable_101();
                                obj_var = (variable_101)Session["obj_var"];
                                obj_var.str_id = rec.Value.ToString();
                                obj_var.str_po = this.Lb_po.Text.Trim(); ;
                                Session["obj_var"] = obj_var;
                                obj_var = null;
                                Response.Redirect("101_WaitMatDoc.aspx", false);
                            }
                        }
                        else
                        {
                            script_func.script_alert(" No found data 'send to SAP'. ", Page);
                            return;
                        }
                    }
                }    
            }
            else
            {
                //no send to sap
                Response.Redirect("101_ScanDetail.aspx", false);
            }
        }

        private int insertTableControl()
        {
            var connect = Convert.ToString(Session["Connection_String"]);
            using (var session = new SkRepositorySession(connect))
            {
                var repo = new GRRepository(session);
                var result = repo.InsertControl( 0, Convert.ToString(Session["HH_ID"]), "GRwithPO", Convert.ToString(Session["user_id"]),"N");

                if (result.Value == true)
                {
                    var rec = repo.GetControlMaxRecord(Convert.ToString(Session["HH_ID"]), "GRwithPO");
                    return rec.Value;
                }

            }
            return 0;
        }
    }
}