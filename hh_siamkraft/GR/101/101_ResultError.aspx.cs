﻿using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using hh_siamkraft.GR._101;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{

    public partial class _101_ResultError : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();
        protected void Page_Init(object sender, EventArgs e)
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Session("user_id") = "jo"
            if ( Session["user_id"] != null)
            {
                Response.Redirect("../../index.aspx");
                return;
            }
            
            if(!(Page.IsPostBack))
            {
                this.Tb_msg.Text = Convert.ToString(Session["error"]);
            }

            if(Convert.ToString(Session["matDoc_timeout"]) == "1")
            {
                this.Bt_back.Visible = false;
            }
        }

        protected void Bt_back_Click(object sender, EventArgs e)
        {
            Session.Remove("error");
            Response.Redirect("101_ScanPO.aspx", false);
        }
    }
}