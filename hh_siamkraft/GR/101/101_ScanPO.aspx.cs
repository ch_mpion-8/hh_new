﻿using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using hh_siamkraft.GR._101;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{

    public partial class _101_ScanPO : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();
        protected void Page_Init(object sender, EventArgs e)
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user_id"] == null)
            {
                Session.RemoveAll();
                Response.Redirect("../../index.aspx");
                return;
            }
            if (!(Page.IsPostBack))
            {
                this.Tb_po_no.Attributes.Add("onkeypress", "javascript:return keyCheck(event,this)");
                script_func.script_focus(this.Tb_po_no.ID, Page);
            }

        }

        protected void Bt_back_Click(object sender, EventArgs e)
        {
            Response.Redirect("../GR_MainMenu.aspx", false);
        }

        protected void Tb_po_no_TextChanged(object sender, EventArgs e)
        {
            GetPO();
        }

        //check PO_fromTxt
        private bool checkPO(string str_po)
        {
            try
            {
                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession(connect))
                {
                    var repo = new GRRepository(session);
                    var result = repo.CountPO(str_po.Trim(), "%","%", null);
                if (result.Value == 0)
                    {
                        //not found po_fromtxt
                        script_func.script_alert("PO -> " + str_po.Trim() + "\r\n not found in PLS system", Page);
                        script_func.script_focus(this.Tb_po_no.ClientID, Page);
                        script_func.script_select(this.Tb_po_no.ClientID, Page);
                        Tb_po_no.Text = "";
                        return false;
                    }
                    else
                    {
                        //found record
                        return true;
                    }
                }
            }
            catch
            {
                script_func.script_alert("Check PO On Error", Page);
                return false;
            }
            
        }

        private bool checkPOcomplete(string str_po)
        {
            try
            {
                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession(connect))
                {
                    var repo = new GRRepository(session);
                    var resultBatch_Sup = repo.CountPO(str_po.Trim(), "%", "%", null);
                    if (resultBatch_Sup.Value > 0)
                    {
                        var resultBatch_Rec = repo.CountOutsource(str_po.Trim());
                        if(resultBatch_Rec.Value > 0)
                        {
                            if (resultBatch_Rec.Value >= resultBatch_Sup.Value) return true; //batch from supplier equal batch send to sap complete
                            else return false;
                        }
                    }
                }
                return false;
            }
            catch
            {
                script_func.script_alert("Check POcomplete Error", Page);
                return false;
            }
        }


        private bool checkCountData(string str_po )
        {
            try
            {
                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession(connect))
                {
                    //select data from HH_GR_Outsourec have not send to sap
                    var repo = new GRRepository(session);
                    var result = repo.GetHROutSource(str_po.Trim(), "N", "%", "%", null);
                    if (result.Value.Count() > 0)
                    {
                        //have record
                        // Session("id_page") = dt_outsource.Rows(0)("Record_ID")
                        var obj_var = new variable_101();
                        obj_var.str_po = this.Tb_po_no.Text.Trim();
                        Session["obj_var"] = obj_var;
                        obj_var = null;
                        return true;  
                    }
                    else //no record
                    {
                        return false;
                    }
                }
            }

            catch 
            {
                script_func.script_alert("Check Data From Outsource On Error", Page);
                return false;
            }
        }


        //function return Record_ID from stored procedure
        private bool GetLastID()
        {
            Session["po_no"] = this.Tb_po_no.Text.Trim();
            var connect = Convert.ToString(Session["Connection_String"]);
            using (var session = new SkRepositorySession(connect))
            {
                var repo = new GRRepository(session);
                var result = repo.getID(Convert.ToString(Session["HH_ID"]), Convert.ToString(Session["user_id"]), this.Tb_po_no.Text.Trim().PadLeft(10, '0'));
            }

            if (Convert.ToString(Session["id_page"]) != "0")
            {
                var obj_var = new variable_101();
                obj_var.str_id = Convert.ToString(Session["id_page"]);
                obj_var.str_po = this.Tb_po_no.Text.Trim();
                Session["obj_var"] = obj_var;
                obj_var = null;
                return true;
            }
            else
            {
                return false;
            }
        }



        private void GetPO()
        {
            if (string.IsNullOrEmpty( this.Tb_po_no.Text.Trim()) )
            {
                script_func.script_alert("Please Insert 'PO Number'.", Page);
                script_func.script_focus(this.Tb_po_no.ClientID, Page);
                return;
            }

            try
            {
                //check PO
                if (checkPO(this.Tb_po_no.Text.Trim().PadLeft(10, '0')) == true)
                {
                    if (checkPOcomplete(this.Tb_po_no.Text.Trim()) == true)
                    {
                        script_func.script_alert("PO -> " + this.Tb_po_no.Text.Trim().PadLeft(10, '0') + "\r\n Receive Completed", Page);
                        script_func.script_focus(this.Tb_po_no.ClientID, Page);
                        script_func.script_select(this.Tb_po_no.ClientID, Page);
                        Tb_po_no.Text = "";
                        return;
                    }
                    if (checkCountData(this.Tb_po_no.Text.Trim().PadLeft(10, '0')) == true)
                    {
                        //'have data on Scan handheld
                        Response.Redirect("101_confirmdelete.aspx", false);
                        return;
                    }

                    if (GetLastID() == true)
                    {
                        Response.Redirect("101_WaitData.aspx", false);
                    }
                    else
                    {
                        script_func.script_alert("Get ID On Error", Page);
                        script_func.script_focus(this.Tb_po_no.ClientID, Page);
                        script_func.script_select(this.Tb_po_no.ClientID, Page);
                        Tb_po_no.Text = "";
                    }
                }

            }
            catch
            {
                script_func.script_alert("Check PO on Error", Page);
                Tb_po_no.Text = "";
            }
        }
      

    }
}