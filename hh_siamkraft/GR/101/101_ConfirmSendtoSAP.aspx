﻿<%@ Register TagPrefix="uc1" TagName="header_control" Src="../../header_control.ascx"   %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="101_ConfirmSendtoSAP.aspx.cs" Inherits="hh_siamkraft._101_ConfirmSendtoSAP" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>101_ConfirmSendtoSAP</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C# .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"/>
		<LINK href="../../css/style.css" type="text/css" rel="stylesheet"/>
  
</head>
 	<body style="MARGIN: 1px" MS_POSITIONING="GridLayout">
		<form id="myForm" method="post" runat="server">
			<table class="table_main" cellSpacing="0" cellPadding="0" width="225" border="0">
				<tr>
					<td class="headtable"><uc1:header_control id="Header_control1" runat="server"></uc1:header_control></td>
				</tr>
				<tr>
					<td class="headtable" align="center">Goods Receipt&nbsp;- Outsource</td>
				</tr>
				<tr>
					<td>
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="normalText" align="left" colSpan="2">PO Number&nbsp;<IMG hspace="1" src="../../images/red_arrow.gif" align="absMiddle">&nbsp;<asp:label id="Lb_po" Runat="server" CssClass="normalText"></asp:label>
								</td>
							</tr>
							<tr>
								<td noWrap colSpan="2"><asp:label id="Lb_info" Runat="server" CssClass="normalText">Material error weight diff 10%</asp:label></td>
							</tr>
							<tr height="1">
								<td colSpan="2"><asp:datagrid id="Dg_report" runat="server" Width="100%" CellPadding="1" BorderColor="#E7E7FF"
										BorderStyle="None" BorderWidth="1px" BackColor="White" CellSpacing="1" AutoGenerateColumns="False">
										<FooterStyle ForeColor="#4A3C8C" BackColor="#B5C7DE"></FooterStyle>
										<SelectedItemStyle Font-Bold="True" ForeColor="#F7F7F7" BackColor="#738A9C"></SelectedItemStyle>
										<AlternatingItemStyle CssClass="row2" BackColor="#F7F7F7"></AlternatingItemStyle>
										<ItemStyle ForeColor="#4A3C8C" CssClass="row1" BackColor="#E7E7FF"></ItemStyle>
										<HeaderStyle Font-Bold="True" ForeColor="#F7F7F7" BackColor="#4A3C8C"></HeaderStyle>
										<Columns>
											<asp:BoundColumn DataField="Material" ReadOnly="True" HeaderText="Material">
												<HeaderStyle HorizontalAlign="Center" ForeColor="White" Width="100px" VerticalAlign="Middle"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="Qty" ReadOnly="True" HeaderText="Qty" DataFormatString="{0:0.000}">
												<HeaderStyle HorizontalAlign="Center" ForeColor="White" Width="75px" VerticalAlign="Middle"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="SAP_Qty" ReadOnly="True" HeaderText="SAP_Qty" DataFormatString="{0:0.000}">
												<HeaderStyle HorizontalAlign="Center" ForeColor="White" Width="75px" VerticalAlign="Middle"></HeaderStyle>
												<ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
											</asp:BoundColumn>
										</Columns>
										<PagerStyle HorizontalAlign="Right" ForeColor="#4A3C8C" BackColor="#E7E7FF" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></td>
							</tr>
							<tr>
								<td noWrap colSpan="2">&nbsp;<IMG src="../../images/question.gif" align="absMiddle">&nbsp;<asp:label id="Lb_question" Runat="server" CssClass="normalText" ForeColor="red">Do you want send data remain ?</asp:label>
								</td>
							</tr>
							<tr>
								<td align="left" colSpan="2">&nbsp;<asp:radiobutton id="Rb_yes" Runat="server" CssClass="normalText" GroupName="confirm" Text="Yes send data"></asp:radiobutton></td>
							</tr>
							<tr>
								<td align="left" colSpan="2">&nbsp;<asp:radiobutton id="Rb_no" Runat="server" CssClass="normalText" GroupName="confirm" Text="No send data"></asp:radiobutton></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr class="tr" style="PADDING-RIGHT: 2px; PADDING-LEFT: 2px; PADDING-BOTTOM: 2px; PADDING-TOP: 2px">
					<td class="normalText" align="left"><asp:button id="Bt_ok" Runat="server" Width="56px" CssClass="buttonTag" Text=" OK " OnClick="Bt_ok_Click"></asp:button></td>
				</tr>
			</table>
			<table>
				<tr>
					<td><asp:label id="Lb_id" Runat="server" Visible="False"></asp:label><asp:label id="Lb_set_no" Runat="server" Visible="False"></asp:label><asp:label id="Lb_plant" Runat="server" Visible="False"></asp:label><asp:label id="Lb_vendor" Runat="server" Visible="False"></asp:label></td>
				</tr>
			</table>
		</form>
	</body>
</html>
