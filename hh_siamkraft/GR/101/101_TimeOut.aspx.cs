﻿using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using hh_siamkraft.GR._101;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{

    public partial class _101_TimeOut : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();
        protected void Page_Init(object sender, EventArgs e)
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Put user code to initialize the page here
            if( Session["obj_var"] != null)
            {
                Response.Redirect("101_ScanPO.aspx", false);
                return;
            }
            
            if(!(Page.IsPostBack))
            {
                variable_101 obj_var = new variable_101();
                obj_var = (variable_101)Session["obj_var"];
                this.Lb_po_no.Text = obj_var.str_po.ToString().Trim();
                this.Lb_id.Text = obj_var.str_id.ToString().Trim();
            }

            if(Convert.ToString(Session["matDoc_timeout"]) == "1")
            {
                this.Bt_back.Visible = false;
            }

        }

        protected void Bt_retry_Click(object sender, EventArgs e)
        {
            if( Convert.ToString(Session["matDoc_timeout"]) == "1" )
            {
                Session.Remove("matDoc_timeout");
                Response.Redirect("101_WaitMatDoc.aspx", false);
            }
            else
            {
                Response.Redirect("101_WaitData.aspx", false);
            }

        }

        protected void Bt_back_Click(object sender, EventArgs e)
        {
            Session.Remove("obj_var");
            Response.Redirect("101_ScanPO.aspx", false);
        }
    }
}