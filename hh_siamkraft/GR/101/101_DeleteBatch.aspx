﻿<%@ Register TagPrefix="uc1" TagName="header_control" Src="../../header_control.ascx"   %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="101_DeleteBatch.aspx.cs" Inherits="hh_siamkraft._101_DeleteBatch" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>101_DeleteBatch</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C# .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"/>
		<LINK href="../../css/style.css" type="text/css" rel="stylesheet"/>
  
</head>
  <body style="MARGIN: 1px" MS_POSITIONING="GridLayout">
		<form id="myForm" name="myForm" method="post" runat="server">
			<table class="table_main" cellSpacing="0" cellPadding="0" width="225" border="0">
				<tr>
					<td class="headtable"><uc1:header_control id="Header_control1" runat="server"></uc1:header_control></td>
				</tr>
				<tr>
					<td class="headtable" align="center">Goods Receipt&nbsp;- Outsource</td>
				</tr>
				<tr>
					<td>
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="normalText" align="left">Batch</td>
								<td class="normalText" align="left"><IMG src="../../images/red_arrow.gif" align="absMiddle">&nbsp;
									<asp:label id="Lb_batch" runat="server" CssClass="normalText"></asp:label></td>
							</tr>
							<tr>
								<td class="normalText" align="left">Qty</td>
								<td class="normalText" align="left"><IMG src="../../images/red_arrow.gif" align="absMiddle">&nbsp;
									<asp:label id="Lb_qty" runat="server" CssClass="normalText"></asp:label></td>
							</tr>
							<tr>
								<td class="normalText" align="left">Materail</td>
								<td class="normalText" align="left"><IMG src="../../images/red_arrow.gif" align="absMiddle">&nbsp;
									<asp:label id="Lb_material" runat="server" CssClass="normalText"></asp:label></td>
							</tr>
							<tr>
								<td noWrap align="left" colSpan="2">&nbsp;<IMG src="../../images/question.gif" align="absMiddle">&nbsp;<asp:label id="Lb_info" CssClass="normalText" Runat="server" ForeColor="red">Batch duplicated Do you want delete data ?</asp:label></td>
							</tr>
							<tr>
								<td align="left" colSpan="2"><asp:radiobutton id="Rb_yes" runat="server" CssClass="normalText" GroupName="confirm" Text="Yes delete data"></asp:radiobutton></td>
							</tr>
							<tr>
								<td align="left" colSpan="2"><asp:radiobutton id="Rb_no" runat="server" CssClass="normalText" GroupName="confirm" Text="No delete data"></asp:radiobutton></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr class="tr" style="PADDING-RIGHT: 2px; PADDING-LEFT: 2px; PADDING-BOTTOM: 2px; PADDING-TOP: 2px">
					<td align="left">
						<P><asp:button id="BT_Save" runat="server" CssClass="buttonTag" Text="OK" Width="50px"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</P>
					</td>
				</tr>
			</table>
			</TABLE>
			<table>
				<tr>
					<td><asp:label id="Lb_id" Visible="False" Runat="server"></asp:label><asp:label id="Lb_po" Visible="False" Runat="server"></asp:label><asp:label id="Lb_set" Visible="False" Runat="server"></asp:label><asp:label id="Lb_plant" Visible="False" Runat="server"></asp:label><asp:label id="Lb_vendor" Visible="False" Runat="server"></asp:label></td>
				</tr>
			</table>
		</form>
		</FORM>
	</body>
</html>
