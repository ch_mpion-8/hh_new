﻿using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using hh_siamkraft.GR._101;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{

    public partial class _101_ScanSet : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();
        protected void Page_Init(object sender, EventArgs e)
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Put user code to initialize the page here
            if (Session["user_id"] == null)
            {
                Session.RemoveAll();
                Response.Redirect("../../index.aspx", false);
                return;
            }

            if (Session["obj_var"] == null)
            {
                Response.Redirect("101_ScanPO.aspx", false);
                return;
            }

            if (!(Page.IsPostBack))
            {
                variable_101 obj_var = new variable_101();
                obj_var = (variable_101)Session["obj_var"];
                this.Lb_po_no.Text = obj_var.str_po.ToString().Trim();
                this.Lb_id.Text = obj_var.str_id.ToString().Trim();
                this.Lb_plant.Text = obj_var.str_plant.Trim();
                this.Lb_vendor.Text = obj_var.str_vendor.Trim();

                obj_var = null;
                script_func.script_focus(this.Tb_set.ClientID, Page);
            }


        }

        protected void Bt_back_Click(object sender, EventArgs e)
        {
            Response.Redirect("101_ScanPO.aspx");
        }

        protected void Tb_set_TextChanged(object sender, EventArgs e)
        {
            SaveSet();
        }

        private void SaveSet()
        {
            if(this.Tb_set.Text == "")
            {
                script_func.script_alert("Please Insert 'Set Number'.", Page);
                script_func.script_focus(this.Tb_set.ID, Page);
                return;
            }

            variable_101 obj_var = new variable_101();
            obj_var = (variable_101)Session["obj_var"];
            obj_var.str_id = this.Lb_id.Text.Trim();
            obj_var.str_po = this.Lb_po_no.Text.Trim();
            obj_var.str_set = this.Tb_set.Text.Trim().ToUpper();
            obj_var.str_plant = this.Lb_plant.Text.Trim();
            obj_var.str_vendor = this.Lb_vendor.Text.Trim();

            Session["obj_var"] = obj_var;
            obj_var = null;
            Response.Redirect("101_ScanDetail.aspx", false);

        }
      

    }
}