﻿<%@ Register TagPrefix="uc1" TagName="header_control" Src="../../header_control.ascx"   %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="101_ScanPO.aspx.cs" Inherits="hh_siamkraft._101_ScanPO" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>MAIN MENU</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C# .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"/>
		<LINK href="../../css/style.css" type="text/css" rel="stylesheet"/>
    <script language="javascript">
			function keyCheck(eventObj, obj){	// key number and dot
				var keyCode

				// Check For Browser Type
				if (document.all){ 
					keyCode=eventObj.keyCode
				}
				else{
					keyCode=eventObj.which
				}
				//alert(keyCode);

				var str=obj.value

				/*if(keyCode==46){ 
					if (str.indexOf(".")>0){
						return false
					}
				}*/

				if((keyCode<48 || keyCode >58) && (keyCode != 13)){ // Allow only integers
					return false
				}

				return true
			}
	
		</script>
</head>
<body bottomMargin="1" leftMargin="1" topMargin="1" rightMargin="1" MS_POSITIONING="GridLayout">
		<form id="myForm" method="post" runat="server">
			<table class="table_main" cellSpacing="0" cellPadding="0" width="225" border="0">
				<tr>
					<td class="headtable"><uc1:header_control id="Header_control1" runat="server"></uc1:header_control></td>
				</tr>
				<tr>
					<td class="headtable" align="center">Goods Receipt&nbsp;- Outsource</td>
				</tr>
				<tr>
					<td>
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="normalText" align="left">PO No.</td>
								<td align="left">&nbsp;<IMG hspace="1" src="../../images/red_arrow.gif" align="absMiddle">&nbsp;
                                    <asp:textbox id="Tb_po_no" MaxLength="10" CssClass="inputTag" Runat="server" AutoPostBack="True" OnTextChanged="Tb_po_no_TextChanged"></asp:textbox>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr class="tr" style="PADDING-BOTTOM: 2px; PADDING-LEFT: 2px; PADDING-RIGHT: 2px; PADDING-TOP: 2px">
					<td align="center">
                        <asp:Button ID="Bt_back" Runat="server" CssClass="buttonTag" Text="GR Menu" Width="72px" tabIndex="2" OnClick="Bt_back_Click"></asp:Button>

					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
