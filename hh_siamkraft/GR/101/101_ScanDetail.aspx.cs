﻿using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using hh_siamkraft.Entities.Common;
using hh_siamkraft.GR._101;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{

    public partial class _101_ScanDetail : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();
        protected void Page_Init(object sender, EventArgs e)
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user_id"] == null)
            {
                Session.RemoveAll();
                Response.Redirect("../../index.aspx");
                return;
            }

            if (Session["obj_var"] == null)
            {
                Response.Redirect("101_ScanPO.aspx", false);
                return;
            }

            if (!(Page.IsPostBack))
            {
                if (Session["msg"] != null)
                {
                    this.Lb_msg.Text = Convert.ToString(Session["msg"]);
                }

                this.Tb_qty.Attributes.Add("onkeypress", "return keyCheck(event,this)");
                variable_101 obj_var = new variable_101();
                obj_var = (variable_101)Session["obj_var"];
                this.Lb_id.Text = Convert.ToString(obj_var.str_id);
                this.Lb_po_no.Text = Convert.ToString(obj_var.str_po);
                this.Lb_set.Text = Convert.ToString(obj_var.str_set);
                this.Lb_plant.Text = Convert.ToString(obj_var.str_plant);
                this.Lb_vendor.Text = Convert.ToString(obj_var.str_vendor);

                obj_var = null;
                bindData();
                initialForm();
            }
        }

        protected void Bt_back_Click(object sender, EventArgs e)
        {
            variable_101 obj_var = new variable_101();
            obj_var = (variable_101)Session["obj_var"];
            obj_var.str_id = this.Lb_id.Text.Trim();
            obj_var.str_po = this.Lb_po_no.Text.Trim();
            obj_var.str_plant = this.Lb_plant.Text.Trim();
            obj_var.str_vendor = this.Lb_vendor.Text.Trim();

            Session["obj_var"] = obj_var;
            obj_var = null;
            Session.Remove("msg");
            Response.Redirect("101_ScanSet.aspx", false);
        }

        protected void Bt_reset_Click(object sender, EventArgs e)
        {
            initialForm();
        }

        private void initialForm()
        {
            this.Tb_batch.Text = "";
            this.Tb_qty.Text = "";
            this.Tb_mat.Text = "";
            script_func.script_focus(this.Tb_batch.ID, Page);
        }

        private void bindData()
        {
            var connect = Convert.ToString(Session["Connection_String"]);
            using (var session = new SkRepositorySession(connect))
            {
                var repo = new GRRepository(session);
                var result = repo.GetHROutSource(this.Lb_po_no.Text.Trim(), "%", "%","%", null);
                this.Dg_detail.DataSource = result.Value;
                this.Dg_detail.DataBind();
            }
        }

        protected void Tb_batch_TextChanged(object sender, EventArgs e)
        {
            SaveScanBarcode();
        }

        protected void Tb_qty_TextChanged(object sender, EventArgs e)
        {
            SaveScanBarcode();
        }

        protected void Tb_mat_TextChanged(object sender, EventArgs e)
        {
            SaveScanBarcode();
        }

        private void SaveScanBarcode()
        {
            string msgErr = "";
            if (this.Tb_batch.Text == "")
            {
                script_func.script_focus(this.Tb_batch.ClientID, Page);
                return;
            }
            if (this.Tb_qty.Text == "")
            {
                script_func.script_focus(this.Tb_qty.ClientID, Page);
                return;
            }
            if (this.Tb_mat.Text == "")
            {
                script_func.script_focus(this.Tb_mat.ClientID, Page);
                return;
            }


            try
            {
                //check data from tbl_fromtext
                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession(connect))
                {
                    var repo = new GRRepository(session);
                    var Pofrom = repo.GetPOFromtxt(Lb_po_no.Text.Trim(), Tb_mat.Text.Trim(), Tb_batch.Text.Trim(), Convert.ToDecimal( Tb_qty.Text.Trim()));
                    if (Pofrom.Value.Count() == 0)
                    {
                        //no data po_fromtxt
                        //alert and write error log to text file
                        script_func.script_alert("Batch Qty Material of 'Supplier' not found \r\n please contact warehouse officer", Page);

                        string strData = Tb_batch.Text.Trim().ToUpper() + "," + Tb_qty.Text + "," + Tb_mat.Text.Trim().ToUpper() + "\r\n not Found";
                        string strFilename = ModuleCommon.dirPathGR_101 + Lb_po_no.Text.Trim() + ".txt";
                        FileLog.WriteLogError(strFilename, strData,Convert.ToString( Session["user_id"]));
                        this.Lb_msg.Text = "<font color=red><b>Batch Qty Material of 'Supplier' not found</b></font><br>please contact warehouse officer";
                        initialForm();
                    }
                    else
                    {
                        var outsource = repo.GetHROutSource(this.Lb_po_no.Text.Trim(),"%", this.Tb_mat.Text.Trim(), this.Tb_batch.Text.Trim(), Convert.ToDecimal(this.Tb_qty.Text.Trim()));
                        if (outsource.Value.Count() > 0)
                        {
                            //data duplicate
                            switch (outsource.Value[0].Status)
                            {
                                case "C":
                                    script_func.script_alert("Batch -> " + this.Tb_batch.Text.ToUpper() + "\r\n send to sap complete", Page);
                                    this.Lb_msg.Text = "<font color=red><b>Batch -> " + this.Tb_batch.Text.Trim().ToUpper() + " send to sap complete</b></font>";
                                    initialForm();
                                    break;
                                case "N":
                                    variable_101 obj_var = new variable_101();
                                    obj_var.str_id = this.Lb_id.Text;
                                    obj_var.str_po = this.Lb_po_no.Text;
                                    obj_var.str_set = this.Lb_set.Text;
                                    obj_var.str_plant = this.Lb_plant.Text;
                                    obj_var.str_vendor = this.Lb_vendor.Text;

                                    Session["obj_var"] = obj_var;
                                    obj_var = null;
                                    Session["101_dup"] = outsource;
                                    Response.Redirect("101_DeleteBatch.aspx", false);
                                    break;
        
                            }
                            return;

                        }//----------------- end outsource => if (outsource.Value.Count ------------------//

                        string str_ItemNo = "";
                        var poitem = repo.GetPOItem(Convert.ToInt32(this.Lb_id.Text.Trim()), this.Lb_po_no.Text.Trim(), Pofrom.Value[0].Material_Number.ToString().Trim());
                        if (poitem.Value.Count() == 0)
                        {
                            script_func.script_alert("Mat-> " + Pofrom.Value[0].Material + "\r\n Not found from PO", Page);
                            return;
                        }
                        else if (poitem.Value.Count() == 1)
                        {
                            str_ItemNo = poitem.Value[0].Item_No.ToString();
                        }
                        else
                        {
                            for (int cnt = 0; cnt <= poitem.Value.Count() - 1; cnt++)
                            {
                                var sumPO = repo.GetViewPOItem(Convert.ToInt32(this.Lb_id.Text), this.Lb_po_no.Text, Pofrom.Value[0].Material_Number.ToString().Trim(), poitem.Value[cnt].Item_No.ToString().Trim());
                                if (sumPO.Value.Count() == 0)
                                {
                                    str_ItemNo = poitem.Value[cnt].Item_No.ToString();
                                    break;
                                }
                                if(Convert.ToDecimal(sumPO.Value[0].Qty) < Convert.ToDecimal(poitem.Value[cnt].Qty))
                                {
                                    str_ItemNo = poitem.Value[cnt].Item_No.ToString();
                                    break;
                                }
                            }
                        }//----------------end poitem ---------------------//  

                        string strBatch;
                        //generate batch
                        if (this.Lb_vendor.Text.Trim() != "300973" && this.Lb_vendor.Text.Trim() != "7544")
                        {
                            string strReel_no = gen_ReelNo();
                            if (strReel_no == "1000")
                            {
                                script_func.script_alert("Reel No on error \r\n please change 'Set Number'.", Page);
                                return;
                            }
                            strBatch = getBatch(strReel_no);
                            while (check_GenBatch(strBatch) == false)
                            {
                                strReel_no = gen_ReelNo();
                                if (strReel_no == "1000")
                                {
                                    script_func.script_alert("Reel No on error \r\n please change 'Set Number'.", Page);
                                    return;
                                }
                                strBatch = getBatch(strReel_no);
                            }
                        }
                        else
                        {
                            strBatch = this.Tb_batch.Text.Trim();
                        }


                        //insert data to HH_GR_Outsource
                       var resultInsert = repo.InsertHROutSource(Convert.ToInt32( this.Lb_id.Text.Trim()), this.Lb_po_no.Text.Trim(), DateTime.Now.ToString("dd.MM.yyyy", new CultureInfo("en-US")),
                                                this.Lb_set.Text.Trim(), this.Tb_mat.Text.Trim().ToUpper(), this.Tb_batch.Text.Trim().ToUpper(),
                                                Convert.ToDecimal(this.Tb_qty.Text.Trim()), Convert.ToString(str_ItemNo).Trim(), Pofrom.Value[0].Material_Number.ToString().Trim(),
                                                strBatch, this.Lb_plant.Text.Trim(), strBatch.Substring(1, 1).ToString(), Pofrom.Value[0].ProdDate.ToString().Trim(),
                                                Convert.ToDecimal( Pofrom.Value[0].Length.ToString()) , "N", Convert.ToString(Session["user_id"]));
                        if(resultInsert.Value == true)
                        {
                            bindData();
                            initialForm();
                            this.Lb_msg.Text = "<font color=lightgreen><b>SUCCESS !!</b></font> save data on success.";
                        }

                    }//------------------end else => if (Pofrom.Value.Count() -------------------//
                }//-----------------end using (var session  ---------------------//
            }
            catch (Exception ex)
            {
                script_func.script_alert("Save Data on Error", Page);
            }

        }

        private bool check_GenBatch(string batch_number)
        {
            var connect1 = Convert.ToString(Session["Connection_String"]);
            using (var session1 = new SkRepositorySession(connect1))
            {
                var repo1 = new GRRepository(session1);
                var result1 = repo1.CheckDupBatch(batch_number);
                return result1.Value;
            }
        }


        private string gen_ReelNo()
        {
            var connect1 = Convert.ToString(Session["Connection_String"]);
            using (var session1 = new SkRepositorySession(connect1))
            {
                //ยังไม่เสร็จ
                var repo1 = new GRRepository(session1);
                var reel = repo1.getReel_No(this.Lb_po_no.Text.Trim(), this.Lb_set.Text.Trim());
                return Convert.ToInt32(reel.Value).ToString("000");
            }

        }

        private string getBatch(string strReel)
        {
            string str = DateTime.Now.Year.ToString(new CultureInfo("en-US"));
            string strBatch = "11" + str.Substring(str.Length - 1) + DateTime.Now.Month.ToString("00", new CultureInfo("en-US")) + strReel.Trim() + this.Lb_set.Text.Trim();
            return strBatch;
        }

        protected void Bt_sendToSap_Click(object sender, EventArgs e)
        {
            if (this.Dg_detail.Items.Count == 0)
            {
                script_func.script_alert("Item Not Found!! \r\n Can't Send to SAP", Page);
                script_func.script_focus(this.Tb_batch.ID, Page);
                return;
            }

            try
            {
                var connect1 = Convert.ToString(Session["Connection_String"]);
                using (var session1 = new SkRepositorySession(connect1))
                {
                    var repo1 = new GRRepository(session1);
                    var dtOutSource = repo1.GetHROutSource(this.Lb_po_no.Text.Trim(), "N", "%", "%", null);
                    if (dtOutSource.Value.Count == 0)
                    {
                        script_func.script_alert("No Record Send to SAP", Page);
                        initialForm();
                        return;
                    }
                    //ยังไม่เสร็จ

                }
            }
            catch
            {
                script_func.script_alert("Send to SAP on Error!!", Page);
            }
         
           

        //    Dim dt_detail As DataTable = sql_outsource.check_WeightDiff(Me.Lb_id.Text.Trim(), Me.Lb_po_no.Text.Trim())
        //    If dt_detail.Rows.Count > 0 Then
        //        'have material diff over 10%
        //        Session("Dg_report") = dt_detail
        //        dt_detail = Nothing
        //        Dim obj_var As New variable_101
        //        With obj_var
        //            .str_id = Me.Lb_id.Text.Trim()
        //            .str_po = Me.Lb_po_no.Text.Trim()
        //            .str_set = Me.Lb_set.Text.Trim()
        //            .str_plant = Me.Lb_plant.Text.Trim()
        //            .str_vendor = Me.Lb_vendor.Text.Trim()
        //        End With
        //        Session("obj_var") = obj_var
        //        obj_var = Nothing
        //        Response.Redirect("101_ConfirmSendtoSAP.aspx", False)
        //        Exit Sub
        //    Else
        //        'no error and get Record_ID
        //        Dim record_id As Integer = sql_outsource.get_RecordID(Me.Lb_id.Text.Trim(), Session("HH_ID"), Session("user_id"), Me.Lb_po_no.Text.Trim(), sql_outsource.sp_GroupType.all_Mat)
        //        If record_id = 0 Then
        //            script_func.script_alert("Get Record ID on Error", Page)
        //            Exit Sub
        //        Else
        //            Dim obj_var As New variable_101
        //            With obj_var
        //                .str_id = record_id
        //                .str_po = Me.Lb_po_no.Text.Trim()
        //            End With
        //            Session("obj_var") = obj_var
        //            obj_var = Nothing
        //            Response.Redirect("101_WaitMatDoc.aspx", False)
        //        End If
        //    End If
        //Catch ex As Exception
        //    script_func.script_alert("Send to SAP on Error!!", Page)
        //End Try

        }
    }
}