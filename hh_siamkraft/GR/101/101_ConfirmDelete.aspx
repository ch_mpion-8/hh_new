﻿<%@ Register TagPrefix="uc1" TagName="header_control" Src="../../header_control.ascx"   %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="101_ConfirmDelete.aspx.cs" Inherits="hh_siamkraft._101_ConfirmDelete" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>_101_ConfirmDelete</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C# .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"/>
		<LINK href="../../css/style.css" type="text/css" rel="stylesheet"/>
  
</head>
    <body style="MARGIN: 1px" MS_POSITIONING="GridLayout">
		<form id="myForm" method="post" runat="server">
			<table class="table_main" cellSpacing="0" cellPadding="0" width="225" border="0">
				<tr>
					<td class="headtable"><uc1:header_control id="Header_control1" runat="server"></uc1:header_control></td>
				</tr>
				<tr>
					<td class="headtable" align="center">Goods Receipt&nbsp;- Outsource</td>
				</tr>
				<TR>
					<td>
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="normalText" align="left" colSpan="2">PO Number&nbsp;<IMG hspace="1" src="../../images/red_arrow.gif" align="absMiddle">&nbsp;<asp:label id="Lb_po" Runat="server"></asp:label>
								</td>
							</tr>
							<tr>
								<td colSpan="2" nowrap>&nbsp;<IMG src="../../images/question.gif" align="absMiddle">&nbsp;<asp:Label ID="Lb_info" Runat="server" ForeColor="red" CssClass="normalText">Have old scan Do you want delete data ?</asp:Label></td>
							</tr>
							<tr>
								<td align="left" colSpan="2" class="normalText"><asp:radiobutton id="Rb_yes" Runat="server" GroupName="confirm" Text="Yes delete all data"></asp:radiobutton></td>
							</tr>
							<tr>
								<td align="left" colSpan="2" class="normalText"><asp:radiobutton id="Rb_no" Runat="server" GroupName="confirm" Text="No delete &amp; keep all data"></asp:radiobutton></td>
							</tr>
						</table>
					</td>
				</TR>
				<tr class="tr" style="PADDING-RIGHT: 2px; PADDING-LEFT: 2px; PADDING-BOTTOM: 2px; PADDING-TOP: 2px">
					<td class="normalText" align="left"><asp:button id="Bt_ok" Runat="server" Text=" OK " CssClass="buttonTag" Width="56px" OnClick="Bt_ok_Click"></asp:button></td>
				</tr>
			</table>
			<table>
				<tr>
					<td><asp:label id="Lb_id" Runat="server" Visible="False"></asp:label></td>
				</tr>
			</table>
		</form>
	</body>
</html>
