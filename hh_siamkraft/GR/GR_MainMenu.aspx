﻿<%@ Register TagPrefix="uc1" TagName="header_control" Src="../header_control.ascx"   %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GR_MainMenu.aspx.cs" Inherits="hh_siamkraft.GR_MainMenu" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>GR_MainMenu</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C# .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"/>
		<LINK href="../css/style.css" type="text/css" rel="stylesheet"/>
	    <style type="text/css">
            .style1
            {
                height: 25px;
            }
        </style>
</head>
<body leftMargin="1" topMargin="1" rightmargin="1" bottommargin="1" MS_POSITIONING="GridLayout">
		<form id="myForm" method="post" runat="server">
			<table class="table_main" cellSpacing="0" cellPadding="0" width="225" border="0">
				<tr>
					<td class="headtable"><uc1:header_control id="Header_control1" runat="server"></uc1:header_control></td>
				</tr>
				<tr>
					<td class="headtable" align="center"><font color="white">Menu Goods Receipt</font></td>
				</tr>
				<tr height="3">
					<td></td>
				</tr>
				<tr>
					<td>
						<table cellSpacing="0" cellPadding="2" width="100%" border="0">
							<tr id="tr_grprod" runat="server" Visible="false">
								<td align="center"><asp:button id="Bt_gr_prod" runat="server" CssClass="buttonTag" Width="190px" Text="GOODS RECEIPT PRODUCTS"
										Height="22px" OnClick="Bt_gr_prod_Click"></asp:button></td>
							</tr>
							<tr style="display: none;">
								<td align="center"><asp:button id="Bt_gr_outsource" runat="server" CssClass="buttonTag" Width="190px" Text="GOODS RECEIPT OUTSOURCE"
										Height="22px" tabIndex="1" OnClick="Bt_gr_outsource_Click"></asp:button></td>
							</tr>
							<tr id="tr_viewstock" runat="server" Visible="false">
								<td align="center"><asp:button id="Bt_viewStock" runat="server" CssClass="buttonTag" Width="190px" Text="VIEW STOCK 525"
										Height="22px" tabIndex="2" OnClick="Bt_viewStock_Click"></asp:button></td>
							</tr>
							<tr>
								<td align="center"><asp:button id="Bt_mainmenu" runat="server" Height="22px" Text="MAIN MENU" Width="190px" CssClass="buttonTag"
										tabIndex="3" OnClick="Bt_mainmenu_Click"></asp:button></td>
							</tr>
							<tr>
								<td align="center"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
