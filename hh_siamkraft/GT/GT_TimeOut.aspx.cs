﻿using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{
    public partial class GT_TimeOut : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();
        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
               
            }
            catch (Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(Page.IsPostBack))
            {
                Page_Init(sender, e);
            }
        }

        protected void Bt_retry_Click(object sender, EventArgs e)
        {
            Response.Redirect("GT_WaitData.aspx", false);
        }
    }
}