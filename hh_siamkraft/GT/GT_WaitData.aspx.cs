﻿using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{
    public partial class GT_WaitData : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();
        protected void Page_Init(object sender, EventArgs e)
        {
            
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(Page.IsPostBack))
            {
                try
                {
                    if (Session["Record_ID"] != null && Session["ScanID"] != null && Session["MovementType"] != null)
                    {
                        lblRecordID.Text = Convert.ToString(Session["Record_ID"]);
                        lblScanID.Text = Convert.ToString(Session["ScanID"]);
                        lblMovementType.Text = Convert.ToString(Session["MovementType"]);
                    }
                    else
                    {
                        Response.Redirect("GT_MainMenu.aspx");
                    }
                    //GetProcessTimeout();
                    if (Session["refresh"] == null)
                    {
                        Session["refresh"] = "1";
                    }

                    //if (Convert.ToInt32(Session["refresh"]) > Convert.ToInt32(Session["time_out"]))
                    if (Convert.ToInt32(Session["refresh"]) > 10)
                    {
                        Session["Record_ID"] = lblRecordID.Text;
                        Session["ScanID"] = lblScanID.Text;
                        Session["MovementType"] = lblMovementType.Text;
                        Session.Remove("time_out");
                        Session.Remove("refresh");
                        Response.Redirect("GT_TimeOut.aspx", false);
                    }

                    CheckResultControl();
                }
                catch (Exception ex)
                {
                    script_func.script_alert(ex.Message, Page);
                }
            }
        }


        protected void CheckResultControl()
        {
            try
            {
                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession(connect))
                {
                    var repo = new CommonRepository(session);
                    var result = repo.GetPlsControl(Convert.ToInt32(lblRecordID.Text));
                    if(result.Value.Return_Code != null)
                    {
                        var msg = result.Value.Return_Msg;
                        if (result.Value.Return_Code == "N")
                        {
                            Session["refresh"] = Convert.ToString(Convert.ToInt32(Session["refresh"]) + 1);
                            Response.Write("<meta http-equiv=refresh content=5 url='GT_WaitData.aspx'");
                        }
                        else if(result.Value.Return_Code == "00") // Complete
                        {
                            // UpdateStatusCompleteSAP
                            var repo2 = new GTRepository(session);
                            var result2 = repo2.UpdateStatusSAP(Convert.ToInt32(lblRecordID.Text), "C", msg);
                            if(result2.Value)
                            {
                                RemoveSession();
                                Session["matDoc"] = msg;
                                Response.Redirect("GT_Matdoc.aspx", false);
                            }
                        }
                        else // Error From SAP
                        {
                            // UpdateStatusErrorSAP
                            var repo2 = new GTRepository(session);
                            var result2 = repo2.UpdateStatusSAP(Convert.ToInt32(lblRecordID.Text), null, null);
                            if (result2.Value)
                            {
                                Session["error"] = msg;
                                Response.Redirect("GT_Matdoc.aspx", false);
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
            }
        }

        protected void RemoveSession()
        {
            Session.Remove("time_out");
            Session.Remove("refresh");
            Session.Remove("Record_ID");
            Session.Remove("MovementType");
            Session.Remove("ScanID");
        }
        protected void GetProcessTimeout()
        {
            try
            {
                if (Session["time_out"] == null)
                {
                    var connect = Convert.ToString(Session["Connection_String"]);
                    using (var session = new SkRepositorySession(connect))
                    {
                        int timeOut = 0;
                        var repo = new GTRepository(session);
                        var result = repo.SelectTimeoutForSAP("GT");
                        if(result.Value <= 0)
                        {
                            timeOut = 10;
                        }
                        Session["time_out"] = Convert.ToString(timeOut);
                    }
                }
            }
            catch(Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
            }
        }
    }
}