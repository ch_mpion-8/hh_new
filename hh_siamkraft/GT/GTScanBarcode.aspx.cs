﻿using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{
    public partial class GTScanBarcode : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();
        protected void Page_Init(object sender, EventArgs e)
        {
            
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(Page.IsPostBack))
            {
                if (Session["MovementType"] == null)
                {
                    Response.Redirect("~/GT/GT_MainMenu.aspx", false);
                }
                script_func.ConfirmButton(btnSendtoSAP, "Do you want Send data to SAP ?");
                Label2.Text = Convert.ToString(Session["PlantTo"]);
                Label3.Text = Convert.ToString(Session["StorageFrom"]);
                Label4.Text = Convert.ToString(Session["StorageTo"]);


                lblMovementType.Text = Convert.ToString(Session["MovementType"]);
                lblScanID.Text = Convert.ToString(Session["ScanID"]);
                lblStorageFrom.Text = Convert.ToString(Session["PlantFrom"]);
                lblStorageT.Text = Convert.ToString(Session["StorageTo"]);
                lblStorageF.Text = Convert.ToString(Session["StorageFrom"]);
                lblPlantT.Text = Convert.ToString(Session["PlantTo"]);


                lblItemText.Text = Convert.ToString(Session["ItemText"]);
                lblUserName.Text = Convert.ToString(Session["user_id"]);

                lblWHNo.Text = Convert.ToString(Session["WH_No"]);
                lblWHCode.Text = Convert.ToString(Session["UserLogin_WHCode"]);

                if (lblMovementType.Text == "343")
                {
                    lblHeaderText.Text = "Transfer 343 Blocked to UR";
                }
                else if (lblMovementType.Text == "344")
                {
                    lblHeaderText.Text = "Transfer 344 UR to Blocked";
                }
                else if (lblMovementType.Text == "301")
                {
                    lblHeaderText.Text = "Mvt 301 Transfer Plant";
                }
                BindingDataGrid();
                script_func.script_focus(txtBatch.ClientID, Page);
            }
        }
        protected void RemoveSession()
        {
            Session.Remove("MovementType");
            Session.Remove("ScanID");
            Session.Remove("StorageFrom");
            Session.Remove("ItemText");
        }
        protected void BindingDataGrid()
        {
            try
            {
                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession(connect))
                {
                    var repo = new GTRepository(session);

                    var result = repo.SelectScanDataDetail(Convert.ToInt32(lblScanID.Text), lblMovementType.Text, Convert.ToInt32(lblWHNo.Text), lblUserName.Text);

                    if (result.Value.Count() > 0)
                    {
                        dgDetail.DataSource = result.Value;
                        dgDetail.DataBind();
                        lblTotalCount.Text = result.Value.Count().ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
            }
        }
        protected void SaveScanBarcode()
        {
            try
            {
                if (txtBatch.Text.Trim().Length == 0)
                {
                    script_func.script_focus(txtBatch.ClientID, Page);
                    return;
                }

                if (txtBatch.Text.Trim().Length != 10)
                {
                    script_func.script_alert("Scan Batch invalid", Page);
                    script_func.script_focus(txtBatch.ClientID, Page);
                    script_func.script_select(txtBatch.ClientID, Page);
                }

                string MvBatch = txtBatch.Text.Trim().ToUpper();
                string storageTo;
                string uom;
                if (lblMovementType.Text == "343" || lblMovementType.Text == "344")
                {
                    storageTo = String.Empty;
                }
                if (rdbRoll.Checked)
                {
                    uom = "ROL";
                }
                else
                {
                    uom = "SH";
                }

                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession(connect))
                {
                    var repo = new GTRepository(session);
                    var result = repo.SaveScanBarcode(Convert.ToInt32(lblScanID.Text), lblMovementType.Text, Label3.Text, Label4.Text, lblItemText.Text
                               , uom, txtBatch.Text.Trim().ToUpper(), MvBatch, Convert.ToInt32(lblWHNo.Text), Label2.Text, lblUserName.Text
                               , "N");
                    if(result.Error == null)
                    {
                        txtBatch.Text = String.Empty;
                        BindingDataGrid();
                        script_func.script_focus(txtBatch.ClientID, Page);
                    }
                    else
                    {
                        if(result.Error.Message.Equals("Duplicate"))
                        {
                            Session["MovementType"] = lblMovementType.Text;
                            Session["ScanID"] = lblScanID.Text;
                            Session["StorageFrom"] = lblStorageFrom.Text;
                            Session["ItemText"] = lblItemText.Text;
                            Session["DataDuplicate"] = result.Value;
                            Response.Redirect("GTConfirmDelete.aspx");
                        }
                        else
                        {
                            if (result.Error.Message.Length > 0)
                            {
                                script_func.script_alert(result.Error.Message, Page);
                            }
                            else
                            {
                                script_func.script_alert("Save Data Error", Page);
                            }
                            txtBatch.Text = String.Empty;
                            script_func.script_focus(txtBatch.ClientID, Page);
                        }
                    }
                }

            }
            catch(Exception ex)
            {
                txtBatch.Text = String.Empty;
                script_func.script_alert(ex.Message, Page);
                script_func.script_focus(txtBatch.ClientID, Page);
            }
        }
        protected void ConfirmToSAP()
        {
            try
            {
                if (dgDetail.Items.Count == 0)
                {
                    script_func.script_alert("Must have data at least 1 item.", Page);
                    script_func.script_focus(txtBatch.ClientID, Page);
                    return;
                }
                string StorageFrom = "";
                string WHC = "";
                if (lblMovementType.Text == "301")
                {
                    StorageFrom = lblStorageF.Text.Trim();
                    WHC = lblStorageFrom.Text.Trim();
                }
                else
                {
                    StorageFrom = lblStorageFrom.Text;
                    WHC = lblWHCode.Text;
                }
                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession(connect))
                {
                    var repo = new GTRepository(session);
                    var result = repo.ConfirmToSAP(Convert.ToString(Session["HH_ID"]), Convert.ToInt32(lblScanID.Text), lblMovementType.Text, StorageFrom, Convert.ToInt32(lblWHNo.Text), WHC, lblUserName.Text, "");

                    if(result.Value > 0)
                    {
                        Session["Record_ID"] = result.Value;
                        Session["MovementType"] = lblMovementType.Text;
                        Session["ScanID"] = lblScanID.Text;
                        Response.Redirect("GT_WaitData.aspx", false);
                    }
                }
            }
            catch(Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("GT_MainMenu.aspx");
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                txtBatch.Text = String.Empty;
                script_func.script_focus(txtBatch.ClientID, Page);
            }
            catch (Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
            }
        }

        protected void btnSendtoSAP_Click(object sender, EventArgs e)
        {
            ConfirmToSAP();
        }

        protected void txtBatch_TextChanged(object sender, EventArgs e)
        {
            SaveScanBarcode();
        }
    }
}