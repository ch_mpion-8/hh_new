﻿using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{
    public partial class GT_Scan321 : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();
        protected void Page_Init(object sender, EventArgs e)
        {
            
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(Page.IsPostBack))
            {
                try
                {
                    if (Session["MovementType"] == null)
                    {
                        Response.Redirect("~/GT/GT_MainMenu.aspx", false);
                    }

                    if (Convert.ToString(Session["MovementType"]) == "321")
                    {
                        lblHeaderText.Text = "Transfer 321 QI to UR";
                    }
                    else if (Convert.ToString(Session["MovementType"]) == "322")
                    {
                        lblHeaderText.Text = "Transfer 322 UR to QI";
                    }
                    script_func.ConfirmButton(btnSendtoSAP, "Do you want Send data to SAP ?");

                    lblMovementType.Text = Convert.ToString(Session["MovementType"]);
                    lblScanID.Text = Convert.ToString(Session["ScanID"]);
                    lblStorageFrom.Text = Convert.ToString(Session["StorageFrom"]);

                    lblItemText.Text = Convert.ToString(Session["ItemText"]);
                    lblUserName.Text = Convert.ToString(Session["user_id"]);
                    lblWHNo.Text = Convert.ToString(Session["WH_No"]);
                    lblWHCode.Text = Convert.ToString(Session["UserLogin_WHCode"]);

                    RemoveSession();
                    BindingDataGrid();
                    script_func.script_focus(txtBatch.ClientID, Page);
                }
                catch (Exception ex)
                {
                    script_func.script_alert(ex.Message, Page);
                }
            }
        }
        protected void BindingDataGrid()
        {
            try
            {
                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession(connect))
                {
                    var repo = new GTRepository(session);
                    var result = repo.SelectScanDataDetail(Convert.ToInt32(lblScanID.Text), lblMovementType.Text, Convert.ToInt32(lblWHNo.Text), lblUserName.Text);
                    if (result.Value.Count() > 0)
                    {
                        dgDetail.DataSource = result.Value;
                        dgDetail.DataBind();
                        lblTotalCount.Text = Convert.ToString(result.Value.Count());
                    }

                }
            }
            catch (Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
            }
        }
        protected void RemoveSession()
        {
            Session.Remove("MovementType");
            Session.Remove("ScanID");
            Session.Remove("StorageFrom");
            Session.Remove("ItemText");
        }

        protected void txtBatch_TextChanged(object sender, EventArgs e)
        {
            SaveScanBarcode();
        }
        protected void SaveScanBarcode()
        {
            try
            {
                if (txtBatch.Text.Trim().Length == 0)
                {
                    script_func.script_focus(txtBatch.ClientID, Page);
                    return;
                }

                if (txtBatch.Text.Trim().Length != 10)
                {
                    script_func.script_alert("Scan Batch invalid", Page);
                    script_func.script_focus(txtBatch.ClientID, Page);
                    script_func.script_select(txtBatch.ClientID, Page);
                }


                string storageTo = "";
                string uom = "";
                string ClearText_ = "N";
                if (rdbRoll.Checked)
                {
                    uom = "ROL";
                }
                else
                {
                    uom = "SH";
                }

                if (ClearText.Checked)
                {
                    ClearText_ = "Y";
                }

                string st_t = "";
                string st_f = "";
                st_f = lblStorageFrom.Text;


                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession(connect))
                {
                    var repo = new GTRepository(session);
                    var result = repo.SaveScanBarcode(Convert.ToInt32(lblScanID.Text), lblMovementType.Text, st_f, st_t, lblItemText.Text
                                            , uom, txtBatch.Text.Trim().ToUpper(), txtBatch.Text.Trim().ToUpper(), Convert.ToInt32(lblWHNo.Text), lblWHCode.Text, lblUserName.Text
                                            , ClearText_);
                    if (result.Error == null)
                    {
                        txtBatch.Text = "";
                        BindingDataGrid();
                        script_func.script_focus(txtBatch.ClientID, Page);
                    }
                    else
                    {
                        if (result.Error.Message.Equals("Duplicate"))
                        {
                            Session["MovementType"] = lblMovementType.Text;
                            Session["ScanID"] = lblScanID.Text;
                            Session["StorageFrom"] = lblStorageFrom.Text;
                            Session["ItemText"] = lblItemText.Text;
                            Session["DataDuplicate"] = result.Value;
                            Response.Redirect("ConfirmDelete321.aspx");
                        }
                        else
                        {
                            if (result.Error != null)
                            {
                                script_func.script_alert(result.Error.Message, Page);
                            }
                            else
                            {
                                script_func.script_alert("Save Data Error", Page);
                            }
                            txtBatch.Text = "";
                            script_func.script_focus(txtBatch.ClientID, Page);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/GT/GT_MainMenu.aspx");
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            txtBatch.Text = "";
            script_func.script_focus(txtBatch.ClientID, Page);
        }

        protected void btnSendtoSAP_Click(object sender, EventArgs e)
        {

            ConfirmToSAP();
        }

        protected void ConfirmToSAP()
        {
            try
            {
                if (dgDetail.Items.Count == 0)
                {
                    script_func.script_alert("Must have data at least 1 item.", Page);
                    script_func.script_focus(txtBatch.ClientID, Page);
                }
                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession(connect))
                {
                    var repo = new GTRepository(session);
                    var result = repo.ConfirmToSAP(Convert.ToString(Session["UserLogin_HHID"]), Convert.ToInt32(lblScanID.Text)
                            , lblMovementType.Text, lblStorageFrom.Text, Convert.ToInt32(lblWHNo.Text), lblWHCode.Text, lblUserName.Text, "");
                    if(result.Value >0)
                    {
                        Session["Record_ID"] = result.Value;
                        Session["MovementType"] = lblMovementType.Text;
                        Session["ScanID"] = lblScanID.Text;
                        Response.Redirect("~/GT/GT_WaitData.aspx", false);
                    }
                    else
                    {
                        //script_func.script_alert("SAP Error", Page);
                        script_func.script_focus(txtBatch.ClientID, Page);
                    }
                }
            }
            catch (Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
            }
        }
    }
}