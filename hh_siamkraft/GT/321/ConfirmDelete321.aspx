﻿<%@ Register TagPrefix="uc1" TagName="header_control" Src="../../header_control.ascx"   %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ConfirmDelete321.aspx.cs" Inherits="hh_siamkraft.ConfirmDelete321" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>ConfirmDelete321</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C# .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"/>
		<LINK href="../../css/style.css" type="text/css" rel="stylesheet"/>
	    <style type="text/css">
            .style1
            {
                height: 25px;
            }
        </style>
</head>
    <body leftMargin="1" topMargin="1" rightmargin="1" bottommargin="1" MS_POSITIONING="GridLayout">
		<form id="myForm" method="post" runat="server">
			<table class="table_main" cellSpacing="0" cellPadding="0" width="225" border="0">
				<tr>
					<td class="headtable"><uc1:header_control id="Header_control1" runat="server"></uc1:header_control></td>
				</tr>
				<tr>
					<td class="headtable" align="center"><font color="#ffffff"><FONT color="#ffffff">Confirm 
								Delete</FONT></font>&nbsp;
					</td>
				</tr>
				<tr>
					<td>
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="normalText" align="left">ID</td>
								<td align="left"><IMG hspace="1" src="../../images/red_arrow.gif" align="absMiddle">
                                </td>
                                <td>
									<asp:label id="lblScanID" runat="server" CssClass="normalText"></asp:label></td>
							</tr>
							<tr>
								<td class="normalText" align="left">Batch</td>
								<td align="left"><IMG hspace="1" src="../../images/red_arrow.gif" align="absMiddle"
                                </td>
                                <td>
									<asp:label id="lblBatch" runat="server" CssClass="normalText"></asp:label></td>
							</tr>
							<tr>
								<td class="normalText" align="left">Qty</td>
								<td align="left"><IMG hspace="1" src="../../images/red_arrow.gif" align="absMiddle">
                                </td>
                                <td>
									<asp:label id="lblQty" runat="server" CssClass="normalText"></asp:label></td>
							</tr>
							<tr>
								<td class="normalText" align="left">Material</td>
								<td align="left"><IMG hspace="1" src="../../images/red_arrow.gif" align="absMiddle">
                                </td>
                                <td>
									<asp:label id="lblMaterial" runat="server" CssClass="normalText"></asp:label></td>
							</tr>
						</table>
						<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD align="left" nowrap>&nbsp;<img src="../../images/question.gif" align="absMiddle">&nbsp;<asp:Label ID="Lb_info" Runat="server" ForeColor="red" CssClass="normalText">Batch duplicated Do you want delete data ?</asp:Label></TD>
							</TR>
							<TR>
								<TD>&nbsp;
									<asp:radiobutton id="rdbNo" runat="server" ForeColor="Black" cssclass="normalText" GroupName="confirm"
										Text="No"></asp:radiobutton><asp:radiobutton id="rdbYes" runat="server" ForeColor="Black" cssclass="normalText" GroupName="confirm"
										Text="Yes"></asp:radiobutton></TD>
							</TR>
						</TABLE>
						&nbsp;&nbsp;&nbsp; </FONT></td>
				</tr>
				<TR class="tr" style="PADDING-BOTTOM: 2px; PADDING-LEFT: 2px; PADDING-RIGHT: 2px; PADDING-TOP: 2px">
					<TD align="center">
						<P><asp:button id="btnSave" runat="server" CssClass="buttonTag" Width="50px" Text="OK"></asp:button></P>
					</TD>
				</TR>
			</table>
			<asp:Label ID="lblMovementType" Runat="server" Visible="False"></asp:Label><asp:Label ID="lblWHNo" Runat="server" Visible="False"></asp:Label>
			<asp:Label ID="lblWHCode" Runat="server" Visible="False"></asp:Label><asp:Label ID="lblUserName" Runat="server" Visible="False"></asp:Label>
			<asp:Label ID="lblStorageFrom" Runat="server" Visible="False"></asp:Label><asp:Label ID="lblStorageTo" Runat="server" Visible="False"></asp:Label>
			<asp:Label ID="lblItemText" Runat="server" Visible="False"></asp:Label>
		</form>
	</body>
</html>
