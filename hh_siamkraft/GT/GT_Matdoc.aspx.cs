﻿using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{
    public partial class GT_Matdoc : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();
        protected void Page_Init(object sender, EventArgs e)
        {
            
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(Page.IsPostBack))
            {
                try
                {
                    if (Session["matDoc"] != null)
                    {
                        Lb_info.Text = "Message";
                        Label1.Text = Convert.ToString(Session["matDoc"]);
                    }
                    else if (Session["error"] != null)
                    {
                        Lb_info.Text = "ERROR";
                        Label1.Text = Convert.ToString(Session["error"]);
                    }
                }
                catch (Exception ex)
                {
                    script_func.script_alert(ex.Message, Page);
                }
            }
        }

        protected void Bt_retry_Click(object sender, EventArgs e)
        {
            Response.Redirect("GT_WaitData.aspx", false);
        }

        protected void Bt_ok_Click(object sender, EventArgs e)
        {
            if(Session["matDoc"] != null)
            {
                Session.Remove("matDoc");
            }
            else
            {
                Session.Remove("error");
            }
            Response.Redirect("GT_MainMenu.aspx", false);
        }
    }
}