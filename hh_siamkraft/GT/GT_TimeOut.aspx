﻿<%@ Register TagPrefix="uc1" TagName="header_control" Src="../header_control.ascx"   %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GT_TimeOut.aspx.cs" Inherits="hh_siamkraft.GT_TimeOut" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>GT_TimeOut</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C# .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"/>
		<LINK href="../css/style.css" type="text/css" rel="stylesheet"/>
	    <style type="text/css">
            .style1
            {
                height: 25px;
            }
        </style>
</head>
    <body leftMargin="1" topMargin="1" rightmargin="1" bottommargin="1" MS_POSITIONING="GridLayout">
		<form id="myForm" method="post" runat="server">
			<table class="table_main" id="tbl_scan" cellSpacing="0" cellPadding="0" width="225" border="0"
				runat="server">
				<tr>
					<td class="headtable"><uc1:header_control id="Header_control1" runat="server"></uc1:header_control></td>
				</tr>
				<tr>
					<td class="headtable" align="center">Goods Transfer</td>
				</tr>
				<tr height="2">
					<td class="normalText">&nbsp;<img src="../images/timeout.gif" align="absMiddle">&nbsp;<asp:label id="Lb_SapMsg" runat="server" ForeColor="Red" CssClass="normalText">TimeOut</asp:label></td>
				</tr>
				<tr>
					<td>
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
						</table>
					</td>
				</tr>
				<tr class="tr" style="PADDING-BOTTOM: 2px; PADDING-LEFT: 2px; PADDING-RIGHT: 2px; PADDING-TOP: 2px">
					<td align="center">
                        <asp:button id="Bt_retry" Width="50px" CssClass="buttonTag" Runat="server" Text="Retry" OnClick="Bt_retry_Click"></asp:button>&nbsp;
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
