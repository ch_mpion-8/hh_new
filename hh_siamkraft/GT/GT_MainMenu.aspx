﻿<%@ Register TagPrefix="uc1" TagName="header_control" Src="../header_control.ascx"   %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GT_MainMenu.aspx.cs" Inherits="hh_siamkraft.GT_MainMenu" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>GT_MainMenu</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C# .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"/>
		<LINK href="../css/style.css" type="text/css" rel="stylesheet"/>
	    <style type="text/css">
            .style1
            {
                height: 25px;
            }
        </style>
</head>
<body leftMargin="1" topMargin="1" rightmargin="1" bottommargin="1" MS_POSITIONING="GridLayout">
		<form id="myForm" method="post" runat="server">
			<table class="table_main" border="0" cellSpacing="0" cellPadding="0" width="225">
				<tr>
					<td class="headtable"><uc1:header_control id="Header_control1" runat="server"></uc1:header_control></td>
				</tr>
				<tr>
					<td class="headtable" align="center">Menu Goods Transfer</td>
				</tr>
				<tr>
					<td>
						<table border="0" cellSpacing="0" cellPadding="0" width="100%">
							<TR id="tr_301" runat="server" style="display:none; ">
								<TD class="smallerText" noWrap align="left"><asp:radiobutton id="Rb_301" runat="server" AutoPostBack="True" GroupName="gt" OnCheckedChanged="Rb_301_CheckedChanged"></asp:radiobutton>301 
									Plant</TD>
								<TD class="smallerText" noWrap align="left"><IMG style="Z-INDEX: 0" align="absMiddle" src="../images/red_arrow.gif">&nbsp;
									<asp:button style="Z-INDEX: 0" id="Bt_301" runat="server" CssClass="buttonTag" Text="ViewID"
										Width="70px" OnClick="Bt_301_Click"></asp:button>&nbsp;
									<asp:label style="Z-INDEX: 0" id="Lb_301" runat="server" CssClass="smallerText" Width="16px"
										ForeColor="White" BackColor="White" BorderColor="Black">ID</asp:label>
                                    <asp:label style="Z-INDEX: 0" id="lblID301" runat="server" CssClass="smallerText"></asp:label>

								</TD>
							</TR>
							<tr id="tr_321" runat="server" visible="false">
								<td class="smallerText" noWrap align="left"><asp:radiobutton id="Rb_321" runat="server" AutoPostBack="True" GroupName="gt" OnCheckedChanged="Rb_321_CheckedChanged"></asp:radiobutton>321 
									QI -&gt; UR</td>
								<td class="smallerText" noWrap align="left"><IMG align="absMiddle" src="../images/red_arrow.gif">&nbsp;
									<asp:button id="Bt_321" runat="server" CssClass="buttonTag" Text="ViewID" Width="70px" OnClick="Bt_321_Click"></asp:button>&nbsp;
                                    <asp:label id="Lb_321" runat="server" CssClass="smallerText" Width="16px" ForeColor="White"
										BackColor="White" BorderColor="Black">ID</asp:label>
									<asp:label id="lblID321" runat="server" CssClass="smallerText"></asp:label>

								</td>
							</tr>
							<tr id="tr_322" runat="server" visible="false">
								<td class="smallerText" noWrap align="left"><asp:radiobutton id="Rb_322" runat="server" AutoPostBack="True" GroupName="gt" CssClass="smallerText"
										ForeColor="Black" OnCheckedChanged="Rb_322_CheckedChanged"></asp:radiobutton>322 UR -&gt; QI</td>
								<td class="smallerText" noWrap align="left"><IMG align="absMiddle" src="../images/red_arrow.gif">&nbsp;
									<asp:button id="Bt_322" runat="server" CssClass="buttonTag" Text="ViewID" Width="70px" OnClick="Bt_322_Click"></asp:button>&nbsp;<asp:label id="Label1" runat="server" CssClass="smallerText" Width="16px" ForeColor="White"
										BackColor="White" BorderColor="Black">ID</asp:label>
									<asp:label id="lblID322" runat="server" CssClass="smallerText"></asp:label></td>
							</tr>
							<tr id="tr_311" runat="server" visible="false">
								<td class="smallerText" noWrap align="left"><asp:radiobutton id="Rb_311" runat="server" AutoPostBack="True" GroupName="gt" CssClass="smallerText"
										ForeColor="Black" OnCheckedChanged="Rb_311_CheckedChanged"></asp:radiobutton>311 Storage</td>
								<td class="smallerText" noWrap align="left"><IMG align="absMiddle" src="../images/red_arrow.gif">&nbsp;
									<asp:button id="Bt_311" runat="server" CssClass="buttonTag" Text="ViewID" Width="70px" OnClick="Bt_311_Click"></asp:button>&nbsp;<asp:label id="Label2" runat="server" CssClass="smallerText" Width="16px" ForeColor="White"
										BackColor="White" BorderColor="Black">ID</asp:label>
									<asp:label id="lblID311" runat="server" CssClass="smallerText"></asp:label></td>
							</tr>
							<tr id="tr_312" runat="server" visible="false">
								<td style="HEIGHT: 20px" class="smallerText" noWrap align="left"><asp:radiobutton id="Rb_312" runat="server" AutoPostBack="True" GroupName="gt" CssClass="smallerText"
										ForeColor="Black" OnCheckedChanged="Rb_312_CheckedChanged"></asp:radiobutton>312 Storage</td>
								<td style="HEIGHT: 20px" class="smallerText" noWrap align="left"><IMG align="absMiddle" src="../images/red_arrow.gif">&nbsp;
									<asp:button id="Bt_312" runat="server" CssClass="buttonTag" Text="ViewID" Width="70px" OnClick="Bt_312_Click"></asp:button>&nbsp;<asp:label id="Label3" runat="server" CssClass="smallerText" Width="16px" ForeColor="White"
										BackColor="White" BorderColor="Black">ID</asp:label>
									<asp:label id="lblID312" runat="server" CssClass="smallerText"></asp:label></td>
							</tr>
							<tr id="tr_343" runat="server" visible="false">
								<td style="HEIGHT: 20px" class="smallerText" noWrap align="left"><asp:radiobutton id="rdb343" runat="server" AutoPostBack="True" GroupName="gt" CssClass="smallerText"
										ForeColor="Black" OnCheckedChanged="rdb343_CheckedChanged"></asp:radiobutton>343 Blocked to UR</td>
								<td style="HEIGHT: 20px" class="smallerText" noWrap align="left"><IMG align="absMiddle" src="../images/red_arrow.gif">&nbsp;
									<asp:button id="btnViewID_343" runat="server" CssClass="buttonTag" Text="ViewID" Width="70px" OnClick="btnViewID_343_Click"></asp:button>&nbsp;<asp:label id="Label4" runat="server" CssClass="smallerText" Width="16px" ForeColor="White"
										BackColor="White" BorderColor="Black">ID</asp:label>
									<asp:label id="lblID343" runat="server" CssClass="smallerText"></asp:label></td>
							</tr>
							<tr id="tr_344" runat="server" visible="false">
								<td style="HEIGHT: 20px" class="smallerText" noWrap align="left"><asp:radiobutton id="rdb344" runat="server" AutoPostBack="True" GroupName="gt" CssClass="smallerText"
										ForeColor="Black" OnCheckedChanged="rdb344_CheckedChanged"></asp:radiobutton>344 UR to Blocked</td>
								<td style="HEIGHT: 20px" class="smallerText" noWrap align="left"><IMG align="absMiddle" src="../images/red_arrow.gif">&nbsp;
									<asp:button id="btnViewID_344" runat="server" CssClass="buttonTag" Text="ViewID" Width="70px" OnClick="btnViewID_344_Click"></asp:button>&nbsp;<asp:label id="Label6" runat="server" CssClass="smallerText" Width="16px" ForeColor="White"
										BackColor="White" BorderColor="Black">ID</asp:label>
									<asp:label id="lblID344" runat="server" CssClass="smallerText"></asp:label></td>
							</tr>
							<tr id="tr_id" runat="server">
								<td class="smallText" noWrap align="left"><font color="red">Key In ID</font></td>
								<td class="smallText" noWrap align="left"><asp:textbox id="Tb_id" CssClass="inputTag" Width="96px" Runat="server"></asp:textbox></td>
							</tr>
							<TR id="tr_from_plant" runat="server">
								<TD class="smallText" noWrap align="left"><FONT color="red">FromPlant</FONT></TD>
								<TD class="smallText" noWrap align="left"><asp:dropdownlist style="Z-INDEX: 0" id="ddlFromPlant" runat="server" AutoPostBack="True" Width="97px"
										cssclass="colorBox" Enabled="False">
										<asp:ListItem Value="2" Selected="True">Select</asp:ListItem>
										<asp:ListItem Value="7550">7550</asp:ListItem>
										<asp:ListItem Value="7523">7523</asp:ListItem>
										<asp:ListItem Value="7521">7521</asp:ListItem>
									</asp:dropdownlist></TD>
							</TR>
							<TR id="tr_to_plant" runat="server">
								<TD class="smallText" noWrap align="left"><FONT color="red">ToPlant</FONT></TD>
								<TD class="smallText" noWrap align="left"><asp:dropdownlist style="Z-INDEX: 0" id="ddlToPlant" runat="server" Width="97px" cssclass="colorBox"
										Enabled="False">
										<asp:ListItem Value="1" Selected="True">Select</asp:ListItem>
										<asp:ListItem Value="7550">7550</asp:ListItem>
										<asp:ListItem Value="7523">7523</asp:ListItem>
										<asp:ListItem Value="7521">7521</asp:ListItem>
									</asp:dropdownlist></TD>
							</TR>
							<tr id="tr_from" runat="server">
								<td class="smallText" noWrap align="left"><font color="red">From</font></td>
								<td class="smallText" noWrap align="left"><asp:dropdownlist id="Db_From" runat="server" Width="97px" cssclass="colorBox" Enabled="False"></asp:dropdownlist></td>
							</tr>
							<tr id="tr_problem" runat="server">
								<td class="smallText" noWrap align="left"><font color="red">Problem</font></td>
								<td class="smallText" noWrap align="left"><asp:dropdownlist id="Db_Problem" runat="server" Width="97px" cssclass="colorBox"></asp:dropdownlist></td>
							</tr>
							<tr id="tr_storage_from" runat="server">
								<td class="smallText" noWrap align="left"><font color="red">StorageFrom</font></td>
								<td class="smallText" noWrap align="left"><asp:dropdownlist id="Db_StFrom" runat="server" AutoPostBack="True" Width="97px" cssclass="colorBox"
										Enabled="False" OnSelectedIndexChanged="Db_StFrom_SelectedIndexChanged">
										<asp:ListItem Value="16">16</asp:ListItem>
										<asp:ListItem Value="P16">P16</asp:ListItem>
									</asp:dropdownlist></td>
							</tr>
							<tr id="tr_storage_to" runat="server">
								<td class="smallText" noWrap align="left"><font color="red">StorageTo</font></td>
								<td class="smallText" noWrap align="left"><asp:dropdownlist id="Db_StTo" runat="server" Width="97px" cssclass="colorBox" Enabled="False">
										<asp:ListItem Value="1">16</asp:ListItem>
										<asp:ListItem Value="P16">P16</asp:ListItem>
									</asp:dropdownlist></td>
							</tr>
							<tr id="tr_desc" runat="server" visible="false">
								<td class="smallText" noWrap align="left"><font color="red">Description</font></td>
								<td class="smallText" noWrap align="left">
                                    <asp:dropdownlist id="Db_StTo0" 
                                        runat="server" Width="97px" cssclass="colorBox" Enabled="False">
									</asp:dropdownlist></td>
							</tr>
							<TR id="tr_license" runat="server">
								<TD class="smallText" noWrap align="left"><font color="red">License Car</font></TD>
								<TD class="smallText" noWrap align="left"><asp:textbox style="Z-INDEX: 0" id="txtLicenCar" CssClass="inputTag" Width="96px" Runat="server"
										Enabled="False" MaxLength="10"></asp:textbox></TD>
							</TR>
						</table>
					</td>
				</tr>
				<tr style="PADDING-BOTTOM: 2px; PADDING-LEFT: 2px; PADDING-RIGHT: 2px; PADDING-TOP: 2px"
					class="tr">
					<td align="center">
						<asp:button id="Bt_Ok" runat="server" CssClass="buttonTag" Text="OK" Width="55px" OnClick="Bt_Ok_Click"></asp:button>&nbsp;
					    <asp:button id="Bt_Mainmenu" runat="server" CssClass="buttonTag" Text="Back" Width="50px" OnClick="Bt_Mainmenu_Click"></asp:button>&nbsp;
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
