﻿using Entities;
using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{
    public partial class ConfirmDelete311 : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();
        protected void Page_Init(object sender, EventArgs e)
        {
            
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(Page.IsPostBack))
            {
                try
                {
                    if (Session["MovementType"] == null)
                    {
                        Response.Redirect("~/GT/GT_MainMenu.aspx", false);
                        return;
                    }
                    lblMovementType.Text = Convert.ToString(Session["MovementType"]);
                    lblScanID.Text = Convert.ToString(Session["ScanID"]);
                    lblStorageFrom.Text = Convert.ToString(Session["StorageFrom"]);
                    lblStorageTo.Text = Convert.ToString(Session["StorageTo"]);
                    lblItemText.Text = Convert.ToString(Session["ItemText"]);
                    lblUserName.Text = Convert.ToString(Session["user_id"]);
                    lblWHNo.Text = Convert.ToString(Session["WH_No"]);

                    List<HH_GT_Item> giItem = (List<HH_GT_Item>)Session["DataDuplicate"];
                    var data = giItem.FirstOrDefault();
                    lblBatch.Text = data.Batch;
                    lblQty.Text = Convert.ToString(data.Qty);
                    lblMaterial.Text = data.Material;

                    RemoveSession();
                }
                catch (Exception ex)
                {
                    script_func.script_alert(ex.Message, Page);
                }
            }
        }

        protected void RemoveSession()
        {
            Session.Remove("MovementType");
            Session.Remove("ScanID");
            Session.Remove("StorageFrom");
            Session.Remove("StorageTo");
            Session.Remove("ItemText");
        }

        protected void DeleteScanBarcode()
        {
            try
            {
                bool isComplete = false;
                if (rdbNo.Checked)
                {
                    isComplete = true;
                }
                else if (rdbYes.Checked)
                {
                    var connect = Convert.ToString(Session["Connection_String"]);
                    using (var session = new SkRepositorySession(connect))
                    {
                        var repo = new GTRepository(session);
                        var result = repo.DeleteScanBarcode(Convert.ToInt32(lblScanID.Text), lblMovementType.Text, lblBatch.Text, Convert.ToInt32(lblWHNo.Text));
                        if (result.Value)
                        {
                            isComplete = true;
                        }
                        else
                        {
                            script_func.script_alert("Delete data error!", Page);
                        }
                    }
                }
                else
                {
                    script_func.script_alert("Please Select 'Yes'/'No'", Page);
                }

                if(isComplete)
                {
                    Session["MovementType"] = lblMovementType.Text;
                    Session["ScanID"] = lblScanID.Text;
                    Session["StorageFrom"] = lblStorageFrom.Text;
                    Session["StorageTo"] = lblStorageTo.Text;
                    Session["ItemText"] = lblItemText.Text;
                    Response.Redirect("GT_Scan311.aspx");
                }

            }
            catch(Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            DeleteScanBarcode();
        }
    }
}