﻿using Entities;
using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using hh_siamkraft.SI_StockInquiry_OAService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{
    public partial class GT_Scan311 : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();
        protected void Page_Init(object sender, EventArgs e)
        {
            
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(Page.IsPostBack))
            {
                try
                {
                    if (Session["MovementType"] == null)
                    {
                        Response.Redirect("~/GT/GT_MainMenu.aspx", false);
                    }
                    lblHeaderText.Text = "Transfer " + Convert.ToString(Session["MovementType"]) + " Storage";

                    script_func.ConfirmButton(btnSendtoSAP, "Do you want Send data to SAP ?");

                    lblMovementType.Text = Convert.ToString(Session["MovementType"]);
                    lblScanID.Text = Convert.ToString(Session["ScanID"]);
                    lblStorageFrom.Text = Convert.ToString(Session["StorageFrom"]);
                    if (Session["Sloc"] != null)
                    {
                        lblStorageTo.Text = Convert.ToString(Session["Sloc"]);
                    }
                    else
                    {
                        lblStorageTo.Text = Convert.ToString(Session["StorageTo"]);
                    }

                    lblItemText.Text = Convert.ToString(Session["ItemText"]);
                    lblUserName.Text = Convert.ToString(Session["user_id"]);
                    lblLicenseCar.Text = Convert.ToString(Session["LicenseCar"]);
                    lblWHNo.Text = Convert.ToString(Session["WH_No"]);
                    lblWHCode.Text = Convert.ToString(Session["UserLogin_WHCode"]);


                    BindingDataGrid();
                    script_func.script_focus(txtBatch.ClientID, Page);
                }
                catch (Exception ex)
                {
                    script_func.script_alert(ex.Message, Page);
                }
            }
        }

        protected void RemoveSession()
        {
            try
            {
                Session.Remove("MovementType");
                Session.Remove("ScanID");
                Session.Remove("StorageFrom");
                Session.Remove("StorageTo");
                Session.Remove("ItemText");
            }
            catch (Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
            }
        }
        protected void BindingDataGrid()
        {
            try
            {
                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession(connect))
                {
                    var repo = new GTRepository(session);
                    var result = repo.SelectScanDataDetail(Convert.ToInt32(lblScanID.Text), lblMovementType.Text, Convert.ToInt32(lblWHNo.Text), lblUserName.Text);
                    if (result.Value.Count() > 0)
                    {
                        dgDetail.DataSource = result.Value;
                        dgDetail.DataBind();
                        lblTotalCount.Text = Convert.ToString(result.Value.Count());
                    }

                }
            }
            catch (Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
            }
        }

        protected void rdbRoll_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbRoll.Checked)
            {
                txtBatch.Text = String.Empty;
                script_func.script_focus(txtBatch.ID, Page);
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/GT/GT_MainMenu.aspx");
            }
            catch (Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
            }
        }

        protected void rdbSheet_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbRoll.Checked)
            {
                rdbSheet.Text = String.Empty;
                script_func.script_focus(txtBatch.ID, Page);
            }
        }

        protected void txtBatch_TextChanged(object sender, EventArgs e)
        {
            SaveScanBarcode();
        }

        protected void SaveScanBarcode()
        {
            try
            {
                if (txtBatch.Text.Trim().Length == 0)
                {
                    script_func.script_focus(txtBatch.ClientID, Page);
                    return;
                }

                if (txtBatch.Text.Trim().Length != 10)
                {
                    script_func.script_alert("Scan Batch invalid", Page);
                    script_func.script_focus(txtBatch.ClientID, Page);
                    script_func.script_select(txtBatch.ClientID, Page);
                }


                string storageTo = String.Empty;
                string uom = String.Empty;
                string ClearText_ = "N";
                if (rdbRoll.Checked)
                {
                    uom = "ROL";
                }
                else
                {
                    uom = "SH";
                }

                if (ClearText.Checked)
                {
                    ClearText_ = "Y";
                }

                string st_t = String.Empty;
                string st_f = String.Empty;
                if (Session["Sloc"] != null)
                {
                    if (lblMovementType.Text == "311")
                    {
                        st_t = lblStorageTo.Text + "" + txtBatch.Text.Trim().Substring(0, 1);
                    }
                    else if (lblMovementType.Text == "312")
                    {
                        st_t = txtBatch.Text.Trim().Substring(0, 1);
                    }
                    st_f = txtBatch.Text.Trim().Substring(0, 1);
                }
                else
                {
                    st_t = lblStorageTo.Text;
                    st_f = lblStorageFrom.Text.Trim();
                }


                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession(connect))
                {
                    var repo = new GTRepository(session);
                    var result = repo.SaveScanBarcode(Convert.ToInt32(lblScanID.Text), lblMovementType.Text, st_f, st_t, lblItemText.Text
                                            , uom, txtBatch.Text.Trim().ToUpper(), txtBatch.Text.Trim().ToUpper(), Convert.ToInt32(lblWHNo.Text), lblWHCode.Text, lblUserName.Text
                                            , ClearText_);
                    if (result.Error == null)
                    {
                        txtBatch.Text = String.Empty;
                        BindingDataGrid();
                        script_func.script_focus(txtBatch.ClientID, Page);
                    }
                    else
                    {
                        if (result.Error.Message.Equals("Duplicate"))
                        {
                            Session["MovementType"] = lblMovementType.Text;
                            Session["ScanID"] = lblScanID.Text;
                            Session["StorageFrom"] = lblStorageFrom.Text;
                            Session["StorageTo"] = lblStorageTo.Text;
                            Session["ItemText"] = lblItemText.Text;
                            Session["DataDuplicate"] = result.Value;
                            Response.Redirect("ConfirmDelete311.aspx");
                        }
                        else
                        {
                            if (result.Error != null)
                            {
                                script_func.script_alert(result.Error.Message, Page);
                            }
                            else
                            {
                                script_func.script_alert("Save Data Error", Page);
                            }
                            txtBatch.Text = String.Empty;
                            script_func.script_focus(txtBatch.ClientID, Page);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                txtBatch.Text = String.Empty;
                script_func.script_focus(txtBatch.ClientID, Page);
            }
            catch (Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
            }
        }

        protected void btnSendtoSAP_Click(object sender, EventArgs e)
        {

            try
            {
                var result = ConfirmToSAP();
                if (result == "0")
                {
                    Panel1.Visible = false;
                    Panel2.Visible = true;
                    Image1.Visible = true;
                    //Timer1.Interval = 10000;
                    //Timer1.Enabled = true;
                }
                else if (result == "X")
                {
                    Panel1.Visible = false;
                    Panel2.Visible = true;
                    Image1.Visible = true;
                    //Timer1.Interval = 10000;
                    //Timer1.Enabled = true;
                }
                else
                {
                    script_func.script_alert("Error :" + result, Page);
                }
            }
            catch (Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
            }
        }

        protected string ConfirmToSAP()
        {
            try
            {
                if(dgDetail.Items.Count == 0)
                {
                    script_func.script_alert("Must have data at least 1 item.", Page);
                    script_func.script_focus(txtBatch.ClientID, Page);
                    return "99";
                }

                //if (Convert.ToString(Session["Connection_String"]).ToUpper().Contains("PLSSKIC"))
                //{
                //    var result = GetBatch(Convert.ToInt32(lblScanID.Text));

                //    if (result.Count() > 0)
                //    {
                //        Session["tranid"] = stockinq(result);
                //    }
                //    return "X";
                //}

                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession(connect))
                {
                    var repo = new GTRepository(session);
                    var result = repo.ConfirmToSAP(Convert.ToString(Session["UserLogin_HHID"]), Convert.ToInt32(lblScanID.Text)
                            , lblMovementType.Text, lblStorageFrom.Text, Convert.ToInt32(lblWHNo.Text), lblWHCode.Text, lblUserName.Text, lblLicenseCar.Text);
                    if (result.Value > 0)
                    {
                        Session["Record_ID"] = result.Value;
                        Session["MovementType"] = lblMovementType.Text;
                        Session["ScanID"] = lblScanID.Text;
                        Response.Redirect("~/GT/GT_WaitData.aspx", false);
                    }
                    else
                    {
                        //script_func.script_alert("SAP Error", Page);
                        script_func.script_focus(txtBatch.ClientID, Page);
                    }
                }
                return "0";

                //    record_ID = bll.ConfirmToSAP(hh_siamkraft.Helper.GetData.Instance.HHID, Convert.ToInt32(lblScanID.Text) _
                //, lblMovementType.Text, lblStorageFrom.Text, Convert.ToInt32(lblWHNo.Text), lblWHCode.Text, lblUserName.Text, lblLicenseCar.Text)
                //If(record_ID > 0) Then
                //    Session("Record_ID") = record_ID
                //    Session("MovementType") = lblMovementType.Text
                //    Session("ScanID") = lblScanID.Text
                //    Response.Redirect("~/GT/GT_WaitData.aspx", False)
                //End If
            }
            catch (Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
                return ex.Message;
            }
        }


        protected string stockinq(IList<HH_GT_Item> batch)
        {
            string TranscID = DateTime.Now.ToString("yyyy", new CultureInfo("en-US")) + "" + DateTime.Now.ToString("MM") + "" + DateTime.Now.ToString("dd") + "" + DateTime.Now.ToString("HHMMssfff");
            DT_StockInquiryReq req = new DT_StockInquiryReq();

            req.COMPANY = "0750";
            req.QUERY_ID = TranscID;
            req.USER_ID = "HH";
            DT_StockInquiryReqItem2 reqItem2 = new DT_StockInquiryReqItem2();

            req.BatchData = new DT_StockInquiryReqItem2[batch.Count];
            var list = new List<DT_StockInquiryReqItem2>();
            //int cnt = 0;
            foreach (var item in batch)
            {
                list.Add(new DT_StockInquiryReqItem2 { BATCH = item.Batch });
                //req.BatchData[cnt] = new DT_StockInquiryReqItem2();
                //req.BatchData[cnt].BATCH = item.Batch;
                //cnt++;
            }
            req.BatchData = list.ToArray();

            var url = ConfigurationManager.AppSettings["webServiceGT"];
            Uri uri = new Uri(url);

            var service = new SI_StockInquiry_OAService.SI_StockInquiry_OAClient();
            

            service.ClientCredentials.UserName.UserName = "PPGPISSI";
            service.ClientCredentials.UserName.Password = "ppgsksi-cxq1";
            //service.Endpoint.Address = new EndpointAddress("http://id.web/Services/EchoService.svc");
            service.SI_StockInquiry_OA(req);

            //http://cxqwd.scg.co.th:8080/XISOAPAdapter/MessageServlet?senderParty=&senderService=BS_PAPER_PLS_Q&receiverParty=&receiverService=&interface=SI_PLSGoodsMovementPost_OS&interfaceNamespace=urn:scg.co.th:PAPER:PLS:Goods
            //http://cxqmsg.scg.co.th:8178/XISOAPAdapter/MessageServlet?senderParty=&amp;amp;senderService=BS_PAPER_COMMON_Q&amp;amp;receiverParty=&amp;amp;receiverService=&amp;amp;interface=SI_StockInquiry_OA&amp;amp;interfaceNamespace=urn%3Ascg.co.th%3APAPER%3ACOMMON%3AStock&quot

            //Dim Uri As New Uri(ws_stock.Url)

            //ws_stock.UseDefaultCredentials = True
            //ws_stock.Credentials = New System.Net.NetworkCredential("PPGPISSI", "ppgsksi-cxq1")
            //ws_stock.Timeout = 60000

            //ws_stock.SI_StockInquiry_OA(req)
            return TranscID;
        }
            
        protected IList<HH_GT_Item> GetBatch(int scanid)
        {
            var connect = Convert.ToString(Session["Connection_String"]);
            using (var session = new SkRepositorySession(connect))
            {
                var repo = new GTRepository(session);
                var result = repo.SelectScanDataDetail(scanid, "311", 0, "");
                return result.Value;
            }
            
        }

        protected string update_sloc(IList<StockInquiryItem> gtItem, int gtID)
        {
            try
            {

                foreach (var item in gtItem)
                {
                    var connect = Convert.ToString(Session["Connection_String"]);
                    using (var session = new SkRepositorySession(connect))
                    {
                        var repo = new GTRepository(session);
                        var result = repo.updateSloc(item.Storage, item.Batch, item.MatCode, gtID);
                    }

                }
                return "";
            }
            catch (Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
                return ex.Message;
            }
        }

        protected IList<StockInquiryItem> GetBatch_fromStock(string queryid)
        {
            var connect = Convert.ToString(Session["Connection_String"]);
            using (var session = new SkRepositorySession(connect))
            {
                var repo = new GTRepository(session);
                var result = repo.GetBatch_fromStock(queryid);
                return result.Value;
            }

        }

        protected void DelItem(string queryID)
        {
            var connect = Convert.ToString(Session["Connection_String"]);
            using (var session = new SkRepositorySession(connect))
            {
                var repo = new GTRepository(session);
                var result = repo.DelItem(queryID);
            }
        }

        protected void DelHead(string queryID)
        {
            var connect = Convert.ToString(Session["Connection_String"]);
            using (var session = new SkRepositorySession(connect))
            {
                var repo = new GTRepository(session);
                var result = repo.DelHead(queryID);
            }
        }

        //protected void Timer1_Tick(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (Session["tranid"] != null)
        //        {
        //            //DataTable dt_stock;
        //            var dt_stock = GetBatch_fromStock(Convert.ToString(Session["tranid"]));
        //            var result = string.Empty;
        //            if (dt_stock.Count > 0)
        //                result = update_sloc(dt_stock, Convert.ToInt32(lblScanID.Text));
        //            DelItem(Convert.ToString(Session["tranid"]));
        //            DelHead(Convert.ToString(Session["tranid"]));

        //            int record_ID = 0;

        //            var connect = Convert.ToString(Session["Connection_String"]);
        //            using (var session = new SkRepositorySession(connect))
        //            {
        //                var repo = new GTRepository(session);
        //                var storage = repo.getStorageFrom2(Convert.ToInt32(lblScanID.Text)).Value;

        //                if (storage.Count() > 0)
        //                {
        //                    foreach (var item in storage) {
        //                        record_ID = repo.ConfirmToSAP_Group(Convert.ToString(Session["UserLogin_HHID"]), Convert.ToInt32(lblScanID.Text)
        //                                    , lblMovementType.Text, item, Convert.ToInt32(lblWHNo.Text), lblWHCode.Text, lblUserName.Text, lblLicenseCar.Text).Value;
        //                    }
        //                    if ((record_ID > 0))
        //                    {
        //                        Session["Record_ID"] = record_ID;
        //                        Session["MovementType"] = lblMovementType.Text;
        //                        Session["ScanID"] = lblScanID.Text;
        //                        Response.Redirect("~/GT/GT_WaitData.aspx", false);
        //                        Session.Remove("Sloc");
        //                    }
        //                }
        //            }

        //        }
        //    }
        //    catch (Exception ex) {
        //        script_func.script_alert(ex.Message, Page);
        //    }
        //}

        //protected void Timer1_Tick(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if(Session["tranid"] != null)
        //        {
        //            var dt_stock = GetBatch_fromStock(Session["tranid"].ToString());
        //            if(dt_stock.Count()>0)
        //            {
        //                var updSloc = update_sloc(dt_stock, Convert.ToInt32(lblScanID.Text));
        //            }
        //            DelItem(Session["tranid"].ToString());
        //            DelHead(Session["tranid"].ToString());


        //            var connect = Convert.ToString(Session["Connection_String"]);
        //            using (var session = new SkRepositorySession(connect))
        //            {
        //                var repo = new GTRepository(session);
        //                var result = repo.getStorageFrom2(Convert.ToInt32(lblScanID.Text));
        //                if(result.Value.Count()>0)
        //                {
        //                    int record_ID = 0;
        //                    foreach (var item in result.Value)
        //                    {
        //                        record_ID = repo.ConfirmToSAP_Group(Convert.ToString(Session["UserLogin_HHID"]), Convert.ToInt32(lblScanID.Text)
        //                                        , lblMovementType.Text, item.ToString(), Convert.ToInt32(lblWHNo.Text), lblWHCode.Text, lblUserName.Text, lblLicenseCar.Text).Value;
        //                    }
        //                    if (record_ID > 0)
        //                    {
        //                        Session["Record_ID"] = record_ID;
        //                        Session["MovementType"] = lblMovementType.Text;
        //                        Session["ScanID"] = lblScanID.Text;
        //                        Response.Redirect("~/GT/GT_WaitData.aspx", false);
        //                        Session.Remove("Sloc");
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        script_func.script_alert(ex.Message, Page);
        //    }
        //}
    }
}