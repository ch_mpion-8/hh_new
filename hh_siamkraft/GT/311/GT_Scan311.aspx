﻿<%@ Register TagPrefix="uc1" TagName="header_control" Src="../../header_control.ascx"   %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GT_Scan311.aspx.cs" Inherits="hh_siamkraft.GT_Scan311" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>GT_Scan311</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C# .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"/>
		<LINK href="../../css/style.css" type="text/css" rel="stylesheet"/>
	    <style type="text/css">
            .style1
            {
                height: 25px;
            }
        </style>
</head>
    <body leftMargin="1" topMargin="1" rightmargin="1" bottommargin="1" MS_POSITIONING="GridLayout">
		<form id="myForm" method="post" runat="server">
			<asp:Panel ID="Panel1" runat="server" Width="245px">
                <TABLE class="table_main" cellSpacing="0" cellPadding="0" width="225" border="0">
                    <tr>
                        <td class="headtable">
                            <uc1:header_control id="Header_control1" runat="server"></uc1:header_control>
                            <%--<asp:ScriptManager ID="ScriptManager1" runat="server">
                            </asp:ScriptManager>--%>
                        </td>
                    </tr>
                    <TR>
                        <td class="headtable" align="center">
                            <font color="#ffffff">
                            <asp:Label id="lblHeaderText" runat="server" ForeColor="White"></asp:Label>
                            </font>
                        </td>
                    </TR>
                    <tr>
                        <td>
                            <table cellSpacing="0" cellPadding="0" width="100%" border="0">
                                <tr>
                                    <td class="normalText" valign="middle" align="left" style="HEIGHT: 13px">
                                        ID</td>
                                    <td class="normalText" valign="middle" align="left" style="HEIGHT: 13px">
                                        <IMG hspace="1" src="../../images/red_arrow.gif" align="absMiddle">
                                    </td>
                                    <td valign="middle" >
                                            <asp:label id="lblScanID" runat="server" Width="72px" CssClass="normalText"></asp:label>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="middle" class="normalText" align="left">
                                        StorageF</td>
                                    <td valign="middle" class="normalText" align="left">
                                        <IMG hspace="1" 
                                        src="../../images/red_arrow.gif" align="absMiddle">
                                    </td>
                                    <td valign="middle" >
                                            <asp:label id="lblStorageFrom" runat="server" Width="72px" 
                                        CssClass="normalText"></asp:label>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="middle" class="normalText" align="left">
                                        StorageTo</td>
                                    <td valign="middle" class="normalText" align="left">
                                        <IMG hspace="1" 
                                        src="../../images/red_arrow.gif" align="absMiddle">
                                    </td>
                                    <td valign="middle" >
                                            <asp:label id="lblStorageTo" runat="server" Width="72px" CssClass="normalText"></asp:label>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="middle" class="normalText" align="left" style="HEIGHT: 13px">
                                        Problem</td>
                                    <td valign="middle" class="normalText" align="left" style="HEIGHT: 13px">
                                        <IMG hspace="1" src="../../images/red_arrow.gif" align="absMiddle">
                                    </td>
                                    <td valign="middle" >
                                            <asp:label id="lblItemText" runat="server" CssClass="normalText" Width="190px"></asp:label>
                                    </td>
                                </tr>
                                <TR>
                                    <TD valign="middle" class="normalText" align="left">
                                        License</TD>
                                    <TD valign="middle" class="normalText" align="left">
                                        <IMG style="Z-INDEX: 0" 
                                        hspace="1" align="absMiddle" src="../../images/red_arrow.gif">
                                    </td>
                                    <td valign="middle" >
                                            <asp:label style="Z-INDEX: 0" id="lblLicenseCar" runat="server" 
                                        CssClass="normalText" Width="72px"></asp:label>
                                    </td>
                                </TR>
                                <TR>
                                    <TD valign="middle" style="HEIGHT: 19px" class="normalText" vAlign="top" align="left">
                                        ClearText</TD>
                                    <TD valign="middle" style="HEIGHT: 19px" class="normalText" vAlign="top" align="left">
                                        <IMG style="Z-INDEX: 0" hspace="1" align="absMiddle" 
                                        src="../../images/red_arrow.gif">
                                    </td>
                                    <td valign="middle" >
                                            <asp:CheckBox style="Z-INDEX: 0" id="ClearText" runat="server" Text="ClearText">
                                            </asp:CheckBox>
                                    </td>
                                </TR>
                                <tr>
                                    <td valign="middle" class="normalText" vAlign="top" align="left" style="HEIGHT: 19px">
                                        UOM</td>
                                    <td valign="middle" class="normalText" vAlign="top" align="left" style="HEIGHT: 19px">
                                        <IMG hspace="1" align="absMiddle" 
                                        src="../../images/red_arrow.gif">
                                    </td>
                                    <td valign="middle" >
                                            <asp:radiobutton style="Z-INDEX: 0" id="rdbRoll" runat="server" 
                                        AutoPostBack="True" Checked="True"
										GroupName="UOM" Text="Roll" OnCheckedChanged="rdbRoll_CheckedChanged">
                                            </asp:radiobutton>
                                            &nbsp;
                                            <asp:radiobutton style="Z-INDEX: 0" id="rdbSheet" runat="server" 
                                        AutoPostBack="True" GroupName="UOM"
										Text="Sheet" OnCheckedChanged="rdbSheet_CheckedChanged">
                                            </asp:radiobutton>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="middle" class="normalText" align="left">
                                        Batch</td>
                                    <td valign="middle" class="normalText" vAlign="top" align="left" style="HEIGHT: 19px">
                                        <IMG hspace="1" align="absMiddle" 
                                        src="../../images/red_arrow.gif">
                                    </td>
                                    <td valign="middle" align="left">
                                        <asp:textbox id="txtBatch" runat="server" CssClass="inputTag" MaxLength="10" 
                                        AutoPostBack="True" OnTextChanged="txtBatch_TextChanged"></asp:textbox>
                                    </td>
                                </tr>
                                <!--
							<tr>
								<td class="normalText" align="left" colspan="2">
								</td>
							</tr>
							-->
                            </table>
                            <asp:datagrid id="dgDetail" runat="server" CellSpacing="1" 
                            AutoGenerateColumns="False" BorderColor="#E7E7FF"
							BorderStyle="None" BorderWidth="1px" BackColor="White" CellPadding="1" Width="100%">
                                <FooterStyle ForeColor="#4A3C8C" BackColor="#B5C7DE"></FooterStyle>
                                <SelectedItemStyle Font-Bold="True" ForeColor="#F7F7F7" BackColor="#738A9C">
                                </SelectedItemStyle>
                                <AlternatingItemStyle BackColor="#F7F7F7"></AlternatingItemStyle>
                                <ItemStyle ForeColor="#4A3C8C" BackColor="#E7E7FF"></ItemStyle>
                                <HeaderStyle Font-Bold="True" ForeColor="#F7F7F7" BackColor="#4A3C8C">
                                </HeaderStyle>
                                <Columns>
                                    <asp:BoundColumn DataField="Batch" HeaderText="Batch">
                                        <HeaderStyle HorizontalAlign="Center" ForeColor="White" VerticalAlign="Middle">
                                        </HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Qty" HeaderText="Qty" DataFormatString="{0:0.000}">
                                        <HeaderStyle HorizontalAlign="Center" ForeColor="White" VerticalAlign="Middle">
                                        </HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="Material" HeaderText="Material">
                                        <HeaderStyle HorizontalAlign="Center" ForeColor="White" VerticalAlign="Middle">
                                        </HeaderStyle>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                    </asp:BoundColumn>
                                </Columns>
                                <PagerStyle HorizontalAlign="Right" ForeColor="#4A3C8C" BackColor="#E7E7FF" 
                                Mode="NumericPages"></PagerStyle>
                            </asp:datagrid>
                            &nbsp;&nbsp;
                            <asp:label id="Label1" runat="server" CssClass="normalText">RecordCount :</asp:label>
                            &nbsp;&nbsp;
                            <asp:label id="lblTotalCount" runat="server" Width="32px" CssClass="normalText"></asp:label>
                        </td>
                    </tr>
                    <tr class="tr" 
                    
                        style="PADDING-BOTTOM: 2px; PADDING-LEFT: 2px; PADDING-RIGHT: 2px; PADDING-TOP: 2px">
                        <td align="center">
                            <asp:button id="btnSendtoSAP" runat="server" Width="80px" CssClass="buttonTag" 
                            Text="Send to SAP" OnClick="btnSendtoSAP_Click"></asp:button>
                            <asp:button id="btnClear" runat="server" Width="50px" CssClass="buttonTag" 
                            Text="Clear" OnClick="btnClear_Click"></asp:button>
                            <asp:button id="btnBack" runat="server" Width="40px" CssClass="buttonTag" 
                            Text="Back" OnClick="btnBack_Click"></asp:button>
                        </td>
                    </tr>
                </TABLE>
            </asp:Panel>
            <asp:Panel ID="Panel2" runat="server" BackColor="#CCCCCC" Height="22px" 
                Visible="False" Width="245px">
                <font color="#ffffff">
                <asp:Label id="lblHeaderText0" runat="server" 
    ForeColor="White">Waiting Data....</asp:Label>
                &nbsp;
                <asp:Image ID="Image1" runat="server" Height="22px" 
                    ImageUrl="~/Images/loading.gif" Visible="False" />
                </font>
            </asp:Panel>
            <br />
&nbsp;<asp:label id="lblMovementType" Visible="False" Runat="server"></asp:label><asp:label id="lblWHNo" Visible="False" Runat="server"></asp:label><asp:label id="lblWHCode" Visible="False" Runat="server"></asp:label><asp:label id="lblUserName" Visible="False" Runat="server"></asp:label>
		    <%--<asp:Timer ID="Timer1" runat="server" Interval="10000" OnTick="Timer1_Tick">
            </asp:Timer>--%>
		</form>
	</body>
</html>
