﻿<%@ Register TagPrefix="uc1" TagName="header_control" Src="../header_control.ascx"   %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GT_WaitData.aspx.cs" Inherits="hh_siamkraft.GT_WaitData" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>GT_WaitData</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C# .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"/>
		<LINK href="../css/style.css" type="text/css" rel="stylesheet"/>
	    <style type="text/css">
            .style1
            {
                height: 25px;
            }
        </style>
</head>
    <body leftMargin="1" topMargin="1" rightmargin="1" bottommargin="1" MS_POSITIONING="GridLayout">
		<form id="myForm" method="post" runat="server">
			<table class="table_main" cellSpacing="0" cellPadding="0" width="225" border="0">
				<tr>
					<td class="headtable"><uc1:header_control id="Header_control1" runat="server"></uc1:header_control></td>
				</tr>
				<tr>
					<td class="headtable" align="center">Goods Transfer</td>
				</tr>
				<tr>
					<td>
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<th class="normalText" align="left" colSpan="2">
									&nbsp;<img src="../images/loading.gif" align="absMiddle">&nbsp;
									<asp:label id="Lb_msg" runat="server" CssClass="normalText">Waiting... Mat Doc From SAP</asp:label></th></tr>
						</table>
					</td>
				</tr>
				<tr class="tr" style="PADDING-BOTTOM: 2px; PADDING-LEFT: 2px; PADDING-RIGHT: 2px; PADDING-TOP: 2px">
					<td align="left">&nbsp;
					</td>
				</tr>
			</table>
			<asp:Label ID="lblRecordID" Runat="server" Visible="False"></asp:Label><asp:Label ID="lblScanID" Runat="server" Visible="False"></asp:Label>
			<asp:Label ID="lblMovementType" Runat="server" Visible="False"></asp:Label>
		</form>
	</body>
</html>
