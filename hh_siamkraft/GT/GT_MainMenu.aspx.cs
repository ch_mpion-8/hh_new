﻿using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{
    public partial class GT_MainMenu : System.Web.UI.Page
    {
        ScriptFunction script_func = new ScriptFunction();

        protected void Page_Init(object sender, EventArgs e)
        {
         
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(Page.IsPostBack))
            {
                Lock();
                showMenu();
                //HideControl();
                GetPlantDesc();
                GetProblemDesc();
                //checkAuthorize(Convert.ToString(Session["user_id"]), "HH")
            }
        }

        private void showMenu()
        {
            var permission = Convert.ToString(Session["Permission"]);
            var list = permission.Split(',').ToList();


            if (list.Where(o => o == "311").Count() > 0)
            {
                tr_311.Visible = true;
            }

            if (list.Where(o => o == "312").Count() > 0)
            {
                tr_312.Visible = true;
            }

            if (list.Where(o => o == "321").Count() > 0)
            {
                tr_321.Visible = true;
            }

            if (list.Where(o => o == "322").Count() > 0)
            {
                tr_322.Visible = true;
            }

            if (list.Where(o => o == "343").Count() > 0)
            {
                tr_343.Visible = true;
            }

            if (list.Where(o => o == "344").Count() > 0)
            {
                tr_344.Visible = true;
            }
        }

        protected void Bt_Mainmenu_Click(object sender, EventArgs e)
        {
            Response.Redirect("../main_menu.aspx");
        }

        protected void GetPlantDesc()
        {
            var connect = Convert.ToString(Session["Connection_String"]);
            var plant = Convert.ToString(Session["HH_Plant"]);
            //using (var session = new SkRepositorySession(connect))
            using (var session = new SkRepositorySession())
            {
                var repo = new GTRepository(session);
                var result = repo.GetPlantDesc(plant);

                foreach (var item in result.Value)
                {
                    Db_StTo0.Items.Add(new ListItem(Convert.ToString(item), Convert.ToString(item)));
                }
                Db_StTo0.Items.Insert(0, new ListItem("Select ", ""));
            }
        }
        protected void GetProblemDesc()
        {
            var connect = Convert.ToString(Session["Connection_String"]);
            using (var session = new SkRepositorySession(connect))
            {
                var repo = new GTRepository(session);
                var result = repo.GetProblemDesc();
                
                foreach (var item in result.Value)
                {
                    Db_Problem.Items.Add(new ListItem(Convert.ToString(item), Convert.ToString(item)));
                }
                Db_Problem.Items.Insert(0, new ListItem("Select ", ""));
            }
        }

        protected void Lock()
        {
            Db_StFrom.Enabled = false;
            Db_StTo.Enabled = false;
        }
        protected void HideControl()
        {
            tr_343.Visible = false;
            tr_344.Visible = false;
            tr_321.Visible = false;
            tr_322.Visible = false;
            tr_from.Visible = false;
            tr_problem.Visible = false;
            tr_id.Visible = false;
            tr_311.Visible = false;
            tr_312.Visible = false;
            tr_storage_from.Visible = false;
            tr_storage_to.Visible = false;
            Bt_Ok.Visible = false;
        }
        protected void SelectMaxID(string movementType)
        {
            try
            {
                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession(connect))
                {
                    var repo = new GTRepository(session);
                    var result = repo.SelectMaxID(movementType);

                    if (result.Value == 0)
                    {
                        script_func.script_alert("Mmt: " + movementType + " not found ID.", Page);
                        return;
                    }
                    else
                    {
                        if(movementType == "311")
                        {
                            lblID311.Text = Convert.ToString(result.Value);
                        } 
                        else if (movementType == "312")
                        {
                            lblID312.Text = Convert.ToString(result.Value);
                        }
                        else if(movementType == "321")
                        {
                            lblID321.Text = Convert.ToString(result.Value);
                        }
                        else if(movementType == "322")
                        {
                            lblID322.Text = Convert.ToString(result.Value);
                        }
                        else if (movementType == "343")
                        {
                            lblID343.Text = Convert.ToString(result.Value);
                        }
                        else if (movementType == "344")
                        {
                            lblID344.Text = Convert.ToString(result.Value);
                        }
                        else if (movementType == "301")
                        {
                            lblID301.Text = Convert.ToString(result.Value);
                        }
                    }
                }
            }
            catch(Exception e)
            {
                script_func.script_alert(e.Message, Page);
            }
        }

        protected void Rb_301_CheckedChanged(object sender, EventArgs e)
        {
            if (Rb_301.Checked)
            {
                var connect = Convert.ToString(Session["Connection_String"]);
                var WHCode = Convert.ToString(Session["UserLogin_WHCode"]);
                using (var session = new SkRepositorySession())
                {
                    var repo = new GTRepository(session);
                    var result = repo.getStorageFrom(WHCode);
                    Db_StFrom.Items.Clear();
                    Db_StFrom.Enabled = true;
                    foreach (var item in result.Value)
                    {
                        Db_StFrom.Items.Add(new ListItem(Convert.ToString(item), Convert.ToString(item)));
                    }
                    Db_StFrom.Items.Insert(0, new ListItem("Select ", ""));
                }

                ddlFromPlant.Enabled = true;
                ddlToPlant.Enabled = true;
                Db_From.Enabled = false;
                Db_Problem.Enabled = true;
                Db_StFrom.Enabled = true;
                Db_StTo.Enabled = true;

                tr_from_plant.Visible = true;
                tr_to_plant.Visible = true;
                tr_from.Visible = false;
                tr_problem.Visible = true;
                tr_storage_from.Visible = true;
                tr_storage_to.Visible = true;
            }
        }

        protected void Rb_321_CheckedChanged(object sender, EventArgs e)
        {
            Db_StFrom.Items.Clear();
            Db_StTo.Items.Clear();
            Db_From.Items.Clear();

            if (Rb_321.Checked)
            {
                var connect = Convert.ToString(Session["Connection_String"]);
                var WHCode = Convert.ToString(Session["UserLogin_WHCode"]);
                using (var session = new SkRepositorySession())
                {
                    var repo = new GTRepository(session);
                    var result = repo.getStorageFrom(WHCode);

                    Db_From.Enabled = true;
                    tr_from.Visible = true;
                    foreach (var item in result.Value)
                    {
                        Db_From.Items.Add(new ListItem(Convert.ToString(item), Convert.ToString(item)));
                    }
                    Db_From.Items.Insert(0, new ListItem("Select ", ""));
                }
            }

            
            txtLicenCar.Enabled = false;
            ddlFromPlant.Enabled = false;
            ddlToPlant.Enabled = false;
            Db_StFrom.Enabled = false;
            Db_StTo.Enabled = false;
            Db_StTo0.Enabled = false;


            tr_license.Visible = false;
            tr_from_plant.Visible = false;
            tr_to_plant.Visible = false;
            tr_storage_from.Visible = false;
            tr_storage_to.Visible = false;
            //tr_desc.Visible = false;
        }

        protected void Rb_322_CheckedChanged(object sender, EventArgs e)
        {
            Db_StFrom.Items.Clear();
            Db_StTo.Items.Clear();
            Db_From.Items.Clear();


            if (Rb_322.Checked)
            {
                var connect = Convert.ToString(Session["Connection_String"]);
                var WHCode = Convert.ToString(Session["UserLogin_WHCode"]);
                using (var session = new SkRepositorySession())
                {
                    var repo = new GTRepository(session);
                    var result = repo.getStorageFrom(WHCode);

                    Db_From.Enabled = true;
                    foreach (var item in result.Value)
                    {
                        Db_From.Items.Add(new ListItem(Convert.ToString(item), Convert.ToString(item)));
                    }
                    Db_From.Items.Insert(0, new ListItem("Select ", ""));
                }
            }
            
            Db_StFrom.Enabled = false;
            ddlFromPlant.Enabled = false;
            ddlToPlant.Enabled = false;
            txtLicenCar.Enabled = false;
            Db_StTo.Enabled = false;
            Db_StTo0.Enabled = false;
            Db_From.Enabled = true;


            tr_storage_from.Visible = false;
            tr_from_plant.Visible = false;
            tr_to_plant.Visible = false;
            tr_license.Visible = false;
            tr_storage_to.Visible = false;
            //tr_desc.Visible = false;
            tr_from.Visible = true;
        }
        protected void Rb_311_CheckedChanged(object sender, EventArgs e)
        {
            Db_StFrom.Items.Clear();
            if (Rb_311.Checked)
            {
                Db_StFrom.Items.Clear();
                Db_StTo.Items.Clear();
                txtLicenCar.Enabled = true;
                Db_StFrom.Enabled = true;
                Db_StTo.Enabled = true;
                Db_StTo0.Enabled = true;

                tr_license.Visible = true;
                tr_storage_from.Visible = true;
                tr_storage_to.Visible = true;
                //tr_desc.Visible = true;

                var connect = Convert.ToString(Session["Connection_String"]);
                var WHCode = Convert.ToString(Session["UserLogin_WHCode"]);
                using (var session = new SkRepositorySession())
                {
                    var repo = new GTRepository(session);
                    var result = repo.getStorageFrom(WHCode);
                    foreach (var item in result.Value)
                    {
                        Db_StFrom.Items.Add(new ListItem(Convert.ToString(item), Convert.ToString(item)));
                    }
                    Db_StFrom.Items.Insert(0, new ListItem("Select ", ""));
                }
            }
            Db_From.Enabled = false;
            ddlFromPlant.Enabled = false;
            ddlToPlant.Enabled = false;

            tr_from.Visible = false;
            tr_from_plant.Visible = false;
            tr_to_plant.Visible = false;
        }

        protected void Rb_312_CheckedChanged(object sender, EventArgs e)
        {
            Db_StFrom.Items.Clear();
            if (Rb_312.Checked)
            {
                Db_StFrom.Enabled = true;
                Db_StTo.Enabled = true;
                txtLicenCar.Enabled = true;

                tr_storage_from.Visible = true;
                tr_storage_to.Visible = true;
                tr_license.Visible = true;

                Db_StFrom.Items.Clear();
                Db_StTo.Items.Clear();
                var connect = Convert.ToString(Session["Connection_String"]);
                var WHCode = Convert.ToString(Session["UserLogin_WHCode"]);
                using (var session = new SkRepositorySession())
                {
                    var repo = new GTRepository(session);
                    var result = repo.getStorageFrom(WHCode);


                    foreach (var item in result.Value)
                    {
                        Db_StFrom.Items.Add(new ListItem(Convert.ToString(item), Convert.ToString(item)));
                    }
                    Db_StFrom.Items.Insert(0, new ListItem("Select ", ""));
                }
            }
            Db_From.Enabled = false;
            ddlFromPlant.Enabled = false;
            ddlToPlant.Enabled = false;
            Db_StTo0.Enabled = true;

            tr_from.Visible = false;
            tr_from_plant.Visible = false;
            tr_to_plant.Visible = false;
            //tr_desc.Visible = true;
        }

        protected void rdb343_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                Db_StFrom.Items.Clear();
                Db_StTo.Items.Clear();
                Db_From.Items.Clear();
                if (rdb343.Checked)
                {
                    Db_From.Enabled = true;
                    tr_from.Visible = true;

                    var connect = Convert.ToString(Session["Connection_String"]);
                    var WHCode = Convert.ToString(Session["UserLogin_WHCode"]);
                    using (var session = new SkRepositorySession())
                    {
                        var repo = new GTRepository(session);
                        var result = repo.getStorageFrom(WHCode);


                        foreach (var item in result.Value)
                        {
                            Db_From.Items.Add(new ListItem(Convert.ToString(item), Convert.ToString(item)));
                        }
                        Db_From.Items.Insert(0, new ListItem("Select ", ""));
                    }
                }
                
                
                Db_StFrom.Enabled = false;
                Db_StTo.Enabled = false;
                Db_StTo0.Enabled = false;
                txtLicenCar.Enabled = false;
                ddlFromPlant.Enabled = false;
                ddlToPlant.Enabled = false;

                tr_storage_from.Visible = false;
                tr_storage_to.Visible = false;
                //tr_desc.Visible = false;
                tr_license.Visible = false;
                tr_from_plant.Visible = false;
                tr_to_plant.Visible = false;

            }
            catch(Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
            }
        }

        protected void rdb344_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                Db_StFrom.Items.Clear();
                Db_StTo.Items.Clear();
                Db_From.Items.Clear();
                if (rdb344.Checked)
                {
                    Db_From.Enabled = true;
                    tr_from.Visible = true;

                    var connect = Convert.ToString(Session["Connection_String"]);
                    var WHCode = Convert.ToString(Session["UserLogin_WHCode"]);
                    using (var session = new SkRepositorySession())
                    {
                        var repo = new GTRepository(session);
                        var result = repo.getStorageFrom(WHCode);


                        foreach (var item in result.Value)
                        {
                            Db_From.Items.Add(new ListItem(Convert.ToString(item), Convert.ToString(item)));
                        }
                        Db_From.Items.Insert(0, new ListItem("Select ", ""));
                    }
                }

                
                Db_StFrom.Enabled = false;
                Db_StTo.Enabled = false;
                Db_StTo0.Enabled = false;
                txtLicenCar.Enabled = false;
                ddlFromPlant.Enabled = false;
                ddlToPlant.Enabled = false;

                tr_storage_from.Visible = false;
                tr_storage_to.Visible = false;
                //tr_desc.Visible = false;
                tr_license.Visible = false;
                tr_from_plant.Visible = false;
                tr_to_plant.Visible = false;
            }
            catch (Exception ex)
            {
                script_func.script_alert(ex.Message, Page);
            }
        }

        protected void Bt_Ok_Click(object sender, EventArgs e)
        {
            if (rdb343.Checked)
            {
                BlockedUR("343");
            }
            else if(rdb344.Checked)
            {
                BlockedUR("344");
            }
            else if(Rb_321.Checked)
            {
                BlockedUR("321");
            }
            else if (Rb_322.Checked)
            {
                BlockedUR("322");
            }
            else if (Rb_311.Checked)
            {
                //Storage("311");
                BlockedUR("311");
            }
            else if (Rb_312.Checked)
            {
                //Storage("312");
                BlockedUR("312");
            }
            else if (Rb_301.Checked)
            {
                BlockedUR("301");
            }
            else
            {
                script_func.script_alert("Please choose GT type", Page);
            }
        }

        protected void Storage(string movementType)
        {
            if (Db_StTo0.SelectedIndex > 0)
            {
                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession(connect))
                {
                    var repo = new GTRepository(session);
                    var plant = Convert.ToString(Session["HH_Plant"]);
                    var Desc1 = Db_StTo0.SelectedItem.Text;

                    var result = repo.GetPlantMasterTransfer(plant, Desc1);
                    if (result.Value.Count() > 0)
                    {
                        var dtScanDetail = result.Value.FirstOrDefault();
                        var result2 = repo.SelectMaxID(movementType);
                        if (result2.Value == 0)
                        {
                            Session["ScanID"] = "1";
                        }
                        else
                        {
                            Session["ScanID"] = Convert.ToString(result2.Value + 1);
                        }

                        Session["Sloc"] = dtScanDetail.Sloc;
                        Session["MovementType"] = movementType;
                        Session["StorageFrom"] = "";
                        Session["StorageTo"] = dtScanDetail.Sloc;
                        Session["LicenseCar"] = txtLicenCar.Text.Trim();
                        if (Db_Problem.SelectedIndex > 0)
                        {
                            Session["ItemText"] = Db_Problem.SelectedItem.Text.ToString();
                        }
                        else
                        {
                            Session["ItemText"] = String.Empty;
                        }
                        if(movementType == "311" || movementType=="312")
                        {
                            Response.Redirect("311/GT_Scan311.aspx");
                        }
                        else if (movementType == "321" || movementType == "322")
                        {
                            Response.Redirect("321/GT_Scan321.aspx");
                        }
                        Response.Redirect(movementType+"/GT_Scan"+ movementType+ ".aspx");
                    }
                }
            }
        }
        protected void BlockedUR(string movementType)
        {
            if (Tb_id.Text.Trim().Length > 0 && movementType != "301")
            {
                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession(connect))
                {
                    var repo = new GTRepository(session);

                    var ScanID = Convert.ToInt32(Tb_id.Text.Trim());
                    var WH_No = Convert.ToInt32(Session["UserLogin_WHID"]);
                    var UserName = Convert.ToString(Session["UserLogin_UserName"]);
                    var result = repo.SelectScanDataDetail(ScanID, movementType, WH_No, UserName);
                    if(result.Value.Count() > 0)
                    {
                        var dtScanDetail = result.Value.FirstOrDefault();
                        Session["MovementType"] = movementType;
                        Session["ScanID"] = Convert.ToInt32(Tb_id.Text);
                        Session["ItemText"] = dtScanDetail.Problem_Desc;

                        if (movementType=="311" || movementType=="312")
                        {
                            Session["StorageFrom"] = dtScanDetail.Storage_From;
                            Session["StorageTo"] = dtScanDetail.Storage_To;
                            Session["LicenseCar"] = txtLicenCar.Text.Trim();
                        }
                        else
                        {
                            Session["StorageFrom"] = dtScanDetail.SFrom;
                            Session["LicenseCar"] = "";
                        }
                    }
                    else
                    {
                        script_func.script_alert("Data not found Scan ID: " + Tb_id.Text.Trim(), Page);
                        script_func.script_focus(Tb_id.ClientID, Page);
                        Tb_id.Text = String.Empty;
                    }
                }
            }
            else
            {
                if (movementType == "311" || movementType == "312" || movementType == "301")
                {
                    if (Db_StFrom.SelectedIndex == 0)
                    {
                        script_func.script_alert("Please Select 'Storage From'", Page);
                        return;
                    }
                    //if(movementType == "311" || movementType == "312")
                    if (movementType == "312")
                    {
                        if (Db_StTo.SelectedIndex == 0)
                        {
                            script_func.script_alert("Please Select 'Storage To'", Page);
                            return;
                        }
                    }
                }
                else
                {
                    if (Db_From.SelectedIndex == 0)
                    {
                        script_func.script_alert("Please Select 'Storage From'", Page);
                        return;
                    }
                }
                

                if (movementType == "343" || movementType == "344" || movementType == "321" || movementType == "322" || movementType == "312" || movementType == "311")
                {
                    if (Db_Problem.SelectedIndex == 0)
                    {
                        script_func.script_alert("Please Select 'Problem'", Page);
                        return;
                    }
                }

                var connect = Convert.ToString(Session["Connection_String"]);
                using (var session = new SkRepositorySession(connect))
                {
                    var repo = new GTRepository(session);
                    var result = repo.SelectMaxID(movementType);
                    var start = 0;
                    if (result.Value == 0)
                    {
                        Session["ScanID"] = "1";
                        start = 1;
                    }
                    else
                    {
                        Session["ScanID"] = Convert.ToString(result.Value + 1);
                        start = result.Value + 1;
                    }

                    var result2 = repo.UpdateMaxID(movementType, start);

                    Session["MovementType"] = movementType;

                    if (movementType=="311" || movementType=="312")
                    {
                        Session["StorageFrom"] = Db_StFrom.SelectedItem.Value.ToString();
                        Session["StorageTo"] = Db_StTo.SelectedItem.Value.ToString();
                        Session["LicenseCar"] = txtLicenCar.Text.Trim();
                    }
                    else if (movementType == "301")
                    {
                        Session["StorageFrom"] = Db_StFrom.SelectedItem.Value.ToString();
                        Session["StorageTo"] = Db_StTo.SelectedItem.Value.ToString();
                        Session["PlantFrom"] = ddlFromPlant.SelectedItem.Text.ToString();
                        Session["PlantTo"] = ddlToPlant.SelectedItem.Text.ToString();
                        Session["LicenseCar"] = "";
                    }
                    else
                    {
                        Session["StorageFrom"] = Db_From.SelectedItem.Value.ToString();
                        Session["LicenseCar"] = "";
                    }


                    if (Db_Problem.SelectedIndex > 0)
                    {
                        Session["ItemText"] = Db_Problem.SelectedItem.Text.ToString();
                    }
                    else
                    {
                        Session["ItemText"] = String.Empty;
                    }
                    
                }
            }

            if (movementType == "343" || movementType == "344")
            {
                Response.Redirect("GT_Scan343_344.aspx");
            }
            else if (movementType == "311" || movementType == "312")
            {
                Response.Redirect("311/GT_Scan311.aspx");
            }
            else if(movementType == "321" || movementType == "322")
            {
                Response.Redirect("321/GT_Scan321.aspx");
            }
            else if(movementType=="301")
            {
                Response.Redirect("GTScanBarcode.aspx");
            }
        }

        protected void Bt_301_Click(object sender, EventArgs e)
        {
            try
            {
                lblID301.Text = String.Empty;
                SelectMaxID("301");
            }
            catch(Exception ex)
            {

            }
        }

        protected void Bt_321_Click(object sender, EventArgs e)
        {
            try
            {
                lblID321.Text = String.Empty;
                SelectMaxID("321");
            }
            catch (Exception ex)
            {

            }
        }
        protected void Bt_322_Click(object sender, EventArgs e)
        {
            try
            {
                lblID322.Text = String.Empty;
                SelectMaxID("322");
            }
            catch (Exception ex)
            {

            }
        }

        protected void Bt_311_Click(object sender, EventArgs e)
        {
            try
            {
                lblID311.Text = String.Empty;
                SelectMaxID("311");
            }
            catch (Exception ex)
            {

            }
        }

        protected void Bt_312_Click(object sender, EventArgs e)
        {
            try
            {
                lblID312.Text = String.Empty;
                SelectMaxID("312");
            }
            catch (Exception ex)
            {

            }
        }

        protected void btnViewID_343_Click(object sender, EventArgs e)
        {
            try
            {
                lblID343.Text = String.Empty;
                SelectMaxID("343");
            }
            catch (Exception ex)
            {

            }
        }

        protected void btnViewID_344_Click(object sender, EventArgs e)
        {
            try
            {
                lblID344.Text = String.Empty;
                SelectMaxID("344");
            }
            catch (Exception ex)
            {

            }
        }

        protected void Db_StFrom_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Rb_311.Checked || Rb_312.Checked || Rb_301.Checked)
            {
                if (Db_StFrom.SelectedIndex != 0)
                {
                    Db_StTo.Items.Clear();
                    var connect = Convert.ToString(Session["Connection_String"]);
                    var WHCode = Convert.ToString(Session["UserLogin_WHCode"]);
                    using (var session = new SkRepositorySession())
                    {
                        var repo = new GTRepository(session);
                        var result = repo.getStorageFrom(WHCode);
                        foreach (var item in result.Value)
                        {
                            Db_StTo.Items.Add(new ListItem(Convert.ToString(item), Convert.ToString(item)));
                        }
                        if (Rb_312.Checked || Rb_301.Checked)
                        {
                            Db_StTo.Items.Insert(0, new ListItem("Select", ""));
                        }
                    }
                }
                script_func.script_focus(Db_StTo.ClientID, Page);
            }
        }
    }
}