﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using System.IO;
using hh_siamkraft.Entities.Common;

namespace hh_siamkraft
{
    public class FileLog 
    {
        private string rptFilePath = String.Concat(HttpContext.Current.Request.ServerVariables["APPL_PHYSICAL_PATH"]);

        public static void WriteLogError(string str_fileName, string str_data)
        {
            CreateFolder();
            StreamWriter streamwriter = new StreamWriter(str_fileName, true, System.Text.Encoding.Unicode);
            streamwriter.WriteLine(str_data);
            streamwriter.Flush();
            streamwriter.Close();
        }

        public static void WriteLogError(string str_fileName, string str_data, string str_username)
        {
            CreateFolder();
            StreamWriter streamwriter = new StreamWriter(str_fileName, true, System.Text.Encoding.Unicode);
            streamwriter.WriteLine(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss", new CultureInfo("en-US")) + "\r\n" + " User : " + str_username + "\r\n B Q M : " + str_data);
            streamwriter.WriteLine("-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
            streamwriter.Flush();
            streamwriter.Close();
        }


        public static void CreateFolder()
        {
            CreateDirectory(ModuleCommon.dirPathLogFile);
            CreateDirectory(ModuleCommon.dirPathGR_101);
            CreateDirectory(ModuleCommon.dirPathGR_525);
            CreateDirectory(ModuleCommon.dirPathGR_101_Report);
        }

        public static void CreateDirectory(string path)
        {
            try
            {
                if (Directory.Exists(path))
                    return;
                else
                {
                    DirectoryInfo di;
                    di = Directory.CreateDirectory(path);
                    return;
                }

            }
            catch (Exception ex)
            {
                throw;
            }
        }
      

    }
}