﻿<%@ Register TagPrefix="uc1" TagName="header_control" Src="header_control.ascx"   %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ChangePlant.aspx.cs" Inherits="hh_siamkraft.ChangePlant" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>MAIN MENU</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C# .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"/>
		<LINK href="css/style.css" type="text/css" rel="stylesheet"/>
	    <style type="text/css">
            .style1
            {
                height: 25px;
            }
        </style>
</head>
<body leftMargin="1" topMargin="1" rightmargin="1" bottommargin="1" MS_POSITIONING="GridLayout">
		<form id="myForm" method="post" runat="server">
			<table class="table_main" cellSpacing="0" cellPadding="0" width="225" border="0">
				<tr>
					<td class="headtable"><uc1:header_control id="Header_control1" runat="server"></uc1:header_control></td>
				</tr>
				<tr>
					<td class="headtable" align="center"><font color="white">Main Menu</font></td>
				</tr>
				<tr>
					<td>
						<table cellSpacing="0" cellPadding="2" width="100%" border="0">
							<tr id="trServer" runat="server">
								<td class="smallerText" align="left">PM</td>
								<td align="left" ><IMG align="absMiddle" src="images/red_arrow.gif"></td>
                                <td>
									<asp:dropdownlist id="ddlServer" runat="server" CssClass="colorBox" AutoPostBack="True" OnSelectedIndexChanged="ddlServer_SelectedIndexChanged"></asp:dropdownlist></td>
							</tr>
							<tr id="trPlant" runat="server">
								<td class="smallerText" align="left">Plant</td>
								<td align="left"><IMG align="absMiddle" src="images/red_arrow.gif"></td>
                                <td>
									<asp:dropdownlist id="ddlWarehouse" runat="server" CssClass="colorBox"></asp:dropdownlist></td>
							</tr>
                            <tr>
                                
                            </tr>
						</table>
					</td>
				</tr>
                <tr>
                    <td class="headtable" align="center">
                        <asp:button id="btnOK" runat="server" Height="22px"
                            CssClass="buttonTag" Text=" OK " OnClick="btnOK_Click"></asp:button>&nbsp;
                        <asp:button id="Bt_MainMenu" runat="server" Height="22px" CssClass="buttonTag"
								Text=" MAIN MENU " style="Z-INDEX: 0" OnClick="Bt_MainMenu_Click1"></asp:button>
                    </td>
                </tr>
			</table>
		</form>
	</body>
</html>
