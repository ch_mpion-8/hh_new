﻿<%@ Register TagPrefix="uc1" TagName="header_control" Src="header_control.ascx"   %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="main_menu.aspx.cs" Inherits="hh_siamkraft.main_menu" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>MAIN MENU</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="C# .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5"/>
		<LINK href="css/style.css" type="text/css" rel="stylesheet"/>
	    <style type="text/css">
            .style1
            {
                height: 25px;
            }
        </style>
</head>
<body leftMargin="1" topMargin="1" rightmargin="1" bottommargin="1" MS_POSITIONING="GridLayout">
		<form id="myForm" method="post" runat="server">
			<table class="table_main" cellSpacing="0" cellPadding="0" width="225" border="0">
				<tr>
					<td class="headtable"><uc1:header_control id="Header_control1" runat="server"></uc1:header_control></td>
				</tr>
				<tr>
					<td class="headtable" align="center"><font color="white">Main Menu</font></td>
				</tr>
				<tr height="3">
					<td class="smallerText">&nbsp;<IMG hspace="1" src="images/red_arrow.gif" align="absMiddle">&nbsp;
						<asp:label id="Lb_msg" runat="server">Select Main Menu</asp:label></td>
				</tr>
				<tr>
					<td>
						<table cellSpacing="0" cellPadding="2" width="100%" border="0">
							<tr id="tr_gr" runat="server" Visible="false">
								<td align="center"><FONT face="Tahoma"><asp:button id="Bt_gr" runat="server" Height="22px"  CssClass="buttonTag" Width="180px" Text="GOODS RECEIPT" OnClick="Bt_gr_Click"></asp:button></FONT></td>
							</tr>
							<tr id="tr_gt" runat="server" Visible="false">
								<td align="center"><asp:button id="Bt_gt" runat="server" Height="22px" CssClass="buttonTag" Width="180px" Text="GOODS TRANSFER" OnClick="Bt_gt_Click"></asp:button></td>
							</tr>
							<tr id="tr_gi" runat="server" Visible="false">
								<td align="center">
                                    <asp:button id="Bt_gi" runat="server" Height="22px" CssClass="buttonTag" Width="180px" Text="GOODS ISSUE" OnClick="Bt_gi_Click"></asp:button></td>
							</tr>
							<tr id="tr_rewind" runat="server" Visible="false">
								<td align="center">
                                    <asp:button id="btnRewind" runat="server" Height="22px" 
                                        CssClass="buttonTag" Width="180px" Text="DP Rewind" OnClick="btnRewind_Click"></asp:button>

								</td>
							</tr>
                            <tr id="tr_Auth" runat="server" Visible="false">
								<td align="center">
                                    <asp:button id="btnAuth" runat="server" Height="22px" 
                                        CssClass="buttonTag" Width="180px" Text="Change Plant" OnClick="btnAuth_Click"></asp:button>

								</td>
							</tr>
							<tr>
								<td align="center">
                                    <asp:button id="Bt_logout" runat="server" Height="22px" 
                                    CssClass="buttonTag" Width="180px" Text="Logout" OnClick="Bt_logout_Click"></asp:button>

								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
