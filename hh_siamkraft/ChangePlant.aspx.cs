﻿using hh_siamkraft.Dals;
using hh_siamkraft.Dals.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace hh_siamkraft
{
    public partial class ChangePlant : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {

        }

        private void showMenu()
        {
            var username = Convert.ToString(Session["UserLogin_UserName"]);

            var repo = new CommonRepository(new SkRepositorySession());
            var data = repo.GetUserServer(username);
            Helper.Control.SetServerDropDownList(ddlServer, data.Value);

            GetWH();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user_id"] == null)
            {
                Session.RemoveAll();
                Response.Redirect("../index.aspx", false);
                return;
            }

            if (!Page.IsPostBack)
            {
                showMenu();
            }
        }

        protected void Bt_MainMenu_Click1(object sender, EventArgs e)
        {
            Response.Redirect("main_menu.aspx", false);
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            Change_Server();
            Change_WH();
            Response.Redirect("main_menu.aspx", false);
        }

        protected void ddlServer_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetWH();
        }

        private void GetWH()
        {
            var commonRepo = new CommonRepository(new SkRepositorySession());
            var username = Convert.ToString(Session["UserLogin_UserName"]);
            var wh = commonRepo.GetUserWH(username, ddlServer.SelectedValue).Value;
            Helper.Control.SetWarehouseDropDownList(ddlWarehouse, wh);
        }

        private void Change_WH()
        {
            var wh_no = Convert.ToInt32(ddlWarehouse.SelectedItem.Value);
            using (var session = new SkRepositorySession())
            {
                var repo = new CommonRepository(session);
                var data = repo.GetWarehouseDetail(wh_no);
                var warehouse = data.Value;
                if (warehouse != null)
                {
                    Session["HH_Plant"] = warehouse.WHCode;
                    Session["WH_No"] = wh_no;
                    Session["UserLogin_WHCode"] = warehouse.WHCode;
                    Session["UserLogin_WHDescription"] = warehouse.WH_Description;
                    Session["UserLogin_ShippingPoint"] = warehouse.ShippingPoint;
                    Session["UserLogin_WHID"] = warehouse.WH_No;
                }
            }
        }

        private void Change_Server()
        {
            var server_id = Convert.ToInt32(ddlServer.SelectedItem.Value);
            using (var session = new SkRepositorySession())
            {
                var repo = new CommonRepository(session);
                var data = repo.GetServer(server_id);
                var server = data.Value;
                if (server != null)
                {
                    Session["Server_ID"] = server.ServerID.ToString();
                    Session["DB_Description"] = server.Description;
                    Session["Connection_String"] = server.ConnectString;
                }
            }
        }
    }
}