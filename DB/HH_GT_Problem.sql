use [PLSSKIC]
go

ALTER TABLE dbo.HH_GT_Problem ADD
	is_active bit null, 
	create_by nvarchar(50) NULL,
	create_date datetime NULL,
	update_by nvarchar(50) NULL,
	update_date datetime NULL
GO

update dbo.HH_GT_Problem
set is_active = 1