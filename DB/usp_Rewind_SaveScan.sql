USE [PLSSKIC]
GO
/****** Object:  StoredProcedure [dbo].[usp_RewindSaveScan]    Script Date: 11/1/2018 10:14:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Kittimasak Praphanbandit
-- Create date: 08/12/2011
-- Description:	Save Save from Scan barcode
-- =============================================
CREATE PROCEDURE [dbo].[usp_RewindSaveScan2] 
	-- Add the parameters for the stored procedure here
	@WHCode varchar(4),
	@DocNo varchar(15),
	@Batch varchar(15),
	@Qty decimal(18,3) = 0,
	@Material varchar(20) = '',
	@IsSAP bit,
	@CreateBy varchar(20),
	@MsgOut nvarchar(255) output,
	@internalID	decimal(20,0) output -- Add By Parinya K. 2018-09-04
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    DECLARE @remainQty int,
			@header_internalID decimal(20,0),
			-- @internalID decimal(20,0), -- Comment By Parinya K. 2018-09-04
			@status varchar(2)
    
    SELECT @header_internalID=InternalID,@status=Status
    FROM tbt_RewindHeader
    WHERE (WHCode=@WHCode) AND (DocNo=@DocNo)
    
	IF(@IsSAP = 1)
	 BEGIN
		-- Interface SAP
		SELECT @Material = Material_number,@Qty = Weight FROM vHH_Roll_amendment WHERE Batch_number = @Batch
		IF @@ROWCOUNT = 0
		BEGIN
			SELECT @Material = Material_number,@Qty = Weight FROM vHH_Roll_transaction WHERE Batch_number = @Batch
			IF @@ROWCOUNT = 0
			BEGIN
				SELECT @Material = Material_number,@Qty = Weight FROM vHH_Roll_transaction_history WHERE Batch_number = @Batch
				IF @@ROWCOUNT = 0
				BEGIN
					SET @MsgOut = N'Batch not found \r\n please contact warehouse officer'
					RETURN 0
				END
			END
		END
		
		SET @Qty = @Qty/1000
		
		/*
		IF(NOT EXISTS(SELECT Material FROM tbt_RewindDetail WHERE (DocNo=@DocNo) AND (Material=@Material)))
		 BEGIN
			SET @MsgOut = N'Material : ' + @Material + N' does not exists in this DP'
			RETURN 0
		 END
		 
		SELECT @RemainQty = SUM(OrderQty - RewindQty)
		FROM tbt_RewindDetail
		WHERE (DocNo=@DocNo) AND (Material=@Material)
		
		IF(@RemainQty = 0)
		 BEGIN
			SET @MsgOut = N'Material : ' + @Material + N' scan completed'
			RETURN 0
		 END
		 
		SELECT TOP 1 @internalID=InternalID
		FROM tbt_RewindDetail
		WHERE (DocNo=@DocNo) AND (Material=@Material) AND (OrderQty - RewindQty > 0)
		ORDER BY InternalID ASC
		
		IF(@internalID IS NULL OR @internalID = 0)
		 BEGIN
			SET @MsgOut = N'Get Data InternalID error'
			RETURN 0
		 END
		 
		INSERT tbt_RewindScan
		(Header_InternalID,DocNo,Material,Batch,Qty,IsReverse,CreateBy,CreateDate)
		VALUES
		(@header_internalID,@DocNo,@Material,@Batch,@Qty/1000,0,@CreateBy,GETDATE())
			
		UPDATE tbt_RewindDetail SET RewindQty=RewindQty + 1,UpdateBy=@CreateBy
		,UpdateDate=GETDATE()
		WHERE (InternalID=@internalID) AND (DocNo=@DocNo) AND (Material=@Material)
		
		SET @MsgOut = N'Save data completed'
		*/
	 END
	ELSE
	 BEGIN
		IF(NOT EXISTS(SELECT Material_number FROM Material_Master WHERE (Material_number=@Material)
						AND (Material_type = 'R')))
		 BEGIN
			SET @MsgOut = N'Material : ' + @Material + N'does not exists in PLS'
			RETURN 0
		 END
	 END

	IF(NOT EXISTS(SELECT Material FROM tbt_RewindDetail WHERE (DocNo=@DocNo) AND (Material=@Material)))
	 BEGIN
		SET @MsgOut = N'Material : ' + @Material + N' does not exists in this DP'
		RETURN 0
	 END
	 
	SELECT @RemainQty = SUM(OrderQty - RewindQty)
	FROM tbt_RewindDetail
	WHERE (DocNo=@DocNo) AND (Material=@Material)
	
	IF(@RemainQty = 0)
	 BEGIN
		SET @MsgOut = N'Material : ' + @Material + N' scan completed'
		RETURN 0
	 END
	 
	SELECT TOP 1 @internalID=InternalID
	FROM tbt_RewindDetail
	WHERE (DocNo=@DocNo) AND (Material=@Material) AND (OrderQty - RewindQty > 0)
	ORDER BY InternalID ASC
	
	IF(@internalID IS NULL OR @internalID = 0)
	 BEGIN
		SET @MsgOut = N'Get Data InternalID error'
		RETURN 0
	 END
	 
	INSERT tbt_RewindScan
	(Header_InternalID,DocNo,Material,Batch,Qty,IsReverse,CreateBy,CreateDate)
	VALUES
	(@header_internalID,@DocNo,@Material,@Batch,@Qty,0,@CreateBy,GETDATE())
		
	UPDATE tbt_RewindDetail SET RewindQty=RewindQty + 1,UpdateBy=@CreateBy
	,UpdateDate=GETDATE()
	WHERE (InternalID=@internalID) AND (DocNo=@DocNo) AND (Material=@Material)
	
	SET @MsgOut = N'Save data completed'
			
	RETURN 1
END







