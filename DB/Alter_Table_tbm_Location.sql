alter table [dbo].[tbm_Location]
  add SC_Stat varchar(2)
  go

  update [dbo].[tbm_Location]
  set SC_Stat = case when IsDamage = 0 then 'UR' else 'QI' end
  go
