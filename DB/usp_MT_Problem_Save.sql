USE [PLSSKIC]
GO
/****** Object:  StoredProcedure [dbo].[usp_MT_Problem_Save]    Script Date: 11/7/2018 2:38:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_MT_Problem_Save]
	@problem_id			as int = null
	,@problem_desc		as nvarchar(500) = null
	,@is_active			as bit = null
	,@updateBy			as nvarchar(50) = null
	,@error_message as nvarchar(200) = null output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	if(@problem_id is null)
	begin 
		insert into [dbo].[HH_GT_Problem]([Problem_Desc],[is_active]
					,[create_by],[create_date],[update_by],[update_date])
		values(@problem_desc, @is_active
				,@updateBy,getdate(),@updateBy,getdate());
	end
	else
	begin
		update [dbo].[HH_GT_Problem]
		set [Problem_Desc] = @problem_desc
			, [is_active] = @is_active
			, [update_by] = @updateBy
			, [update_date] = getdate()
		where [Problem_ID] = @problem_id;
	end
END






