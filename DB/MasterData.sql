USE [PLSSKIC]
GO
/****** Object:  StoredProcedure [dbo].[usp_MT_ShipTo_Save]    Script Date: 11/7/2018 1:28:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_MT_ShipTo_Save]
	-- Add the parameters for the stored procedure here
	@InternalID		decimal(18,0) = null,
	@CustomerCode	nvarchar(20) = null,
	@ShipToName		nvarchar(50) = null,
	@Remark			nvarchar(255) = null,
	@IsActive		bit = null,
	@CreateBy		varchar(50) = null,
	@error_message as nvarchar(200) = null output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	if @InternalID is null
		begin
			if exists(select * from tbm_ShipTo where CustomerCode = @CustomerCode and ShipToName = @ShipToName)
			begin
				set @error_message = 'มีข้อมูล Ship To นี้อยู่แล้วในระบบ';
			end 
			else
			begin
				INSERT INTO tbm_ShipTo (CustomerCode, ShipToName, Remark, IsActive, CreateBy, CreateDate)
				VALUES (@CustomerCode, @ShipToName, @Remark, @IsActive, @CreateBy, GetDate())
			end
		end 
	else
		begin
			UPDATE tbm_ShipTo
			SET CustomerCode = @CustomerCode, 
				ShipToName = @ShipToName, 
				Remark = @Remark, 
				IsActive = @IsActive, 
				CreateBy = @CreateBy, 
				CreateDate = GetDate()
			WHERE InternalID = @InternalID
		end

END

/****** Object:  StoredProcedure [dbo].[usp_MT_LicenseCar_Save]    Script Date: 11/7/2018 1:28:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_MT_LicenseCar_Save]
	-- Add the parameters for the stored procedure here
	@LicenseCarCode nvarchar(20) = null,
	@LicenseCarCodeOld nvarchar(20) = null,
	@DriverName nvarchar(50) = null,
	@CarrierName nvarchar(50) = null,
	@TruckType nvarchar(50) = null,
	@Remark nvarchar(255) = null,
	@IsActive bit = null,
	@CreateBy varchar(50) = null,
	@error_message as nvarchar(200) = null output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	if @LicenseCarCodeOld is null
		begin
			if exists(select * from tbm_LicenseCar where LicenseCarCode = @LicenseCarCode)
			begin
				set @error_message = 'มีข้อมูลทะเบียนรถคันนี้อยู่แล้วในระบบ';
			end 
			else
			begin
				INSERT INTO tbm_LicenseCar (LicenseCarCode, DriverName, CarrierName, TruckType, Remark, IsActive, CreateBy, CreateDate)
			VALUES (@LicenseCarCode, @DriverName, @CarrierName, @TruckType, @Remark, @IsActive, @CreateBy, GetDate())
			end
		end 
	else
		begin
			UPDATE tbm_LicenseCar
			SET DriverName = @DriverName, 
				CarrierName = @CarrierName, 
				TruckType = @TruckType, 
				Remark = @Remark, 
				IsActive = @IsActive, 
				CreateBy = @CreateBy, 
				CreateDate = GetDate()
			WHERE LicenseCarCode = @LicenseCarCode
		end
END

/****** Object:  StoredProcedure [dbo].[usp_MT_Customer_Save]    Script Date: 11/7/2018 1:28:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_MT_Customer_Save]
	-- Add the parameters for the stored procedure here
	@CustomerCode		nvarchar(20) = null,
	@CustomerCodeOld	nvarchar(20) = null,
	@CustomerName		nvarchar(50) = null,
	@Initials			nvarchar(50) = null,
	@Remark				nvarchar(255) = null,
	@IsActive			bit = null,
	@CreateBy			varchar(50) = null,
	@error_message		varchar(200) = null output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	if @CustomerCodeOld is null
		begin
			if exists(select * from tbm_Customer where CustomerCode = @CustomerCode)
			begin
				set @error_message = 'มีข้อมูลลูกค้านี้อยู่แล้วในระบบ';
			end 
			else
			begin
				INSERT INTO tbm_Customer (CustomerCode, CustomerName, Initials, Remark, IsActive, CreateBy, CreateDate)
				VALUES (@CustomerCode, @CustomerName, @Initials, @Remark, @IsActive, @CreateBy, GetDate())
			end
		end 
	else
		begin
			UPDATE tbm_Customer
			SET CustomerName = @CustomerName, 
				Initials = @Initials, 
				Remark = @Remark, 
				IsActive = @IsActive, 
				CreateBy = @CreateBy, 
				CreateDate = GetDate()
			WHERE CustomerCode = @CustomerCode
		end

END



