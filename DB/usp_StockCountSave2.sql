
CREATE PROCEDURE [dbo].[usp_StockCountSave2] 
	-- Add the parameters for the stored procedure here
	@WHCode varchar(4),
	@LocationCodeID decimal(20,0),
	@LocationCodeFrom varchar(20),
	@LocationCodeTo varchar(20),
	@Section varchar(20),
	@Row varchar(20),
	@Grade varchar(10),
	@Size varchar(5),
	@CustomerNo varchar(10),
	@PaperType varchar(10),
	@Qty int,
	@IsDamage bit,
	@SC_Stat varchar(2) = '',
	@CreateBy varchar(20),
	@MsgOut nvarchar(255) output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @rowCount int,
			@pSTGEFrom varchar(20),
			@pSTGETo varchar(20)
			
			
	SET @pSTGEFrom = 'P' + @LocationCodeFrom
	SET @pSTGETo = 'P' + @LocationCodeTo
	
	if @SC_Stat = '' 
	begin
		IF(@IsDamage = 0) set @SC_Stat = 'UR'
		else set @SC_Stat = 'QI'
	end

	IF(@SC_Stat = 'UR')
	 BEGIN
		IF(NOT EXISTS(SELECT * FROM dbo.vw_StockCount_SAP
			WHERE (Plant=@WHCode) AND (Grade=@Grade) AND (Size=@Size)
			AND (Storage BETWEEN @LocationCodeFrom AND @LocationCodeTo)
			AND (URQty > 0)
		))
		BEGIN
			SET @MsgOut = N'ไม่พบข้อมูล Grade : ' + @Grade
						+ N'\r\nSize : ' + @Size
						+ N'\r\nในข้อมูล Stock ของ SAP'
			RETURN 0	 		
		END
	 END
	ELSE IF (@SC_Stat = 'QI')
	 BEGIN
		IF(NOT EXISTS(SELECT * FROM dbo.vw_StockCount_SAP
			WHERE (Plant=@WHCode) AND (Grade=@Grade) AND (Size=@Size)
			AND (Storage BETWEEN @LocationCodeFrom AND @LocationCodeTo)
			AND (QIQty > 0)
		))
		BEGIN
			SET @MsgOut = N'ไม่พบข้อมูล Grade : ' + @Grade
						+ N'\r\nSize : ' + @Size
						+ N'\r\nในข้อมูล Stock ของ SAP'
			RETURN 0	 		
		END	 
	 END
	ELSE IF (@SC_Stat = 'BL')
	 BEGIN
		IF(NOT EXISTS(SELECT * FROM dbo.vw_StockCount_SAP
			WHERE (Plant=@WHCode) AND (Grade=@Grade) AND (Size=@Size)
			AND (Storage BETWEEN @LocationCodeFrom AND @LocationCodeTo)
			AND (BLQty > 0)
		))
		BEGIN
			SET @MsgOut = N'ไม่พบข้อมูล Grade : ' + @Grade
						+ N'\r\nSize : ' + @Size
						+ N'\r\nในข้อมูล Stock ของ SAP'
			RETURN 0	 		
		END	 
	 END
	
	INSERT INTO tbt_StockCountDetail
	(WHCode,Location_InternalID,LocationCodeFrom,LocationCodeTo,Section,[Row],Grade,Size,Qty,IsDamage,CreateBy,CreateDate)
	VALUES
	(@WHCode,@LocationCodeID,@LocationCodeFrom,@LocationCodeTo,@Section,@Row,@Grade,@Size,@Qty,@IsDamage,@CreateBy,GETDATE())
	
	
	RETURN 1
END







