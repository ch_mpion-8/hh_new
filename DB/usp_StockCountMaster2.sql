
create PROCEDURE [dbo].[usp_StockCountMaster2] 
	-- Add the parameters for the stored procedure here
	@IsAppend bit,
	@CreateBy varchar(20),
	@IPAddress varchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @interfaceID decimal(20,0)
	
	INSERT INTO tbt_StockCountControl
	(IPAddress,CreateBy,CreateDate)
	VALUES
	(@IPAddress,@CreateBy,GETDATE())
	
	SET @interfaceID = SCOPE_IDENTITY()
	
	IF(@IsAppend = 0)
	 BEGIN
		-- Insert All data
		DELETE tbt_StockCountMaster
		
		INSERT INTO tbt_StockCountMaster
		(InterfaceID,Plant,Storage,Material,URQty,QIQty,BLQty,URWeight,QIWeight, BLWeight,CreateBy,CreateDate)
		SELECT @interfaceID,Plant,Storage,Material,URQty,QIQty,BLQty,URWeight,QIWeight,BLWeight,@CreateBy,GETDATE()
		FROM tbt_StockCountImport
	 END
	ELSE
	 BEGIN
		DECLARE @cursor CURSOR
		SET @cursor = CURSOR FAST_FORWARD FOR
		SELECT Plant,Storage,Material,URQty,QIQty,BLQty,URWeight,QIWeight,BLWeight FROM tbt_StockCountImport
		
		OPEN @cursor
		
		DECLARE @plant varchar(5),
				@storage varchar(10),
				@material varchar(20),
				@urQty int,
				@qiQty int,
				@blQty int,
				@urWeight decimal(18,3),
				@qiWeight decimal(18,3),
				@blWeight decimal(18,3)
		
		FETCH FROM @cursor INTO @plant,@storage,@material,@urQty,@qiQty,@blQty,@urWeight,@qiWeight,@blWeight
		WHILE(@@FETCH_STATUS = 0)
		 BEGIN
			IF(EXISTS(SELECT * FROM tbt_StockCountMaster WHERE (Plant=@plant) AND (Storage=@storage) AND (Material=@material)  ))
			 BEGIN
				-- this batch exists in system
				UPDATE tbt_StockCountMaster
				SET InterfaceID=@interfaceID,URQty=@urQty,QIQty=@qiQty,BLQty=@blQty,URWeight=@urWeight,QIWeight=@qiWeight,BLWeight=@blWeight,UpdateBy=@CreateBy,UpdateDate=GETDATE()
				WHERE (Plant=@plant) AND (Storage=@storage) AND (Material=@material)
			 END
			ELSE
			 BEGIN
				-- this batch does not exists
				INSERT INTO tbt_StockCountMaster
				(InterfaceID,Plant,Storage,Material,URQty,QIQty,BLQty,URWeight,QIWeight,BLWeight,CreateBy,CreateDate)
				VALUES
				(@interfaceID,@plant,@storage,@material,@urQty,@qiQty,@blQty,@urWeight,@qiWeight,@blWeight,@CreateBy,GETDATE())
			 END
		 
			FETCH NEXT FROM @cursor INTO @plant,@storage,@material,@urQty,@qiQty,@blQty,@urWeight,@qiWeight,@blWeight
		 END
		CLOSE @cursor
		DEALLOCATE @cursor
				
	 END
	
	RETURN 1
	
END
