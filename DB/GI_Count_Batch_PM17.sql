USE [PLSPM17]
GO
/****** Object:  StoredProcedure [dbo].[GI_Count_Batch]    Script Date: 11/7/2018 3:23:18 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO





CREATE PROCEDURE [dbo].[GI_Count_Batch2] 												
 @Delivery_No VARCHAR(10) ,												
 @Record_ID INT,
 @pRoll_Total FLOAT OUTPUT,
 @pRoll_Scan INT OUTPUT 												
AS												
	DECLARE @Roll_Total FLOAT											
	DECLARE @Roll_Scan INT											
												
	SELECT @Roll_Total = CASE WHEN SUM(ACTUAL_QTY) IS NULL THEN 0 ELSE ROUND(SUM(ACTUAL_QTY),0) END FROM Delivery_Item WHERE Record_ID=@Record_ID AND Delivery_No=@Delivery_No											
												
	SELECT @Roll_Scan = COUNT(Batch_No) FROM HH_GI_Item WHERE Delivery_No=@Delivery_No											
												
	--SELECT 'Roll_Total' = @Roll_Total
	--SELECT 'Roll_Scan' = @Roll_Scan

	SET @pRoll_Total =  @Roll_Total
	SET  @pRoll_Scan = @Roll_Scan									
												





