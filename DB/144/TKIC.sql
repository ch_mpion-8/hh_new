USE [PLSTKIC]
GO
/****** Object:  StoredProcedure [dbo].[usp_GTSave]    Script Date: 12/24/2018 2:09:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO



/****** Object:  Stored Procedure dbo.usp_GTSave    Script Date: 9/4/2014 9:50:52 AM ******/
-- =============================================
-- Author:		Kittimasak Praphanbandit
-- Create date: 22/07/2013
-- Description:	Save Data Scan Batch for Goods Transfer
-- =============================================
CREATE PROCEDURE [dbo].[usp_GTSave2] 
	-- Add the parameters for the stored procedure here
	@ScanID int,
	@MovementType varchar(3),
	@StorageFrom varchar(4),
	@StorageTo varchar(4) = '',
	@ItemText nvarchar(50) = '',
	@UOM varchar(3),
	@Batch char(10),
	@MvBatch char(10),
	@WH_No int,
	@WH_Code varchar(4),
	@ClearTxt nvarchar(1),
	@UserName varchar(20),
	@MsgOut nvarchar(255) = '' output

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	/*
		RETURN_VALUE
		0 = Error
		1 = Save data complete
		2 = Batch not found
		900 = Scan batch duplicate
	*/

    -- Insert statements for procedure here
	DECLARE @material varchar(18),
			@qty decimal(8,3),
			@rowCount int
    
    SELECT * FROM HH_GT_Item
    WHERE (GT_ID = @ScanID) AND (MovementType = @MovementType) AND (Batch = @Batch)
    AND (DATEDIFF(day,CreateDateTime,GETDATE()) = 0) AND ([Status] IS NULL OR [Status] <> 'C')
    AND (WH_No = @WH_No)
    IF(@@ROWCOUNT > 0)
     BEGIN
		SET @MsgOut = N'Scan Batch Duplicate'
		RETURN 900
     END
    
    IF(@UOM = 'ROL')
     BEGIN
		-- Roll Amendment
		SELECT @material = Material_number, @qty = [Weight]/1000
		FROM vHH_Roll_amendment
		WHERE (Batch_number = @Batch)
		SET @rowCount = @@ROWCOUNT
		IF(@rowCount = 0)
		 BEGIN
			-- Roll Transaction
			SELECT @material = Material_number, @qty = [Weight]/1000
			FROM vHH_Roll_transaction
			WHERE (Batch_number = @Batch)
			SET @rowCount = @@ROWCOUNT
			IF(@rowCount = 0)
			 BEGIN
				-- Roll Transaction History
				SELECT @material = Material_number, @qty = [Weight]/1000
				FROM vHH_Roll_transaction_History
				WHERE (Batch_number = @Batch)
				SET @rowCount = @@ROWCOUNT
				IF(@rowCount = 0)
				 BEGIN
					SET @MsgOut = N'Batch: ' + @Batch + N' not found in PLS'
					RETURN 2 -- Batch not found
				 END
			 END	
		 END
     END
    ELSE IF(@UOM = 'SH')
     BEGIN
		-- Sheet Amendment
		SELECT @material = Material_Number, @qty = Ream_Per_Pallet
		FROM Sheet_amendment
		WHERE (Batch_Number = @Batch)
		SET @rowCount = @@ROWCOUNT
		IF(@rowCount = 0)
		 BEGIN
			-- Sheet Transaction
			SELECT @material = Material_Number, @qty = Ream_Per_Pallet
			FROM Sheet_transaction
			WHERE (Batch_Number = @Batch)
			SET @rowCount = @@ROWCOUNT
			IF(@rowCount = 0)
			 BEGIN
				SET @MsgOut = N'Batch: ' + @Batch + N' not found in PLS'
				RETURN 2
			 END		
		 END
     END
     
    IF(@material IS NOT NULL AND @qty IS NOT NULL)
	 BEGIN
		IF(@MovementType IN ('321','322','343','344'))
		 BEGIN
			INSERT INTO HH_GT_Item
			(GT_ID,MovementType,Material,Batch,Qty,Problem_Desc,CreateUser,CreateDateTime,SFrom,UOM,WH_No,WH_Code)
			VALUES
			(@ScanID,@MovementType,@material,@Batch,@qty,@ItemText,@UserName,GETDATE(),@StorageFrom,@UOM,@WH_No,@WH_Code)		 
		 END
		ELSE IF(@MovementType IN ('311','312'))
		 BEGIN
			INSERT INTO HH_GT_Item
			(GT_ID,MovementType,Material,Batch,Qty,Problem_Desc,Storage_From,Storage_To,CreateUser,CreateDateTime,UOM,WH_No,WH_Code)
			VALUES
			(@ScanID,@MovementType,@material,@Batch,@qty,@ItemText,@StorageFrom,@StorageTo,@UserName,GETDATE(),@UOM,@WH_No,@WH_Code)		 
		 END
     END
    ELSE
     BEGIN
		RETURN 0
     END
	
    
    
	RETURN 1
END


USE [PLSTKIC]
GO
/****** Object:  StoredProcedure [dbo].[usp_GTSendToSAP]    Script Date: 12/25/2018 9:53:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/****** Object:  Stored Procedure dbo.usp_GTSendToSAP    Script Date: 9/4/2014 9:50:52 AM ******/

-- =============================================
-- Author:		Kittimasak Praphanbandit
-- Create date: 25/07/2013
-- Description:	Confirm Data to SAP
-- =============================================
CREATE PROCEDURE [dbo].[usp_GTSendToSAP2] 
	-- Add the parameters for the stored procedure here
	@HH_ID varchar(2),
	@ScanID int,
	@MovementType varchar(3),
	@StorageFrom varchar(4),
	@WH_No int,
	@WH_Code varchar(4),
	@UserName varchar(20),
	@LicenseCar varchar(20),
	@Record_ID int output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @rowNo int,
			@cursorGT CURSOR
			
	-- Insert data to Table Interface
	INSERT INTO [Control]
	(Prev_Process,HH_ID,Process_ID,[User_Name],Create_Date_Time,Flag_Del)
	VALUES
	(0,@HH_ID,'TP',@UserName,getdate(),'N')
	
	-- Get Value Record_ID
	SET @Record_ID = SCOPE_IDENTITY()			
			
	SET @cursorGT = CURSOR FAST_FORWARD FOR
	SELECT Internal_ID,Batch FROM HH_GT_Item
	WHERE (GT_ID = @ScanID) AND (MovementType = @MovementType) 
	AND (DATEDIFF(day, CreateDateTime, GETDATE()) = 0) AND (WH_No = @WH_No) AND (CreateUser = @UserName)
	AND ([Status] NOT IN ('C','W') OR [Status] IS NULL)
	
	DECLARE @batch char(10),
			@internal_ID decimal(18,0)
	
	SET @rowNo = 1
	
	OPEN @cursorGT
	FETCH FROM @cursorGT INTO @internal_ID,@batch
	WHILE(@@FETCH_STATUS = 0)
	 BEGIN
		print '@rowNo: ' + convert(varchar,@rowNo)
		print '@internal_ID: ' + convert(varchar,@internal_ID)
	 
		UPDATE HH_GT_Item SET Item_No = @rowNo,Interface_ID = @Record_ID,[Status] = 'W'
		WHERE (Internal_ID = @internal_ID)
		
		SET @rowNo = @rowNo + 1

		FETCH FROM @cursorGT INTO @internal_ID,@batch
	 END
	CLOSE @cursorGT
	DEALLOCATE @cursorGT
	

	INSERT INTO SAP_TP_Head
	(Record_ID,DocDate,PostDate,MovementType,Plant,Storage,Licensecar)
	VALUES
	(@Record_ID,CONVERT(char(10),GETDATE(),104),CONVERT(char(10),GETDATE(),104),@MovementType,@WH_Code,@StorageFrom,@LicenseCar)
	
	IF(@MovementType IN ('312'))
	 BEGIN
	 
		INSERT INTO SAP_TP_Item 
		(Record_ID ,Item_No,Material_No,Batch,Qty,Storage_Loc,Recv_SLoc,[Text]) 
		SELECT @Record_ID,Item_No,Material,Batch,Qty,Storage_To,Storage_From,Problem_Desc 
		FROM HH_GT_Item 
		WHERE (GT_ID = @ScanID) AND (MovementType = @MovementType)
		AND (DATEDIFF(day,CreateDateTime,GETDATE()) = 0) AND (WH_No = @WH_No) AND (CreateUser = @UserName)
	 END
	ELSE
	 BEGIN
		
		INSERT INTO SAP_TP_Item 
		(Record_ID ,Item_No,Material_No,Batch,Qty,Storage_Loc,Recv_SLoc,[Text]) 
		SELECT @Record_ID,Item_No,Material,Batch,Qty,Storage_From,Storage_To,Problem_Desc 
		FROM HH_GT_Item 
		WHERE (GT_ID = @ScanID) AND (MovementType = @MovementType)
		AND (DATEDIFF(day,CreateDateTime,GETDATE()) = 0) AND (WH_No = @WH_No) AND (CreateUser = @UserName)
		
	 END
	
	UPDATE [Control] SET Return_Code='N' 
	WHERE (Record_ID = @Record_ID)
	

	
	
	RETURN 1
END