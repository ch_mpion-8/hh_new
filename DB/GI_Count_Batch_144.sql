USE [PLSSKIC]
GO
/****** Object:  StoredProcedure [dbo].[GI_Count_Batch]    Script Date: 10/22/2018 10:36:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GI_Count_Batch2] 
 @Delivery_No VARCHAR(10) ,
 @Record_ID INT,
 @pRoll_Total FLOAT OUTPUT,
 @pRoll_Scan INT OUTPUT
AS
	DECLARE @Roll_Total FLOAT
	DECLARE @Roll_Scan INT,
			@rm_Qty decimal(8,3)

	SELECT @Roll_Total = CASE WHEN SUM(ACTUAL_QTY) IS NULL THEN 0 ELSE ROUND(SUM(ACTUAL_QTY),0) END FROM Delivery_Item WHERE Record_ID=@Record_ID AND Delivery_No=@Delivery_No

	SELECT @Roll_Scan = COUNT(Batch_No) FROM HH_GI_Item 
	WHERE (Record_ID=@Record_ID) AND (Delivery_No=@Delivery_No) AND (UOM='ROL')
	
	SELECT @rm_Qty = ISNULL(SUM(Qty),0)
	FROM HH_GI_Item
	WHERE (Record_ID = @Record_ID) AND (Delivery_No = @Delivery_No) AND (UOM <> 'ROL')

	--SELECT 'Roll_Total' = @Roll_Total
	--SELECT 'Roll_Scan' = (@Roll_Scan + @rm_Qty)

	SET @pRoll_Total =  @Roll_Total
	SET  @pRoll_Scan  =  (@Roll_Scan + @rm_Qty)

GO