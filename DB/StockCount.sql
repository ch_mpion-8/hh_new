
  alter table [dbo].[tbt_StockCountImport]
  add [BLQty] int,
	  [BLWeight] decimal(18,3);

  alter table [dbo].[tbt_StockCountMaster]
  add [BLQty] int,
	  [BLWeight] decimal(18,3);
