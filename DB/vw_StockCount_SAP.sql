
ALTER VIEW [dbo].[vw_StockCount_SAP]
AS
SELECT        TOP 100 PERCENT Plant, Storage, SUBSTRING(Material, 4, 7) AS Grade, SUBSTRING(Material, 11, 4) AS Size, SUM(URQty) AS URQty, SUM(QIQty) AS QIQty, 
                         SUM(URWeight) AS URWeight, SUM(QIWeight) AS QIWeight, SUM(BLQty) AS BLQty, SUM(BLWeight) AS BLWeight
FROM            dbo.tbt_StockCountMaster
GROUP BY Plant, Storage, SUBSTRING(Material, 4, 7), SUBSTRING(Material, 11, 4)
ORDER BY Plant, Storage, Grade, Size

GO