

alter table [dbo].[tbt_StockCountImport]
add [BLQty] int,
	  [BLWeight] decimal(18,3);
go
alter table [dbo].[tbt_StockCountMaster]
  add [BLQty] int,
	  [BLWeight] decimal(18,3);
go
	  

ALTER TABLE dbo.HH_GT_Problem ADD
	is_active bit null, 
	create_by nvarchar(50) NULL,
	create_date datetime NULL,
	update_by nvarchar(50) NULL,
	update_date datetime NULL
GO

alter table [dbo].[tbm_Location]
  add SC_Stat varchar(2)
  go

  update [dbo].[tbm_Location]
  set SC_Stat = case when IsDamage = 0 then 'UR' else 'QI' end
  go

update dbo.HH_GT_Problem
set is_active = 1;
  go
alter table [dbo].[HH_GR_Prod]
add [Interface_ID] int;
  go
alter table [dbo].[HH_GR_Prod]
add [Storage] nvarchar(4);
  go
alter table [dbo].[HH_GR_Prod]
add [Return_Msg] nvarchar(250);
  go
ALTER VIEW [dbo].[vw_StockCount_SAP]
AS
SELECT        TOP 100 PERCENT Plant, Storage, SUBSTRING(Material, 4, 7) AS Grade, SUBSTRING(Material, 11, 4) AS Size, SUM(URQty) AS URQty, SUM(QIQty) AS QIQty, 
                         SUM(URWeight) AS URWeight, SUM(QIWeight) AS QIWeight, SUM(BLQty) AS BLQty, SUM(BLWeight) AS BLWeight
FROM            dbo.tbt_StockCountMaster
GROUP BY Plant, Storage, SUBSTRING(Material, 4, 7), SUBSTRING(Material, 11, 4)
ORDER BY Plant, Storage, Grade, Size

GO

CREATE PROCEDURE [dbo].[GI_Count_Batch2] 
 @Delivery_No VARCHAR(10) ,
 @Record_ID INT,
 @pRoll_Total FLOAT OUTPUT,
 @pRoll_Scan INT OUTPUT
AS
	DECLARE @Roll_Total FLOAT
	DECLARE @Roll_Scan INT,
			@rm_Qty decimal(8,3)

	SELECT @Roll_Total = CASE WHEN SUM(ACTUAL_QTY) IS NULL THEN 0 ELSE ROUND(SUM(ACTUAL_QTY),0) END FROM Delivery_Item WHERE Record_ID=@Record_ID AND Delivery_No=@Delivery_No

	SELECT @Roll_Scan = COUNT(Batch_No) FROM HH_GI_Item 
	WHERE (Record_ID=@Record_ID) AND (Delivery_No=@Delivery_No) AND (UOM='ROL')
	
	SELECT @rm_Qty = ISNULL(SUM(Qty),0)
	FROM HH_GI_Item
	WHERE (Record_ID = @Record_ID) AND (Delivery_No = @Delivery_No) AND (UOM <> 'ROL')

	--SELECT 'Roll_Total' = @Roll_Total
	--SELECT 'Roll_Scan' = (@Roll_Scan + @rm_Qty)

	SET @pRoll_Total =  @Roll_Total
	SET  @pRoll_Scan  =  (@Roll_Scan + @rm_Qty)

GO

CREATE PROCEDURE [dbo].[usp_RewindSaveScan2] 
	-- Add the parameters for the stored procedure here
	@WHCode varchar(4),
	@DocNo varchar(15),
	@Batch varchar(15),
	@Qty decimal(18,3) = 0,
	@Material varchar(20) = '',
	@IsSAP bit,
	@CreateBy varchar(20),
	@MsgOut nvarchar(255) output,
	@internalID	decimal(20,0) output -- Add By Parinya K. 2018-09-04
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    DECLARE @remainQty int,
			@header_internalID decimal(20,0),
			-- @internalID decimal(20,0), -- Comment By Parinya K. 2018-09-04
			@status varchar(2)
    
    SELECT @header_internalID=InternalID,@status=Status
    FROM tbt_RewindHeader
    WHERE (WHCode=@WHCode) AND (DocNo=@DocNo)
    
	IF(@IsSAP = 1)
	 BEGIN
		-- Interface SAP
		SELECT @Material = Material_number,@Qty = Weight FROM vHH_Roll_amendment WHERE Batch_number = @Batch
		IF @@ROWCOUNT = 0
		BEGIN
			SELECT @Material = Material_number,@Qty = Weight FROM vHH_Roll_transaction WHERE Batch_number = @Batch
			IF @@ROWCOUNT = 0
			BEGIN
				SELECT @Material = Material_number,@Qty = Weight FROM vHH_Roll_transaction_history WHERE Batch_number = @Batch
				IF @@ROWCOUNT = 0
				BEGIN
					SET @MsgOut = N'Batch not found \r\n please contact warehouse officer'
					RETURN 0
				END
			END
		END
		
		SET @Qty = @Qty/1000
		
		/*
		IF(NOT EXISTS(SELECT Material FROM tbt_RewindDetail WHERE (DocNo=@DocNo) AND (Material=@Material)))
		 BEGIN
			SET @MsgOut = N'Material : ' + @Material + N' does not exists in this DP'
			RETURN 0
		 END
		 
		SELECT @RemainQty = SUM(OrderQty - RewindQty)
		FROM tbt_RewindDetail
		WHERE (DocNo=@DocNo) AND (Material=@Material)
		
		IF(@RemainQty = 0)
		 BEGIN
			SET @MsgOut = N'Material : ' + @Material + N' scan completed'
			RETURN 0
		 END
		 
		SELECT TOP 1 @internalID=InternalID
		FROM tbt_RewindDetail
		WHERE (DocNo=@DocNo) AND (Material=@Material) AND (OrderQty - RewindQty > 0)
		ORDER BY InternalID ASC
		
		IF(@internalID IS NULL OR @internalID = 0)
		 BEGIN
			SET @MsgOut = N'Get Data InternalID error'
			RETURN 0
		 END
		 
		INSERT tbt_RewindScan
		(Header_InternalID,DocNo,Material,Batch,Qty,IsReverse,CreateBy,CreateDate)
		VALUES
		(@header_internalID,@DocNo,@Material,@Batch,@Qty/1000,0,@CreateBy,GETDATE())
			
		UPDATE tbt_RewindDetail SET RewindQty=RewindQty + 1,UpdateBy=@CreateBy
		,UpdateDate=GETDATE()
		WHERE (InternalID=@internalID) AND (DocNo=@DocNo) AND (Material=@Material)
		
		SET @MsgOut = N'Save data completed'
		*/
	 END
	ELSE
	 BEGIN
		IF(NOT EXISTS(SELECT Material_number FROM Material_Master WHERE (Material_number=@Material)
						AND (Material_type = 'R')))
		 BEGIN
			SET @MsgOut = N'Material : ' + @Material + N'does not exists in PLS'
			RETURN 0
		 END
	 END

	IF(NOT EXISTS(SELECT Material FROM tbt_RewindDetail WHERE (DocNo=@DocNo) AND (Material=@Material)))
	 BEGIN
		SET @MsgOut = N'Material : ' + @Material + N' does not exists in this DP'
		RETURN 0
	 END
	 
	SELECT @RemainQty = SUM(OrderQty - RewindQty)
	FROM tbt_RewindDetail
	WHERE (DocNo=@DocNo) AND (Material=@Material)
	
	IF(@RemainQty = 0)
	 BEGIN
		SET @MsgOut = N'Material : ' + @Material + N' scan completed'
		RETURN 0
	 END
	 
	SELECT TOP 1 @internalID=InternalID
	FROM tbt_RewindDetail
	WHERE (DocNo=@DocNo) AND (Material=@Material) AND (OrderQty - RewindQty > 0)
	ORDER BY InternalID ASC
	
	IF(@internalID IS NULL OR @internalID = 0)
	 BEGIN
		SET @MsgOut = N'Get Data InternalID error'
		RETURN 0
	 END
	 
	INSERT tbt_RewindScan
	(Header_InternalID,DocNo,Material,Batch,Qty,IsReverse,CreateBy,CreateDate)
	VALUES
	(@header_internalID,@DocNo,@Material,@Batch,@Qty,0,@CreateBy,GETDATE())
		
	UPDATE tbt_RewindDetail SET RewindQty=RewindQty + 1,UpdateBy=@CreateBy
	,UpdateDate=GETDATE()
	WHERE (InternalID=@internalID) AND (DocNo=@DocNo) AND (Material=@Material)
	
	SET @MsgOut = N'Save data completed'
			
	RETURN 1
END

go
CREATE PROCEDURE [dbo].[usp_MT_ShipTo_Save]
	-- Add the parameters for the stored procedure here
	@InternalID		decimal(18,0) = null,
	@CustomerCode	nvarchar(20) = null,
	@ShipToName		nvarchar(50) = null,
	@Remark			nvarchar(255) = null,
	@IsActive		bit = null,
	@CreateBy		varchar(50) = null,
	@error_message as nvarchar(200) = null output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	if @InternalID is null
		begin
			if exists(select * from tbm_ShipTo where CustomerCode = @CustomerCode and ShipToName = @ShipToName)
			begin
				set @error_message = 'มีข้อมูล Ship To นี้อยู่แล้วในระบบ';
			end 
			else
			begin
				INSERT INTO tbm_ShipTo (CustomerCode, ShipToName, Remark, IsActive, CreateBy, CreateDate)
				VALUES (@CustomerCode, @ShipToName, @Remark, @IsActive, @CreateBy, GetDate())
			end
		end 
	else
		begin
			UPDATE tbm_ShipTo
			SET CustomerCode = @CustomerCode, 
				ShipToName = @ShipToName, 
				Remark = @Remark, 
				IsActive = @IsActive, 
				CreateBy = @CreateBy, 
				CreateDate = GetDate()
			WHERE InternalID = @InternalID
		end

END

/****** Object:  StoredProcedure [dbo].[usp_MT_LicenseCar_Save]    Script Date: 11/7/2018 1:28:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_MT_LicenseCar_Save]
	-- Add the parameters for the stored procedure here
	@LicenseCarCode nvarchar(20) = null,
	@LicenseCarCodeOld nvarchar(20) = null,
	@DriverName nvarchar(50) = null,
	@CarrierName nvarchar(50) = null,
	@TruckType nvarchar(50) = null,
	@Remark nvarchar(255) = null,
	@IsActive bit = null,
	@CreateBy varchar(50) = null,
	@error_message as nvarchar(200) = null output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	if @LicenseCarCodeOld is null
		begin
			if exists(select * from tbm_LicenseCar where LicenseCarCode = @LicenseCarCode)
			begin
				set @error_message = 'มีข้อมูลทะเบียนรถคันนี้อยู่แล้วในระบบ';
			end 
			else
			begin
				INSERT INTO tbm_LicenseCar (LicenseCarCode, DriverName, CarrierName, TruckType, Remark, IsActive, CreateBy, CreateDate)
			VALUES (@LicenseCarCode, @DriverName, @CarrierName, @TruckType, @Remark, @IsActive, @CreateBy, GetDate())
			end
		end 
	else
		begin
			UPDATE tbm_LicenseCar
			SET DriverName = @DriverName, 
				CarrierName = @CarrierName, 
				TruckType = @TruckType, 
				Remark = @Remark, 
				IsActive = @IsActive, 
				CreateBy = @CreateBy, 
				CreateDate = GetDate()
			WHERE LicenseCarCode = @LicenseCarCode
		end
END

/****** Object:  StoredProcedure [dbo].[usp_MT_Customer_Save]    Script Date: 11/7/2018 1:28:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_MT_Customer_Save]
	-- Add the parameters for the stored procedure here
	@CustomerCode		nvarchar(20) = null,
	@CustomerCodeOld	nvarchar(20) = null,
	@CustomerName		nvarchar(50) = null,
	@Initials			nvarchar(50) = null,
	@Remark				nvarchar(255) = null,
	@IsActive			bit = null,
	@CreateBy			varchar(50) = null,
	@error_message		varchar(200) = null output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	if @CustomerCodeOld is null
		begin
			if exists(select * from tbm_Customer where CustomerCode = @CustomerCode)
			begin
				set @error_message = 'มีข้อมูลลูกค้านี้อยู่แล้วในระบบ';
			end 
			else
			begin
				INSERT INTO tbm_Customer (CustomerCode, CustomerName, Initials, Remark, IsActive, CreateBy, CreateDate)
				VALUES (@CustomerCode, @CustomerName, @Initials, @Remark, @IsActive, @CreateBy, GetDate())
			end
		end 
	else
		begin
			UPDATE tbm_Customer
			SET CustomerName = @CustomerName, 
				Initials = @Initials, 
				Remark = @Remark, 
				IsActive = @IsActive, 
				CreateBy = @CreateBy, 
				CreateDate = GetDate()
			WHERE CustomerCode = @CustomerCode
		end

END
go
CREATE PROCEDURE [dbo].[usp_MT_Problem_Save]
	@problem_id			as int = null
	,@problem_desc		as nvarchar(500) = null
	,@is_active			as bit = null
	,@updateBy			as nvarchar(50) = null
	,@error_message as nvarchar(200) = null output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	if(@problem_id is null)
	begin 
		insert into [dbo].[HH_GT_Problem]([Problem_Desc],[is_active]
					,[create_by],[create_date],[update_by],[update_date])
		values(@problem_desc, @is_active
				,@updateBy,getdate(),@updateBy,getdate());
	end
	else
	begin
		update [dbo].[HH_GT_Problem]
		set [Problem_Desc] = @problem_desc
			, [is_active] = @is_active
			, [update_by] = @updateBy
			, [update_date] = getdate()
		where [Problem_ID] = @problem_id;
	end
END

go

create PROCEDURE [dbo].[usp_StockCountReport2] 
	-- Add the parameters for the stored procedure here
	@WHCode varchar(4),
	@LocationFrom varchar(20) = '', 
	@LocationTo varchar(20) = '',
	@IsDamage bit = 0,
	@ReportName varchar(100),
	@ConditionSAP varchar(8000) = '',
	@ConditionBarcode varchar(8000) = '',
	@SC_Stat varchar(2) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    DECLARE @sql varchar(8000)
    if @SC_Stat = ''
	begin
		IF(@IsDamage = 0) set @SC_Stat = 'UR'
		else set @SC_Stat = 'QI'
	end

    IF(@ReportName = 'StockSAP')
     BEGIN
		-- condition is Plant,Storage
		SET @sql = 'SELECT Plant AS WHCode, SUBSTRING(Material, 4, 7) as Grade, SUBSTRING(Material, 11, 4) as Size,SUM(' + @SC_Stat + 'Qty) AS SAPQty,SUM(' + @SC_Stat + 'Weight) AS SAPWeight'
		+ ' FROM tbt_StockCountMaster WHERE (' + @SC_Stat + 'Qty > 0) ' + @ConditionSAP
		+ ' GROUP BY Plant, SUBSTRING(Material, 4, 7), SUBSTRING(Material, 11, 4)'
		+ ' ORDER BY SUBSTRING(Material, 4, 7), SUBSTRING(Material, 11, 4)'		


		--IF(@SC_Stat = 'UR')
		-- BEGIN
		--	SET @sql = 'SELECT Plant AS WHCode, SUBSTRING(Material, 4, 7) as Grade, SUBSTRING(Material, 11, 4) as Size,SUM(URQty) AS SAPQty,SUM(URWeight) AS SAPWeight'
		--				+ ' FROM tbt_StockCountMaster WHERE (URQty > 0) ' + @ConditionSAP
		--				+ ' GROUP BY Plant, SUBSTRING(Material, 4, 7), SUBSTRING(Material, 11, 4)'
		--				+ ' ORDER BY SUBSTRING(Material, 4, 7), SUBSTRING(Material, 11, 4)'			
		-- END
		--ELSE IF(@SC_Stat = 'QI')
		-- BEGIN
		--	SET @sql = 'SELECT Plant AS WHCode, SUBSTRING(Material, 4, 7) as Grade, SUBSTRING(Material, 11, 4) as Size,SUM(QIQty) AS SAPQty,SUM(QIWeight) AS SAPWeight'
		--				+ ' FROM tbt_StockCountMaster WHERE (QIQty > 0) ' + @ConditionSAP
		--				+ ' GROUP BY Plant, SUBSTRING(Material, 4, 7), SUBSTRING(Material, 11, 4)'
		--				+ ' ORDER BY SUBSTRING(Material, 4, 7), SUBSTRING(Material, 11, 4)'			 
		-- END
     END
    ELSE IF(@ReportName = 'StockCountCompare')
     BEGIN
		-- ConditionSAP is Plant,Storage
		-- ConditionBarcode is WHCode,Location_InternalID
		SET @sql = 'SELECT _SAP.*,ISNULL(CountQty,0) AS CountQty,ISNULL(CountQty,0) - SAPQty AS DiffQty
				FROM
				(SELECT Plant AS WHCode, SUBSTRING(Material, 4, 7) as Grade, SUBSTRING(Material, 11, 4) as Size,SUM(' + @SC_Stat + 'Qty) AS SAPQty,SUM(' + @SC_Stat + 'Weight) AS SAPWeight
				FROM tbt_StockCountMaster WHERE (' + @SC_Stat + 'Qty > 0) ' + @ConditionSAP
				+ ' GROUP BY Plant, SUBSTRING(Material, 4, 7), SUBSTRING(Material, 11, 4)
				) AS _SAP LEFT JOIN
				(SELECT WHCode,Grade,Size,CountQty
				FROM vw_StockCount_Barcode WHERE (1=1) ' + @ConditionBarcode
				+ ') AS _BARCODE 
				ON _SAP.WHCode collate Thai_CI_AS = _Barcode.WHCode collate Thai_CI_AS 
				AND _SAP.Grade collate Thai_CI_AS = _BARCODE.Grade collate Thai_CI_AS
				AND _SAP.Size collate Thai_CI_AS = _BARCODE.Size collate Thai_CI_AS
				ORDER BY _SAP.Grade,_SAP.SIZE'
		--IF(@SC_Stat = 'UR')
		-- BEGIN
		--	print ''
			
		--	SET @sql = 'SELECT _SAP.*,ISNULL(CountQty,0) AS CountQty,ISNULL(CountQty,0) - SAPQty AS DiffQty
		--				FROM
		--				(SELECT Plant AS WHCode, SUBSTRING(Material, 4, 7) as Grade, SUBSTRING(Material, 11, 4) as Size,SUM(URQty) AS SAPQty,SUM(URWeight) AS SAPWeight
		--				FROM tbt_StockCountMaster WHERE (URQty > 0) ' + @ConditionSAP
		--				+ ' GROUP BY Plant, SUBSTRING(Material, 4, 7), SUBSTRING(Material, 11, 4)
		--				) AS _SAP LEFT JOIN
		--				(SELECT WHCode,Grade,Size,CountQty
		--				FROM vw_StockCount_Barcode WHERE (1=1) ' + @ConditionBarcode
		--				+ ') AS _BARCODE 
		--				ON _SAP.WHCode collate Thai_CI_AS = _Barcode.WHCode collate Thai_CI_AS 
		--				AND _SAP.Grade collate Thai_CI_AS = _BARCODE.Grade collate Thai_CI_AS
		--				AND _SAP.Size collate Thai_CI_AS = _BARCODE.Size collate Thai_CI_AS
		--				ORDER BY _SAP.Grade,_SAP.SIZE'
						
						
			
		-- END
		--ELSE IF(@SC_Stat = 'QI')
		-- BEGIN
		--	print ''

		--	SET @sql = 'SELECT _SAP.*,ISNULL(CountQty,0) AS CountQty,ISNULL(CountQty,0) - SAPQty AS DiffQty
		--				FROM
		--				(SELECT Plant AS WHCode, SUBSTRING(Material, 4, 7) as Grade, SUBSTRING(Material, 11, 4) as Size,SUM(QIQty) AS SAPQty,SUM(QIWeight) AS SAPWeight
		--				FROM tbt_StockCountMaster WHERE (QIQty > 0) ' + @ConditionSAP
		--				+ ' GROUP BY Plant, SUBSTRING(Material, 4, 7), SUBSTRING(Material, 11, 4)
		--				) AS _SAP LEFT JOIN
		--				(SELECT WHCode,Grade,Size,CountQty
		--				FROM vw_StockCount_Barcode WHERE (1=1) ' + @ConditionBarcode
		--				+ ') AS _BARCODE 
		--				ON _SAP.WHCode collate Thai_CI_AS = _Barcode.WHCode collate Thai_CI_AS 
		--				AND _SAP.Grade collate Thai_CI_AS = _BARCODE.Grade collate Thai_CI_AS
		--				AND _SAP.Size collate Thai_CI_AS = _BARCODE.Size collate Thai_CI_AS
		--				ORDER BY _SAP.Grade,_SAP.SIZE'			
		-- END
     END
    ELSE IF(@ReportName = 'StockCountDiff')
     BEGIN
		-- ConditionSAP is Plant,Storage
		-- ConditionBarcode is WHCode,Location_InternalID
		SET @sql = 'SELECT _SAP.*,ISNULL(CountQty,0) AS CountQty,ISNULL(CountQty,0) - SAPQty AS DiffQty
					FROM
					(SELECT Plant AS WHCode, SUBSTRING(Material, 4, 7) as Grade, SUBSTRING(Material, 11, 4) as Size,SUM(' + @SC_Stat + 'Qty) AS SAPQty,SUM(' + @SC_Stat + 'Weight) AS SAPWeight
					FROM tbt_StockCountMaster WHERE (' + @SC_Stat + 'Qty > 0) ' + @ConditionSAP
					+ ' GROUP BY Plant, SUBSTRING(Material, 4, 7), SUBSTRING(Material, 11, 4)
					) AS _SAP LEFT JOIN
					(SELECT WHCode,Grade,Size,CountQty
					FROM vw_StockCount_Barcode WHERE (1=1) ' + @ConditionBarcode
					+ ') AS _BARCODE 
					ON _SAP.WHCode collate Thai_CI_AS = _Barcode.WHCode collate Thai_CI_AS 
					AND _SAP.Grade collate Thai_CI_AS = _BARCODE.Grade collate Thai_CI_AS
					AND _SAP.Size collate Thai_CI_AS = _BARCODE.Size collate Thai_CI_AS
					WHERE (ISNULL(_BARCODE.CountQty,0) - _SAP.SAPQty <> 0)
					ORDER BY _SAP.Grade,_SAP.SIZE'
		--IF(@IsDamage = 0)
		-- BEGIN
		--	print ''

		--	SET @sql = 'SELECT _SAP.*,ISNULL(CountQty,0) AS CountQty,ISNULL(CountQty,0) - SAPQty AS DiffQty
		--				FROM
		--				(SELECT Plant AS WHCode, SUBSTRING(Material, 4, 7) as Grade, SUBSTRING(Material, 11, 4) as Size,SUM(URQty) AS SAPQty,SUM(URWeight) AS SAPWeight
		--				FROM tbt_StockCountMaster WHERE (URQty > 0) ' + @ConditionSAP
		--				+ ' GROUP BY Plant, SUBSTRING(Material, 4, 7), SUBSTRING(Material, 11, 4)
		--				) AS _SAP LEFT JOIN
		--				(SELECT WHCode,Grade,Size,CountQty
		--				FROM vw_StockCount_Barcode WHERE (1=1) ' + @ConditionBarcode
		--				+ ') AS _BARCODE 
		--				ON _SAP.WHCode collate Thai_CI_AS = _Barcode.WHCode collate Thai_CI_AS 
		--				AND _SAP.Grade collate Thai_CI_AS = _BARCODE.Grade collate Thai_CI_AS
		--				AND _SAP.Size collate Thai_CI_AS = _BARCODE.Size collate Thai_CI_AS
		--				WHERE (ISNULL(_BARCODE.CountQty,0) - _SAP.SAPQty <> 0)
		--				ORDER BY _SAP.Grade,_SAP.SIZE'
			
		-- END
		--ELSE
		-- BEGIN
		--	print ''
			
		--	SET @sql = 'SELECT _SAP.*,ISNULL(CountQty,0) AS CountQty,ISNULL(CountQty,0) - SAPQty AS DiffQty
		--				FROM
		--				(SELECT Plant AS WHCode, SUBSTRING(Material, 4, 7) as Grade, SUBSTRING(Material, 11, 4) as Size,SUM(QIQty) AS SAPQty,SUM(QIWeight) AS SAPWeight
		--				FROM tbt_StockCountMaster WHERE (QIQty > 0) ' + @ConditionSAP
		--				+ ' GROUP BY Plant, SUBSTRING(Material, 4, 7), SUBSTRING(Material, 11, 4)
		--				) AS _SAP LEFT JOIN
		--				(SELECT WHCode,Grade,Size,CountQty
		--				FROM vw_StockCount_Barcode WHERE (1=1) ' + @ConditionBarcode
		--				+ ') AS _BARCODE 
		--				ON _SAP.WHCode collate Thai_CI_AS = _Barcode.WHCode collate Thai_CI_AS 
		--				AND _SAP.Grade collate Thai_CI_AS = _BARCODE.Grade collate Thai_CI_AS
		--				AND _SAP.Size collate Thai_CI_AS = _BARCODE.Size collate Thai_CI_AS
		--				WHERE (ISNULL(_BARCODE.CountQty,0) - _SAP.SAPQty <> 0)
		--				ORDER BY _SAP.Grade,_SAP.SIZE'						
		-- END
     END
    ELSE IF(@ReportName = 'StockCountBarcode')
     BEGIN
		-- Condition is WHCode,Location_InternalID,Grade not require,Size not require
		SET @sql = 'SELECT WHCode,Section,Row,Grade,Size,SUM(Qty) AS CountQty
					FROM tbt_StockCountDetail
					WHERE (1=1) ' + @ConditionBarcode
					+ ' GROUP BY WHCode,Section,Row,Grade,Size
					ORDER BY WHCode,Section,Row,Grade,Size '
     END
    ELSE IF(@ReportName = 'StockCountSection')
     BEGIN
		-- Condition is WHCode,Location_InternalID
		SET @sql = 'SELECT WHCode,Section,SUM(Qty) AS CountQty
					FROM tbt_StockCountDetail
					WHERE (1=1) ' + @ConditionBarcode
					+ ' GROUP BY WHCode,Section
					ORDER BY WHCode,Section'
     END
    
    
    
    --print @sql
    EXEC (@sql)
    --print 'Total records : ' + convert(varchar,@@ROWCOUNT)
    
	RETURN 1
END



go


CREATE PROCEDURE [dbo].[usp_StockCountSave2] 
	-- Add the parameters for the stored procedure here
	@WHCode varchar(4),
	@LocationCodeID decimal(20,0),
	@LocationCodeFrom varchar(20),
	@LocationCodeTo varchar(20),
	@Section varchar(20),
	@Row varchar(20),
	@Grade varchar(10),
	@Size varchar(5),
	@CustomerNo varchar(10),
	@PaperType varchar(10),
	@Qty int,
	@IsDamage bit,
	@SC_Stat varchar(2) = '',
	@CreateBy varchar(20),
	@MsgOut nvarchar(255) output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @rowCount int,
			@pSTGEFrom varchar(20),
			@pSTGETo varchar(20)
			
			
	SET @pSTGEFrom = 'P' + @LocationCodeFrom
	SET @pSTGETo = 'P' + @LocationCodeTo
	
	if @SC_Stat = '' 
	begin
		IF(@IsDamage = 0) set @SC_Stat = 'UR'
		else set @SC_Stat = 'QI'
	end

	IF(@SC_Stat = 'UR')
	 BEGIN
		IF(NOT EXISTS(SELECT * FROM dbo.vw_StockCount_SAP
			WHERE (Plant=@WHCode) AND (Grade=@Grade) AND (Size=@Size)
			AND (Storage BETWEEN @LocationCodeFrom AND @LocationCodeTo)
			AND (URQty > 0)
		))
		BEGIN
			SET @MsgOut = N'ไม่พบข้อมูล Grade : ' + @Grade
						+ N'\r\nSize : ' + @Size
						+ N'\r\nในข้อมูล Stock ของ SAP'
			RETURN 0	 		
		END
	 END
	ELSE IF (@SC_Stat = 'QI')
	 BEGIN
		IF(NOT EXISTS(SELECT * FROM dbo.vw_StockCount_SAP
			WHERE (Plant=@WHCode) AND (Grade=@Grade) AND (Size=@Size)
			AND (Storage BETWEEN @LocationCodeFrom AND @LocationCodeTo)
			AND (QIQty > 0)
		))
		BEGIN
			SET @MsgOut = N'ไม่พบข้อมูล Grade : ' + @Grade
						+ N'\r\nSize : ' + @Size
						+ N'\r\nในข้อมูล Stock ของ SAP'
			RETURN 0	 		
		END	 
	 END
	ELSE IF (@SC_Stat = 'BL')
	 BEGIN
		IF(NOT EXISTS(SELECT * FROM dbo.vw_StockCount_SAP
			WHERE (Plant=@WHCode) AND (Grade=@Grade) AND (Size=@Size)
			AND (Storage BETWEEN @LocationCodeFrom AND @LocationCodeTo)
			AND (BLQty > 0)
		))
		BEGIN
			SET @MsgOut = N'ไม่พบข้อมูล Grade : ' + @Grade
						+ N'\r\nSize : ' + @Size
						+ N'\r\nในข้อมูล Stock ของ SAP'
			RETURN 0	 		
		END	 
	 END
	
	INSERT INTO tbt_StockCountDetail
	(WHCode,Location_InternalID,LocationCodeFrom,LocationCodeTo,Section,[Row],Grade,Size,Qty,IsDamage,CreateBy,CreateDate)
	VALUES
	(@WHCode,@LocationCodeID,@LocationCodeFrom,@LocationCodeTo,@Section,@Row,@Grade,@Size,@Qty,@IsDamage,@CreateBy,GETDATE())
	
	
	RETURN 1
END



go






create PROCEDURE [dbo].[usp_StockCountMaster2] 
	-- Add the parameters for the stored procedure here
	@IsAppend bit,
	@CreateBy varchar(20),
	@IPAddress varchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @interfaceID decimal(20,0)
	
	INSERT INTO tbt_StockCountControl
	(IPAddress,CreateBy,CreateDate)
	VALUES
	(@IPAddress,@CreateBy,GETDATE())
	
	SET @interfaceID = SCOPE_IDENTITY()
	
	IF(@IsAppend = 0)
	 BEGIN
		-- Insert All data
		DELETE tbt_StockCountMaster
		
		INSERT INTO tbt_StockCountMaster
		(InterfaceID,Plant,Storage,Material,URQty,QIQty,BLQty,URWeight,QIWeight, BLWeight,CreateBy,CreateDate)
		SELECT @interfaceID,Plant,Storage,Material,URQty,QIQty,BLQty,URWeight,QIWeight,BLWeight,@CreateBy,GETDATE()
		FROM tbt_StockCountImport
	 END
	ELSE
	 BEGIN
		DECLARE @cursor CURSOR
		SET @cursor = CURSOR FAST_FORWARD FOR
		SELECT Plant,Storage,Material,URQty,QIQty,BLQty,URWeight,QIWeight,BLWeight FROM tbt_StockCountImport
		
		OPEN @cursor
		
		DECLARE @plant varchar(5),
				@storage varchar(10),
				@material varchar(20),
				@urQty int,
				@qiQty int,
				@blQty int,
				@urWeight decimal(18,3),
				@qiWeight decimal(18,3),
				@blWeight decimal(18,3)
		
		FETCH FROM @cursor INTO @plant,@storage,@material,@urQty,@qiQty,@blQty,@urWeight,@qiWeight,@blWeight
		WHILE(@@FETCH_STATUS = 0)
		 BEGIN
			IF(EXISTS(SELECT * FROM tbt_StockCountMaster WHERE (Plant=@plant) AND (Storage=@storage) AND (Material=@material)  ))
			 BEGIN
				-- this batch exists in system
				UPDATE tbt_StockCountMaster
				SET InterfaceID=@interfaceID,URQty=@urQty,QIQty=@qiQty,BLQty=@blQty,URWeight=@urWeight,QIWeight=@qiWeight,BLWeight=@blWeight,UpdateBy=@CreateBy,UpdateDate=GETDATE()
				WHERE (Plant=@plant) AND (Storage=@storage) AND (Material=@material)
			 END
			ELSE
			 BEGIN
				-- this batch does not exists
				INSERT INTO tbt_StockCountMaster
				(InterfaceID,Plant,Storage,Material,URQty,QIQty,BLQty,URWeight,QIWeight,BLWeight,CreateBy,CreateDate)
				VALUES
				(@interfaceID,@plant,@storage,@material,@urQty,@qiQty,@blQty,@urWeight,@qiWeight,@blWeight,@CreateBy,GETDATE())
			 END
		 
			FETCH NEXT FROM @cursor INTO @plant,@storage,@material,@urQty,@qiQty,@blQty,@urWeight,@qiWeight,@blWeight
		 END
		CLOSE @cursor
		DEALLOCATE @cursor
				
	 END
	
	RETURN 1
	
END
