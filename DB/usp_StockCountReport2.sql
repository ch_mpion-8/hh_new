
create PROCEDURE [dbo].[usp_StockCountReport2] 
	-- Add the parameters for the stored procedure here
	@WHCode varchar(4),
	@LocationFrom varchar(20) = '', 
	@LocationTo varchar(20) = '',
	@IsDamage bit = 0,
	@ReportName varchar(100),
	@ConditionSAP varchar(8000) = '',
	@ConditionBarcode varchar(8000) = '',
	@SC_Stat varchar(2) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    DECLARE @sql varchar(8000)
    if @SC_Stat = ''
	begin
		IF(@IsDamage = 0) set @SC_Stat = 'UR'
		else set @SC_Stat = 'QI'
	end

    IF(@ReportName = 'StockSAP')
     BEGIN
		-- condition is Plant,Storage
		SET @sql = 'SELECT Plant AS WHCode, SUBSTRING(Material, 4, 7) as Grade, SUBSTRING(Material, 11, 4) as Size,SUM(' + @SC_Stat + 'Qty) AS SAPQty,SUM(' + @SC_Stat + 'Weight) AS SAPWeight'
		+ ' FROM tbt_StockCountMaster WHERE (' + @SC_Stat + 'Qty > 0) ' + @ConditionSAP
		+ ' GROUP BY Plant, SUBSTRING(Material, 4, 7), SUBSTRING(Material, 11, 4)'
		+ ' ORDER BY SUBSTRING(Material, 4, 7), SUBSTRING(Material, 11, 4)'		


		--IF(@SC_Stat = 'UR')
		-- BEGIN
		--	SET @sql = 'SELECT Plant AS WHCode, SUBSTRING(Material, 4, 7) as Grade, SUBSTRING(Material, 11, 4) as Size,SUM(URQty) AS SAPQty,SUM(URWeight) AS SAPWeight'
		--				+ ' FROM tbt_StockCountMaster WHERE (URQty > 0) ' + @ConditionSAP
		--				+ ' GROUP BY Plant, SUBSTRING(Material, 4, 7), SUBSTRING(Material, 11, 4)'
		--				+ ' ORDER BY SUBSTRING(Material, 4, 7), SUBSTRING(Material, 11, 4)'			
		-- END
		--ELSE IF(@SC_Stat = 'QI')
		-- BEGIN
		--	SET @sql = 'SELECT Plant AS WHCode, SUBSTRING(Material, 4, 7) as Grade, SUBSTRING(Material, 11, 4) as Size,SUM(QIQty) AS SAPQty,SUM(QIWeight) AS SAPWeight'
		--				+ ' FROM tbt_StockCountMaster WHERE (QIQty > 0) ' + @ConditionSAP
		--				+ ' GROUP BY Plant, SUBSTRING(Material, 4, 7), SUBSTRING(Material, 11, 4)'
		--				+ ' ORDER BY SUBSTRING(Material, 4, 7), SUBSTRING(Material, 11, 4)'			 
		-- END
     END
    ELSE IF(@ReportName = 'StockCountCompare')
     BEGIN
		-- ConditionSAP is Plant,Storage
		-- ConditionBarcode is WHCode,Location_InternalID
		SET @sql = 'SELECT _SAP.*,ISNULL(CountQty,0) AS CountQty,ISNULL(CountQty,0) - SAPQty AS DiffQty
				FROM
				(SELECT Plant AS WHCode, SUBSTRING(Material, 4, 7) as Grade, SUBSTRING(Material, 11, 4) as Size,SUM(' + @SC_Stat + 'Qty) AS SAPQty,SUM(' + @SC_Stat + 'Weight) AS SAPWeight
				FROM tbt_StockCountMaster WHERE (' + @SC_Stat + 'Qty > 0) ' + @ConditionSAP
				+ ' GROUP BY Plant, SUBSTRING(Material, 4, 7), SUBSTRING(Material, 11, 4)
				) AS _SAP LEFT JOIN
				(SELECT WHCode,Grade,Size,CountQty
				FROM vw_StockCount_Barcode WHERE (1=1) ' + @ConditionBarcode
				+ ') AS _BARCODE 
				ON _SAP.WHCode collate Thai_CI_AS = _Barcode.WHCode collate Thai_CI_AS 
				AND _SAP.Grade collate Thai_CI_AS = _BARCODE.Grade collate Thai_CI_AS
				AND _SAP.Size collate Thai_CI_AS = _BARCODE.Size collate Thai_CI_AS
				ORDER BY _SAP.Grade,_SAP.SIZE'
		--IF(@SC_Stat = 'UR')
		-- BEGIN
		--	print ''
			
		--	SET @sql = 'SELECT _SAP.*,ISNULL(CountQty,0) AS CountQty,ISNULL(CountQty,0) - SAPQty AS DiffQty
		--				FROM
		--				(SELECT Plant AS WHCode, SUBSTRING(Material, 4, 7) as Grade, SUBSTRING(Material, 11, 4) as Size,SUM(URQty) AS SAPQty,SUM(URWeight) AS SAPWeight
		--				FROM tbt_StockCountMaster WHERE (URQty > 0) ' + @ConditionSAP
		--				+ ' GROUP BY Plant, SUBSTRING(Material, 4, 7), SUBSTRING(Material, 11, 4)
		--				) AS _SAP LEFT JOIN
		--				(SELECT WHCode,Grade,Size,CountQty
		--				FROM vw_StockCount_Barcode WHERE (1=1) ' + @ConditionBarcode
		--				+ ') AS _BARCODE 
		--				ON _SAP.WHCode collate Thai_CI_AS = _Barcode.WHCode collate Thai_CI_AS 
		--				AND _SAP.Grade collate Thai_CI_AS = _BARCODE.Grade collate Thai_CI_AS
		--				AND _SAP.Size collate Thai_CI_AS = _BARCODE.Size collate Thai_CI_AS
		--				ORDER BY _SAP.Grade,_SAP.SIZE'
						
						
			
		-- END
		--ELSE IF(@SC_Stat = 'QI')
		-- BEGIN
		--	print ''

		--	SET @sql = 'SELECT _SAP.*,ISNULL(CountQty,0) AS CountQty,ISNULL(CountQty,0) - SAPQty AS DiffQty
		--				FROM
		--				(SELECT Plant AS WHCode, SUBSTRING(Material, 4, 7) as Grade, SUBSTRING(Material, 11, 4) as Size,SUM(QIQty) AS SAPQty,SUM(QIWeight) AS SAPWeight
		--				FROM tbt_StockCountMaster WHERE (QIQty > 0) ' + @ConditionSAP
		--				+ ' GROUP BY Plant, SUBSTRING(Material, 4, 7), SUBSTRING(Material, 11, 4)
		--				) AS _SAP LEFT JOIN
		--				(SELECT WHCode,Grade,Size,CountQty
		--				FROM vw_StockCount_Barcode WHERE (1=1) ' + @ConditionBarcode
		--				+ ') AS _BARCODE 
		--				ON _SAP.WHCode collate Thai_CI_AS = _Barcode.WHCode collate Thai_CI_AS 
		--				AND _SAP.Grade collate Thai_CI_AS = _BARCODE.Grade collate Thai_CI_AS
		--				AND _SAP.Size collate Thai_CI_AS = _BARCODE.Size collate Thai_CI_AS
		--				ORDER BY _SAP.Grade,_SAP.SIZE'			
		-- END
     END
    ELSE IF(@ReportName = 'StockCountDiff')
     BEGIN
		-- ConditionSAP is Plant,Storage
		-- ConditionBarcode is WHCode,Location_InternalID
		SET @sql = 'SELECT _SAP.*,ISNULL(CountQty,0) AS CountQty,ISNULL(CountQty,0) - SAPQty AS DiffQty
					FROM
					(SELECT Plant AS WHCode, SUBSTRING(Material, 4, 7) as Grade, SUBSTRING(Material, 11, 4) as Size,SUM(' + @SC_Stat + 'Qty) AS SAPQty,SUM(' + @SC_Stat + 'Weight) AS SAPWeight
					FROM tbt_StockCountMaster WHERE (' + @SC_Stat + 'Qty > 0) ' + @ConditionSAP
					+ ' GROUP BY Plant, SUBSTRING(Material, 4, 7), SUBSTRING(Material, 11, 4)
					) AS _SAP LEFT JOIN
					(SELECT WHCode,Grade,Size,CountQty
					FROM vw_StockCount_Barcode WHERE (1=1) ' + @ConditionBarcode
					+ ') AS _BARCODE 
					ON _SAP.WHCode collate Thai_CI_AS = _Barcode.WHCode collate Thai_CI_AS 
					AND _SAP.Grade collate Thai_CI_AS = _BARCODE.Grade collate Thai_CI_AS
					AND _SAP.Size collate Thai_CI_AS = _BARCODE.Size collate Thai_CI_AS
					WHERE (ISNULL(_BARCODE.CountQty,0) - _SAP.SAPQty <> 0)
					ORDER BY _SAP.Grade,_SAP.SIZE'
		--IF(@IsDamage = 0)
		-- BEGIN
		--	print ''

		--	SET @sql = 'SELECT _SAP.*,ISNULL(CountQty,0) AS CountQty,ISNULL(CountQty,0) - SAPQty AS DiffQty
		--				FROM
		--				(SELECT Plant AS WHCode, SUBSTRING(Material, 4, 7) as Grade, SUBSTRING(Material, 11, 4) as Size,SUM(URQty) AS SAPQty,SUM(URWeight) AS SAPWeight
		--				FROM tbt_StockCountMaster WHERE (URQty > 0) ' + @ConditionSAP
		--				+ ' GROUP BY Plant, SUBSTRING(Material, 4, 7), SUBSTRING(Material, 11, 4)
		--				) AS _SAP LEFT JOIN
		--				(SELECT WHCode,Grade,Size,CountQty
		--				FROM vw_StockCount_Barcode WHERE (1=1) ' + @ConditionBarcode
		--				+ ') AS _BARCODE 
		--				ON _SAP.WHCode collate Thai_CI_AS = _Barcode.WHCode collate Thai_CI_AS 
		--				AND _SAP.Grade collate Thai_CI_AS = _BARCODE.Grade collate Thai_CI_AS
		--				AND _SAP.Size collate Thai_CI_AS = _BARCODE.Size collate Thai_CI_AS
		--				WHERE (ISNULL(_BARCODE.CountQty,0) - _SAP.SAPQty <> 0)
		--				ORDER BY _SAP.Grade,_SAP.SIZE'
			
		-- END
		--ELSE
		-- BEGIN
		--	print ''
			
		--	SET @sql = 'SELECT _SAP.*,ISNULL(CountQty,0) AS CountQty,ISNULL(CountQty,0) - SAPQty AS DiffQty
		--				FROM
		--				(SELECT Plant AS WHCode, SUBSTRING(Material, 4, 7) as Grade, SUBSTRING(Material, 11, 4) as Size,SUM(QIQty) AS SAPQty,SUM(QIWeight) AS SAPWeight
		--				FROM tbt_StockCountMaster WHERE (QIQty > 0) ' + @ConditionSAP
		--				+ ' GROUP BY Plant, SUBSTRING(Material, 4, 7), SUBSTRING(Material, 11, 4)
		--				) AS _SAP LEFT JOIN
		--				(SELECT WHCode,Grade,Size,CountQty
		--				FROM vw_StockCount_Barcode WHERE (1=1) ' + @ConditionBarcode
		--				+ ') AS _BARCODE 
		--				ON _SAP.WHCode collate Thai_CI_AS = _Barcode.WHCode collate Thai_CI_AS 
		--				AND _SAP.Grade collate Thai_CI_AS = _BARCODE.Grade collate Thai_CI_AS
		--				AND _SAP.Size collate Thai_CI_AS = _BARCODE.Size collate Thai_CI_AS
		--				WHERE (ISNULL(_BARCODE.CountQty,0) - _SAP.SAPQty <> 0)
		--				ORDER BY _SAP.Grade,_SAP.SIZE'						
		-- END
     END
    ELSE IF(@ReportName = 'StockCountBarcode')
     BEGIN
		-- Condition is WHCode,Location_InternalID,Grade not require,Size not require
		SET @sql = 'SELECT WHCode,Section,Row,Grade,Size,SUM(Qty) AS CountQty
					FROM tbt_StockCountDetail
					WHERE (1=1) ' + @ConditionBarcode
					+ ' GROUP BY WHCode,Section,Row,Grade,Size
					ORDER BY WHCode,Section,Row,Grade,Size '
     END
    ELSE IF(@ReportName = 'StockCountSection')
     BEGIN
		-- Condition is WHCode,Location_InternalID
		SET @sql = 'SELECT WHCode,Section,SUM(Qty) AS CountQty
					FROM tbt_StockCountDetail
					WHERE (1=1) ' + @ConditionBarcode
					+ ' GROUP BY WHCode,Section
					ORDER BY WHCode,Section'
     END
    
    
    
    --print @sql
    EXEC (@sql)
    --print 'Total records : ' + convert(varchar,@@ROWCOUNT)
    
	RETURN 1
END







