

alter table [dbo].[tbt_StockCountImport]
DROP COLUMN [BLQty],[BLWeight] ;

go
alter table [dbo].[tbt_StockCountMaster]
DROP COLUMN [BLQty],[BLWeight] ;
go
	  

ALTER TABLE dbo.HH_GT_Problem
DROP COLUMN is_active, 
	create_by,
	create_date ,
	update_by,
	update_date
GO

alter table [dbo].[tbm_Location]
  DROP COLUMN SC_Stat
  go
  
alter table [dbo].[HH_GR_Prod]
DROP COLUMN [Interface_ID];
  go
alter table [dbo].[HH_GR_Prod]
DROP COLUMN [Storage];
  go
alter table [dbo].[HH_GR_Prod]
DROP COLUMN [Return_Msg];
  go
  
 
ALTER VIEW [dbo].[vw_StockCount_SAP]
AS
SELECT        TOP 100 PERCENT Plant, Storage, SUBSTRING(Material, 4, 7) AS Grade, SUBSTRING(Material, 11, 4) AS Size, SUM(URQty) AS URQty, SUM(QIQty) AS QIQty, 
                         SUM(URWeight) AS URWeight, SUM(QIWeight) AS QIWeight
FROM            dbo.tbt_StockCountMaster
GROUP BY Plant, Storage, SUBSTRING(Material, 4, 7), SUBSTRING(Material, 11, 4)
ORDER BY Plant, Storage, Grade, Size

GO

drop PROCEDURE [dbo].[GI_Count_Batch2] 
GO

drop PROCEDURE [dbo].[usp_RewindSaveScan2] 
GO

drop PROCEDURE [dbo].[usp_MT_ShipTo_Save]
GO

DROP PROCEDURE [dbo].[usp_MT_LicenseCar_Save]
GO

DROP PROCEDURE [dbo].[usp_MT_Customer_Save]
go

DROP PROCEDURE [dbo].[usp_MT_Problem_Save]
go

DROP PROCEDURE [dbo].[usp_StockCountReport2] 
go

DROP PROCEDURE [dbo].[usp_StockCountSave2] 
GO

DROP PROCEDURE [dbo].[usp_StockCountMaster2] 
GO
