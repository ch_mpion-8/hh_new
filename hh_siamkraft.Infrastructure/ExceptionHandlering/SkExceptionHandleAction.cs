﻿namespace hh_siamkraft.Infrastructure.ExceptionHandlering
{
    public enum SkExceptionHandleAction
    {
        None,
        Log,
        Rethrow,
        LogAndRethrow,
    }
}
