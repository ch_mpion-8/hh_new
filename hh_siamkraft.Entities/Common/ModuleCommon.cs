﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hh_siamkraft.Entities.Common
{
    public static class ModuleCommon
    {
        public const string CONFIG_STORAGE_WHH = "CONFIG_STORAGE_WHH";
        public const string tbl_handheld = "HandHeld_ID";
        public const string tbl_timeout = "HH_TimeOut";
        public const string sap_gr = "SAP_GR";
        public const string gr_running = "HH_GR_Running";
        public const string gr_prod = "HH_GR_Prod";
        public const string gr_weight_error = "HH_GR_Weight_Error";
        public const string gr_prod_log = "HH_GR_Prod_Log";
        public const string hh_qtystd = "HH_QtyStd";
        public const string tbl_gr_running = "HH_GR_Running";
        public const string hh_pm = "HH_PM";
        public const string tbl_po_fromtext = "PO_FromTxt";
        public const string tbl_po_head = "PO_Head";
        public const string tbl_po_item = "PO_Item";
        public const string tbl_hh_reel_no = "HH_Reel_No";
        public const string tbl_outsource = "HH_GR_Outsource";
        public const string tbl_outsource_log = "HH_GR_Outsource_Log";
        public const string tbl_sap_grwithPO_head = "SAP_GRwithPO_Head";
        public const string tbl_sap_grwithPO_item = "SAP_GRwithPO_Item";
        public const string tbl_StockInquiry_Header = "StockInquiry_Header";
        public const string tbl_StockInquiry_Item = "StockInquiry_Item";
        public const string tbt_RewindHeader = "tbt_RewindHeader";
        public const string tbt_RewindDetail = "tbt_RewindDetail";
        public const string tbt_RewindScan = "tbt_RewindScan";
        public const string view_po_item = "sum_po_item";
        public const string view_getItem = "GetItem";
        public const string view_gr_checkBatch = "GR_checkBatch";
        public const string HH_GT_Item = "HH_GT_Item";
        public const string HH_GT_Running = "HH_GT_Running";
        public const string HH_Sloc = "HH_Sloc";
        public const string HH_View_Stock = "HH_View_Stock";
        public const string HH_GT_Problem = "HH_GT_Problem";
        public const string HH_WH = "HH_WH";
        public const string HH_TimeOut = "HH_TimeOut";
        public const string tbl_dl_head = "Delivery_Head";
        public const string tbl_dl_item = "Delivery_Item";
        public const string tbl_gi_item = "HH_GI_Item";
        public const string tbl_picking_head = "Picking_Head";
        public const string tbl_picking_item = "Picking_item";
        public const string view_gi_detail = "GI_Detail";
        public const string view_gi_mat_detail = "GI_Mat_Detail";
        public const string view_gi_batch_detail = "GI_Batch_Detail";
        public const string view_gi_check_id = "GI_Check_ID";
        public const string view_gi_report = "GI_Report";
        public const string tbl_siamkraft_user = "PC_Siamkraft_User";
        public const string tbl_work_detail = "PC_Siamkraft_Work_Detail";
        public const string tbl_control = "Control";
        public const string tbl_roll_amendment = "Roll_amendment";
        public const string tbl_roll_transaction = "Roll_transaction";
        public const string tbl_sap_plant = "SAP_Plant";
        public const string view_groupby_hh_gi_item = "Groupby_HH_GI_Item";
        public const string dirPathGR_101_Report = "D:\\HandHeldLog\\GR_101\\Report\\";
        public const string dirPathLogFile = "D:\\HandHeldLog";
        public const string dirPathGR_525 = "D:\\HandHeldLog\\GR_525\\";
        public const string dirPathGR_101 = "D:\\HandHeldLog\\GR_101\\";
    }
}
