﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hh_siamkraft.Entities.Common
{
    public class UserBase
    {
        public string User_ID { get; set; }
        public string User_Password { get; set; }
        public string User_Name { get; set; }
        public string User_Lastname { get; set; }
        public string User_Position { get; set; }
        public int User_Sex { get; set; }
        public int User_Status { get; set; }
        public string User_Company { get; set; }
        public int? User_Wh { get; set; }
    }
}
