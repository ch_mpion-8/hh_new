﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hh_siamkraft.Entities.Common
{
    public class ServerDetail
    {
        public int ServerID { get; set; }
        public string Description { get; set; }
        public string ConnectString { get; set; }
        public string Pm { get; set; }
        public bool? ServerMain { get; set; }
    }
}
