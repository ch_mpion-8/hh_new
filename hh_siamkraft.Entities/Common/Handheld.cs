﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hh_siamkraft.Entities.Common
{
    public class Handheld
    {
        public string IP_Address { get; set; }
        public string HH_ID { get; set; }
        public string Plant { get; set; }
        public string Information { get; set; }
    }
}
