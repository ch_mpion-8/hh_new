﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hh_siamkraft.Entities.Common
{
    public class User
    {
        public User()
        {
            UserLogin = new UserLogin();
        }
        public UserBase UserDetail { get; set; }
        public IList<ServerDetail> ServerDetail { get; set; }
        public IList<UserPermission> Permission { get; set; }
        public IList<WarehouseDetail> WarehouseDetail { get; set; }
        public IList<Handheld> Handheld { get; set; }
        public IList<string> ConfigValue { get; set; }
        public UserLogin UserLogin { get; set; }
    }
}
