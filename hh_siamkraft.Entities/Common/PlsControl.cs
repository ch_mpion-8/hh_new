﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hh_siamkraft.Entities.Common
{
    public class PlsControl
    {
        public int Record_ID { get; set; }
        public int? Prev_Process { get; set; }
        public string HH_ID { get; set; }
        public string Process_ID { get; set; }
        public string Return_Code { get; set; }
        public string Return_Msg { get; set; }
        public string User_Name { get; set; }
        public DateTime? Create_Date_Time { get; set; }
        public DateTime? Complete_Date_Time { get; set; }
        public string Flag_Del { get; set; }
        public DateTime? Call_Date_Time { get; set; }
    }
}
