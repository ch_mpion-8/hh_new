﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hh_siamkraft.Entities.Common
{
    public class UserLogin
    {
        public UserLogin()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private int whID;
        private string whCode;
        private string whDescription;
        private string shippingPoint;
        private string userName;
        private string companyCode;
        private string ipAddress;
        private string hhID;
        private DataTable authenDetail;

        public DataTable AuthenDetail
        {
            get
            {
                return authenDetail;
            }
            set { authenDetail = value; }
        }

        public string HHID
        {
            get
            {
                return hhID;
            }
            set
            {
                hhID = value;
            }
        }

        public string IPAddress
        {
            get
            {
                return ipAddress;
            }
            set
            {
                ipAddress = value;
            }
        }

        public string CompanyCode
        {
            get
            {
                return companyCode;
            }
            set
            {
                companyCode = value;
            }
        }

        public string UserName
        {
            get
            {
                return userName;
            }
            set
            {
                userName = value;
            }
        }

        public string ShippingPoint
        {
            get
            {
                return shippingPoint;
            }
            set
            {
                shippingPoint = value;
            }
        }

        public string WHDescription
        {
            get
            {
                return whDescription;
            }
            set
            {
                whDescription = value;
            }
        }

        public string WHCode
        {
            get
            {
                return whCode;
            }
            set
            {
                whCode = value;
            }
        }

        public Int32 WHID
        {
            get
            {
                return whID;
            }
            set
            {
                whID = value;
            }
        }
    }
}
