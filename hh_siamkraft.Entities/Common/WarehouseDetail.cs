﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hh_siamkraft.Entities.Common
{
    public class WarehouseDetail
    {
        public int WH_No { get; set; }
        public string WHCode { get; set; }
        public string Location { get; set; }
        public string WH_Description { get; set; }
        public string WH_Description2 { get; set; }
        public string ShippingPoint { get; set; }
    }
}
