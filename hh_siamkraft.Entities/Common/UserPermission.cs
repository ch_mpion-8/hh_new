﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hh_siamkraft.Entities.Common
{
    public class UserPermission
    {
        public string User_ID { get; set; }
        public string Function_Name { get; set; }
        public string Platform { get; set; }
    }
}
