﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class HH_GT_Item
    {
        public decimal Internal_ID { get; set; }
        public int GT_ID { get; set; }
        public string MovementType { get; set; }
        public string Material { get; set; }
        public string Batch { get; set; }
        public decimal Qty { get; set; }
        public string Problem_Desc { get; set; }
        public string Storage_From { get; set; }
        public string Storage_To { get; set; }
        public string CreateUser { get; set; }
        public DateTime CreateDateTime { get; set; }
        public string Status { get; set; }
        public int Item_No { get; set; }
        public string SFrom { get; set; }
        public string UOM { get; set; }
        public int WH_No { get; set; }
        public string WH_Code { get; set; }
        public int Interface_ID { get; set; }
        public string Return_Msg { get; set; }
        public string ClearText { get; set; }
        public string Move_Batch { get; set; }

    }
    public class StockInquiryItem
    {
        public string QueryID { get; set; }
        public string Batch { get; set; }
        public string Storage { get; set; }
        public string MatCode { get; set; }
    }
        
}
