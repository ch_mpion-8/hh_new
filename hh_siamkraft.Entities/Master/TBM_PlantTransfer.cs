﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class TBM_PlantTransfer
    {
        public string Plant { get; set; }
        public string Sloc { get; set; }
        public string WH1 { get; set; }
        public string Desc1 { get; set; }
        public string Desc2 { get; set; }

    }
}
