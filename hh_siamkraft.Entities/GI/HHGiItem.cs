﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hh_siamkraft.Entities.GI
{
    public class HHGiItem
    {
        public int HH_Item_ID { get; set; }
        public decimal Record_ID { get; set; }
        public string Delivery_No { get; set; }
        public string Material_No { get; set; }
        public string Batch_No { get; set; }
        public string Plant { get; set; }
        public string Storage_Loc { get; set; }
        public string Item_No { get; set; }
        public string Line_No { get; set; }
        public decimal Actual_Qty { get; set; }
        public decimal Qty { get; set; }
        public string Status { get; set; }
        public string CreateUser { get; set; }
        public DateTime CreateDateTime { get; set; }
        public int WH_No { get; set; }
        public string UOM { get; set; }
        public int Interface_ID { get; set; }
    }
}
