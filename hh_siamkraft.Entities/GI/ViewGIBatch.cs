﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hh_siamkraft.Entities.GI
{
    public class ViewGIBatch
    {
        public string Material_No { get; set; }
        public string Batch_No { get; set; }
        public string Pick_HH { get; set; }
        public string PI { get; set; }
        public string Delivery_No { get; set; }
        public int Record_ID { get; set; }
        public string Batch { get; set; }
        public string Qty { get; set; }
        public string Actual_Qty { get; set; }


    }
}
