﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hh_siamkraft.Entities.GI
{
    public class DeliveryItem
    {
        public int Delivery_Item_ID { get; set; }
        public int Record_ID { get; set; }
        public string Delivery_No { get; set; }
        public string Item_No { get; set; }
        public string Material_No { get; set; }
        public string Material_Desc { get; set; }
        public string Batch { get; set; }
        public string HghLevItmBatch { get; set; }
        public decimal Actual_Qty { get; set; }
        public string Storage_Loc { get; set; }
        public string Plant { get; set; }
        public string Item_Category { get; set; }
        public int Conversion_Factor { get; set; }
        public int Conversion_Divisor { get; set; }
        public string Sales_unit { get; set; }


        //view GROUPby_HH_GI_ITEM
        public decimal Mat_Order { get; set; }
        public int AO { get; set; }
        public decimal HH_Qty { get; set; }
        public string UOM { get; set; }
        public decimal Qty { get; set; }
    }
}
