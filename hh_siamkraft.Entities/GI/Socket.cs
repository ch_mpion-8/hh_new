﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hh_siamkraft.Entities.GI
{
    public class Socket
    {
        public int Socket_ID { get; set; }
        public string Description { get; set; }
        public string Socket_Name { get; set; }
    }
}
