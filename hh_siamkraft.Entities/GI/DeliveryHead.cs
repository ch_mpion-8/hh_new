﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hh_siamkraft.Entities.GI
{
    public class DeliveryHead
    {
        public int Record_ID { get; set; }
        public string Delivery_No { get; set; }
        public string Delivery_Type { get; set; }
        public DateTime Time_Create { get; set; }
        public DateTime Time_Createcomplete { get; set; }
        public DateTime Time_Scan { get; set; }
        public DateTime Time_ScanComplete { get; set; }
        public DateTime Time_Send { get; set; }
        public DateTime Time_SendComplete { get; set; }
        public string Status { get; set; }
        public string Text { get; set; }
        public int WH_No { get; set; }
        public string Sales_Org { get; set; }
    }
}
