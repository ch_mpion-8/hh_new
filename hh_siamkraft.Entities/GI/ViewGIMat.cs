﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hh_siamkraft.Entities.GI
{
    public class ViewGIMat
    {
        public string Material_No { get; set; }
        public string Mat_Order { get; set; }
        public int AO { get; set; }
        public string Pick_HH { get; set; } //6
        public string PI { get; set; }
        public string Delivery_No { get; set; } 
        public int Record_ID { get; set; }
        public string Actual_Qty  { get; set; }
        public int Count_Batch { get; set; }
        

    }
}
