﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hh_siamkraft.Entities.GI
{
    public class ViewGICheckID
    {
        public string Plant { get; set; }
        public int Record_ID { get; set; }
        public string Delivery_No { get; set; }
        public string Text { get; set; }
    }
}
