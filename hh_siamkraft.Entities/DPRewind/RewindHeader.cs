﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class RewindHeader
    {
        public int? InterfaceID { get; set; }

        public string DocNo { get; set; }
    }

    public class RewindDetail
    {
        public string WHCode { get; set; }
        public string RewindNo { get; set; }
        public string DocNo { get; set; }
        public string Material { get; set; }
        public int OrderQty { get; set; }
        public bool InterfaceSAP { get; set; }
        public string Status { get; set; }
        public int RewindQty { get; set; }
        public decimal TotalWeight { get; set; }
        public decimal InternalID { get; set; }
    }

    public class TotalScan
    {
        public string DocNo { get; set; }
        public int TotalOrderQty { get; set; }
        public int TotalScanQty { get; set; }
    }
    public class RewindScan
    {
        public string Material { get; set; }
        public string Batch { get; set; }
        public decimal Qty { get; set; }
        public string DocNo { get; set; }
        public decimal InternalID { get; set; }
    }
}
