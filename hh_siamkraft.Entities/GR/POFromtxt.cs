﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hh_siamkraft.Entities.GR
{
    public class POFromtxt
    {
        public string PO_Number { get; set; }
        public string ProdDate { get; set; }
        public string Material { get; set; }
        public string Batch { get; set; }
        public decimal Qty { get; set; }
        public decimal Length { get; set; }
        public string Material_Number { get; set; }
        public string CreateUser { get; set; }
        public DateTime CreateDateTime { get; set; }
    }
}
