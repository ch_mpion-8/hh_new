﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hh_siamkraft.Entities.GR
{
    public class ViewGetItem
    {
        public string VendorCode { get; set; }
        public int Record_ID { get; set; }
        public string Plant { get; set; }
    }
}
