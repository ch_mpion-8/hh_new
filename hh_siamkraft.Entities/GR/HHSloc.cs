﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hh_siamkraft.Entities.GR
{
    public class HHSloc
    {
        public int Internal_ID { get; set; }
        public string WHCode { get; set; }
        public string StorageLocation { get; set; }
        public string Description { get; set; }
        public byte IsTransferStatus { get; set; }
        public byte IsTransferPlant { get; set; }
        public int CreateBy { get; set; }
        public DateTime CreateDate { get; set; }
        public int UpdateBy { get; set; }
        public DateTime UpdateDate { get; set; }

    }
}
