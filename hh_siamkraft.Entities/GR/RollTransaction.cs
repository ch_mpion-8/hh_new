﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hh_siamkraft.Entities.GR
{
    public class RollTransaction
    {
        public string Batch_number { get; set; }
        public string Material_number { get; set; }
        public decimal Weight { get; set; }
        public string Container_Item { get; set; }
    }
}
