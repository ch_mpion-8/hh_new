﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hh_siamkraft.Entities.GR
{
    public class GROutsource
    {
        public decimal Record_ID { get; set; }
        public string PO_Number { get; set; }
        public string DocDate { get; set; }
        public string Set_Number { get; set; }
        public string Sup_Material { get; set; }
        public string Sup_Batch { get; set; }
        public decimal Qty { get; set; }
        public string Item_No { get; set; }
        public string Material { get; set; }
        public string Batch { get; set; }
        public string Plant { get; set; }
        public string Storage { get; set; }
        public string ProdDate { get; set; }
        public decimal Length { get; set; }
        public string Status { get; set; }
        public string CreateUser { get; set; }
        public DateTime CreateDateTime { get; set; }

    }
}
