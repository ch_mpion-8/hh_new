﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hh_siamkraft.Entities.GR
{
   public class POItem
    {
        public int Key_ID { get; set; }
        public int Record_ID { get; set; }
        public string PO_No { get; set; }
        public string Item_No { get; set; }
        public string Material_No { get; set; }
        public string Material_Desc { get; set; }
        public string Batch { get; set; }
        public decimal Qty { get; set; }
        public string Storage_Loc { get; set; }
        public string Plant { get; set; }
        public string Delivery_Completed { get; set; }
    }
}
