﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hh_siamkraft.Entities.GR
{
    public class SapGr
    {
        public SapGr()
        {
        }
        public SapGr(string batch, decimal qty, string material)
        {
            Batch = batch;
            Qty = qty;
            Material = material;
        }
        public string Batch { get; set; }
        public decimal Qty { get; set; }
        public string Material { get; set; }
    }
}
