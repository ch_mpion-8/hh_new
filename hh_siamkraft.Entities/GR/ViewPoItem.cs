﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hh_siamkraft.Entities.GR
{
    public class ViewPoItem
    {
        public decimal Record_ID { get; set; }
        public string PO_Number { get; set; }
        public decimal Qty { get; set; }
        public string Item_No { get; set; }
        public string Material { get; set; }

    }
}
