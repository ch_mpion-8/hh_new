﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hh_siamkraft.Entities.GR
{
    public class GRProd
    {
        public int GR_ID { get; set; }
        public string Batch { get; set; }
        public decimal Qty { get; set; }
        public string Material { get; set; }
        public int Container_No { get; set; }
        public string Status { get; set; }
        public DateTime CreateDateTime { get; set; }
        public string CreateUser { get; set; }
        public decimal Internal_ID { get; set; }
        public string UOM { get; set; }
        public int WH_No { get; set; }
        public string WH_Code { get; set; }
        public string ProductType { get; set; }
        public string Return_Msg { get; set; }
    }
}
