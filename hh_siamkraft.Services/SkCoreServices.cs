﻿using hh_siamkraft.Dals.Logging;
using hh_siamkraft.Infrastructure.Ioc;
using hh_siamkraft.Infrastructure.Logging;

namespace hh_siamkraft.Services
{
    public static class SkCoreServices
    {
        public static void Initialize()
        {
            // Register Logger
            SkServiceLocator.Register<SkLoggerBase>(new SkDbLogger());
        }
    }
}
