﻿using hh_siamkraft.Dals.Extensions;
using hh_siamkraft.Dals.Helper;
using hh_siamkraft.Entities.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hh_siamkraft.Dals.Repositories
{
    public class CommonRepository : RepositoryBase
    {
        public CommonRepository(SkRepositorySession session) : base(session)
        {
        }

        public DataAccessResult<IList<ServerDetail>> GetUserServer(string username)
        {
            return ExecuteDataAccess<IList<ServerDetail>>(response =>
            {

                var param = new List<SqlParameter>{
                    AdoHelper.CreateSqlParameter("@username",username,dbType:SqlDbType.NVarChar, size: 50),
                };
                
                var query = new StringBuilder();
                query.Append(" SELECT a.[ServerID], b.[Description], b.[ConnectString], a.[Server_Main]");
                query.Append(" FROM [dbo].[PC_Server_Detail] a ");
                query.Append(" inner join[dbo].[tbl_server] b on a.[ServerID] = b.[Server_ID]");
                query.Append(" WHERE upper(a.[User_ID]) = upper(@username)");
                query.Append(" order by b.[Description]");

                var result = new List<ServerDetail>();
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query.ToString(), ConvertToServer2, param.ToArray());
                    result = returnData.ToList();
                });

                taskResult.Wait();
                return result;
            });
        }

        public DataAccessResult<User> GetUser(string user, string pass, string hh, string flag)
        {
            return ExecuteDataAccess<User>(response =>
            {
                var result = new User();
                var param = new List<SqlParameter>
                {
                    AdoHelper.CreateSqlParameter("@strUserName", user, dbType: SqlDbType.NVarChar, size: 20),
                    AdoHelper.CreateSqlParameter("@strPassword", pass, dbType: SqlDbType.NVarChar, size: 20),
                    AdoHelper.CreateSqlParameter("@strHH", hh, dbType: SqlDbType.NVarChar,  size: 20),
                    AdoHelper.CreateSqlParameter("@flag", flag, dbType: SqlDbType.NVarChar,  size: 1),
                };

                var taskResult = Task.Run(async () =>
                {
                    var setters = new List<Func<SqlDataReader, Task<object>>> { ConvertToUserBase, ConvertToServer, ConvertToWarehouse, ConvertToHandheld
                                , async (d) => { return await d.GetValueAsync<string>("ConfigValue"); }, ConvertToUserPermission, ConvertToWarehouse };
                    var returnData = await ExecuteStoredProcedureAsync("dbo.PC_GetUser", param, setters);
                    result.UserDetail = returnData != null ? returnData[0].Cast<UserBase>().FirstOrDefault() : new UserBase();
                    result.ServerDetail = returnData != null ? returnData[1].Cast<ServerDetail>().ToList() : new List<ServerDetail>();
                    //result.WarehouseDetail = returnData != null ? returnData[2].Cast<WarehouseDetail>().ToList() : new List<WarehouseDetail>();
                    var xx = returnData != null ? returnData[2].Cast<WarehouseDetail>().ToList() : new List<WarehouseDetail>();
                    result.Handheld = returnData != null ? returnData[3].Cast<Handheld>().ToList() : new List<Handheld>();
                    result.ConfigValue = returnData != null ? returnData[4].Cast<string>().ToList() : new List<string>();
                    result.Permission = returnData != null ? returnData[5].Cast<UserPermission>().ToList() : new List<UserPermission>();
                    result.WarehouseDetail = returnData != null ? returnData[6].Cast<WarehouseDetail>().ToList() : new List<WarehouseDetail>();
                });

                taskResult.Wait();
                return result;
            });

        }

        
        public DataAccessResult<IList<WarehouseDetail>> GetUserWH(string username, string ServerID)
        {
            return ExecuteDataAccess<IList<WarehouseDetail>>(response =>
            {

                var param = new List<SqlParameter>{
                    AdoHelper.CreateSqlParameter("@username",username,dbType:SqlDbType.NVarChar, size: 50),
                    AdoHelper.CreateSqlParameter("@Server_ID",ServerID,dbType:SqlDbType.NVarChar, size: 50),

                };

                var query = new StringBuilder();
                query.Append(" SELECT b.[WH_No], b.[WHCode], b.[Location], b.[WH_Description], b.[WH_Description2], b.[ShippingPoint]");
                query.Append(" FROM [dbo].[PC_Warehouse_Detail] a ");
                query.Append(" inner join [dbo].[HH_WH] b on a.[WH_No] = b.[WH_No] ");
                query.Append(" WHERE upper(a.[User_ID]) = upper(@username) ");
                query.Append(" and b.[Server_ID] = @Server_ID ");
                query.Append(" order by b.[WH_No]");

                var result = new List<WarehouseDetail>();
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query.ToString(), ConvertToWarehouse2, param.ToArray());
                    result = returnData.ToList();
                });

                taskResult.Wait();
                return result;
            });
        }
        public DataAccessResult<WarehouseDetail>  GetWarehouseDetail(int whNo)
        {
            return ExecuteDataAccess<WarehouseDetail>(response =>
            {
                var result = new WarehouseDetail();
                var param = new List<SqlParameter>
                {
                    AdoHelper.CreateSqlParameter("@whNo", whNo),
                };
                var query = " SELECT WH_No, WHCode, Location, WH_Description, WH_Description2, ShippingPoint FROM " + ModuleCommon.HH_WH + " WHERE (WH_No = @whNo)";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, ConvertToWarehouse2, param.ToArray());
                    result = returnData.FirstOrDefault();
                });

                taskResult.Wait();
                return result;
            });
        }

        public DataAccessResult<string>  GetPLSConnectionstring(string pm)
        {
            return ExecuteDataAccess<string>(response =>
            {
                var result =  string.Empty;
                var param = new List<SqlParameter>
                {
                    AdoHelper.CreateSqlParameter("@pm", pm, dbType: SqlDbType.NVarChar,
                        size: 100),
                };
                var query = " SELECT ConnectString FROM dbo.tbl_first_batch b inner join dbo.tbl_server s on b.Server_ID = s.Server_ID where (@pm  is null or b.First_Batch = @pm ) ";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, async (d) => { return await d.GetValueAsync<string>("ConnectString"); }, param.ToArray());
                    result = returnData.FirstOrDefault();
                });

                taskResult.Wait();
                return result;
            });
            
        }

        public DataAccessResult<IList<ServerDetail>> GetAllPLSConnectionstring()
        {
            return ExecuteDataAccess<IList<ServerDetail>>(response =>
            {
                var result = new List<ServerDetail>();
                var query = " SELECT b.Server_ID,ConnectString,First_Batch ,[Description]  FROM dbo.tbl_first_batch b inner join dbo.tbl_server s on b.Server_ID = s.Server_ID  ";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, async (d) => {
                        return new ServerDetail
                        {
                            ServerID = await d.GetValueAsync<int>("Server_ID"),
                            ConnectString = await d.GetValueAsync<string>("ConnectString"),
                            Description = await d.GetValueAsync<string>("Description"),
                            Pm = await d.GetValueAsync<string>("First_Batch"),
                        };
                    }, null);
                    result = returnData.ToList();
                });

                taskResult.Wait();
                return result;
            });
        }

        public DataAccessResult<ServerDetail>  GetServer(int ServerID)
        {
            return ExecuteDataAccess<ServerDetail>(response =>
            {
                var result = new ServerDetail();
                var param = new List<SqlParameter>
                {
                    AdoHelper.CreateSqlParameter("@ServerID", ServerID, dbType: SqlDbType.Int),
                };
                var query = " SELECT b.Server_ID, b.ConnectString , b.[Description]  FROM dbo.tbl_server b Where b.Server_ID = @ServerID and is_active = 1 order by b.Server_ID";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, async (d) => {
                        return new ServerDetail {
                            ServerID = await d.GetValueAsync<int>("Server_ID"),
                            ConnectString = await d.GetValueAsync<string>("ConnectString") ,
                            Description = await d.GetValueAsync<string>("Description"),
                        }; }, param.ToArray());
                    result = returnData.FirstOrDefault();
                });

                taskResult.Wait();
                return result;
            });
        }

        public DataAccessResult<IList<string>>  GetPMByWHCode(string whCode)
        {
            return ExecuteDataAccess<IList<string>>(response =>
            {
                var result = new List<string>();
                var param = new List<SqlParameter>
                {
                    AdoHelper.CreateSqlParameter("@whCode", whCode, dbType: SqlDbType.NVarChar,
                        size: 100),
                };
                var query = " Select PM_Number From " + ModuleCommon.hh_pm + " Where WHCode = @whCode ";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, async (d) => { return await d.GetValueAsync<string>("PM_Number"); }, param.ToArray());
                    result = returnData.ToList();
                });

                taskResult.Wait();
                return result;
            });
            
        }

        public DataAccessResult<IList<UserFunctionDetail>>  GetAuthorize(string user, string platform)
        {
            return ExecuteDataAccess<IList<UserFunctionDetail>>(response =>
            {
                var result = new List<UserFunctionDetail>();
                var param = new List<SqlParameter>
                {
                    AdoHelper.CreateSqlParameter("@userId", user, dbType: SqlDbType.NVarChar,
                        size: 100),
                    AdoHelper.CreateSqlParameter("@platform", platform, dbType: SqlDbType.NVarChar,
                        size: 100),
                };
                var query = " SELECT User_ID,Function_Name,Platform FROM PC_Siamkraft_Work_Detail WHERE  User_ID = @userId and Platform = @platform";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, ConvertToUserPermission2, param.ToArray());
                    result = returnData.ToList();
                });

                taskResult.Wait();
                return result;
            });
            
        }

        public DataAccessResult<int>  GetTimeoutForSap(string processType)
        {
            return ExecuteDataAccess<int>(response =>
            {
                var result = 0;
                var param = new List<SqlParameter>
                {
                    AdoHelper.CreateSqlParameter("@ProcessType", processType, dbType: SqlDbType.NVarChar,
                        size: 100),
                };
                var query = " SELECT Time_Out FROM HH_TimeOut WHERE (Process_Type = @ProcessType) ";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, async (d) => { return await d.GetValueAsync<int>("Time_Out"); }, param.ToArray());
                    result = returnData.FirstOrDefault();
                });

                taskResult.Wait();
                return result;
            });
        }


        public DataAccessResult<PlsControl>  GetPlsControl(int recordId)
        {

            return ExecuteDataAccess<PlsControl>(response =>
            {
                var result = new PlsControl();
                var param = new List<SqlParameter>
                {
                    AdoHelper.CreateSqlParameter("@Record_ID", recordId),
                };
                var query = " SELECT Record_ID,Prev_Process,HH_ID,Process_ID,Return_Code,Return_Msg,User_Name,Create_Date_Time,Complete_Date_Time,Flag_Del,Call_Date_Time FROM Control WHERE (Record_ID = @Record_ID)";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, ConvertToPlsControl, param.ToArray());
                    result = returnData.FirstOrDefault();
                });

                taskResult.Wait();
                return result;
            });
        }

        private async Task<object> ConvertToUserBase(SqlDataReader data)
        {
            return new UserBase
            {
                User_ID = await data.GetValueAsync<string>("User_ID"),
                User_Password = await data.GetValueAsync<string>("User_Password"),
                User_Name = await data.GetValueAsync<string>("User_Name"),
                User_Lastname = await data.GetValueAsync<string>("User_Lastname"),
                User_Position = await data.GetValueAsync<string>("User_Position"),
                User_Sex = await data.GetValueAsync<int>("User_Sex"),
                User_Status = await data.GetValueAsync<int>("User_Status"),
                User_Company = await data.GetValueAsync<string>("User_Company"),
                User_Wh = await data.GetValueAsync<int>("User_Wh"),
            };
        }
        private async Task<object> ConvertToServer(SqlDataReader datas)
        {
            var data = datas;
            return new ServerDetail
            {
                ServerID = await data.GetValueAsync<int>("ServerID"),
                Description = await data.GetValueAsync<string>("Description"),
                ConnectString = await data.GetValueAsync<string>("ConnectString"),
                ServerMain = await data.GetValueAsync<bool?>("Server_Main") ?? false,
            };
        }

        private async Task<ServerDetail> ConvertToServer2(SqlDataReader datas)
        {
            var data = datas;
            return new ServerDetail
            {
                ServerID = await data.GetValueAsync<int>("ServerID"),
                Description = await data.GetValueAsync<string>("Description"),
                ConnectString = await data.GetValueAsync<string>("ConnectString"),
                ServerMain = await data.GetValueAsync<bool?>("Server_Main") ?? false,
            };
        }
        private async Task<object> ConvertToWarehouse(SqlDataReader datas)
        {
            var data = datas;
            return new WarehouseDetail
            {
                WH_No = await data.GetValueAsync<int>("WH_No"),
                WHCode = await data.GetValueAsync<string>("WHCode"),
                Location = await data.GetValueAsync<string>("Location"),
                WH_Description = await data.GetValueAsync<string>("WH_Description"),
                WH_Description2 = await data.GetValueAsync<string>("WH_Description2"),
                ShippingPoint = await data.GetValueAsync<string>("ShippingPoint"),
            };
        }
        private async Task<WarehouseDetail> ConvertToWarehouse2(SqlDataReader datas)
        {
            var data = datas;
            return new WarehouseDetail
            {
                WH_No = await data.GetValueAsync<int>("WH_No"),
                WHCode = await data.GetValueAsync<string>("WHCode"),
                Location = await data.GetValueAsync<string>("Location"),
                WH_Description = await data.GetValueAsync<string>("WH_Description"),
                WH_Description2 = await data.GetValueAsync<string>("WH_Description2"),
                ShippingPoint = await data.GetValueAsync<string>("ShippingPoint"),
            };
        }
        private async Task<object> ConvertToHandheld(SqlDataReader data)
        {
            return new Handheld
            {
                HH_ID = await data.GetValueAsync<string>("HH_ID"),
                Information = await data.GetValueAsync<string>("Information"),
                IP_Address = await data.GetValueAsync<string>("IP_Address"),
                Plant = await data.GetValueAsync<string>("Plant"),
            };
        }
        private async Task<object> ConvertToUserPermission(SqlDataReader data)
        {
            return new UserPermission
            {
                Function_Name = await data.GetValueAsync<string>("Function_Name"),
                Platform = await data.GetValueAsync<string>("Platform"),
                User_ID = await data.GetValueAsync<string>("User_ID"),
            };
        }

        //private async Task<UserPermission> ConvertToUserPermission2(SqlDataReader data)
        //{
        //    return new UserPermission
        //    {
        //        Function_Name = await data.GetValueAsync<string>("Function_Name"),
        //        Platform = await data.GetValueAsync<string>("Platform"),
        //        User_ID = await data.GetValueAsync<string>("User_ID"),
        //    };
        //}

        private async Task<UserFunctionDetail> ConvertToUserPermission2(SqlDataReader data)
        {
            return new UserFunctionDetail
            {
                User_ID = await data.GetValueAsync<string>("User_ID"),
                Function_Name = await data.GetValueAsync<string>("Function_Name"),
                Platform = await data.GetValueAsync<string>("Platform"),
            };
        }

        private async Task<PlsControl>  ConvertToPlsControl(SqlDataReader data)
        {
            return new PlsControl
            {
                Record_ID = await data.GetValueAsync<int>("Record_ID"),
                Prev_Process = await data.GetValueAsync<int?>("Prev_Process") ,
                HH_ID = await data.GetValueAsync<string>("HH_ID"),
                Process_ID = await data.GetValueAsync<string>("Process_ID"),
                Return_Code = await data.GetValueAsync<string>("Return_Code") ,
                Return_Msg = await data.GetValueAsync<string>("Return_Msg") ,
                User_Name = await data.GetValueAsync<string>("User_Name") ,
                Create_Date_Time = await data.GetValueAsync<DateTime?>("Create_Date_Time") ,
                Complete_Date_Time = await data.GetValueAsync<DateTime?>("Complete_Date_Time")  ,
                Flag_Del = await data.GetValueAsync<string>("Flag_Del") ,
                Call_Date_Time = await data.GetValueAsync<DateTime?>("Call_Date_Time") ,
            };
        }
    }
}
