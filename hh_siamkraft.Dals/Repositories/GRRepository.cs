﻿using hh_siamkraft.Dals.Extensions;
using hh_siamkraft.Dals.Helper;
using hh_siamkraft.Entities.Common;
using hh_siamkraft.Entities.GR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hh_siamkraft.Dals.Repositories
{
    public class GRRepository : RepositoryBase
    {
        public GRRepository(SkRepositorySession session) : base(session)
        {

        }


        
        public DataAccessResult<bool> GetGRID(string GRID)
        {
            return ExecuteDataAccess<bool>(response =>
            {
                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@GRID", GRID, dbType: SqlDbType.NVarChar,
                        size: 100),
                    };

                var result = 0;
                var query = "select count(1) as cnt from " + ModuleCommon.gr_prod + " Where GR_ID = @GRID and (Status <> 'C' or Status is null) and datediff([day],CreateDateTime,getdate()) = 0";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, async (d) => { return await d.GetValueAsync<int>("cnt"); }, param.ToArray());
                    result = returnData.FirstOrDefault();
                });

                taskResult.Wait();

                var result2 = false;
                if (result > 0)
                {
                    result2 = true;
                }
                return result2;
            });

        }

        public DataAccessResult<int> GetNewGRID()
        {
            return ExecuteDataAccess<int>(response =>
            {
                var GR_ID = 0;

                var query = "select Top 1 * from " + ModuleCommon.gr_running + " Where datediff([day],GRdatetime,getdate()) = 0 order by GR_ID DESC";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, async (d) => { return await d.GetValueAsync<int>("GR_ID"); }, null);
                    GR_ID = returnData.FirstOrDefault();
                });

                taskResult.Wait();

                if (GR_ID > 0)
                {
                    GR_ID++;
                    var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@GR_ID", GR_ID , dbType: SqlDbType.Int),
                    };

                    var query2 = "Update " + ModuleCommon.gr_running + " SET GR_ID = @GR_ID Where datediff([day],GRdatetime,getdate()) = 0 ";
                    var taskResult2 = Task.Run(async () =>
                    {
                        await ExecuteNonQueryAsync(query2, param);
                    });
                    taskResult2.Wait();
                }
                else
                {
                    GR_ID++;
                    var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@GR_ID", GR_ID , dbType: SqlDbType.Int),
                    };

                    var query2 = "Insert  " + ModuleCommon.gr_running + "(GRdatetime, GR_ID) Values (DATEADD(dd, 0, DATEDIFF(dd, 0, getdate())) ,  @GR_ID) ";
                    var taskResult2 = Task.Run(async () =>
                    {
                        await ExecuteNonQueryAsync(query2, param);
                    });
                    taskResult2.Wait();
                }
                return GR_ID;
            });

        }

        public DataAccessResult<int> GetRunningGRID()
        {
            return ExecuteDataAccess<int>(response =>
            {
                var result = 0;
                var query = "Select * FROM " + ModuleCommon.gr_running + " WHERE (DATEDIFF(dd, GRDateTime, GETDATE()) = 0)";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, async (d) => { return await d.GetValueAsync<int>("GR_ID"); }, null);
                    result = returnData.FirstOrDefault();
                });
                taskResult.Wait();

                return result;
            });

        }

        public DataAccessResult<IList<GRProd>> GetGRProd(string GR_ID)
        {
            return ExecuteDataAccess<IList<GRProd>>(response =>
            {
                var result = new List<GRProd>();
                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@GR_ID", Convert.ToInt32(GR_ID) , dbType: SqlDbType.Int),
                    };
                var query = "Select GR_ID,Batch,Qty,Material From " + ModuleCommon.gr_prod + " Where GR_ID = @GR_ID And (DATEDIFF(dd, CreateDateTime, GETDATE()) = 0) ";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, ConvertToGRProd, param.ToArray());
                    result = returnData.ToList();
                });
                taskResult.Wait();

                return result;
            });

        }

        public DataAccessResult<RollTransaction> GetBatchDetail(string Batch)
        {
            return ExecuteDataAccess<RollTransaction>(response =>
            {
                var result = new RollTransaction();
                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@Batch", Batch , dbType: SqlDbType.VarChar),
                    };
                var query = "Select [Batch_number], [Material_number], [Weight], [Container_Item] From " + ModuleCommon.tbl_roll_transaction + " Where [Batch_number] = @Batch";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, ConvertToRollTransaction, param.ToArray());
                    result = returnData.FirstOrDefault();
                });
                taskResult.Wait();

                return result;
            });

        }

        public DataAccessResult<bool> CheckDupBatch(string Batch, string GR_ID)
        {
            return ExecuteDataAccess<bool>(response =>
            {
                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@Batch", Batch, dbType: SqlDbType.NVarChar,size: 100),
                         AdoHelper.CreateSqlParameter("@GR_ID", GR_ID, dbType: SqlDbType.NVarChar,size: 100),
                    };

                var result = 0;
                var query = "Select count(Batch) as cnt from " + ModuleCommon.gr_prod + " Where (Batch = @Batch) AND (Status IS NULL OR Status <> 'C')and (GR_ID = @GR_ID)";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, async (d) => { return await d.GetValueAsync<int>("cnt"); }, param.ToArray());
                    result = returnData.FirstOrDefault();
                });

                taskResult.Wait();

                var result2 = false;
                if (result > 0)
                {
                    result2 = true;
                }
                return result2;
            });

        }

        public DataAccessResult<IList<GRProd>> GetBatchDup(string Batch)
        {
            return ExecuteDataAccess<IList<GRProd>>(response =>
            {
                var result = new List<GRProd>();
                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@Batch", Batch , dbType: SqlDbType.NVarChar, size: 100),
                    };
                //var query = "Select Batch,GR_ID, Return_Msg from " + ModuleCommon.gr_prod + " Where (Batch = @Batch) AND Status = 'C' ";
                var query = "Select Batch,GR_ID, Return_Msg from " + ModuleCommon.gr_prod + " Where (Batch = @Batch) AND(datediff(day, CreateDatetime, getdate()) = 0) AND Status = 'C'";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, ConvertToGRProd, param.ToArray());
                    result = returnData.ToList();
                });
                taskResult.Wait();

                return result;
            });

        }

        public DataAccessResult<bool> InsertGRProd(string GR_ID, string Batch, string Qty, string Material, int? Container_No, string WHID, string WH_Code, string ProductType, string Storage, string strUser)
        {
            return ExecuteDataAccess<bool>(response =>
            {
                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@GR_ID",Convert.ToInt32( GR_ID), dbType: SqlDbType.Int),
                         AdoHelper.CreateSqlParameter("@Batch", Batch, dbType: SqlDbType.NVarChar, size: 100),
                         AdoHelper.CreateSqlParameter("@Qty",Convert.ToDecimal(Qty), dbType: SqlDbType.Decimal),
                         AdoHelper.CreateSqlParameter("@Material", Material, dbType: SqlDbType.NVarChar, size: 100),
                         AdoHelper.CreateSqlParameter("@CreateUser", strUser, dbType: SqlDbType.NVarChar, size: 20),
                         AdoHelper.CreateSqlParameter("@Container_No", Container_No, dbType: SqlDbType.Int),
                         AdoHelper.CreateSqlParameter("@WHID", Convert.ToInt32( WHID), dbType: SqlDbType.Int),
                         AdoHelper.CreateSqlParameter("@WH_Code", WH_Code, dbType: SqlDbType.NVarChar, size: 100),
                         AdoHelper.CreateSqlParameter("@ProductType", ProductType, dbType: SqlDbType.NVarChar, size: 100),
                         AdoHelper.CreateSqlParameter("@Storage", Storage, dbType: SqlDbType.NVarChar, size: 100),
                    };


                var query = "Insert into " + ModuleCommon.gr_prod + " (GR_ID,Batch,Qty,Material,CreateDateTime,CreateUser,Container_No,WH_No,WH_Code,ProductType,Storage) ";
                query = query + " VALUES (@GR_ID, @Batch, @Qty, @Material, getdate(), @CreateUser, @Container_No, @WHID, @WH_Code, @ProductType, @Storage) ";
                var taskResult = Task.Run(async () =>
                {
                    await ExecuteNonQueryAsync(query, param.ToArray());
                });

                taskResult.Wait();
                return true;
            });
        }

        public DataAccessResult<bool> InsertHROutSource(int Record_ID, string PO_Number, string DocDate, string Set_Number,  string Sup_Material, string Sup_Batch, decimal Qty, string Item_No, string Material, string Batch, string Plant, string Storage, string ProdDate, decimal Length, string Status, string CreateUser )
        {
            return ExecuteDataAccess<bool>(response =>
            {
                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@Record_ID", Record_ID, dbType: SqlDbType.Int),
                         AdoHelper.CreateSqlParameter("@PO_Number", PO_Number, dbType: SqlDbType.NVarChar, size: 10),
                         AdoHelper.CreateSqlParameter("@DocDate", DocDate, dbType: SqlDbType.NVarChar, size: 10),
                         AdoHelper.CreateSqlParameter("@Set_Number", Set_Number, dbType: SqlDbType.NVarChar, size: 10),
                         AdoHelper.CreateSqlParameter("@Sup_Material", Sup_Material, dbType: SqlDbType.NVarChar, size: 50),
                         AdoHelper.CreateSqlParameter("@Sup_Batch", Sup_Batch, dbType: SqlDbType.NVarChar, size: 50),
                         AdoHelper.CreateSqlParameter("@Qty",Convert.ToDecimal(Qty), dbType: SqlDbType.Decimal),
                         AdoHelper.CreateSqlParameter("@Item_No", Item_No, dbType: SqlDbType.NVarChar, size: 6),
                         AdoHelper.CreateSqlParameter("@Material", Material, dbType: SqlDbType.NVarChar, size: 18),
                         AdoHelper.CreateSqlParameter("@Batch", Batch, dbType: SqlDbType.NVarChar, size: 10),
                         AdoHelper.CreateSqlParameter("@Plant", Plant, dbType: SqlDbType.NVarChar, size: 4),
                         AdoHelper.CreateSqlParameter("@Storage", Storage, dbType: SqlDbType.NVarChar, size: 4),
                         AdoHelper.CreateSqlParameter("@ProdDate", ProdDate, dbType: SqlDbType.NVarChar, size: 50),
                         AdoHelper.CreateSqlParameter("@Length", Length, dbType: SqlDbType.Decimal),
                         AdoHelper.CreateSqlParameter("@Status", Status, dbType: SqlDbType.NVarChar, size: 2),
                         AdoHelper.CreateSqlParameter("@CreateUser", CreateUser, dbType: SqlDbType.NVarChar, size: 20),
                         
                    };

                var query = "Insert into " + ModuleCommon.tbl_outsource + " (Record_ID,PO_Number,DocDate,Set_Number,Sup_Material,Sup_Batch,Qty,Item_No,Material,Batch,Plant,Storage,ProdDate,Length,Status,CreateUser) ";
                query = query + " VALUES (@Record_ID, @PO_Number, @DocDate, @Set_Number, @Sup_Material, @Sup_Batch, @Qty, @Item_No, @Material, @Batch, @Plant, @Storage, @ProdDate, @Length, @Status, @CreateUser) ";
                var taskResult = Task.Run(async () =>
                {
                    await ExecuteNonQueryAsync(query, param.ToArray());
                });

                taskResult.Wait();
                return true;
            });
        }


        public DataAccessResult<bool> DeleteGRProd(string batchNo, string GR_ID)
        {
            return ExecuteDataAccess<bool>(response =>
            {
                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@Batch", batchNo, dbType: SqlDbType.NVarChar,size: 100),
                         AdoHelper.CreateSqlParameter("@GR_ID", GR_ID, dbType: SqlDbType.NVarChar,size: 100),
                    };

                var query = "DELETE From " + ModuleCommon.gr_prod + " Where(Batch = @Batch) AND (Status IS NULL OR Status <> 'C') and (GR_ID = @GR_ID)";
                var taskResult = Task.Run(async () =>
                {
                    await ExecuteNonQueryAsync(query, param.ToArray());
                });

                taskResult.Wait();
                return true;
            });

        }

        public DataAccessResult<bool> DeleteOutSourceAll(string PO_Number)
        {
            return ExecuteDataAccess<bool>(response =>
            {
                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@PO_Number", PO_Number, dbType: SqlDbType.NVarChar,size: 100),
                    };

                var query = "DELETE From " + ModuleCommon.tbl_outsource + "PO_Number = @PO_Number and Status ='N'; DELETE From " + ModuleCommon.tbl_outsource_log + "PO_Number = @PO_Number and Status = 'N'";
                var taskResult = Task.Run(async () =>
                {
                    await ExecuteNonQueryAsync(query, param);
                });

                taskResult.Wait();
                return true;
            });

        }

        public DataAccessResult<bool> UpdateSapGrMovementType(string batchNo)
        {
            return ExecuteDataAccess<bool>(response =>
            {
                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@Batch", batchNo, dbType: SqlDbType.NVarChar,
                        size: 100),
                    };

                var query = "Update " + ModuleCommon.sap_gr + " SET MovementType = '521' Where Batch = @Batch";
                var taskResult = Task.Run(async () =>
                {
                    await ExecuteNonQueryAsync(query, param);
                });

                taskResult.Wait();
                return true;
            });

        }

        public DataAccessResult<bool> UpdateGrProductStatus(string batchNo)
        {
            return ExecuteDataAccess<bool>(response =>
            {
                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@Batch", batchNo, dbType: SqlDbType.NVarChar,
                        size: 100),
                    };

                var query = "Update " + ModuleCommon.gr_prod + " SET Status = 'C' Where Batch = @Batch";
                var taskResult = Task.Run(async () =>
                {
                    await ExecuteNonQueryAsync(query, param);
                });

                taskResult.Wait();
                return true;
            });

        }

        public DataAccessResult<int> CountPO(string PO_Number, string Material, string Batch, decimal? Qty)
        {
            return ExecuteDataAccess<int>(response =>
            {
                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@PO_Number", PO_Number, dbType: SqlDbType.NVarChar,size: 10),
                         AdoHelper.CreateSqlParameter("@Material", Material, dbType: SqlDbType.NVarChar,size: 20),
                         AdoHelper.CreateSqlParameter("@Batch", Batch, dbType: SqlDbType.NVarChar,size: 15),
                         AdoHelper.CreateSqlParameter("@Qty", Qty , dbType: SqlDbType.Decimal),
                    };

                var result = 0;
                var query = "Select COUNT(Batch) AS Batch_Sup from " + ModuleCommon.tbl_po_fromtext + " Where (PO_Number = @PO_Number) and (Material = @Material or @Material = '%') " +
                            " and (Batch = @Batch or @Batch = '%') and (Qty = @Qty or @Qty = null)";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, async (d) => { return await d.GetValueAsync<int>("Batch_Sup"); }, param.ToArray());
                    result = returnData.FirstOrDefault();
                });

                taskResult.Wait();

                return result;
            });

        }

     

        public DataAccessResult<int> CountOutsource(string PO_Number)
        {
            return ExecuteDataAccess<int>(response =>
            {
                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@PO_Number", PO_Number, dbType: SqlDbType.NVarChar,size: 100),
                    };

                var result = 0;
                var query = "Select COUNT(Batch) AS Batch_Rec from " + ModuleCommon.tbl_outsource + " Where (PO_Number = @PO_Number) AND Status='C'";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, async (d) => { return await d.GetValueAsync<int>("Batch_Rec"); }, param.ToArray());
                    result = returnData.FirstOrDefault();
                });

                taskResult.Wait();

                return result;
            });

        }

        public DataAccessResult<IList<GRProd>> GetHROutSource(string PO_Number, string Status, string Sup_Material , string Sup_Batch, decimal? Qty)
        {
            return ExecuteDataAccess<IList<GRProd>>(response =>
            {
                var result = new List<GRProd>();
                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@PO_Number", PO_Number , dbType: SqlDbType.NVarChar, size: 10),
                         AdoHelper.CreateSqlParameter("@Status", Status , dbType: SqlDbType.NVarChar, size: 2),
                         AdoHelper.CreateSqlParameter("@Sup_Material", Sup_Material , dbType: SqlDbType.NVarChar, size: 50),
                         AdoHelper.CreateSqlParameter("@Sup_Batch", Sup_Batch , dbType: SqlDbType.NVarChar, size: 50),
                         AdoHelper.CreateSqlParameter("@Qty", Qty , dbType: SqlDbType.Decimal),
                    };
                var query = "Select *, '' as Return_Msg from " + ModuleCommon.tbl_outsource + " Where (PO_Number = @PO_Number) AND (Status = @Status OR @Status ='%')" +
                            " AND (Sup_Material= @Sup_Material or @Sup_Material = '%') AND (Sup_Batch = @Sup_Batch or @Sup_Batch = '%') AND (Qty = @Qty or @Qty = null) ";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, ConvertToGRProd, param.ToArray());
                    result = returnData.ToList();
                });
                taskResult.Wait();

                return result;
            });

        }

        public DataAccessResult<IList<POFromtxt>> GetPOFromtxt(string PO_Number, string Material, string Batch, decimal? Qty)
        {
            return ExecuteDataAccess<IList<POFromtxt>>(response =>
            {
                var result = new List<POFromtxt>();
                var param = new List<SqlParameter>
                    {
                        AdoHelper.CreateSqlParameter("@PO_Number", PO_Number, dbType: SqlDbType.NVarChar,size: 10),
                         AdoHelper.CreateSqlParameter("@Material", Material, dbType: SqlDbType.NVarChar,size: 20),
                         AdoHelper.CreateSqlParameter("@Batch", Batch, dbType: SqlDbType.NVarChar,size: 15),
                         AdoHelper.CreateSqlParameter("@Qty", Qty , dbType: SqlDbType.Decimal),
                    };


                var query = "Select * from " + ModuleCommon.tbl_po_fromtext + " Where (PO_Number = @PO_Number) and (Material = @Material or @Material = '%') " +
                            " and (Batch = @Batch or @Batch = '%') and (Qty = @Qty or @Qty = null)";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, ConvertToPOFromtxt, param.ToArray());
                    result = returnData.ToList();
                });
                taskResult.Wait();

                return result;
            });

        }

        public DataAccessResult<IList<POItem>> GetPOItem(int Record_ID, string PO_No, string Material_No)
        {
            return ExecuteDataAccess<IList<POItem>>(response =>
            {
                var result = new List<POItem>();
                var param = new List<SqlParameter>
                    {
                        AdoHelper.CreateSqlParameter("@Record_ID", Record_ID, dbType: SqlDbType.Int),
                         AdoHelper.CreateSqlParameter("@PO_No", PO_No, dbType: SqlDbType.NVarChar,size: 10),
                         AdoHelper.CreateSqlParameter("@Material_No", Material_No, dbType: SqlDbType.NVarChar,size: 18),
                    };

                var query = "Select * from " + ModuleCommon.tbl_po_item + " Where (Record_ID = @Record_ID) and (PO_No = @PO_No or @PO_No = '%') " +
                            " and (Material_No = @Material_No or Material_No = '%') ";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, ConvertToPOItem, param.ToArray());
                    result = returnData.ToList();
                });
                taskResult.Wait();

                return result;
            });

        }

        public DataAccessResult<IList<ViewPoItem>> GetViewPOItem(int Record_ID, string PO_No, string Material, string Item_No)
        {
            return ExecuteDataAccess<IList<ViewPoItem>>(response =>
            {
                var result = new List<ViewPoItem>();
                var param = new List<SqlParameter>
                    {
                        AdoHelper.CreateSqlParameter("@Record_ID", Record_ID, dbType: SqlDbType.Int),
                         AdoHelper.CreateSqlParameter("@PO_No", PO_No, dbType: SqlDbType.NVarChar,size: 10),
                         AdoHelper.CreateSqlParameter("@Material", Material, dbType: SqlDbType.NVarChar,size: 18),
                         AdoHelper.CreateSqlParameter("@Item_No", Item_No, dbType: SqlDbType.NVarChar,size: 6),
                    };

                var query = "Select * from " + ModuleCommon.view_po_item + " Where (Record_ID = @Record_ID) and (PO_No = @PO_No or @PO_No = '%') " +
                            " and (Material = @Material or Material_No = '%') AND (Item_No =@Item_No or @Item_No = '%') order by Item_No asc";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, ConvertToViewPoItem, param.ToArray());
                    result = returnData.ToList();
                });
                taskResult.Wait();

                return result;
            });

        }

        public DataAccessResult<int> GetTimeOut()
        {
            return ExecuteDataAccess<int>(response =>
            {

                var result = 0;
                var query = "Select timeOut from " + ModuleCommon.tbl_timeout + " Where Process_Type='GR_Outsource'";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, async (d) => { return await d.GetValueAsync<int>("timeOut"); });
                    result = returnData.FirstOrDefault();
                });

                taskResult.Wait();

                return result;
            });

        }


        public DataAccessResult<int> GetCountPlant(string plant)
        {
            return ExecuteDataAccess<int>(response =>
            {
                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@Plant", plant , dbType: SqlDbType.NVarChar, size: 100),
                    };
                var result = 0;
                var query = "Select count(1) as cnt  from " + ModuleCommon.tbl_sap_plant + " Where Plant= @Plant";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, async (d) => { return await d.GetValueAsync<int>("cnt"); }, param.ToArray());
                    result = returnData.FirstOrDefault();
                });

                taskResult.Wait();

                return result;
            });

        }

    //Public Function check_WeightDiff(ByVal str_id As String, ByVal str_po As String) As DataTable
    //    Dim ds As DataSet

    //    Try
      
    //        sqlDataAdapter.Fill(ds, "tmp_material")
    //        Return ds.Tables("tmp_material")
    //    Catch ex As Exception
    //        Return Nothing
    //        Throw
    //        'script_func.script_alert("Check Weight On Error", Page)
    //    Finally
    //        'localConnect.Close()
    //    End Try
    //End Function

        //ยังไม่เสร็จ
        //return dataTable have weight diff over 10% from Stored Procedure
        public DataAccessResult<int> check_WeightDiff(int record_id, string po_no)
        {
            return ExecuteDataAccess<int>(response =>
            {
                var getID = AdoHelper.CreateSqlParameter("@getID", 0, ParameterDirection.InputOutput, SqlDbType.Int);
                var param = new List<SqlParameter>
                {
                    AdoHelper.CreateSqlParameter("@record_id",record_id),
                    AdoHelper.CreateSqlParameter("@po_no",po_no),

                   // getID,
                };
                var taskResult = Task.Run(async () =>
                {
                    await ExecuteStoredProcedureAsync("dbo.GR_Outsource_WeightDiff", param.ToArray());
                });

                int result = getID.Value != null ? Convert.ToInt32(getID.Value) : 0;
                taskResult.Wait();
                return result;
            });
        }


        //Get Record_ID on Table Control
        public DataAccessResult<int> getID(string hh_id, string user_id, string po_no)
        {
            return ExecuteDataAccess<int>(response =>
            {
                var getID = AdoHelper.CreateSqlParameter("@getID", 0, ParameterDirection.InputOutput, SqlDbType.Int);
                var param = new List<SqlParameter>
                {
                    AdoHelper.CreateSqlParameter("@hh_id",hh_id),
                    AdoHelper.CreateSqlParameter("@user_id",user_id),
                    AdoHelper.CreateSqlParameter("@po_no",po_no),
                    getID,
                };
                var taskResult = Task.Run(async () =>
                {
                    await ExecuteStoredProcedureAsync("dbo.GR_Outsource_RecordID", param.ToArray());
                });

                int result = getID.Value != null ? Convert.ToInt32(getID.Value) : 0;
                taskResult.Wait();
                return result;
            });
        }

        public DataAccessResult<int> get_RecordID(int rec_id , string hh_id, string user_id, string po_no, sp_GroupType sp_GroupType)
        {
            return ExecuteDataAccess<int>(response =>
            {
                string strStore = "";
                if (sp_GroupType == sp_GroupType.all_Mat) strStore = "dbo.GR_Outsource_SendtoSAP;1";
                else if (sp_GroupType == sp_GroupType.some_Mat) strStore = "dbo.GR_Outsource_SendtoSAP; 2";
                else return 0;

                var result = 0;
                if (sp_GroupType == sp_GroupType.some_Mat)
                { 
                    var param = new List<SqlParameter>
                    {
                        AdoHelper.CreateSqlParameter("@record_id",rec_id),
                        AdoHelper.CreateSqlParameter("@po_number",po_no),
                    };

                    var taskResult = Task.Run(async () =>
                    {
                        var returnData = await SelectDbViewAsync(strStore, async (d) => { return await d.GetValueAsync<int>("Record_ID"); }, param.ToArray());
                        result = returnData.FirstOrDefault();
                    });

                }
                else
                { 
                    var param = new List<SqlParameter>
                    {
                        AdoHelper.CreateSqlParameter("@record_id",rec_id),
                        AdoHelper.CreateSqlParameter("@hh_id",hh_id),
                        AdoHelper.CreateSqlParameter("@user_id",user_id),
                        AdoHelper.CreateSqlParameter("@po_number",po_no),
                    };

                    var taskResult = Task.Run(async () =>
                    {
                        var returnData = await SelectDbViewAsync(strStore, async (d) => { return await d.GetValueAsync<int>("Record_ID"); }, param.ToArray());
                        result = returnData.FirstOrDefault();
                    });

                }

               
                return result;
            });
        }

        //ยังไม่เสร็จ
        //get Reel_No 
        public DataAccessResult<string> getReel_No(string PO_Number, string Set_Number)
        {
            return ExecuteDataAccess<string>(response =>
            {
                var result = "";
                //var param = new List<SqlParameter>
                //    {
                //         AdoHelper.CreateSqlParameter("@PO_Number", PO_Number, dbType: SqlDbType.NVarChar,size: 10),
                //         AdoHelper.CreateSqlParameter("@Set_Number", Set_Number, dbType: SqlDbType.NVarChar,size: 2),
                //    };

                //var query = "Select * from " + ModuleCommon.tbl_hh_reel_no + " Where (PO_Number = @PO_Number) and (Set_Number = @Set_Number) ";
                //var taskResult = Task.Run(async () =>
                //{
                //    var returnData = await SelectDbViewAsync(query, async (d) => { return await d.GetValueAsync<int>("Record_ID"); }, param.ToArray());
                //    result = returnData.ToList();
                //});
                //taskResult.Wait();
                return result;
            });
        }


        //Public Function Reel_No(ByVal tbl As String, ByVal po_no As String, ByVal set_no As String) As String
        //    Try
        //        Dim strSql As String = SqlFunction.GetInstance.String_Select(1, tbl, "PO_Number='" & po_no & "'" & _
        //                                                                    " AND Set_Number='" & set_no & "'", 0)
        //        Dim dataAdapter As SqlDataAdapter = New SqlDataAdapter(strSql, sqlConn.sqlConnect)
        //        Dim dataTable As New DataTable
        //        Dim dataRow As DataRow
        //        dataAdapter.Fill(dataTable)

        //        If(dataTable.Rows.Count > 0) Then
        //           dataRow = dataTable.Rows(0)
        //            dataRow.BeginEdit()
        //            If dataRow.Item(0) = Now.Date.ToString("MM") Then
        //                dataRow.Item(3) = dataRow.Item(3) + 1
        //            Else
        //                dataRow.Item(0) = Now.Date.ToString("MM")
        //                dataRow.Item(3) = 1
        //            End If
        //            dataRow.EndEdit()
        //        Else
        //            dataRow = dataTable.NewRow
        //            dataRow(0) = Now.Date.ToString("MM")
        //            dataRow(1) = po_no
        //            dataRow(2) = set_no
        //            dataRow(3) = 1
        //            dataTable.Rows.Add(dataRow)
        //        End If

        //        Dim commandBuilder As New SqlCommandBuilder(dataAdapter)
        //        dataAdapter.Update(dataTable)
        //        dataAdapter.Dispose()
        //        commandBuilder.Dispose()
        //        Return dataRow.Item(3).ToString()
        //    Catch ex As Exception
        //        Return 1000
        //    End Try

        //End Function

        public DataAccessResult<bool> CheckDupBatch(string batch_number)
        {
            return ExecuteDataAccess<bool>(response =>
            {
                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@batch_number", batch_number, dbType: SqlDbType.NVarChar,size: 10),
                    };

                var result = 0;
                var result2 = false;
                var query = "Select Count( 1) as cnt from " + ModuleCommon.view_gr_checkBatch + " Where (batch_number = @batch_number)";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, async (d) => { return await d.GetValueAsync<int>("cnt"); }, param.ToArray());
                    result = returnData.FirstOrDefault();
                });

                taskResult.Wait();

                if (result > 0)
                    result2 = false;
                else
                    result2 = true;

                return result2;
            });
        }

        public DataAccessResult<IList<TblControl>> GetTblControl(int Record_ID, string HH_ID)
        {
            return ExecuteDataAccess<IList<TblControl>>(response =>
            {
                var result = new List<TblControl>();
                var param = new List<SqlParameter>
                            {
                                 AdoHelper.CreateSqlParameter("@Record_ID", Record_ID, dbType: SqlDbType.Int),
                                 AdoHelper.CreateSqlParameter("@HH_ID", HH_ID , dbType: SqlDbType.NVarChar, size: 2),
                            };
                var query = "Select * From " + ModuleCommon.tbl_control + " Where Record_ID = @Record_ID and HH_ID = @HH_ID AND  Process_ID='GetPO' and  Return_Code <> 'N'  ";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, ConvertToTblControl, param.ToArray());
                    result = returnData.ToList();
                });
                taskResult.Wait();

                return result;
            });

        }

        public DataAccessResult<IList<ViewGetItem>> GetViewItem(int Record_ID)
        {
            return ExecuteDataAccess<IList<ViewGetItem>>(response =>
            {
                var result = new List<ViewGetItem>();
                var param = new List<SqlParameter>
                            {
                                 AdoHelper.CreateSqlParameter("@Record_ID", Record_ID, dbType: SqlDbType.Int),
                            };
                var query = "Select * From " + ModuleCommon.view_getItem + " Where Record_ID = @Record_ID and Plant is not null and VendorCode is not null ";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, ConvertToViewGetItem, param.ToArray());
                    result = returnData.ToList();
                });
                taskResult.Wait();

                return result;
            });

        }
        public DataAccessResult<IList<HHSloc>> GetHHSloc(string WHCode)
        {
            return ExecuteDataAccess<IList<HHSloc>>(response =>
            {
                var result = new List<HHSloc>();
                var param = new List<SqlParameter>
                            {
                                 AdoHelper.CreateSqlParameter("@WHCode", WHCode, dbType: SqlDbType.NChar , size:10),
                            };
                var query = "Select * From " + ModuleCommon.HH_View_Stock + " Where WHCode = @WHCode and is_active = 1";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, ConvertToHHSloc, param.ToArray());
                    result = returnData.ToList();
                });
                taskResult.Wait();

                return result;
            });

        }

        //Select Batch, Qty, Material From SAP_GR Where MovementType = '525' And Batch Like
        public DataAccessResult<IList<SapGr>> GetSapGr(string Storage)
        {
            return ExecuteDataAccess<IList<SapGr>>(response =>
            {
                var result = new List<SapGr>();
                var param = new List<SqlParameter>
                {
                        AdoHelper.CreateSqlParameter("@Storage", Storage, dbType: SqlDbType.NVarChar , size:10),
                };
                // var query = "Select Batch, Qty, Material From " + ModuleCommon.sap_gr + " Where MovementType = '525' And Storage = @Storage and DATEADD(dd, 0, DATEDIFF(dd, 0, [CreateDateTime])) >= DATEADD(dd, 0, DATEDIFF(dd, 0, getdate()-7))";
                var query = "Select Batch, Qty, Material From " + ModuleCommon.sap_gr + " Where MovementType = '525' And Storage = @Storage and DATEADD(dd, 0, DATEDIFF(dd, 0, [CreateDateTime])) >= DATEADD(dd, 0, DATEDIFF(dd, 0, getdate()-15)) order by [CreateDateTime] desc";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, ConvertToSapGr, param.ToArray());
                    result = returnData.ToList();
                });
                taskResult.Wait();

                return result;
            });

        }

        public DataAccessResult<IList<SapGr>> Get521(string Batch)
        {
            return ExecuteDataAccess<IList<SapGr>>(response =>
            {
                var result = new List<SapGr>();
                var param = new List<SqlParameter>
                            {
                                 AdoHelper.CreateSqlParameter("@Batch", Batch, dbType: SqlDbType.NChar , size:10),
                            };
                // var query = "Select Batch, Qty, Material From " + ModuleCommon.sap_gr + " Where MovementType = '525' And Storage = @Storage and DATEADD(dd, 0, DATEDIFF(dd, 0, [CreateDateTime])) >= DATEADD(dd, 0, DATEDIFF(dd, 0, getdate()-7))";
                var query = "Select Batch, Qty, Material From " + ModuleCommon.sap_gr + " Where MovementType = '521' And Batch = @Batch";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, ConvertToSapGr, param.ToArray());
                    result = returnData.ToList();
                });
                taskResult.Wait();

                return result;
            });

        }

        public DataAccessResult<int> SelectTimeoutForSAP(string ProcessType)
        {
            return ExecuteDataAccess<int>(response =>
            {
                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@ProcessType", ProcessType, dbType: SqlDbType.NVarChar, size: 20),
                    };


                var timeout = 0;

                var query = " SELECT Time_Out FROM " + ModuleCommon.tbl_timeout + " WHERE(Process_Type = @ProcessType)";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, async (d) => { return await d.GetValueAsync<int>("Time_Out"); }, param.ToArray());
                    timeout = returnData.FirstOrDefault();
                });

                taskResult.Wait();
                return timeout;
            });

        }

        public DataAccessResult<IList<TblControl>> SelectResultControl(int Record_ID)
        {
            return ExecuteDataAccess<IList<TblControl>>(response =>
            {
                var result = new List<TblControl>();

                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@Record_ID", Record_ID , dbType: SqlDbType.Int),
                    };
                var query = "SELECT * FROM " + ModuleCommon.tbl_control + " WHERE (Record_ID = @Record_ID)";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, ConvertToTblControl, param.ToArray());
                    result = returnData.ToList();
                });

                taskResult.Wait();

                return result;
            });

        }

        public DataAccessResult<bool> UpdateStatusCompleteSAP_GR(int Interface_ID, string Return_Msg)
        {
            return ExecuteDataAccess<bool>(response =>
            {
                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@Return_Msg", Return_Msg, dbType: SqlDbType.NVarChar,size: 250),
                         AdoHelper.CreateSqlParameter("@Interface_ID", Interface_ID, dbType: SqlDbType.Int),
                    };

                var query = " UPDATE " + ModuleCommon.gr_prod + " SET Status = \'C\', Return_Msg = @Return_Msg WHERE (Status = \'W\') AND (Interface_ID = @Interface_ID)";
                var taskResult = Task.Run(async () =>
                {
                    await ExecuteNonQueryAsync(query, param.ToArray());
                });

                taskResult.Wait();
                return true;
            });

        }

        public DataAccessResult<bool> UpdateStatusErrorSAP_GR(int Interface_ID)
        {
            return ExecuteDataAccess<bool>(response =>
            {
                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@Interface_ID", Interface_ID, dbType: SqlDbType.Int),
                    };

                var query = "Update " + ModuleCommon.gr_prod + " SET Status = NULL WHERE(Status = \'W\') AND (Interface_ID = @Interface_ID)";
                var taskResult = Task.Run(async () =>
                {
                    await ExecuteNonQueryAsync(query, param);
                });

                taskResult.Wait();
                return true;
            });

        }


        public DataAccessResult<bool> InsertControl(int Prev_Process, string HH_ID, string Process_ID, string User_Name, string Flag_Del)
        {
            return ExecuteDataAccess<bool>(response =>
            {
                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@Prev_Process", Prev_Process, dbType: SqlDbType.Int),
                         AdoHelper.CreateSqlParameter("@HH_ID", HH_ID, dbType: SqlDbType.NVarChar, size: 2),
                         AdoHelper.CreateSqlParameter("@Process_ID", Process_ID, dbType: SqlDbType.NVarChar, size: 50),
                         AdoHelper.CreateSqlParameter("@User_Name", User_Name, dbType: SqlDbType.NVarChar, size: 15),
                         AdoHelper.CreateSqlParameter("@Flag_Del", Flag_Del, dbType: SqlDbType.NVarChar, size: 1),
                    };

                var query = "Insert " + ModuleCommon.tbl_control + " (Prev_Process, HH_ID, Process_ID, User_Name,Create_Date_Time, Flag_Del, Call_Date_Time) " +
                            " values ( @Prev_Process, @HH_ID, @Process_ID, @User_Name, getdate(), @Flag_Del, getdate() ) ";
                var taskResult = Task.Run(async () =>
                {
                    await ExecuteNonQueryAsync(query, param.ToArray());
                });

                taskResult.Wait();
                return true;
            });

        }


        public DataAccessResult<int> GetControlMaxRecord(string HH_ID, string Process_ID)
        {
            return ExecuteDataAccess<int>(response =>
            {
                int result = 0;
                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@HH_ID", HH_ID, dbType: SqlDbType.NVarChar, size: 2),
                         AdoHelper.CreateSqlParameter("@Process_ID", Process_ID, dbType: SqlDbType.NVarChar, size: 50),
                    };

                var query = " select MAX(Record_ID) as rec from " + ModuleCommon.tbl_control + " where Process_ID = @Process_ID and HH_ID = @HH_ID";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, async (d) => { return await d.GetValueAsync<int>("rec"); }, param.ToArray());
                    result = returnData.FirstOrDefault();
                });

                taskResult.Wait();

                return result;
            });

        }

        public DataAccessResult<IList<GROutsource>> GetOutsourceNoMaterial(string Material, string PO_Number)
        {
            return ExecuteDataAccess<IList<GROutsource>>(response =>
            {
                var result = new List<GROutsource>();
                var param = new List<SqlParameter>
                            {
                                 AdoHelper.CreateSqlParameter("@PO_Number", PO_Number, dbType: SqlDbType.NChar , size:10),
                            };
                var query = "Select Item_No,Plant,Storage,Material,Batch,Qty,ProdDate,Length From " + ModuleCommon.tbl_outsource +
                            " Where Status='N' and Material NOT IN (" + Material + ") and PO_Number = @PO_Number";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, ConvertToGROutsource, param.ToArray());
                    result = returnData.ToList();
                });
                taskResult.Wait();

                return result;
            });

        }


        public DataAccessResult<bool> InsertSap_grwithPO_item(int Record_ID, string PO_No, string Item_No, string Plant, string Storage, string Material, string Batch, decimal Qty, string ProdDate, decimal Length)
        {
            return ExecuteDataAccess<bool>(response =>
            {
                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@Record_ID", Record_ID, dbType: SqlDbType.Int),
                         AdoHelper.CreateSqlParameter("@PO_No", PO_No, dbType: SqlDbType.NVarChar, size: 10),
                         AdoHelper.CreateSqlParameter("@Item_No", Item_No, dbType: SqlDbType.NVarChar, size: 6),
                         AdoHelper.CreateSqlParameter("@Plant", Plant, dbType: SqlDbType.NVarChar, size: 4),
                         AdoHelper.CreateSqlParameter("@Storage", Storage, dbType: SqlDbType.NVarChar, size: 4),
                         AdoHelper.CreateSqlParameter("@Material", Material, dbType: SqlDbType.NVarChar, size: 18),
                         AdoHelper.CreateSqlParameter("@Batch", Batch, dbType: SqlDbType.NVarChar, size: 10),
                         AdoHelper.CreateSqlParameter("@Qty", Qty, dbType: SqlDbType.Decimal),
                         AdoHelper.CreateSqlParameter("@ProdDate", ProdDate, dbType: SqlDbType.NVarChar, size: 50),
                         AdoHelper.CreateSqlParameter("@Length", Length, dbType: SqlDbType.Decimal),
                    };

                var query = "Insert " + ModuleCommon.tbl_sap_grwithPO_item + " (Record_ID,PO_No,Item_No,Plant,Storage,Material,Batch,Qty,ProdDate,Length) " +
                            " values ( @Record_ID, @PO_No, @Item_No, @Plant, @Storage, @Material , @Batch , @Qty, @ProdDate, @Length) ";
                var taskResult = Task.Run(async () =>
                {
                    await ExecuteNonQueryAsync(query, param.ToArray());
                });

                taskResult.Wait();
                return true;
            });

        }


        /*Example */
        public DataAccessResult<bool> ActivateLockClient(string ip, string username)
        {
            return ExecuteDataAccess<bool>(response =>
            {
                var param = new List<SqlParameter>
                {
                    AdoHelper.CreateSqlParameter("@ip",ip),
                    AdoHelper.CreateSqlParameter("@username",username),
                    AdoHelper.CreateSqlParameter("@is_success",false),
                };
                var taskResult = Task.Run(async () =>
                {
                    await ExecuteStoredProcedureAsync("dbo.insert_client_login", param);
                });

                taskResult.Wait();
                return true;
            });
        }

        private async Task<GRProd> ConvertToGRProd(SqlDataReader datas)
        {
            var data = datas;
            return new GRProd
            {
                GR_ID = await data.GetValueAsync<int>("GR_ID"),
                Batch = await data.GetValueAsync<string>("Batch"),
                Qty = await data.GetValueAsync<decimal>("Qty"),
                Material = await data.GetValueAsync<string>("Material"),
                Container_No = await data.GetValueAsync<int>("Container_No"),
                Status = await data.GetValueAsync<string>("Status"),
                CreateDateTime = await data.GetValueAsync<DateTime>("CreateDateTime"),
                CreateUser = await data.GetValueAsync<string>("CreateUser"),
                Internal_ID = await data.GetValueAsync<decimal>("Internal_ID"),
                UOM = await data.GetValueAsync<string>("UOM"),
                WH_No = await data.GetValueAsync<int>("WH_No"),
                WH_Code = await data.GetValueAsync<string>("WH_Code"),
                ProductType = await data.GetValueAsync<string>("ProductType"),
                Return_Msg = await data.GetValueAsync<string>("Return_Msg"),
            };
        }

        private async Task<TblControl> ConvertToTblControl(SqlDataReader datas)
        {
            var data = datas;
            return new TblControl
            {
                Record_ID = await data.GetValueAsync<int>("Record_ID"),
                Prev_Process = await data.GetValueAsync<int>("Prev_Process"),
                HH_ID = await data.GetValueAsync<string>("HH_ID"),
                Process_ID = await data.GetValueAsync<string>("Process_ID"),
                Return_Code = await data.GetValueAsync<string>("Return_Code"),
                Return_Msg = await data.GetValueAsync<string>("Return_Msg"),
                User_Name = await data.GetValueAsync<string>("User_Name"),
                Create_Date_Time = await data.GetValueAsync<DateTime>("Create_Date_Time"),
                Complete_Date_Time = await data.GetValueAsync<DateTime>("Complete_Date_Time"),
                Flag_Del = await data.GetValueAsync<string>("Flag_Del"),
                Call_Date_Time = await data.GetValueAsync<DateTime>("Call_Date_Time"),

            };
        }

        private async Task<ViewGetItem> ConvertToViewGetItem(SqlDataReader datas)
        {
            var data = datas;
            return new ViewGetItem
            {
                VendorCode = await data.GetValueAsync<string>("VendorCode"),
                Record_ID = await data.GetValueAsync<int>("Record_ID"),
                Plant = await data.GetValueAsync<string>("Plant"),

            };
        }

        private async Task<HHSloc> ConvertToHHSloc(SqlDataReader datas)
        {
            var data = datas;
            return new HHSloc
            {
                Internal_ID = await data.GetValueAsync<int>("Internal_ID"),
                WHCode = await data.GetValueAsync<string>("WHCode"),
                StorageLocation = await data.GetValueAsync<string>("StorageLocation"),
                Description = await data.GetValueAsync<string>("Description"),
                IsTransferStatus = await data.GetValueAsync<byte>("IsTransferStatus"),
                IsTransferPlant = await data.GetValueAsync<byte>("IsTransferPlant"),
                CreateBy = await data.GetValueAsync<int>("CreateBy"),
                CreateDate = await data.GetValueAsync<DateTime>("CreateDate"),
                UpdateBy = await data.GetValueAsync<int>("UpdateBy"),
                UpdateDate = await data.GetValueAsync<DateTime>("UpdateDate"),
            };
        }

        private async Task<SapGr> ConvertToSapGr(SqlDataReader datas)
        {
            var data = datas;
            return new SapGr
            {
                Batch = await data.GetValueAsync<string>("Batch"),
                Qty = await data.GetValueAsync<decimal>("Qty"),
                Material = await data.GetValueAsync<string>("Material"),

            };
        }

        private async Task<GROutsource> ConvertToGROutsource(SqlDataReader datas)
        {
            var data = datas;
            return new GROutsource
            {
                Record_ID = await data.GetValueAsync<decimal>("Record_ID"),
                PO_Number = await data.GetValueAsync<string>("PO_Number"),
                DocDate = await data.GetValueAsync<string>("DocDate"),
                Set_Number = await data.GetValueAsync<string>("Set_Number"),
                Sup_Material = await data.GetValueAsync<string>("Sup_Material"),
                Sup_Batch = await data.GetValueAsync<string>("Sup_Batch"),
                Qty = await data.GetValueAsync<decimal>("Qty"),
                Item_No = await data.GetValueAsync<string>("Item_No"),
                Material = await data.GetValueAsync<string>("Material"),
                Batch = await data.GetValueAsync<string>("Batch"),
                Plant = await data.GetValueAsync<string>("Plant"),
                Storage = await data.GetValueAsync<string>("Storage"),
                ProdDate = await data.GetValueAsync<string>("ProdDate"),
                Length = await data.GetValueAsync<decimal>("Length"),
                Status = await data.GetValueAsync<string>("Status"),
                CreateUser = await data.GetValueAsync<string>("CreateUser"),
                CreateDateTime = await data.GetValueAsync<DateTime>("CreateDateTime"),
            };
        }

        private async Task<POFromtxt> ConvertToPOFromtxt(SqlDataReader datas)
        {
            var data = datas;
            return new POFromtxt
            {
                PO_Number = await data.GetValueAsync<string>("PO_Number"),
                ProdDate = await data.GetValueAsync<string>("ProdDate"),
                Material = await data.GetValueAsync<string>("Material"),
                Batch = await data.GetValueAsync<string>("Batch"),
                Qty = await data.GetValueAsync<decimal>("Qty"),
                Length = await data.GetValueAsync<decimal>("Length"),
                Material_Number = await data.GetValueAsync<string>("Material_Number"),
                CreateUser = await data.GetValueAsync<string>("CreateUser"),
                CreateDateTime = await data.GetValueAsync<DateTime>("CreateDateTime"),
            };
        }

        private async Task<POItem> ConvertToPOItem(SqlDataReader datas)
        {
            var data = datas;
            return new POItem
            {
                Key_ID = await data.GetValueAsync<int>("Key_ID"),
                Record_ID = await data.GetValueAsync<int>("Record_ID"),
                PO_No = await data.GetValueAsync<string>("PO_No"),
                Item_No = await data.GetValueAsync<string>("Item_No"),
                Material_No = await data.GetValueAsync<string>("Material_No"),
                Material_Desc = await data.GetValueAsync<string>("Material_Desc"),
                Batch = await data.GetValueAsync<string>("Batch"),
                Qty = await data.GetValueAsync<decimal>("Qty"),
                Storage_Loc = await data.GetValueAsync<string>("Storage_Loc"),
                Plant = await data.GetValueAsync<string>("Plant"),
                Delivery_Completed = await data.GetValueAsync<string>("Delivery_Completed"),
            };
        }

        private async Task<ViewPoItem> ConvertToViewPoItem(SqlDataReader datas)
        {
            var data = datas;
            return new ViewPoItem
            {
                Record_ID = await data.GetValueAsync<int>("Record_ID"),
                PO_Number = await data.GetValueAsync<string>("PO_Number"),
                Item_No = await data.GetValueAsync<string>("Item_No"),
                Material = await data.GetValueAsync<string>("Material"),
                Qty = await data.GetValueAsync<decimal>("Qty"),
            };
        }

        private async Task<RollTransaction> ConvertToRollTransaction(SqlDataReader datas)
        {
            var data = datas;
            return new RollTransaction
            {
                Batch_number = await data.GetValueAsync<string>("Batch_number"),
                Material_number = await data.GetValueAsync<string>("Material_number"),
                Weight = await data.GetValueAsync<decimal>("Weight"),
                Container_Item = await data.GetValueAsync<string>("Container_Item"),
            };
        }
        
    }
}
