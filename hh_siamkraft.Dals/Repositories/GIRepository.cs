﻿using hh_siamkraft.Dals.Extensions;
using hh_siamkraft.Dals.Helper;
using hh_siamkraft.Entities.Common;
using hh_siamkraft.Entities.GI;
using hh_siamkraft.Entities.GR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hh_siamkraft.Dals.Repositories
{
    public class GIRepository : RepositoryBase
    {
        public GIRepository(SkRepositorySession session) : base(session)
        {

        }

        public DataAccessResult<bool> IsMaterialMater(string material)
        {
            return ExecuteDataAccess<bool>(response =>
            {

                var result = false;
                var param = new List<SqlParameter>
                {
                        AdoHelper.CreateSqlParameter("@Material", material , dbType: SqlDbType.NVarChar),
                };
                var query = "SELECT Material_number FROM Material_Master WHERE (Material_number = @Material) AND (Material_type = 'R')";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, async (d) => { return await d.GetValueAsync<string>("Material_number"); });
                    if(returnData.ToList().Count > 0)
                    {
                        result = true;
                    }
                });

                taskResult.Wait();

                return result;
            });

        }

        public DataAccessResult<bool> SaveSocket(string Delivery_No, int Socket_ID, int Server_ID, string user_id)
        {
            return ExecuteDataAccess<bool>(response =>
            {
                var param = new List<SqlParameter>
                        {
                             AdoHelper.CreateSqlParameter("@Delivery_No", Delivery_No , dbType: SqlDbType.NVarChar , size:10),
                             AdoHelper.CreateSqlParameter("@Socket_ID", Socket_ID , dbType: SqlDbType.Int),
                             AdoHelper.CreateSqlParameter("@Server_ID", Server_ID , dbType: SqlDbType.Int),
                             AdoHelper.CreateSqlParameter("@user", user_id , dbType: SqlDbType.NVarChar , size:50),
                        };

                var taskResult = Task.Run(async () =>
                {
                    await ExecuteStoredProcedureAsync("dbo.usp_Save_Socket", param.ToList());
                });

                taskResult.Wait();
                return true;
            });
        }

        public DataAccessResult<IList<Socket>> GetSocket(int Server_ID)
        {
            return ExecuteDataAccess<IList<Socket>>(response =>
            {

                var result = new List<Socket>();
                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@Server_ID", Server_ID , dbType: SqlDbType.Int),
                    };
                var query = "SELECT s.Socket_ID, t.Description, s.Socket_Name FROM dbo.master_socket s INNER JOIN dbo.tbl_server t ON t.Server_ID = s.Server_ID WHERE s.is_active = 1 AND t.Is_Active = 1 AND t.Server_ID = @Server_ID";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, ConvertToSocket, param.ToArray());
                    result = returnData != null ? returnData.ToList() : new List<Socket>();
                });

                taskResult.Wait();

                return result;
            });

        }

        public DataAccessResult<IList<TblControl>> GetControl(int Record_ID, string HH_ID , string Process_ID, string Return_Code)
        {
            return ExecuteDataAccess<IList<TblControl>>(response =>
            {

                var result = new List<TblControl>();
                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@Record_ID", Record_ID , dbType: SqlDbType.Int),
                         AdoHelper.CreateSqlParameter("@HH_ID", HH_ID , dbType: SqlDbType.NVarChar , size:2),
                         AdoHelper.CreateSqlParameter("@Process_ID", Process_ID , dbType: SqlDbType.NVarChar , size:50),
                         AdoHelper.CreateSqlParameter("@Return_Code", Return_Code , dbType: SqlDbType.NVarChar , size:2),
                    };
                var query = "Select * from " + ModuleCommon.tbl_control + " Where (Record_ID = @Record_ID) AND (HH_ID = @HH_ID) AND (Process_ID = @Process_ID) AND (Return_Code <> @Return_Code) ";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, ConvertToTblControl, param.ToArray());
                    result = returnData != null ? returnData.ToList() : new List<TblControl>();
                });

                taskResult.Wait();

                return result;
            });

        }

        public DataAccessResult<IList<ViewGIMat>> GetVwMatDetail(string Delivery_NO, int Record_ID)
        {
            return ExecuteDataAccess<IList<ViewGIMat>>(response =>
            {

                var result = new List<ViewGIMat>();
                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@Delivery_NO", Delivery_NO , dbType: SqlDbType.NVarChar , size:10),
                         AdoHelper.CreateSqlParameter("@Record_ID", Record_ID , dbType: SqlDbType.Int),
                    };
                //var query = "Select * from " + ModuleCommon.view_gi_mat_detail + " Where (Record_ID = @Record_ID) AND (Delivery_NO = @Delivery_NO) ";

                var query = new StringBuilder();
                query.Append(" SELECT a.[Material_No], convert(varchar(100), a.[Mat_Order]) as [Mat_Order], a.[AO], a.[Pick_HH], convert(varchar(100), a.[PI]) as [PI], a.[Delivery_No], a.[Record_ID], convert(varchar(100), a.[Actual_Qty]) as [Actual_Qty], count(1) as [Count_Batch]");
                query.Append(" FROM [dbo].[GI_Mat_Detail] a");
                query.Append(" INNER JOIN [dbo].[GI_Batch_Detail2] b on a.[Delivery_No] = b.[Delivery_No] and a.[Record_ID] = b.[Record_ID] and a.[Material_No] = b.[Material_No]");
                query.Append(" WHERE a.[Delivery_No] = @Delivery_NO");
                query.Append(" AND a.[Record_ID] = @Record_ID");
                query.Append(" GROUP BY a.[Material_No], convert(varchar(100), a.[Mat_Order]) , a.[AO], a.[Pick_HH], convert(varchar(100), a.[PI]) , a.[Delivery_No], a.[Record_ID], convert(varchar(100), a.[Actual_Qty])");
                query.Append(" ORDER BY a.[Material_No], convert(varchar(100), a.[Mat_Order]) , a.[AO]");

                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query.ToString(), ConvertToViewGIMat, param.ToArray());
                    result = returnData != null ? returnData.ToList() : new List<ViewGIMat>();
                });

                taskResult.Wait();

                return result;
            });

        }

        public DataAccessResult<IList<ViewGIBatch>> GetVwGIBatch(string Delivery_No, string Material_No, int Record_ID)
        {
            return ExecuteDataAccess<IList<ViewGIBatch>>(response =>
            {

                var result = new List<ViewGIBatch>();
                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@Delivery_No", Delivery_No , dbType: SqlDbType.NVarChar , size:10),
                         AdoHelper.CreateSqlParameter("@Material_No", Material_No , dbType: SqlDbType.NVarChar , size:18),
                         AdoHelper.CreateSqlParameter("@Record_ID", Record_ID , dbType: SqlDbType.Int),
                    };
                //var query = "Select * from " + ModuleCommon.view_gi_batch_detail + " Where (Material_No = @Material_No) AND (Delivery_No = @Delivery_No) AND (Record_ID = @Record_ID) ";
                var query = new StringBuilder();
                query.Append(" SELECT [Material_No], [Batch_No], [Pick_HH], convert(varchar(100), [PI]) as [PI], [Delivery_No], [Record_ID], [Batch], [Qty], convert(varchar(100), [Actual_Qty]) as [Actual_Qty]");
                query.Append(" FROM [dbo].[GI_Batch_Detail2]");
                query.Append(" WHERE [Delivery_No] = @Delivery_No");
                query.Append(" AND [Record_ID] = @Record_ID");
                query.Append(" AND [Material_No] = @Material_No");
                query.Append(" ORDER BY [Material_No], [Batch_No]");

                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query.ToString(), ConvertToViewGIBatch, param.ToArray());
                    result = returnData != null ? returnData.ToList() : new List<ViewGIBatch>();
                });

                taskResult.Wait();

                return result;
            });

        }

        public DataAccessResult<IList<DeliveryHead>> GetDLHeadStatus(string Status)
        {
            return ExecuteDataAccess<IList<DeliveryHead>>(response =>
            {
                var result = new List<DeliveryHead>();

                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@Status", Status , dbType: SqlDbType.NVarChar , size:2),
                    };
                var query =  "Select * FROM " + ModuleCommon.tbl_dl_head + " WHERE (Status = @Status)  ORDER BY Delivery_No";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, ConvertToDeliveryHead, param.ToArray());
                    result = returnData != null ? returnData.ToList() : new List<DeliveryHead>();
                });

                taskResult.Wait();

                return result;
            });

        }

        public DataAccessResult<IList<DeliveryHead>> GetDLHeadNotStatus(string Delivery_No, string Status)
        {
            return ExecuteDataAccess<IList<DeliveryHead>>(response =>
            {
                var result = new List<DeliveryHead>();

                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@Delivery_No", Delivery_No , dbType: SqlDbType.NVarChar , size:10),
                         AdoHelper.CreateSqlParameter("@Status", Status , dbType: SqlDbType.NVarChar , size:2),
                    };
                var query = "Select * FROM " + ModuleCommon.tbl_dl_head + " WHERE ( Delivery_No = @Delivery_No) AND (Status IS NOT NULL AND  Status <> @Status )  ORDER BY [Time_Create] desc";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, ConvertToDeliveryHead, param.ToArray());
                    result = returnData != null ? returnData.ToList() : new List<DeliveryHead>();
                });

                taskResult.Wait();

                return result;
            });

        }

        public DataAccessResult<IList<DeliveryHead>> GetDLHead(int Record_ID, string Delivery_No)
        {
            return ExecuteDataAccess<IList<DeliveryHead>>(response =>
            {
                var result = new List<DeliveryHead>();

                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@Record_ID", Record_ID , dbType: SqlDbType.Int),
                         AdoHelper.CreateSqlParameter("@Delivery_No", Delivery_No , dbType: SqlDbType.NVarChar , size:10),
                    };
                var query = "Select * FROM " + ModuleCommon.tbl_dl_head + " WHERE (Record_ID = @Record_ID) AND (Delivery_No = @Delivery_No) ORDER BY Delivery_No";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, ConvertToDeliveryHead, param.ToArray());
                    result = returnData != null ? returnData.ToList() : new List<DeliveryHead>();
                });

                taskResult.Wait();

                return result;
            });

        }

        public DataAccessResult<Tuple<int,IList<string>>> GetDLHeadNotStatus4()
        {
            return ExecuteDataAccess<Tuple<int, IList<string>>>(response =>
            {
                var result = new Tuple<int, IList<string>>(0, new List<string>());

                return result;
               
            });

        }

        public DataAccessResult<IList<HHGiItem>> GetHHGiItem(string Delivery_No, string Plant)
        {
            return ExecuteDataAccess<IList<HHGiItem>>(response =>
            {
                var result = new List<HHGiItem>();

                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@Delivery_No", Delivery_No , dbType: SqlDbType.NVarChar , size:10),
                         AdoHelper.CreateSqlParameter("@Plant", Plant , dbType: SqlDbType.NVarChar , size:4),
                    };
                var query = "Select * FROM " + ModuleCommon.tbl_gi_item + " WHERE ( Delivery_No = @Delivery_No) AND (Plant = @Plant OR @Plant = '%')  ORDER BY Delivery_No";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, ConvertToHHGiItem, param.ToArray());
                    result = returnData != null ? returnData.ToList() : new List<HHGiItem>();
                });

                taskResult.Wait();

                return result;
            });

        }

         public DataAccessResult<IList<ViewGICheckID>> GetViewGICheckID(string Delivery_No, int Record_ID, string Plant)
        {
            return ExecuteDataAccess<IList<ViewGICheckID>>(response =>
            {
                var result = new List<ViewGICheckID>();

                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@Delivery_No", Delivery_No , dbType: SqlDbType.NVarChar , size:10),
                         AdoHelper.CreateSqlParameter("@Record_ID", Record_ID , dbType: SqlDbType.Int),
                    };
                var query = "Select * FROM " + ModuleCommon.view_gi_check_id + " WHERE ( Delivery_No = @Delivery_No) AND (Record_ID = @Record_ID)  AND (Plant in (" + Plant + "))  ORDER BY Delivery_No";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, ConvertToViewGICheckID, param.ToArray());
                    result = returnData != null ? returnData.ToList() : new List<ViewGICheckID>();
                });

                taskResult.Wait();

                return result;
            });

        }

        public DataAccessResult<IList<ViewGICheckID>> GetViewGICheckID(string Delivery_No)
        {
            return ExecuteDataAccess<IList<ViewGICheckID>>(response =>
            {
                var result = new List<ViewGICheckID>();

                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@Delivery_No", Delivery_No , dbType: SqlDbType.NVarChar , size:10),
                    };
                var query = "Select * FROM " + ModuleCommon.view_gi_check_id + " WHERE ( Delivery_No = @Delivery_No) ";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, ConvertToViewGICheckID, param.ToArray());
                    result = returnData != null ? returnData.ToList() : new List<ViewGICheckID>();
                });

                taskResult.Wait();

                return result;
            });

        }

        public DataAccessResult<IList<TblControl>> GetPickingControl(string Delivery_No, string Process_ID)
        {
            return ExecuteDataAccess<IList<TblControl>>(response =>
            {
                var result = new List<TblControl>();

                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@Delivery_No", Delivery_No.PadLeft(10, '0') , dbType: SqlDbType.NVarChar , size:10),
                         AdoHelper.CreateSqlParameter("@Process_ID", Process_ID , dbType: SqlDbType.NVarChar  , size:50),
                    };
                var query = "Select b.Record_ID ,a.Return_Code ,a.Return_Msg  FROM " + ModuleCommon.tbl_control +
                " as a INNER JOIN " + ModuleCommon.tbl_picking_head + " as b ON a.Record_ID = b.Record_ID" +
                " WHERE  (DATEDIFF(day,a.Create_Date_Time,getdate()) = 0) AND (a.Process_ID = @Process_ID ) AND ( b.Delivery_No = @Delivery_No)";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, ConvertToTblControl, param.ToArray());
                    result = returnData != null ? returnData.ToList() : new List<TblControl>();
                });

                taskResult.Wait();

                return result;
            });

        }

        public DataAccessResult<IList<DeliveryItem>> GetDeliveryOrder(string Delivery_No, int Record_ID)
        {
            return ExecuteDataAccess<IList<DeliveryItem>>(response =>
            {
                var result = new List<DeliveryItem>();

                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@Delivery_No", Delivery_No , dbType: SqlDbType.NVarChar , size:10),
                         AdoHelper.CreateSqlParameter("@Record_ID", Record_ID , dbType: SqlDbType.Int),
                    };

                var query = "Select  a.Material_No, SUM(a.Actual_Qty) AS Mat_Order, ISNULL(b.AO, 0) AS AO " +
                              " ,ISNULL(b.HH_Qty, 0) AS HH_Qty, a.Delivery_No, a.Record_ID, a.Sales_unit, b.UOM" +
                              " FROM " + ModuleCommon.tbl_dl_item + " as a LEFT OUTER JOIN " + ModuleCommon.view_groupby_hh_gi_item + "  as b " +
                              " ON b.Delivery_No = a.Delivery_No AND b.Material_No = a.Material_No AND b.Record_ID = a.Record_ID " +
                              " WHERE (a.Delivery_No = @Delivery_No) AND(a.Record_ID = @Record_ID) " +
                              " GROUP BY a.Material_No, a.Delivery_No, a.Record_ID, b.AO, b.HH_Qty, a.Sales_unit, b.UOM ";


                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, ConvertToDeliveryItem, param.ToArray());
                    result = returnData != null ? returnData.ToList() : new List<DeliveryItem>();
                });

                taskResult.Wait();

                return result;
            });

        }

        public DataAccessResult<Tuple<float, IList<HHGiItem>>> GetDeliveryDetail(string Delivery_No, int Record_ID)
        {
            return ExecuteDataAccess < Tuple<float, IList<HHGiItem>>>(response =>
            {
                var result = new Tuple<float, IList<HHGiItem>>(0, new List<HHGiItem>());


                var RollTotal = AdoHelper.CreateSqlParameter("@RollTotal", 0, ParameterDirection.InputOutput, SqlDbType.Float);
                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@Delivery_No", Delivery_No , dbType: SqlDbType.NVarChar , size:10),
                         AdoHelper.CreateSqlParameter("@Record_ID", Record_ID , dbType: SqlDbType.Int),
                         RollTotal,
                    };

               
                var taskResult = Task.Run(async () =>
                {
                    var setters = new List<Func<SqlDataReader, Task<object>>> { ConvertToHHGiItem2 };
                    var returnData = await ExecuteStoredProcedureAsync("dbo.GI_ViewDetail", param.ToArray(), setters);

                    float roll = RollTotal.Value != null ? Convert.ToInt32(RollTotal.Value) : 0;
                    List<HHGiItem> listHH = returnData != null ? returnData[0].Cast<HHGiItem>().ToList() : new List<HHGiItem>();

                    result = new Tuple<float, IList<HHGiItem>>(roll, listHH);
                });


                taskResult.Wait();

                return result;
            });
        }

        public DataAccessResult<string> GICountBatch(string Delivery_No, int Record_ID)
        {
            return ExecuteDataAccess<string>(response =>
            {
                var result = new Tuple<decimal, decimal>(0, 0);
                var pRoll_Total = AdoHelper.CreateSqlParameter("@pRoll_Total", 0, ParameterDirection.InputOutput, SqlDbType.Float);
                var pRoll_Scan = AdoHelper.CreateSqlParameter("@pRoll_Scan", 0, ParameterDirection.InputOutput, SqlDbType.Decimal);

                var flag = AdoHelper.CreateSqlParameter("@flag", string.Empty, ParameterDirection.InputOutput, SqlDbType.NVarChar);

                var param = new List<SqlParameter>
                        {
                             AdoHelper.CreateSqlParameter("@Delivery_No", Delivery_No , dbType: SqlDbType.NVarChar , size:10),
                             AdoHelper.CreateSqlParameter("@Record_ID", Record_ID , dbType: SqlDbType.Int),
                             //pRoll_Total,
                             //pRoll_Scan,
                             flag,
                        };
             
                var taskResult = Task.Run(async () =>
                {
                    await ExecuteStoredProcedureAsync("dbo.GI_Count_Batch3", param.ToList());
                });
                
                taskResult.Wait();
                return Convert.ToString(flag.Value);
            });
        }


        public DataAccessResult<int> GIRecordID(string hh_id, string user_id, string record_id, string delivery_no)
        {
           return ExecuteDataAccess<int>(response =>
            {
                var result = 0;
                var RETURN_VALUE = AdoHelper.CreateSqlParameter("@RETURN_VALUE", 0, ParameterDirection.ReturnValue, SqlDbType.Int);
                var param = new List<SqlParameter>
                        {
                             AdoHelper.CreateSqlParameter("@hh_id", hh_id , dbType: SqlDbType.NVarChar , size:2),
                             AdoHelper.CreateSqlParameter("@user_id", user_id , dbType: SqlDbType.NVarChar , size:15),
                             AdoHelper.CreateSqlParameter("@record_id", record_id , dbType: SqlDbType.Int),
                             AdoHelper.CreateSqlParameter("@delivery_no", delivery_no , dbType: SqlDbType.NVarChar , size:10),
                             RETURN_VALUE,
                        };
                var taskResult = Task.Run(async () =>
                {
                    var setters = new List<Func<SqlDataReader, Task<object>>> { ConvertToHHGiItem2 };
                    var returnData = await ExecuteStoredProcedureAsync("dbo.GI_RecordID", param.ToArray(), setters);
                    List<HHGiItem> listHH = returnData != null ? returnData[0].Cast<HHGiItem>().ToList() : new List<HHGiItem>();

                    result = Convert.ToInt32(RETURN_VALUE.Value);
                });

                taskResult.Wait();
                return result;
            });
        }

       

        public DataAccessResult<Tuple<int, IList<DeliveryItem>>> GetBatchDetailRM(string Batch_No, string Delivery_No, string Plant, int Record_ID)
        {
            return ExecuteDataAccess<Tuple<int, IList<DeliveryItem>>>(response =>
            {
                var result = new Tuple<int, IList<DeliveryItem>>(0, new List<DeliveryItem>());
                var RETURN_VALUE = AdoHelper.CreateSqlParameter("@RETURN_VALUE", 0, ParameterDirection.ReturnValue, SqlDbType.Int);
                var msgOut = AdoHelper.CreateSqlParameter("@msgOut", 0, ParameterDirection.InputOutput, SqlDbType.NVarChar, size: 255);
                var param = new List<SqlParameter>
                        {
                             AdoHelper.CreateSqlParameter("@Batch_No", Batch_No , dbType: SqlDbType.NVarChar , size:10),
                             AdoHelper.CreateSqlParameter("@Delivery_No", Delivery_No , dbType: SqlDbType.NVarChar , size:10),
                             AdoHelper.CreateSqlParameter("@Plant", Plant , dbType: SqlDbType.NVarChar , size:4),
                             AdoHelper.CreateSqlParameter("@Record_ID", Record_ID , dbType: SqlDbType.Int),
                             msgOut,
                             RETURN_VALUE,
                        };


                var taskResult = Task.Run(async () =>
                {
                    var setters = new List<Func<SqlDataReader, Task<object>>> { ConvertToDeliveryItem2 };
                    var returnData = await ExecuteStoredProcedureAsync("dbo.usp_GetBatchDetail_RM", param.ToArray(), setters);

                    int preturn_value = Convert.ToInt32(RETURN_VALUE.Value);
                    List<DeliveryItem> listHH = returnData != null ? returnData[0].Cast<DeliveryItem>().ToList() : new List<DeliveryItem>();

                    result = new Tuple<int, IList<DeliveryItem>>(preturn_value, listHH);
                });

                response.Error = new Exception(msgOut.Value.ToString());
                taskResult.Wait();

                return result;
            });

        }

        public DataAccessResult<bool> SaveScanBarcodeRM(string Delivery_No, int Record_ID, string Material_No, string Batch_No, decimal QTY, string Plant, string user_ID, int WH_NO)
        {
            return ExecuteDataAccess<bool>(response =>
            {
                bool result = false;
                var RETURN_VALUE = AdoHelper.CreateSqlParameter("@RETURN_VALUE", 0, ParameterDirection.ReturnValue, SqlDbType.Int);
                var msgOut = AdoHelper.CreateSqlParameter("@msgOut", 0, ParameterDirection.InputOutput, SqlDbType.NVarChar, size: 255);
                var param = new List<SqlParameter>
                        {
                             AdoHelper.CreateSqlParameter("@Delivery_No", Delivery_No , dbType: SqlDbType.NVarChar , size:10),
                             AdoHelper.CreateSqlParameter("@Record_ID", Record_ID , dbType: SqlDbType.Int),
                             AdoHelper.CreateSqlParameter("@Material_No", Material_No , dbType: SqlDbType.NVarChar , size:18),
                             AdoHelper.CreateSqlParameter("@Batch_No", Batch_No , dbType: SqlDbType.NVarChar , size:10),
                             AdoHelper.CreateSqlParameter("@QTY", QTY , dbType: SqlDbType.Decimal),
                             AdoHelper.CreateSqlParameter("@Plant", Plant , dbType: SqlDbType.NVarChar , size:4),
                             AdoHelper.CreateSqlParameter("@user_ID", user_ID , dbType: SqlDbType.NVarChar , size:50),
                             AdoHelper.CreateSqlParameter("@WH_NO", WH_NO , dbType: SqlDbType.Int),
                             msgOut,
                             RETURN_VALUE,
                        };


                var taskResult = Task.Run(async () =>
                {
                    await ExecuteStoredProcedureAsync("dbo.usp_GISaveScan_RM", param.ToArray());

                    if (Convert.ToInt32(RETURN_VALUE.Value) == 0)
                        result = false;
                    else
                        result = true;

                });

                response.Error = new Exception(msgOut.Value.ToString());
                taskResult.Wait();

                return result;
            });

        }


        public DataAccessResult<int> GetGIViewWeight(string hh_id, string user_id, string delivery_no)
        {
            return ExecuteDataAccess<int>(response =>
            {
                var result = 0;
                var param = new List<SqlParameter>
                        {
                             AdoHelper.CreateSqlParameter("@hh_id", hh_id , dbType: SqlDbType.NVarChar , size:2),
                             AdoHelper.CreateSqlParameter("@user_id", user_id , dbType: SqlDbType.NVarChar , size:15),
                             AdoHelper.CreateSqlParameter("@delivery_no", delivery_no , dbType: SqlDbType.NVarChar , size:10),
                        };
                var taskResult = Task.Run(async () =>
                {
                    var setters = new List<Func<SqlDataReader, Task<object>>> { async (d) => { return await d.GetValueAsync<int>("Record_ID"); } };
                    var returnData = await ExecuteStoredProcedureAsync("dbo.GI_ViewWeight", param.ToArray(), setters);

                    var rtn = returnData.FirstOrDefault();
                    result = returnData != null ? Convert.ToInt32(rtn.FirstOrDefault())  : 0;

                });

                taskResult.Wait();
                return result;
            });
        }


        public DataAccessResult<bool> UpdateStatusDeliveryHead(string Status, string Delivery_No, int Record_ID)
        {
            return ExecuteDataAccess<bool>(response =>
            {
                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@Status", Status, dbType: SqlDbType.NVarChar,size: 2),
                         AdoHelper.CreateSqlParameter("@Delivery_No", Delivery_No, dbType: SqlDbType.NVarChar,size: 10),
                         AdoHelper.CreateSqlParameter("@Record_ID", Record_ID, dbType: SqlDbType.Int),
                    };

                var query = " UPDATE " + ModuleCommon.tbl_dl_head + " SET Status = @Status, Time_Createcomplete = GETDATE() WHERE(Delivery_No = @Delivery_No) AND (Record_ID = @Record_ID)";
                var taskResult = Task.Run(async () =>
                {
                    await ExecuteNonQueryAsync(query, param.ToArray());
                });

                taskResult.Wait();
                return true;
            });

        }

        public DataAccessResult<bool> UpdateStatusDLHead_NoTime(string Status, string Delivery_No, int Record_ID)
        {
            return ExecuteDataAccess<bool>(response =>
            {
                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@Status", Status, dbType: SqlDbType.NVarChar,size: 2),
                         AdoHelper.CreateSqlParameter("@Delivery_No", Delivery_No, dbType: SqlDbType.NVarChar,size: 10),
                         AdoHelper.CreateSqlParameter("@Record_ID", Record_ID, dbType: SqlDbType.Int),
                    };

                var query = " UPDATE " + ModuleCommon.tbl_dl_head + " SET Status = @Status WHERE(Delivery_No = @Delivery_No) AND (Record_ID = @Record_ID)";
                var taskResult = Task.Run(async () =>
                {
                    await ExecuteNonQueryAsync(query, param.ToArray());
                });

                taskResult.Wait();
                return true;
            });

        }

        public DataAccessResult<bool> DeleteGiItem(string Delivery_No)
        {
            return ExecuteDataAccess<bool>(response =>
            {
                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@Delivery_No", Delivery_No, dbType: SqlDbType.NVarChar,size: 10),
                    };

                var query = " DELETE " + ModuleCommon.tbl_gi_item + " WHERE (Delivery_No = @Delivery_No)";
                var taskResult = Task.Run(async () =>
                {
                    await ExecuteNonQueryAsync(query, param.ToArray());
                });

                taskResult.Wait();
                return true;
            });

        }

        public DataAccessResult<bool> DeleteGiItem(string Delivery_No, string Batch_No)
        {
            return ExecuteDataAccess<bool>(response =>
            {
                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@Delivery_No", Delivery_No, dbType: SqlDbType.NVarChar,size: 10),
                         AdoHelper.CreateSqlParameter("@Batch_No", Batch_No, dbType: SqlDbType.NVarChar,size: 10),
                    };

                var query = " DELETE " + ModuleCommon.tbl_gi_item + " WHERE (Delivery_No = @Delivery_No) AND (Batch_No = @Batch_No)";
                var taskResult = Task.Run(async () =>
                {
                    await ExecuteNonQueryAsync(query, param.ToArray());
                });

                taskResult.Wait();
                return true;
            });

        }

        public DataAccessResult<bool> UpdateGiItem(string Delivery_No, int Record_ID)
        {
            return ExecuteDataAccess<bool>(response =>
            {
                var param = new List<SqlParameter>
                    {
                       AdoHelper.CreateSqlParameter("@Delivery_No", Delivery_No, dbType: SqlDbType.NVarChar,size: 10),
                       AdoHelper.CreateSqlParameter("@Record_ID", Record_ID, dbType: SqlDbType.Int),
                    };

                var query = " UPDATE " + ModuleCommon.tbl_gi_item + " SET Record_ID = @Record_ID WHERE (Delivery_No = @Delivery_No)";
                var taskResult = Task.Run(async () =>
                {
                    await ExecuteNonQueryAsync(query, param.ToArray());
                });

                taskResult.Wait();
                return true;
            });

        }

        //public DataAccessResult<Tuple<float, IList<HHGiItem>>> GetDeliveryDetail(string Delivery_No, int Record_ID)
        //{
        //    return ExecuteDataAccess<Tuple<float, IList<HHGiItem>>>(response =>
        //    {
        //    var result = new Tuple<float, IList<HHGiItem>>(0, new List<HHGiItem>());

        public DataAccessResult<Tuple<int, IList<HHGiItem>>> SaveGIScan(string Delivery_No, decimal Record_ID, string Batch_no, string Plant, string User_ID, int WH_No, string UOM)
        {
            return ExecuteDataAccess < Tuple <int, IList < HHGiItem >>> (response =>
            {
                var result = new Tuple<int, IList<HHGiItem>>(0, new List<HHGiItem>());
                var MessageErr = AdoHelper.CreateSqlParameter("@MessageErr", 0, ParameterDirection.InputOutput, SqlDbType.NVarChar, size: 400);
                var Flag = AdoHelper.CreateSqlParameter("@Flag", 0, ParameterDirection.InputOutput, SqlDbType.Int);

                var param = new List<SqlParameter>
                {
                    AdoHelper.CreateSqlParameter("@Delivery_No",Delivery_No),
                    AdoHelper.CreateSqlParameter("@Record_ID",Record_ID),
                    AdoHelper.CreateSqlParameter("@Batch_no",Batch_no),
                    AdoHelper.CreateSqlParameter("@Plant",Plant),
                    AdoHelper.CreateSqlParameter("@User_ID",User_ID),
                    AdoHelper.CreateSqlParameter("@WH_No",WH_No),
                    AdoHelper.CreateSqlParameter("@UOM",UOM),
                    MessageErr,
                    Flag,
                };

                var taskResult = Task.Run(async () =>
                {
                    var setters = new List<Func<SqlDataReader, Task<object>>> { ConvertToHHGiItem2 };
                    var returnData = await ExecuteStoredProcedureAsync("dbo.usp_GISaveScan", param.ToArray(), setters);
                    int pFlag = Flag.Value != null ? Convert.ToInt32(Flag.Value) : -1;
                    List<HHGiItem> listHH = returnData != null ? returnData[0].Cast<HHGiItem>().ToList() : new List<HHGiItem>();
                    result = new Tuple<int, IList<HHGiItem>>(pFlag, listHH);
                });
                taskResult.Wait();

                var error = MessageErr.Value != null ? MessageErr.Value.ToString() : null;
                if (!string.IsNullOrEmpty(error))
                {
                    response.Error = new Exception(error);
                }

                

                return result;
            });
        }

      
        public DataAccessResult<int> SaveGIToSap(int Record_ID, string Delivery_No, string HH_ID, string User_ID, int WH_No)
        {
            return ExecuteDataAccess<int>(response =>
            {
                var Interface_ID = AdoHelper.CreateSqlParameter("@Interface_ID", 0, ParameterDirection.InputOutput, SqlDbType.Int);
                var Flag = AdoHelper.CreateSqlParameter("@Flag", 0, ParameterDirection.InputOutput, SqlDbType.Int);
                var param = new List<SqlParameter>
                {
                    AdoHelper.CreateSqlParameter("@HH_ID",HH_ID),
                    AdoHelper.CreateSqlParameter("@User_ID",User_ID),
                    AdoHelper.CreateSqlParameter("@Record_ID",Record_ID),
                    AdoHelper.CreateSqlParameter("@Delivery_No",Delivery_No),
                    AdoHelper.CreateSqlParameter("@WH_No",WH_No),
                    Interface_ID,
                };
                var taskResult = Task.Run(async () =>
                {
                    await ExecuteStoredProcedureAsync("dbo.usp_GISendToSAP", param.ToArray());
                });

                taskResult.Wait();
                return Convert.ToInt32(Interface_ID.Value);
            });
        }

        private async Task<DeliveryHead> ConvertToDeliveryHead(SqlDataReader datas)
        {
            var data = datas;
            return new DeliveryHead
            {
                Record_ID = await data.GetValueAsync<int>("Record_ID"),
                Delivery_No = await data.GetValueAsync<string>("Delivery_No"),
                Delivery_Type = await data.GetValueAsync<string>("Delivery_Type"),
                Time_Create = await data.GetValueAsync<DateTime>("Time_Create"),
                Time_Createcomplete = await data.GetValueAsync<DateTime>("Time_Createcomplete"),
                Time_Scan = await data.GetValueAsync<DateTime>("Time_Scan"),
                Time_ScanComplete = await data.GetValueAsync<DateTime>("Time_ScanComplete"),
                Time_Send = await data.GetValueAsync<DateTime>("Time_Send"),
                Time_SendComplete = await data.GetValueAsync<DateTime>("Time_SendComplete"),
                Status = await data.GetValueAsync<string>("Status"),
                Text = await data.GetValueAsync<string>("Text"),
                WH_No = await data.GetValueAsync<int>("WH_No"),
                Sales_Org = await data.GetValueAsync<string>("Sales_Org"),
            };
        }

        private async Task<HHGiItem> ConvertToHHGiItem(SqlDataReader datas)
        {
            var data = datas;
            return new HHGiItem
            {
                HH_Item_ID = await data.GetValueAsync<int>("HH_Item_ID"),
                Record_ID = await data.GetValueAsync<decimal>("Record_ID"),
                Delivery_No = await data.GetValueAsync<string>("Delivery_No"),
                Material_No = await data.GetValueAsync<string>("Material_No"),
                Batch_No = await data.GetValueAsync<string>("Batch_No"),
                Plant = await data.GetValueAsync<string>("Plant"),
                Storage_Loc = await data.GetValueAsync<string>("Storage_Loc"),
                Item_No = await data.GetValueAsync<string>("Item_No"),
                Line_No = await data.GetValueAsync<string>("Line_No"),
                Actual_Qty = await data.GetValueAsync<decimal>("Actual_Qty"),
                Qty = await data.GetValueAsync<decimal>("Qty"),
                Status = await data.GetValueAsync<string>("Status"),
                CreateUser = await data.GetValueAsync<string>("CreateUser"),
                CreateDateTime = await data.GetValueAsync<DateTime>("CreateDateTime"),
                WH_No = await data.GetValueAsync<int>("WH_No"),
                UOM = await data.GetValueAsync<string>("UOM"),
                Interface_ID = await data.GetValueAsync<int>("Interface_ID"),
            };
        }

        private async Task<object> ConvertToHHGiItem2(SqlDataReader datas)
        {
            var data = datas;
            return new HHGiItem
            {
                HH_Item_ID = await data.GetValueAsync<int>("HH_Item_ID"),
                Record_ID = await data.GetValueAsync<decimal>("Record_ID"),
                Delivery_No = await data.GetValueAsync<string>("Delivery_No"),
                Material_No = await data.GetValueAsync<string>("Material_No"),
                Batch_No = await data.GetValueAsync<string>("Batch_No"),
                Plant = await data.GetValueAsync<string>("Plant"),
                Storage_Loc = await data.GetValueAsync<string>("Storage_Loc"),
                Item_No = await data.GetValueAsync<string>("Item_No"),
                Line_No = await data.GetValueAsync<string>("Line_No"),
                Actual_Qty = await data.GetValueAsync<decimal>("Actual_Qty"),
                Qty = await data.GetValueAsync<decimal>("Qty"),
                Status = await data.GetValueAsync<string>("Status"),
                CreateUser = await data.GetValueAsync<string>("CreateUser"),
                CreateDateTime = await data.GetValueAsync<DateTime>("CreateDateTime"),
                WH_No = await data.GetValueAsync<int>("WH_No"),
                UOM = await data.GetValueAsync<string>("UOM"),
                Interface_ID = await data.GetValueAsync<int>("Interface_ID"),
            };
        }

        private async Task<DeliveryItem> ConvertToDeliveryItem(SqlDataReader datas)
        {
            var data = datas;
            return new DeliveryItem
            {
                Delivery_Item_ID = await data.GetValueAsync<int>("Delivery_Item_ID"),
                Record_ID = await data.GetValueAsync<int>("Record_ID"),
                Delivery_No = await data.GetValueAsync<string>("Delivery_No"),
                Item_No = await data.GetValueAsync<string>("Item_No"),
                Material_No = await data.GetValueAsync<string>("Material_No"),
                Material_Desc = await data.GetValueAsync<string>("Material_Desc"),
                Batch = await data.GetValueAsync<string>("Batch"),
                HghLevItmBatch = await data.GetValueAsync<string>("HghLevItmBatch"),
                Actual_Qty = await data.GetValueAsync<decimal>("Actual_Qty"),
                Storage_Loc = await data.GetValueAsync<string>("Storage_Loc"),
                Plant = await data.GetValueAsync<string>("Plant"),
                Item_Category = await data.GetValueAsync<string>("Item_Category"),
                Conversion_Factor = await data.GetValueAsync<int>("Conversion_Factor"),
                Conversion_Divisor = await data.GetValueAsync<int>("Conversion_Divisor"),
                Sales_unit = await data.GetValueAsync<string>("Sales_unit"),

                Mat_Order =(decimal) await data.GetValueAsync<double>("Mat_Order"),
                AO = await data.GetValueAsync<int>("AO"),
                HH_Qty = await data.GetValueAsync<decimal>("HH_Qty"),
                UOM = await data.GetValueAsync<string>("UOM"),
            };
        }

        private async Task<object> ConvertToDeliveryItem2(SqlDataReader datas)
        {
            var data = datas;
            return new DeliveryItem
            {
                Delivery_Item_ID = await data.GetValueAsync<int>("Delivery_Item_ID"),
                Record_ID = await data.GetValueAsync<int>("Record_ID"),
                Delivery_No = await data.GetValueAsync<string>("Delivery_No"),
                Item_No = await data.GetValueAsync<string>("Item_No"),
                Material_No = await data.GetValueAsync<string>("Material_No"),
                Material_Desc = await data.GetValueAsync<string>("Material_Desc"),
                Batch = await data.GetValueAsync<string>("Batch"),
                HghLevItmBatch = await data.GetValueAsync<string>("HghLevItmBatch"),
                Actual_Qty = await data.GetValueAsync<decimal>("Actual_Qty"),
                Storage_Loc = await data.GetValueAsync<string>("Storage_Loc"),
                Plant = await data.GetValueAsync<string>("Plant"),
                Item_Category = await data.GetValueAsync<string>("Item_Category"),
                Conversion_Factor = await data.GetValueAsync<int>("Conversion_Factor"),
                Conversion_Divisor = await data.GetValueAsync<int>("Conversion_Divisor"),
                Sales_unit = await data.GetValueAsync<string>("Sales_unit"),

                Mat_Order = await data.GetValueAsync<decimal>("Mat_Order"),
                AO = await data.GetValueAsync<int>("AO"),
                HH_Qty = await data.GetValueAsync<decimal>("HH_Qty"),
                UOM = await data.GetValueAsync<string>("UOM"),
                Qty = await data.GetValueAsync<decimal>("Qty"),
            };
        }


        private async Task<ViewGICheckID> ConvertToViewGICheckID(SqlDataReader datas)
        {
            var data = datas;
            return new ViewGICheckID
            {
                Plant = await data.GetValueAsync<string>("Plant"),
                Record_ID = await data.GetValueAsync<int>("Record_ID"),
                Delivery_No = await data.GetValueAsync<string>("Delivery_No"),
                Text = await data.GetValueAsync<string>("Text"),
            };
        }

        private async Task<TblControl> ConvertToTblControl(SqlDataReader datas)
        {
            var data = datas;
            return new TblControl
            {
                Record_ID = await data.GetValueAsync<int>("Record_ID"),
                Prev_Process = await data.GetValueAsync<int>("Prev_Process"),
                HH_ID = await data.GetValueAsync<string>("HH_ID"),
                Process_ID = await data.GetValueAsync<string>("Process_ID"),
                Return_Code = await data.GetValueAsync<string>("Return_Code"),
                Return_Msg = await data.GetValueAsync<string>("Return_Msg"),
                User_Name = await data.GetValueAsync<string>("User_Name"),
                Create_Date_Time = await data.GetValueAsync<DateTime>("Create_Date_Time"),
                Complete_Date_Time = await data.GetValueAsync<DateTime>("Complete_Date_Time"),
                Flag_Del = await data.GetValueAsync<string>("Flag_Del"),
                Call_Date_Time = await data.GetValueAsync<DateTime>("Call_Date_Time"),

            };
        }

        private async Task<ViewGIMat> ConvertToViewGIMat(SqlDataReader datas)
        {
            var data = datas;
            return new ViewGIMat
            {
                //Material_No = await data.GetValueAsync<string>("Material_No"),
                //Mat_Order = await data.GetValueAsync<string>("Mat_Order"),
                //AO = await data.GetValueAsync<string>("AO"),
                //Pick_HH = await data.GetValueAsync<string>("Pick_HH"),
                //PI = await data.GetValueAsync<string>("PI"),
                //Delivery_No = await data.GetValueAsync<string>("Delivery_No"),
                //Record_ID = await data.GetValueAsync<int>("Record_ID"),
                //Actual_Qty = await data.GetValueAsync<int>("Actual_Qty"),
                Material_No = await data.GetValueAsync<string>("Material_No"),
                Mat_Order = await data.GetValueAsync<string>("Mat_Order"),
                AO = await data.GetValueAsync<int>("AO"),
                Pick_HH = await data.GetValueAsync<string>("Pick_HH"),
                PI = await data.GetValueAsync<string>("PI"),
                Delivery_No = await data.GetValueAsync<string>("Delivery_No"),
                Record_ID = await data.GetValueAsync<int>("Record_ID"),
                Actual_Qty = await data.GetValueAsync<string>("Actual_Qty"),
                Count_Batch = await data.GetValueAsync<int>("Count_Batch"),
            };
        }

        private async Task<ViewGIBatch> ConvertToViewGIBatch(SqlDataReader datas)
        {
            var data = datas;
            return new ViewGIBatch
            {
                //Material_No = await data.GetValueAsync<string>("Material_No"),
                //Batch_No = await data.GetValueAsync<string>("Batch_No"),
                //Pick_HH = await data.GetValueAsync<string>("Pick_HH"),
                //PI = await data.GetValueAsync<decimal>("PI"),
                //Delivery_No = await data.GetValueAsync<string>("Delivery_No"),
                //Record_ID = await data.GetValueAsync<int>("Record_ID"),
                //Batch = await data.GetValueAsync<string>("Batch"),
                //Qty = await data.GetValueAsync<string>("Qty"),
                //Actual_Qty = await data.GetValueAsync<decimal>("Actual_Qty"),
                Material_No = await data.GetValueAsync<string>("Material_No"),
                Batch_No = await data.GetValueAsync<string>("Batch_No"),
                Pick_HH = await data.GetValueAsync<string>("Pick_HH"),
                PI = await data.GetValueAsync<string>("PI"),
                Delivery_No = await data.GetValueAsync<string>("Delivery_No"),
                Record_ID = await data.GetValueAsync<int>("Record_ID"),
                Batch = await data.GetValueAsync<string>("Batch"),
                Qty = await data.GetValueAsync<string>("Qty"),
                Actual_Qty = await data.GetValueAsync<string>("Actual_Qty"),
            };
        }

        private async Task<Socket> ConvertToSocket(SqlDataReader datas)
        {
            var data = datas;
            return new Socket
            {
                Socket_ID = await data.GetValueAsync<int>("Socket_ID"),
                Description = await data.GetValueAsync<string>("Description"),
                Socket_Name = await data.GetValueAsync<string>("Socket_Name"),
            };
        }
    }
}
