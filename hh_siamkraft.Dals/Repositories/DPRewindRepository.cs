﻿using Entities;
using hh_siamkraft.Dals.Extensions;
using hh_siamkraft.Dals.Helper;
using hh_siamkraft.Entities.Common;
using hh_siamkraft.Entities.GR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hh_siamkraft.Dals.Repositories
{
    public class DPRewindRepository : RepositoryBase
    {
        public DPRewindRepository(SkRepositorySession session) : base(session)
        {

        }

        public DataAccessResult<bool> UpdateStatusError(string DocNo, string MsgError)
        {
            return ExecuteDataAccess<bool>(response =>
            {
                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@MsgError", MsgError , dbType: SqlDbType.VarChar),
                         AdoHelper.CreateSqlParameter("@DocNo", DocNo , dbType: SqlDbType.VarChar),
                    };

                var query = "UPDATE " + ModuleCommon.tbt_RewindHeader + " SET Status = 'P', MsgError = @MsgError WHERE (DocNo = @DocNo)";
                var taskResult = Task.Run(async () =>
                {
                    await ExecuteNonQueryAsync(query, param);
                });
                taskResult.Wait();
                return true;
            });

        }
        

        public DataAccessResult<IList<RewindScan>> SelectScanDetail(string DocNo, string Batch)
        {
            return ExecuteDataAccess<IList<RewindScan>>(response =>
            {
                var param = new List<SqlParameter>
                {
                    AdoHelper.CreateSqlParameter("@DocNo", DocNo, dbType: SqlDbType.VarChar),
                    AdoHelper.CreateSqlParameter("@Batch", Batch, dbType: SqlDbType.VarChar),
                };
                var result = new List<RewindScan>();
                var query = " SELECT Material, Batch, Qty, DocNo, InternalID FROM tbt_RewindScan WHERE(DocNo = @DocNo) AND (Batch = @Batch OR @Batch = '-') ORDER BY InternalID DESC";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, ConvertToRewindScan, param.ToArray());
                    result = returnData.ToList();
                });

                taskResult.Wait();
                return result;
            });
        }
        
        public DataAccessResult<bool> ConfirmScanRewindManual(string DocNo, string UserName)
        {
            return ExecuteDataAccess<bool>(response =>
            {
                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@DocNo", DocNo , dbType: SqlDbType.VarChar),
                         AdoHelper.CreateSqlParameter("@UserName", UserName , dbType: SqlDbType.VarChar),
                    };

                var query = "UPDATE " + ModuleCommon.tbt_RewindHeader + " SET Status = \'F\', CloseBy = @UserName, CloseDate = GETDATE() WHERE (DocNo = @DocNo)";
                var taskResult = Task.Run(async () =>
                {
                    await ExecuteNonQueryAsync(query, param);
                });
                taskResult.Wait();
                return true;
            });
        }
        public DataAccessResult<bool> SaveScanBarcode(string WHCode, string DocNo, string Batch, decimal Qty, string Material, bool IsSAP, string CreateBy)
        {
            return ExecuteDataAccess<bool>(response =>
            {
                var msg = AdoHelper.CreateSqlParameter("@MsgOut", string.Empty, ParameterDirection.InputOutput, SqlDbType.VarChar);
                var InternalID = AdoHelper.CreateSqlParameter("@internalID", 0, ParameterDirection.InputOutput, SqlDbType.Int);
                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@WHCode", WHCode , dbType: SqlDbType.VarChar),
                         AdoHelper.CreateSqlParameter("@DocNo", DocNo , dbType: SqlDbType.VarChar),
                         AdoHelper.CreateSqlParameter("@Batch", Batch , dbType: SqlDbType.VarChar),
                         AdoHelper.CreateSqlParameter("@Qty", Qty , dbType: SqlDbType.Decimal),
                         AdoHelper.CreateSqlParameter("@Material", Material , dbType: SqlDbType.VarChar),
                         AdoHelper.CreateSqlParameter("@IsSAP", IsSAP , dbType: SqlDbType.Bit),
                         AdoHelper.CreateSqlParameter("@CreateBy", CreateBy , dbType: SqlDbType.VarChar),
                         msg,
                         InternalID,
                    };

                var taskResult = Task.Run(async () =>
                {
                    await ExecuteStoredProcedureAsync("dbo.usp_RewindSaveScan2", param);
                });
                taskResult.Wait();

                var error = msg.Value != null ? msg.Value.ToString() : null;
                if (!string.IsNullOrEmpty(error))
                {
                    response.Error = new Exception(error);
                    return false;
                }
                return true;
            });
        }

        public DataAccessResult<bool> DeleteScanBarcode(string WHCode, string DocNo, string Material, string Batch, string UserName)
        {
            return ExecuteDataAccess<bool>(response =>
            {
                var msg = AdoHelper.CreateSqlParameter("@MsgOut", string.Empty, ParameterDirection.InputOutput, SqlDbType.VarChar);
               
                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@WHCode", WHCode , dbType: SqlDbType.VarChar),
                         AdoHelper.CreateSqlParameter("@DocNo", DocNo , dbType: SqlDbType.VarChar),
                         AdoHelper.CreateSqlParameter("@Batch", Batch , dbType: SqlDbType.VarChar),
                         AdoHelper.CreateSqlParameter("@Material", Material , dbType: SqlDbType.VarChar),
                         AdoHelper.CreateSqlParameter("@CreateBy", UserName , dbType: SqlDbType.VarChar),
                         msg,
                         
                    };

                var taskResult = Task.Run(async () =>
                {
                    await ExecuteStoredProcedureAsync("dbo.usp_RewindDeleteScan", param);
                });
                taskResult.Wait();

                var error = msg.Value != null ? msg.Value.ToString() : null;
                if (!string.IsNullOrEmpty(error))
                {
                    response.Error = new Exception(error);
                    return false;
                }
                return true;
            });
        }
        public DataAccessResult<decimal> ConfirmSendToSAP(string whCode, string docNo, string hhID, string createBy, string docDate, string postDate, string itemText)
        {
            return ExecuteDataAccess<decimal>(response =>
            {
                var msg = AdoHelper.CreateSqlParameter("@MsgOut", string.Empty, ParameterDirection.InputOutput, SqlDbType.VarChar);
                var InterfaceID = AdoHelper.CreateSqlParameter("@InterfaceID", 0, ParameterDirection.InputOutput, SqlDbType.Int);
                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@WHCode", whCode , dbType: SqlDbType.VarChar),
                         AdoHelper.CreateSqlParameter("@DocNo", docNo , dbType: SqlDbType.VarChar),
                         AdoHelper.CreateSqlParameter("@HHID", hhID , dbType: SqlDbType.VarChar),
                         AdoHelper.CreateSqlParameter("@CreateBy", createBy , dbType: SqlDbType.VarChar),
                         AdoHelper.CreateSqlParameter("@DocDate", docDate , dbType: SqlDbType.VarChar),
                         AdoHelper.CreateSqlParameter("@PostDate", postDate , dbType: SqlDbType.VarChar),
                         AdoHelper.CreateSqlParameter("@ItemText", itemText , dbType: SqlDbType.VarChar),
                         InterfaceID,
                         msg,
                    };

                var taskResult = Task.Run(async () =>
                {
                    await ExecuteStoredProcedureAsync("dbo.usp_RewindSendToSAP", param);
                });

                taskResult.Wait();

                var error = msg.Value != null ? msg.Value.ToString() : null;
                if (!string.IsNullOrEmpty(error))
                {
                    response.Error = new Exception(error);
                    return 0;
                }
                return Convert.ToDecimal(InterfaceID.Value.ToString());
            });
        }

        public DataAccessResult<IList<TotalScan>> SelectTotalScanQty(string DocNo)
        {
            return ExecuteDataAccess<IList<TotalScan>>(response =>
            {
                var param = new List<SqlParameter>
                {
                    AdoHelper.CreateSqlParameter("@DocNo", DocNo, dbType: SqlDbType.VarChar),
                };
                var result = new List<TotalScan>();
                var query = "SELECT a.DocNo, a.TotalOrderQty, ISNULL(b.TotalScanQty, 0) AS TotalScanQty";
                query += " FROM(";
                query += "     SELECT Header_InternalID, DocNo, SUM(OrderQty) AS TotalOrderQty ";
                query += "     FROM tbt_RewindDetail ";
                query += "     GROUP BY Header_InternalID, DocNo";
                query += " ) a";
                query += " LEFT OUTER JOIN (";
                query += "     SELECT Header_InternalID, DocNo, COUNT(Batch) AS TotalScanQty ";
                query += "     FROM tbt_RewindScan ";
                query += "     GROUP BY Header_InternalID, DocNo";
                query += " ) b ON a.Header_InternalID = b.Header_InternalID AND a.DocNo = b.DocNo ";
                query += " WHERE(a.DocNo = @DocNo)";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, ConvertToTotalScan, param.ToArray());
                    result = returnData.ToList();
                });

                taskResult.Wait();
                return result;
            });

        }
        public DataAccessResult<bool> UpdateStausComplete(string DocNo, string MatDoc, string CreateBy)
        {
            return ExecuteDataAccess<bool>(response =>
            {
                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@MatDoc", MatDoc , dbType: SqlDbType.VarChar),
                         AdoHelper.CreateSqlParameter("@CreateBy", CreateBy , dbType: SqlDbType.VarChar),
                         AdoHelper.CreateSqlParameter("@DocNo", DocNo , dbType: SqlDbType.VarChar),
                    };

                var query = "UPDATE " + ModuleCommon.tbt_RewindHeader + " SET Status = 'F', MatDoc = @MatDoc, PostCompleteDate = GETDATE(), CloseBy = @CreateBy, CloseDate = GETDATE() WHERE (DocNo = @DocNo)";
                var taskResult = Task.Run(async () =>
                {
                    await ExecuteNonQueryAsync(query, param);
                });
                taskResult.Wait();
                return true;
            });

        }


        public DataAccessResult<IList<RewindDetail>> SelectRewindDetailByDocNo(string WHCode, string DocNo, bool IsSAP)
        {
            return ExecuteDataAccess<IList<RewindDetail>>(response =>
            {
                var param = new List<SqlParameter>
                {
                    AdoHelper.CreateSqlParameter("@WHCode", WHCode, dbType: SqlDbType.VarChar),
                    AdoHelper.CreateSqlParameter("@DocNo", DocNo, dbType: SqlDbType.VarChar),
                    AdoHelper.CreateSqlParameter("@IsSAP", IsSAP, dbType: SqlDbType.Bit),
                };
                var result = new List<RewindDetail>();
                var query = "SELECT a.WHCode, a.RewindNo, a.DocNo, b.Material, SUM(b.OrderQty) AS OrderQty, a.InterfaceSAP, a.Status, COUNT(c.Batch) AS RewindQty, ISNULL(SUM(c.Qty), 0) AS TotalWeight, a.InternalID";
                query += " FROM " + ModuleCommon.tbt_RewindHeader + " a";
                query += " INNER JOIN " + ModuleCommon.tbt_RewindDetail + " b ON a.InternalID = b.Header_InternalID AND a.DocNo = b.DocNo";
                query += " LEFT OUTER JOIN " + ModuleCommon.tbt_RewindScan + " c ON b.Header_InternalID = c.Header_InternalID AND b.DocNo = c.DocNo AND b.Material = c.Material";
                query += " GROUP BY a.WHCode, a.RewindNo, a.DocNo, b.Material, a.InterfaceSAP, a.Status, a.InternalID";
                query += " HAVING(a.WHCode = @WHCode) AND(a.DocNo = @DocNo) AND(a.InterfaceSAP = @IsSAP)";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, ConvertToRewindDetail, param.ToArray());
                    result = returnData.ToList();
                });

                taskResult.Wait();
                return result;
            });

        }

        public DataAccessResult<IList<RewindHeader>> SelectInterfaceDisconnect(string WHCode, string CreateBy)
        {
            return ExecuteDataAccess<IList<RewindHeader>>(response =>
            {
                var param = new List<SqlParameter>
                {
                    AdoHelper.CreateSqlParameter("@WHCode", WHCode, dbType: SqlDbType.VarChar),
                    AdoHelper.CreateSqlParameter("@CreateBy", CreateBy, dbType: SqlDbType.VarChar),
                };
                var result = new List<RewindHeader>();
                var query = "SELECT InterfaceID, DocNo FROM " + ModuleCommon.tbt_RewindHeader + " WHERE(WHCode = @WHCode) AND(InterfaceSAP = 1) AND (Status = \'W\') AND (PostBy = @CreateBy) ORDER BY InternalID";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, ConvertToRewindHeader, param.ToArray());
                    result = returnData.ToList();
                });

                taskResult.Wait();
                return result;
            });

        }

        
        private async Task<RewindScan> ConvertToRewindScan(SqlDataReader datas)
        {
            var data = datas;
            return new RewindScan
            {
                Material = await data.GetValueAsync<string>("Material"),
                Batch = await data.GetValueAsync<string>("Batch"),
                Qty = await data.GetValueAsync<decimal>("Qty"),
                DocNo = await data.GetValueAsync<string>("DocNo"),
                InternalID = await data.GetValueAsync<decimal>("InternalID"),
            };
        }
        private async Task<TotalScan> ConvertToTotalScan(SqlDataReader datas)
        {
            var data = datas;
            return new TotalScan
            {
                TotalOrderQty = await data.GetValueAsync<int>("TotalOrderQty"),
                TotalScanQty = await data.GetValueAsync<int>("TotalScanQty"),
                DocNo = await data.GetValueAsync<string>("DocNo"),
            };
        }
        private async Task<RewindHeader> ConvertToRewindHeader(SqlDataReader datas)
        {
            var data = datas;
            return new RewindHeader
            {
                InterfaceID = await data.GetValueAsync<int?>("InterfaceID"),
                DocNo = await data.GetValueAsync<string>("DocNo"),
            };
        }

        private async Task<RewindDetail> ConvertToRewindDetail(SqlDataReader datas)
        {
            var data = datas;
            return new RewindDetail
            {
                WHCode = await data.GetValueAsync<string>("WHCode"),
                RewindNo = await data.GetValueAsync<string>("RewindNo"),
                DocNo = await data.GetValueAsync<string>("DocNo"),
                Material = await data.GetValueAsync<string>("Material"),
                OrderQty = await data.GetValueAsync<int>("OrderQty"),
                InterfaceSAP = await data.GetValueAsync<bool>("InterfaceSAP"),
                Status = await data.GetValueAsync<string>("Status"),
                RewindQty = await data.GetValueAsync<int>("RewindQty"),
                TotalWeight = await data.GetValueAsync<decimal>("TotalWeight"),
                InternalID = await data.GetValueAsync<decimal>("InternalID"),
            };
        }
    }
}
