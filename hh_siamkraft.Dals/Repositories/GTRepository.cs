﻿using Entities;
using hh_siamkraft.Dals.Extensions;
using hh_siamkraft.Dals.Helper;
using hh_siamkraft.Entities.Common;
using hh_siamkraft.Entities.GR;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hh_siamkraft.Dals.Repositories
{
    public class GTRepository : RepositoryBase
    {
        public GTRepository(SkRepositorySession session) : base(session)
        {

        }


        
        public DataAccessResult<int> SelectTimeoutForSAP(string ProcessType)
        {
            return ExecuteDataAccess<int>(response =>
            {
                var param = new List<SqlParameter>
                {
                    AdoHelper.CreateSqlParameter("@ProcessType",ProcessType),
                };

                var result = 0;
                var query = "SELECT Time_Out FROM " + ModuleCommon.HH_TimeOut + " WHERE(Process_Type = @ProcessType)";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, async (d) => { return await d.GetValueAsync<int>("Time_Out"); }, param.ToArray());
                    result = returnData.FirstOrDefault();
                });

                taskResult.Wait();
                return result;
            });
        }
        
        public DataAccessResult<int> ConfirmToSAP_Group(string hh_ID, int scanID, string movementType, string storageFrom, int wh_No, string wh_Code, string userName, string licensecar)
        {
            return ExecuteDataAccess<int>(response =>
            {
                var recordID = AdoHelper.CreateSqlParameter("@Record_ID", 0, ParameterDirection.InputOutput, SqlDbType.Int);
                var param = new List<SqlParameter>
                {
                    AdoHelper.CreateSqlParameter("@HH_ID",hh_ID),
                    AdoHelper.CreateSqlParameter("@ScanID",scanID),
                    AdoHelper.CreateSqlParameter("@MovementType",movementType),
                    AdoHelper.CreateSqlParameter("@StorageFrom",storageFrom),
                    AdoHelper.CreateSqlParameter("@WH_No",wh_No),
                    AdoHelper.CreateSqlParameter("@WH_Code",wh_Code),
                    AdoHelper.CreateSqlParameter("@UserName",userName),
                    AdoHelper.CreateSqlParameter("@LicenseCar",licensecar),
                    recordID,
                };
                var taskResult = Task.Run(async () =>
                {
                    await ExecuteStoredProcedureAsync("dbo.usp_GTSendToSAP_GroupSloc", param);
                });
                taskResult.Wait();
                int result = recordID.Value != null ? Convert.ToInt32(recordID.Value) : 0;
                
                return result;
            });
        }
        public DataAccessResult<int> ConfirmToSAP(string hh_ID, int scanID, string movementType, string storageFrom, int wh_No, string wh_Code, string userName, string licensecar)
        {
            return ExecuteDataAccess<int>(response =>
            {
                var recordID = AdoHelper.CreateSqlParameter("@Record_ID", 0, ParameterDirection.InputOutput, SqlDbType.Int);
                var param = new List<SqlParameter>
                {
                    AdoHelper.CreateSqlParameter("@HH_ID",hh_ID),
                    AdoHelper.CreateSqlParameter("@ScanID",scanID),
                    AdoHelper.CreateSqlParameter("@MovementType",movementType),
                    AdoHelper.CreateSqlParameter("@StorageFrom",storageFrom),
                    AdoHelper.CreateSqlParameter("@WH_No",wh_No),
                    AdoHelper.CreateSqlParameter("@WH_Code",wh_Code),
                    AdoHelper.CreateSqlParameter("@UserName",userName),
                    AdoHelper.CreateSqlParameter("@LicenseCar",licensecar),
                    recordID,
                };
                var taskResult = Task.Run(async () =>
                {
                    await ExecuteStoredProcedureAsync("dbo.usp_GTSendToSAP2", param);
                });
                taskResult.Wait();

                int result = recordID.Value != null ? Convert.ToInt32(recordID.Value) : 0;
                return result;
            });
        }

        public DataAccessResult<int> GetIDGT(string movementType)
        {
            return ExecuteDataAccess<int>(response =>
            {
                var param = new List<SqlParameter>
                {
                        AdoHelper.CreateSqlParameter("@MovementType", movementType, dbType: SqlDbType.NVarChar,size: 100),
                };


                var result = 0;
                var query = "SELECT [GT_ID] FROM " + ModuleCommon.HH_GT_Running + " where DATEADD(dd, 0, DATEDIFF(dd, 0, [GTDateTime])) = DATEADD(dd, 0, DATEDIFF(dd, 0, getdate()))  and [MovementType] = @MovementType";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, async (d) => { return await d.GetValueAsync<int>("GT_ID"); }, param.ToArray());
                    result = returnData.FirstOrDefault();
                });

                taskResult.Wait();
                return result;
            });

        }
        public DataAccessResult<int> SelectMaxID(string movementType)
        {
            return ExecuteDataAccess<int>(response =>
            {
                var param = new List<SqlParameter>
                {
                        AdoHelper.CreateSqlParameter("@MovementType", movementType, dbType: SqlDbType.NVarChar,size: 100),
                };
                

                var result = 0;
                var query = "SELECT ISNULL(MAX(GT_ID), 0) AS GT_ID FROM " + ModuleCommon.HH_GT_Running + " WHERE (MovementType = @MovementType) AND (DATEDIFF(day, GTDateTime, GETDATE()) = 0)";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, async (d) => { return await d.GetValueAsync<int>("GT_ID"); }, param.ToArray());
                    result = returnData.FirstOrDefault();
                });

                taskResult.Wait();
                return result;
            });
        }
        public DataAccessResult<bool> UpdateMaxID(string movementType, int start)
        {
            return ExecuteDataAccess<bool>(response =>
            {
                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@MovementType", movementType , dbType: SqlDbType.VarChar),
                    };

                var query = "Update " + ModuleCommon.HH_GT_Running + " SET GT_ID = GT_ID + 1 Where MovementType = @MovementType and GTDateTime = DATEADD(dd, 0, DATEDIFF(dd, 0, getdate()))";
                if(start == 1)
                {
                    //query = "insert into " + ModuleCommon.HH_GT_Running + " (MovementType, GTDateTime, GT_ID) values (@MovementType, DATEADD(dd, 0, DATEDIFF(dd, 0, getdate())), 1)";
                    query = "Update " + ModuleCommon.HH_GT_Running + " SET GT_ID = 1, GTDateTime = DATEADD(dd, 0, DATEDIFF(dd, 0, getdate())) Where MovementType = @MovementType";
                }
                var taskResult = Task.Run(async () =>
                {
                    await ExecuteNonQueryAsync(query, param);
                });
                taskResult.Wait();
                return true;
            });

        }

        public DataAccessResult<bool> UpdateStatusSAP(int Interface_ID, string status, string msg)
        {
            return ExecuteDataAccess<bool>(response =>
            {
                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@Interface_ID", Interface_ID , dbType: SqlDbType.Int),
                         AdoHelper.CreateSqlParameter("@status", status , dbType: SqlDbType.VarChar),
                         AdoHelper.CreateSqlParameter("@msg", msg , dbType: SqlDbType.VarChar),
                    };

                var query = "Update " + ModuleCommon.HH_GT_Item + " SET Status = @status, Return_Msg = @msg Where Status = 'W' and Interface_ID = @Interface_ID";
                var taskResult = Task.Run(async () =>
                {
                    await ExecuteNonQueryAsync(query, param);
                });
                taskResult.Wait();
                return true;
            });

        }

        public DataAccessResult<bool> DeleteScanBarcode(int scanID, string movementType, string batch, int wh_No)
        {
            return ExecuteDataAccess<bool>(response =>
            {
                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@ScanID", scanID , dbType: SqlDbType.Int),
                         AdoHelper.CreateSqlParameter("@MovementType", movementType , dbType: SqlDbType.VarChar),
                         AdoHelper.CreateSqlParameter("@Batch", batch , dbType: SqlDbType.VarChar),
                         AdoHelper.CreateSqlParameter("@WH_No", wh_No , dbType: SqlDbType.VarChar),
                    };

                var query = "DELETE FROM " + ModuleCommon.HH_GT_Item + " WHERE (MovementType = @MovementType) AND (Batch = @Batch) AND (WH_No = @WH_No)";
                var taskResult = Task.Run(async () =>
                {
                    await ExecuteNonQueryAsync(query, param);
                });
                taskResult.Wait();
                return true;
            });

        }

        public DataAccessResult<bool> updateSloc(string Storage, string Batch, string MatCode, int GTID)
        {
            return ExecuteDataAccess<bool>(response =>
            {
                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@Storage", Storage , dbType: SqlDbType.VarChar),
                         AdoHelper.CreateSqlParameter("@Batch", Batch , dbType: SqlDbType.VarChar),
                         AdoHelper.CreateSqlParameter("@MatCode", MatCode , dbType: SqlDbType.VarChar),
                         AdoHelper.CreateSqlParameter("@GTID", GTID , dbType: SqlDbType.Int),
                    };

                var query = "update " + ModuleCommon.HH_GT_Item + " set Storage_From = @Storage where Batch = @Batch and Material = @MatCode and GT_ID = @GTID AND (DATEDIFF(day, CreateDateTime, GETDATE()) = 0) ";
                var taskResult = Task.Run(async () =>
                {
                    await ExecuteNonQueryAsync(query, param);
                });
                taskResult.Wait();
                return true;
            });

        }

        public DataAccessResult<bool> DelItem(string queryID)
        {
            return ExecuteDataAccess<bool>(response =>
            {
                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@queryID", queryID , dbType: SqlDbType.VarChar),
                    };

                var query = "delete from " + ModuleCommon.tbl_StockInquiry_Item + "WHERE QUERY_ID = @queryID";
                var taskResult = Task.Run(async () =>
                {
                    await ExecuteNonQueryAsync(query, param);
                });
                taskResult.Wait();
                return true;
            });
        }

        public DataAccessResult<bool> DelHead(string queryID)
        {
            return ExecuteDataAccess<bool>(response =>
            {
                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@queryID", queryID , dbType: SqlDbType.VarChar),
                    };

                var query = "delete from " + ModuleCommon.tbl_StockInquiry_Header + "WHERE QUERY_ID = @queryID";
                var taskResult = Task.Run(async () =>
                {
                    await ExecuteNonQueryAsync(query, param);
                });
                taskResult.Wait();
                return true;
            });
        }

        public DataAccessResult<IList<HH_GT_Item>> SaveScanBarcode(int scanID, string movementType, string storageFrom, string storageTo, string itemText
            , string uom, string batch, string Mvbatch, int wh_no, string wh_code, string userName, string cleartext)
        {
            return ExecuteDataAccess<IList<HH_GT_Item>>(response =>
            {
                var msg = AdoHelper.CreateSqlParameter("@MsgOut", string.Empty, ParameterDirection.InputOutput, SqlDbType.VarChar, size:200);
                var param = new List<SqlParameter>
                    {
                         AdoHelper.CreateSqlParameter("@ScanID", scanID , dbType: SqlDbType.Int),
                         AdoHelper.CreateSqlParameter("@MovementType", movementType , dbType: SqlDbType.VarChar),
                         AdoHelper.CreateSqlParameter("@StorageFrom", storageFrom , dbType: SqlDbType.VarChar),
                         AdoHelper.CreateSqlParameter("@StorageTo", storageTo , dbType: SqlDbType.VarChar),
                         AdoHelper.CreateSqlParameter("@ItemText", itemText , dbType: SqlDbType.VarChar),
                         AdoHelper.CreateSqlParameter("@UOM", uom , dbType: SqlDbType.VarChar),
                         AdoHelper.CreateSqlParameter("@Batch", batch , dbType: SqlDbType.VarChar),
                         AdoHelper.CreateSqlParameter("@MvBatch", Mvbatch , dbType: SqlDbType.VarChar),
                         AdoHelper.CreateSqlParameter("@WH_No", wh_no , dbType: SqlDbType.Int),
                         AdoHelper.CreateSqlParameter("@WH_Code", wh_code , dbType: SqlDbType.VarChar),
                         AdoHelper.CreateSqlParameter("@UserName", userName , dbType: SqlDbType.VarChar),
                         AdoHelper.CreateSqlParameter("@ClearTxt", cleartext , dbType: SqlDbType.VarChar),
                         msg,
                    };


                var result = new List<HH_GT_Item>();
                var taskResult = Task.Run(async () =>
                {
                    var setters = new List<Func<SqlDataReader, Task<object>>> { ConvertToHHGTItem2 };
                    var returnData = await ExecuteStoredProcedureAsync("dbo.usp_GTSave2", param, setters);
                    result = returnData != null ? returnData[0].Cast<HH_GT_Item>().ToList() : new List<HH_GT_Item>();
                });
                taskResult.Wait();

                var error = msg.Value != null ? msg.Value.ToString() : null;
                if (!string.IsNullOrEmpty(error))
                {
                    response.Error = new Exception(error);
                }
                return result;
            });

        }

        public DataAccessResult<IList<StockInquiryItem>> GetBatch_fromStock(string queryid)
        {
            return ExecuteDataAccess<IList<StockInquiryItem>>(response =>
            {
                var param = new List<SqlParameter>
                {
                        AdoHelper.CreateSqlParameter("@queryid", queryid, dbType: SqlDbType.VarChar),
                };


                var result = new List<StockInquiryItem>();
                var query = "SELECT QUERY_ID, Batch, STORAGE, MAT_CODE FROM " + ModuleCommon.tbl_StockInquiry_Item + "  WHERE QUERY_ID = @querid";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, ConvertToStockInquiryItem, param.ToArray());
                    result = returnData != null ? returnData.ToList() : new List<StockInquiryItem>();
                });

                taskResult.Wait();
                return result;
            });

        }
        public DataAccessResult<IList<string>> getStorageFrom2(int scanid)
        {
            return ExecuteDataAccess<IList<string>>(response =>
            {
                var param = new List<SqlParameter>
                {
                        AdoHelper.CreateSqlParameter("@scanid", scanid, dbType: SqlDbType.Int),
                };
                var result = new List<string>();
                var query = "SELECT distinct [Storage_From] FROM "+ModuleCommon.HH_GT_Item+"  where GT_ID = @scanid and ([Status] NOT IN ('C','W') OR [Status] IS NULL) AND (DATEDIFF(day, CreateDateTime, GETDATE()) = 0)";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, async (d) => { return await d.GetValueAsync<string>("Storage_From"); }, param.ToArray());
                    result = returnData.ToList();
                });

                taskResult.Wait();
                return result;
            });

        }

        public DataAccessResult<IList<string>> getStorageFrom(string WHCode)
        {
            return ExecuteDataAccess<IList<string>>(response =>
            {
                var result = new List<string>();
                
                //var query = "Select Storage_ID From " + ModuleCommon.CONFIG_STORAGE_WHH + " where IsActive=1";
                var param = new List<SqlParameter>
                            {
                                 AdoHelper.CreateSqlParameter("@WHCode", WHCode, dbType: SqlDbType.NChar , size:10),
                            };
                var query = "Select distinct [StorageLocation] From " + ModuleCommon.HH_Sloc + " Where WHCode = @WHCode and is_active = 1 order by [StorageLocation]";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, async (d) => { return await d.GetValueAsync<string>("StorageLocation"); }, param.ToArray());
                    result = returnData.ToList();
                });

                taskResult.Wait();
                return result;
            });

        }

        public DataAccessResult<IList<string>> GetProblemDesc()
        {
            return ExecuteDataAccess<IList<string>>(response =>
            {
                var result = new List<string>();
                var query = "Select Problem_Desc From " + ModuleCommon.HH_GT_Problem + " where is_active = 1 order by Problem_Desc";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, async (d) => { return await d.GetValueAsync<string>("Problem_Desc"); }, null);
                    result = returnData.ToList();
                });

                taskResult.Wait();
                return result;
            });

        }

        public DataAccessResult<IList<string>> GetPlantDesc(string plant)
        {
            return ExecuteDataAccess<IList<string>>(response =>
            {
                var param = new List<SqlParameter>
                {
                        AdoHelper.CreateSqlParameter("@plant", plant, dbType: SqlDbType.NVarChar,size: 100),
                };

                var result = new List<string>();
                //var query = "SELECT [Desc1] FROM [tbm_PlantMasterTransfer] where Plant = @plant and desc1 is not null group by [Desc1]";
                var query = "select distinct [StorageLocation] from [HH_Sloc] where [WHCode] = @plant and is_active = 1 order by [StorageLocation]";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, async (d) => { return await d.GetValueAsync<string>("StorageLocation"); }, param.ToArray());
                    result = returnData.ToList();
                });

                taskResult.Wait();
                return result;
            });

        }

        

        public DataAccessResult<IList<HH_GT_Item>> SelectScanDataDetail(int ScanID, string MovementType, int WH_No, string UserName)
        {
            return ExecuteDataAccess<IList<HH_GT_Item>>(response =>
            {
                var result = new List<HH_GT_Item>();
                var param = new List<SqlParameter>
                            {
                                 AdoHelper.CreateSqlParameter("@ScanID", ScanID, dbType: SqlDbType.Int),
                                 AdoHelper.CreateSqlParameter("@MovementType", MovementType , dbType: SqlDbType.NVarChar),
                                 AdoHelper.CreateSqlParameter("@WH_No", WH_No , dbType: SqlDbType.Int),
                                 AdoHelper.CreateSqlParameter("@UserName", UserName , dbType: SqlDbType.NVarChar),
                            };
                var query = "SELECT HH_GT_Item.* FROM " +ModuleCommon.HH_GT_Item+ "  WHERE (GT_ID = @ScanID) AND (MovementType = @MovementType) AND(Status <> 'C' OR Status IS NULL) AND (DATEDIFF(day, CreateDateTime, GETDATE()) = 0) AND (WH_No = @WH_No or @WH_No = 0) AND (upper(CreateUser) = upper(@UserName) or @UserName = '') order by CreateDateTime desc";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, ConvertToHHGTItem, param.ToArray());
                    result = returnData.ToList();
                });
                taskResult.Wait();

                return result;
            });
        }

        public DataAccessResult<IList<TBM_PlantTransfer>> GetPlantMasterTransfer(string plant, string Desc1)
        {
            return ExecuteDataAccess<IList<TBM_PlantTransfer>>(response =>
            {
                var result = new List<TBM_PlantTransfer>();
                var param = new List<SqlParameter>
                {
                        AdoHelper.CreateSqlParameter("@plant", plant, dbType: SqlDbType.NVarChar),
                        AdoHelper.CreateSqlParameter("@Desc1", Desc1 , dbType: SqlDbType.NVarChar),
                };
                var query = "SELECT distinct substring(Sloc,0,2) as Sloc, Desc1  FROM [tbm_PlantMasterTransfer] where plant = @plant and sloc not in('16','17') and Desc1 = @Desc1";
                var taskResult = Task.Run(async () =>
                {
                    var returnData = await SelectDbViewAsync(query, ConvertToTBM_PlantTransfer, param.ToArray());
                    result = returnData.ToList();
                });
                taskResult.Wait();

                return result;
            });

        }
        private async Task<TBM_PlantTransfer> ConvertToTBM_PlantTransfer(SqlDataReader datas)
        {
            var data = datas;
            return new TBM_PlantTransfer
            {

                Plant = await data.GetValueAsync<string>("Plant"),
                Sloc = await data.GetValueAsync<string>("Sloc"),
                WH1 = await data.GetValueAsync<string>("WH1"),
                Desc1 = await data.GetValueAsync<string>("Desc1"),
                Desc2 = await data.GetValueAsync<string>("Desc2"),
            };
        }
        private async Task<StockInquiryItem> ConvertToStockInquiryItem(SqlDataReader datas)
        {
            var data = datas;
            return new StockInquiryItem
            {

                Batch = await data.GetValueAsync<string>("Batch"),
                MatCode = await data.GetValueAsync<string>("MatCode"),
                QueryID = await data.GetValueAsync<string>("QueryID"),
                Storage = await data.GetValueAsync<string>("Storage"),
            };
        }

        
        private async Task<object> ConvertToHHGTItem2(SqlDataReader datas)
        {
            var data = datas;
            return new HH_GT_Item
            {
                Internal_ID = await data.GetValueAsync<decimal>("Internal_ID"),
                GT_ID = await data.GetValueAsync<int>("GT_ID"),
                MovementType = await data.GetValueAsync<string>("MovementType"),
                Material = await data.GetValueAsync<string>("Material"),
                Batch = await data.GetValueAsync<string>("Batch"),
                Qty = await data.GetValueAsync<decimal>("Qty"),
                Problem_Desc = await data.GetValueAsync<string>("Problem_Desc"),
                Storage_From = await data.GetValueAsync<string>("Storage_From"),
                Storage_To = await data.GetValueAsync<string>("Storage_To"),
                CreateUser = await data.GetValueAsync<string>("CreateUser"),
                CreateDateTime = await data.GetValueAsync<DateTime>("CreateDateTime"),
                Status = await data.GetValueAsync<string>("Status"),
                Item_No = await data.GetValueAsync<int>("Item_No"),
                SFrom = await data.GetValueAsync<string>("SFrom"),
                UOM = await data.GetValueAsync<string>("UOM"),
                WH_No = await data.GetValueAsync<int>("WH_No"),
                WH_Code = await data.GetValueAsync<string>("WH_Code"),
                Interface_ID = await data.GetValueAsync<int>("Interface_ID"),
                Return_Msg = await data.GetValueAsync<string>("Return_Msg"),
                ClearText = await data.GetValueAsync<string>("ClearText"),
                Move_Batch = await data.GetValueAsync<string>("Move_Batch"),
            };
        }

        private async Task<HH_GT_Item> ConvertToHHGTItem(SqlDataReader datas)
        {
            var data = datas;
            return new HH_GT_Item
            {
                Internal_ID = await data.GetValueAsync<decimal>("Internal_ID"),
                GT_ID = await data.GetValueAsync<int>("GT_ID"),
                MovementType = await data.GetValueAsync<string>("MovementType"),
                Material = await data.GetValueAsync<string>("Material"),
                Batch = await data.GetValueAsync<string>("Batch"),
                Qty = await data.GetValueAsync<decimal>("Qty"),
                Problem_Desc = await data.GetValueAsync<string>("Problem_Desc"),
                Storage_From = await data.GetValueAsync<string>("Storage_From"),
                Storage_To = await data.GetValueAsync<string>("Storage_To"),
                CreateUser = await data.GetValueAsync<string>("CreateUser"),
                CreateDateTime = await data.GetValueAsync<DateTime>("CreateDateTime"),
                Status = await data.GetValueAsync<string>("Status"),
                Item_No = await data.GetValueAsync<int>("Item_No"),
                SFrom = await data.GetValueAsync<string>("SFrom"),
                UOM = await data.GetValueAsync<string>("UOM"),
                WH_No = await data.GetValueAsync<int>("WH_No"),
                WH_Code = await data.GetValueAsync<string>("WH_Code"),
                Interface_ID = await data.GetValueAsync<int>("Interface_ID"),
                Return_Msg = await data.GetValueAsync<string>("Return_Msg"),
                ClearText = await data.GetValueAsync<string>("ClearText"),
                Move_Batch = await data.GetValueAsync<string>("Move_Batch"),
            }; 
        }
    }
}
